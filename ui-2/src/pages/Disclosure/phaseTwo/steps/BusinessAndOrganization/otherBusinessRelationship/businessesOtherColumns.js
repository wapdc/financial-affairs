import { format } from 'quasar';
import { businessesOtherStep } from './businessesOtherStep';

const { capitalize } = format;

const keys = businessesOtherStep.fields.otherBusinessesArray.template;

const officer = {
  key: 'who',
  label: 'Officer',
  align: 'left',
  sortable: false,
  width: '50%',
  format: (value) => capitalize(value?.join(', ')),
};

const businessLegalName = {
  key: keys.legalName.as,
  label: 'Business Legal Name',
  align: 'left',
  sortable: false,
  width: '50%',
};

export default [officer, businessLegalName];
