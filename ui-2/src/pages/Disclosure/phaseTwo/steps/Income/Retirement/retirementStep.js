import { CategoryHandler } from 'src/utils/CategoryHandler.class';
import rules from 'src/utils/rules';
import useWizard from 'src/wizard/useWizard';

const { lazyLoadWrapper, dataPath, getData } = useWizard();
const incomeCategoryHandler = new CategoryHandler('income', 'retirement');

export const retirementIncomeStep = {
  title: 'Retirement Income',
  order: 2,
  component: lazyLoadWrapper(() => import('./RetirementIncome.vue')),
  fields: {
    hasRetirementIncome: {
      rules: [rules.requiredString],
      data() {
        return dataPath(this, 'pendingTransform.hasRetirementIncome');
      },
      transform: (field) => ({
        writeOut: () => incomeCategoryHandler.writeOutHas(field),
        readIn: () => incomeCategoryHandler.readInHas(field),
      }),
    },
    retirementIncomeArray: {
      blocking: false,
      data() {
        return dataPath(this, 'pendingTransform.retirementIncomeArray');
      },
      transform: (field) => ({
        writeOut: () => incomeCategoryHandler.writeOutData(field),
        readIn: () => incomeCategoryHandler.readInData(field),
      }),
      requiredIf: () =>
        ['true'].includes(getData().pendingTransform.hasRetirementIncome),
      template: {
        receivedBy: {
          as: 'who',
          rules: [rules.requiredNonEmptyArray],
        },
        providerName: {
          as: 'name',
          rules: [
            rules.requiredString,
            rules.maxLength(100),
          ],
        },
        natureOfIncome: {
          as: 'why',
          rules: [rules.requiredString],
        },
        compensation: {
          as: 'income',
          rules: [rules.required],
        },
        streetAddress: {
          as: 'address',
          rules: [
            rules.requiredString,
            rules.maxLength(120),
          ],
        },
        city: {
          rules: [
            rules.requiredString,
            rules.alphbetical,
            rules.maxLength(120),
          ],
        },
        state: {
          rules: [rules.requiredString, rules.state],
        },
        zip: {
          as: 'postcode',
          rules: [rules.required, rules.exactLength(5), rules.numbersOnly],
        },
      },
    },
  },
};
