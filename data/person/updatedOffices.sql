SELECT
  oh.office_code,
  o.offtitle as office,
  oh.address,
  oh.city,
  oh.state,
  oh.postcode,
  oh.email,
  j.jurisdiction_id,
  j.name as jurisdiction,
  oh.start_date,
  oh.end_date
FROM official oh
       JOIN foffice o ON o.offcode = oh.office_code
       JOIN jurisdiction j ON j.jurisdiction_id = oh.jurisdiction_id
WHERE oh.person_id = :person_id and
  :period_end >= oh.start_date - interval '1 year' and
  :period_start <= coalesce(oh.end_date, :period_start)
order by oh.start_date asc