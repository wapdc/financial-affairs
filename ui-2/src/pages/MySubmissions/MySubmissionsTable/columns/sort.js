function parseDateTimeRange(str) {
  const [start] = str.split(' - ');
  const [datePart, timePart] = start.trim().split(' ');

  const [month, day, year] = datePart.split('/').map(Number);

  let hour = 0;
  let minute = 0;

  if (timePart) {
    const match = timePart.match(/(\d+):(\d+)(am|pm)/i);
    if (match) {
      hour = parseInt(match[1], 10);
      minute = parseInt(match[2], 10);
      const ampm = match[3].toLowerCase();

      if (ampm === 'pm' && hour < 12) hour += 12;
      if (ampm === 'am' && hour === 12) hour = 0;
    }
  }

  return new Date(year, month - 1, day, hour, minute);
}

export function sortDateTimeStrings(a, b) {
  return parseDateTimeRange(a) - parseDateTimeRange(b);
}
