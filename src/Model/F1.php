<?php

namespace WAPDC\FinancialAffairs\Model;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class F1
 *
 * @package WAPDC\FinancialAffairs\Model
 * @Entity
 * @Table(name="f1..F1")
 * @HasLifecycleCallbacks
 * @property integer $repno;
 * @property string $date_covered;
 * @property string $date_received;
 * @property string $last_name;
 * @property string $first_name;
 * @property string $middle_initial;
 * @property string $address;
 * @property string $city;
 * @property string $zip;
 * @property string $county;
 * @property integer $status;
 * @property array $offices;
 * @property string $election_month;
 * @property string $election_year;
 * @property string $volunteer;
 * @property string $amendment;
 * @property string $term_expired;
 * @property string $certification_email;
 */
#[ORM\Entity]
#[ORM\Table(name: 'f1..F1')]
#[ORM\HasLifecycleCallbacks]
class F1 {

  /** @Id @Column(type="integer") */
  #[ORM\Column(type: 'integer')]
  #[ORM\Id]
  public $ident;

  /**
   * @Column(type="integer")
   */
  public $repno;
  #[ORM\Column(type: 'integer')]

  /**
   * @Column
   */
  #[ORM\Column(type: 'integer')]
  public $filer_id;

  /**
   * @Column (name="xrf_file")
   * @var string
   */
  #[ORM\Column(name: 'xrf_file')]
  private $_xrf_file;

  /**
   * @var \SimpleXMLElement
   */
  private $xml;

  /**
   * @Column
   */
  #[ORM\Column]
  public $rpt_type;

  /**
   * @Column (type="smallint")
   */
  #[ORM\Column(type: 'smallint')]
  public $f1_status;

  /**
   * @Column (type="smallint")
   */
  #[ORM\Column(type:'smallint')]
  public $imaging_status;

  /**
   * @var \DateTime
   * @Column (type="datetime")
   */
  #[ORM\Column(type: 'datetime')]
  public $date_prepared;

  /**
   * @PostLoad
   */
  #[ORM\PostLoad]
  public function loadXrf(){
    $this->xml = simplexml_load_string($this->_xrf_file);
  }

  /**
   * @PrePersist
   * @PreFlush
   */
  #[ORM\PreUpdate]
  #[ORM\PrePersist]
  public function saveXrf(){
    $this->xml->f1->repno = $this->repno;
    $this->_xrf_file = $this->xml->asXML();
  }

  /**
   * @param $name
   * @return array|string|\DateTime
   */
  public function __get($name) {
    switch ($name){
      case 'status':
      case 'date_covered':
      case 'date_received':
      case 'zip':
      case 'election_month':
      case 'election_year':
      case 'last_name':
      case 'first_name':
      case 'middle_initial':
      case 'address':
      case 'city':
      case 'county':
      case 'volunteer':
      case 'amendment':
      case 'covers':
      case 'to':
      case 'received':
        return (string) $this->xml->f1->{$name};
      break;
      case 'term_expired':
        $value = (string) $this->xml->f1->{$name};
        return $value ? \Datetime::createFromFormat('m-d-Y', $value) : NULL;
        break;
      case 'certification_email':
        return (string) $this->xml->certification->email_work;
        break;
      case 'offices':
        $offices = [];
        foreach ($this->xml->office_held_sought as $office){
          $o = new \stdClass();
          $o->office = (string) $office->office;
          $o->position = (string) $office->position;
          $o->jurisdiction_name = (string) $office->jurisdiction_name;
          $o->term_begins = (string) $office->term_begins;
          $o->term_ends = (string) $office->term_ends;
          $offices[] = $o;
        }
        return $offices;
      case 'jurisdiction_name':
      case 'term_begins':
      case 'term_ends':
        return (string) $this->xml->office_held_sought->{$name};
      default:
        return null;
    }
  }

  public function __set($name, $value) {
    switch($name){
      case 'repno':
      case 'covers':
      case 'to':
      case 'received':
      case 'election_year':
      case 'amendment':
      case 'date_covered':
      case 'date_received':
        $this->xml->f1->{$name} = $value;
        break;
    }
  }
}
