import { useApollo } from '@wapdc/pdc-ui/application';
import { publicPositionsSection } from 'src/pages/Disclosure/phaseOne/steps/PublicPositions/publicPositions';
import { candidaciesSection } from 'src/pages/Disclosure/phaseOne/steps/Candidacies/candidacies';

const service = useApollo('service/financial-affairs');

export const REQUIRED = 'required';
export const NOT_REQUIRED = 'not required';
export const CONDITIONAL = 'conditional';

export default async function useGetRequiredStatement(payload) {
  const result = await service.post('/submission/validate-identity', {
    ...payload,
  });

  if (!result?.success) {
    console.error(result);
    return;
  }

  const { validation } = result;
  const noInvalidResults = Array.isArray(validation) && validation.length === 0;

  if (noInvalidResults) {
    return await handleValidCase(payload);
  }

  return handleInvalidCase(validation);
}

async function handleValidCase(payload) {
  const missingStatements = await service.post(
    '/request-missing-statements',
    payload,
  );

  if (!missingStatements?.success) {
    console.error(missingStatements);
    return;
  }

  const { statements_owed: statementsOwed } = missingStatements;

  if (statementsOwed.length === 0) {
    const validationInformation = {
      invalidSteps: [],
      isValid: true,
      statementRequirement: NOT_REQUIRED,
    };

    return validationInformation;
  }

  const statement = getOwedStatement(statementsOwed);

  return {
    invalidSteps: [],
    isValid: true,
    statementRequirement: statement.required ? REQUIRED : CONDITIONAL,
    periodStart: statement.period_start,
    periodEnd: statement.period_end,
  };
}

function getOwedStatement(statementsOwed) {
  const currentRequired = statementsOwed.filter(
    (statement) => statement.legacy_filing !== true,
  );

  if (currentRequired.length === 0) {
    return null;
  }

  return currentRequired[0];
}

function handleInvalidCase(validations) {
  const invalidSteps = getInvalidSteps(validations);
  return {
    invalidSteps,
    isValid: false,
    statementRequirement: null,
  };
}

function getInvalidSteps(validation) {
  const validationMap = {
    offices: {
      sectionIndex: publicPositionsSection.order,
      stepIndex: publicPositionsSection.steps.publicPositionsStep.order,
      title: publicPositionsSection.title,
    },
    candidacies: {
      sectionIndex: candidaciesSection.order,
      stepIndex: candidaciesSection.steps.candidacies.order,
      title: candidaciesSection.title,
    },
  };

  return Object.entries(validation).map(([key, message]) => {
    const [section] = key.split('.');
    return {
      sectionIndex: validationMap[section].sectionIndex,
      stepIndex: validationMap[section].stepIndex,
      title: validationMap[section].title,
      message,
    };
  });
}
