#!/usr/bin/env bash
set -e
pushd db/postgres/open_data
$PSQL_CMD wapdc -f od_imaged_documents_and_reports_financial_affairs.sql
popd
pushd db/postgres/open_data
$PSQL_CMD wapdc -f prod_od_config.sql
