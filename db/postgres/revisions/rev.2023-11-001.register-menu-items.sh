#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc -f db/postgres/fa_count_unverified_statements.sql
$PSQL_CMD wapdc <<EOF
insert into menu_links(category, menu, title, abstract, url, permission, count_function)
  values (
          'Financial affairs',
          'staff',
          'Unverified FA statements',
          'Verify financial statements that need to be matched to candidates and people',
          '/financial-affairs/admin/unverified-statements',
          'enter wapdc data',
          'fa_count_unverified_statements'
         );
insert into menu_links(category, menu, title, abstract, url, permission)
values (
           'Financial affairs',
           'staff',
           'Financial statement history',
           'Find statement history for a person and perform data entry tasks',
           '/financial-affairs/person',
           'access wapdc data'
       );
delete from menu_links where url='/financial-affairs/admin';
EOF