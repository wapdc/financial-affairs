SELECT e.election_code, e.title, e.is_primary, e.is_special, e.election_date
FROM election e
ORDER BY election_date ASC