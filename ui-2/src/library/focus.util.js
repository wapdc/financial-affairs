const FOCUSABLE_QUERY = 'a, button, input, select, textarea, [tabindex]:not([tabindex=\'-1\'])';

/**
 * Handles Tab key press event and redirects focus based on an eventResponseMap.
 *
 * @param {KeyboardEvent} event - The keydown event object.
 * @param {Object} eventResponseMap - An object mapping element IDs to focus redirection rules.
 *
 * eventResponseMap structure:
 * ```javascript
 * const eventResponseMap = {
 *
 *  // Provide a string to redirect focus to a specific element by ID.
 *  [fromElementIdString]: {
 *    next: toElementIdString, // Tab
 *    prev: toElementIdString, // Shift + Tab
 *  },
 *
 *  // OR
 *
 *  // Focus the first focusable child of an element matching the query.
 *  [fromElementIdString]: {
 *    next: {
 *      rel: 'firstFocusableChild',
 *      query: string,
 *   },
 *  },
 *
 *  // OR
 *
 *  // Focus a sibling element matching the query, next or previous.
 *  [fromElementIdString]: {
 *    prev: {
 *      rel: 'sibling',
 *      query: string,
 *      direction: ['next' | 'prev'],
 *    },
 *  },
 *
 * };
 * ```
 *
 * @returns {void}
 */
export function interceptTabNavigation(
  event,
  eventResponseMap,
) {
  if (event.key !== 'Tab') return;
  const shiftKeyPressed = event.shiftKey;
  const targetId = event.target.id;

  const direction = shiftKeyPressed ? 'prev' : 'next';
  const eventResponse = eventResponseMap[targetId]?.[direction];

  if (eventResponse) {
    event.preventDefault();

    if (typeof eventResponse === 'string') {
      focusElementById(eventResponse);
    } else if (typeof eventResponse === 'object') {
      _handleSpecialResponse(eventResponse)
    }
  }
}

/**
 * Helper function to handle special focus redirection rules for `interceptTabNavigation`.
 * @param {Object} eventResponse - The eventResponse object.
 * @returns {void}
 */
function _handleSpecialResponse(eventResponse) {
  const { rel } = eventResponse;

  if (rel === 'sibling') {
    const { direction = 'next', query } = eventResponse;
    focusElementSiblingByQuery(query, direction);

  } else if (rel === 'firstFocusableChild') {
    const { query } = eventResponse;
    focusFirstFocusableChildByQuery(query);
  }
}

/**
 * Focuses an element by its ID.
 * @param {string} id - The ID of the element to focus.
 * @returns {void}
 */
export function focusElementById(id) {
  const el = document.getElementById(id);
  if (el) el.focus();
}

/**
 * Focuses the first focusable child of an element matching the query.
 * @param {string} query - The selector query to find the parent element.
 * @returns {void}
 */
export function focusFirstFocusableChildByQuery(query) {
  const parentElement = document.querySelector(query);
  const firstFocusableChild = parentElement.querySelector(FOCUSABLE_QUERY);
  if (firstFocusableChild) firstFocusableChild.focus();
}

/**
 * Focuses a sibling element matching the query, next or previous.
 * @param {string} query - The selector query to find the sibling element.
 * @param {string} [direction='next'] - The direction ('prev' or 'next', defaults to 'next') to search for the sibling element.
 * @returns {void}
 */
export function focusElementSiblingByQuery(query, direction = 'next') {
  const sequenceAccessor = direction === 'prev'
    ? 'previousElementSibling'
    : 'nextElementSibling';

  let sibling = document.querySelector(query)[sequenceAccessor];
  while (sibling) {
    if (sibling.matches(FOCUSABLE_QUERY && !sibling.disabled)) {
      sibling.focus();
      break;
    }
    sibling = sibling[sequenceAccessor];
  }
}
