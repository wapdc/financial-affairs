import useWizard from 'src/wizard/useWizard';
import { realEstateOwnershipStep } from './realEstate';

const wizard = useWizard();

/** Conditional field rules for property identification */
export function validateLocationObj(locationObj) {
  const propertyIdType =
    locationObj?.propertyIdType || PROPERTY_ID_TYPES.ADDRESS;

  if (propertyIdType === PROPERTY_ID_TYPES.ADDRESS) {
    if (!locationObj?.address?.trim().length) return 'Address is required';
    if (!locationObj?.city?.trim().length) return 'City is required';
    if (!locationObj?.zip?.trim().length) return 'Zip is required';
    return true;
  } else if (propertyIdType === PROPERTY_ID_TYPES.PARCEL) {
    if (!locationObj?.parcel?.trim().length) return 'Parcel number is required';
    if (!locationObj?.county?.trim().length) return 'County is required';
    return true;
  } else if (propertyIdType === PROPERTY_ID_TYPES.LEGAL_DESCRIPTION) {
    if (!locationObj?.legal_description?.trim().length)
      return 'Legal description is required';
    if (!locationObj?.county?.trim().length) return 'County is required';
    return true;
  } else if (propertyIdType === PROPERTY_ID_TYPES.EXEMPT) {
    if (!locationObj?.city?.trim().length) return 'City is required';
    if (!locationObj?.residence_type?.trim().length)
      return 'Type of residence is required';
    return true;
  }
  return 'Property identification is required';
}

/** Field rules for property purchaser */
export function validatePurchaserInformation(purchaserObj) {
  const getHasPurchaser = (propertyIndex) => {
    const propertyArrayDef = realEstateOwnershipStep.fields.propertyArray;
    const propertyArrayUtils =
      wizard.getFieldSetUtilsByPointer(propertyArrayDef);
    const property = propertyArrayUtils.get(propertyIndex);
    return ['true', true].includes(property?.getValue()?.sold);
  };

  const propertyIndex = purchaserObj?.parentIndex;
  const hasPurchaser = getHasPurchaser(propertyIndex);

  if (!hasPurchaser) return true;

  if (!purchaserObj?.sale_amount) return 'Sale amount is required';
  if (!purchaserObj?.name) return 'Name is required';
  if (!purchaserObj?.address) return 'Address is required';
  if (!purchaserObj?.city) return 'City is required';
  if (!purchaserObj?.state) return 'State is required';
  if (!purchaserObj?.postcode) return 'Zip is required';

  return true;
}

/** Field rules for property creditor */
export function validateCreditorInformation(creditorObjArr) {
  const getHasMortgage = (propertyIndex) => {
    const propertyArrayDef = realEstateOwnershipStep.fields.propertyArray;
    const propertyArrayUtils =
      wizard.getFieldSetUtilsByPointer(propertyArrayDef);
    const property = propertyArrayUtils.get(propertyIndex);
    return property?.getValue()?.has_mortgage;
  };

  const propertyIndex = creditorObjArr?.[0]?.parentIndex;
  const hasMortgage = getHasMortgage(propertyIndex);

  if (!hasMortgage) return true;

  for (const creditorObj of creditorObjArr) {
    if (!creditorObj?.name) return 'Name is required';
    if (!creditorObj?.address) return 'Address is required';
    if (!creditorObj?.city) return 'City is required';
    if (!creditorObj?.state) return 'State is required';
    if (!creditorObj?.postcode) return 'Zip is required';
    if (!creditorObj?.terms) return 'Terms of loan is required';
    if (!creditorObj?.security_given) return 'Security given is required';
    if (!creditorObj?.original_amount) return 'Original amount is required';
    if (!creditorObj?.current_amount) return 'Current amount is required';
  }

  return true;
}

export const PROPERTY_ID_TYPES = {
  PARCEL: 'parcel',
  LEGAL_DESCRIPTION: 'legal_description',
  ADDRESS: 'address',
  EXEMPT: 'exempt',
};

export function inferPropertyIdType(dataSet) {
  const { location } = dataSet ?? {};

  if (!location) {
    return null;
  }

  if (location?.parcel) {
    return PROPERTY_ID_TYPES.PARCEL;
  }
  if (location?.legal_description) {
    return PROPERTY_ID_TYPES.LEGAL_DESCRIPTION;
  }
  if (location?.address) {
    return PROPERTY_ID_TYPES.ADDRESS;
  }
  if (location?.exempt_from_disclosure) {
    return PROPERTY_ID_TYPES.EXEMPT;
  }
  return null;
}

export function translateApiValuesToField(dataSet) {
  const { location } = dataSet;

  if (!location) {
    return null;
  }

  const propertyIdType = inferPropertyIdType(dataSet);

  const typeToFieldMap = {
    [PROPERTY_ID_TYPES.PARCEL]: {
      parcelNum: location.parcel,
      county: location.county,
      state: location.state,
    },
    [PROPERTY_ID_TYPES.LEGAL_DESCRIPTION]: {
      legal_description: location.legal_description,
      county: location.county,
      state: location.state,
    },
    [PROPERTY_ID_TYPES.ADDRESS]: {
      streetAddress: location.address,
      city: location.city,
      state: location.state,
      zip: location.zip,
    },
    [PROPERTY_ID_TYPES.EXEMPT]: {
      typeOfResidence: location.residence_type,
      city: location.city,
      state: location.state,
    },
  };

  return typeToFieldMap[propertyIdType];
}
