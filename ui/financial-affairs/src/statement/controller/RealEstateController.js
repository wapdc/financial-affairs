export default {
  statement: {},
  init(statement) {

    if (!statement.real_estate) {
      statement.real_estate = [];
    }

    this.statement = statement;
  },

  addProperty() {
    this.statement.real_estate.push(
      {
        purchaser: {},
        location: {},
      }
    );
    this.statement.draft_state.state.selectedPropertyIndex = this.statement.real_estate.length - 1;
  }
}