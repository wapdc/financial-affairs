import useWizard from 'src/wizard/useWizard';

const { lazyLoadWrapper } = useWizard();

export const introSection = {
  title: 'Overview',
  order: 0,
  steps: {
    overview: {
      title: 'Overview',
      order: 0,
      component: lazyLoadWrapper(() => import('./Intro.vue')),
      hideToc: true,
    },
  },
};
