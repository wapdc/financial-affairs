<?php


namespace WAPDC\FinancialAffairs\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class FAStatementLog
 * @Entity
 * @Table(name="fa_statement_log")
 */
#[ORM\Entity]
#[ORM\Table(name: 'fa_statement_log')]
class FAStatementLog {

  /** @Column @Id @GeneratedValue */
  #[ORM\Column]
  #[ORM\Id]
  #[ORM\GeneratedValue]
  private $log_id;

  /** @Column(type="integer") */
  #[ORM\Column(type: 'integer')]
  public $fa_statement_id;

  /** @Column(type="integer") */
  #[ORM\Column(type: 'integer')]
  public $submission_id;

  /** @Column(type="integer") */
  #[ORM\Column(type: 'integer')]
  public $person_id;

  /** @Column(type="integer") */
  #[ORM\Column(type: 'integer')]
  public $repno;

  /** @Column */
  #[ORM\Column]
  public $action;

  /** @Column */
  #[ORM\Column]
  public $message;

  /** @Column */
  #[ORM\Column]
  public $user_name;

  /**
   * @Column(type="datetimetz")
   * @var \DateTime
   */
  #[ORM\Column(type: 'datetimetz')]
  public $updated_at;

}