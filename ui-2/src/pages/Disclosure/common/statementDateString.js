import useWizard from 'src/wizard/useWizard';
import { formatPrettyDate } from 'src/utils/format';

const wizard = useWizard();

/** Gets period range from given wizard data, returns formatted period range string */
export function statementDateString(seperator = 'and') {
  const periodStart = wizard.getData()?.statement?.period_start;
  const periodEnd = wizard.getData()?.statement?.period_end;
  if (!periodStart || !periodEnd) return '';

  return `${formatPrettyDate(periodStart)} ${seperator} ${formatPrettyDate(
    periodEnd,
  )}`;
}
