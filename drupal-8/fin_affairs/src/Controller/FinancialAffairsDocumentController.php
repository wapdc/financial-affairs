<?php

namespace Drupal\fin_affairs\Controller;

use Drupal\Core\Access\AccessException;
use Drupal\Core\Http\Exception\CacheableAccessDeniedHttpException;
use \Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use WAPDC\Core\DataSigner;
use Drupal\wapdc_core\Controller\CoreControllerBase;
use Exception;
use WAPDC\FinancialAffairs\FinancialAffairs;


class FinancialAffairsDocumentController extends CoreControllerBase {
  /**
   * @throws \Exception
   */
  public function redirectToLegacyDocument() {
    $signing_data = new \stdClass();
    $pdc_user = $this->getCurrentUser();
    $drupal_user = $this->currentUser();
    if (!empty($_GET['repno'])) {
      $parameters['repno'] = $_GET['repno'];
      $parameters['docid'] = null;
    } else if (!empty($_GET['docid'])) {
      $parameters['docid'] = $_GET['docid'];
      $parameters['repno'] = null;
    } else {
      throw new Exception("Missing docid or repno for legacy document");
    }
    /*
    Check to find out if user has 'access wapdc data' permission and build the data based
    on the permissions that are set.  If they do not have permission check the rds.
    */
    $sp = FinancialAffairs::service()->getStatementProcessor();
    if ($drupal_user->hasPermission("access wapdc data")
      || $sp->documentAccess($pdc_user, $parameters['doc_id'] ?? null, $parameters['repno'] ?? null)) {

      $signing_data = new \StdClass();
      if (!empty($_GET['repno'])) {
        $json = json_encode(array('repno' => $parameters['repno'], 'timestamp' => time()));
      } elseif (!empty($_GET['docid'])) {
        $json = json_encode(array('docid' => $parameters['docid'], 'timestamp' => time()));
      }
      $signing_data->json = $json;
      $dir = \Drupal::service('file_system')->realpath("private://");
      $file = "$dir/data-request-signing.key";
      $signer = new DataSigner($file);
      $site = 'https://web.pdc.wa.gov';
      $url = $signer->sign($signing_data->json);
      $full_url = "$site/RPTImg/default.aspx?signature=" . $this->base64_encode_url($url) . "&data=" . $this->base64_encode_url($signing_data->json);
      return new TrustedRedirectResponse($full_url);
    }
    else {
      throw new AccessDeniedHttpException();
    }
  }
  function base64_encode_url($string) {
    return str_replace(['+','/','='], ['%2B','%2F','%3D'], base64_encode($string));
  }
}