<script>
import useWizard from 'src/wizard/useWizard.js';
import useUniqueString from 'src/composables/useUniqueString.js';

export const inputProps = {
  modelValue: {
    required: true,
    default: null,
  },
  type: {
    type: String,
    default: 'text',
  },
  label: {
    type: String,
    default: null,
  },
  description: {
    type: String,
    default: '',
  },
  widthPx: {
    type: Number,
    default: null,
  },
  startFocused: {
    type: Boolean,
    default: false,
  },
  rules: {
    type: Array,
    default: () => [],
  },
  inputRef: {
    type: Object,
    default: null,
  },
};

export const inputEmits = ['update:modelValue', 'focusout'];
export const inputListeners = (emit) =>
  inputEmits.reduce((acc, _emit) => {
    acc[_emit] = (value) => emit(_emit, value);
    return acc;
  }, {});
</script>

<script setup>
import { computed, inject, onMounted, ref } from 'vue';

import { capitalize } from 'src/utils/format.js';
import rules from 'src/utils/rules.js';
import useDebounce from 'src/composables/useDebounce.js';

defineOptions({
  name: 'base-input',
});

const props = defineProps({
  ...inputProps,
});

const { hideHints, stackLabel } = inject('formOpts', { hideHints: false });

const emit = defineEmits(inputEmits);

const inputWrapperRef = ref(null);
const inputRef = props?.inputRef || ref(null);
const uniqueId = ref('');
const isDirty = ref(false);

const wizard = useWizard();
const uniqueString = useUniqueString();

const debouncedFunction = useDebounce();

// Value is not arbitrary; it's what works with quasar input and validation update debounces
const DEBOUNCE_MS = 700;

const isRequired = computed(() => {
  return (
    props.rules?.includes(rules.required) ||
    props.rules?.includes(rules.requiredString)
  );
});

const hasError = computed(() => {
  return inputRef.value?.hasError || false;
});

const label = computed(() => {
  return capitalize(props.label) || '';
});

// Use the description if it exists, otherwise use 'Required' if the field is required
const fieldHint = computed(() => {
  if (hideHints) {
    return '';
  }

  const { description: dsc } = props;
  if (dsc.length > 0) {
    return dsc;
  }
  const required = isRequired.value;
  const error = hasError.value;
  const dirty = isDirty.value;
  return required && (!error || !dirty) ? 'Required' : '';
});

/**
 * Quasar input shows either a description ("hint") or an error message, but not both. This function
 * sets the aria-describedby attribute to point to the description element, if it exists, or if an
 * error message exists, sets the aria-errormessage attribute to point to the error message element.
 */
function updateAriaAttributes({ firstUpdate } = { firstUpdate: false }) {
  const error = firstUpdate ? false : hasError.value;
  const { nativeInputElement, fieldMessages } = getElements();
  if (!nativeInputElement || !fieldMessages) return;

  if (error) {
    const errorMessageElement = fieldMessages?.querySelector('[role=alert]');
    if (!errorMessageElement) return;

    nativeInputElement.removeAttribute('aria-describedby');

    nativeInputElement.setAttribute(
      'aria-errormessage',
      elementId(errorMessageElement, 'error'),
    );
    nativeInputElement.setAttribute('aria-invalid', 'true');
  } else {
    const descriptionElement = fieldMessages?.querySelector(':not([role])');
    if (!descriptionElement) return;

    nativeInputElement.setAttribute('aria-invalid', 'false');
    nativeInputElement.removeAttribute('aria-errormessage');

    nativeInputElement.setAttribute(
      'aria-describedby',
      elementId(descriptionElement, 'description'),
    );
  }
}

/**
 * Returns native HTML input element and HTML element containing messages
 */
function getElements() {
  const nativeInputElement = inputRef?.value?.nativeEl;

  if (!nativeInputElement) return {};

  const fieldGreatGrandParent =
    nativeInputElement.parentElement.parentElement.parentElement;
  const fieldMessages =
    fieldGreatGrandParent.querySelector('.q-field__messages');

  return { nativeInputElement, fieldMessages };
}

/**
 * Assigns an id to an element if it does not already have one then returns the id
 * @param {HTMLElement} element The element to assign an id to
 * @param {string} type The type of element, e.g. 'error' or 'description'. Must be
 * unique per component instance
 */
function elementId(element, type) {
  return element?.id || (element.id = `${uniqueId.value}-${type}`);
}

/**
 * Intercepts the update:model-value event emitted by the QInput component to
 * call the updateAriaDescribedBy function
 * @param {any} value The new value of the input field.
 */
function updateValue(value) {
  emit('update:modelValue', value);

  inputRef.value?.validate?.();

  debouncedFunction(() => {
    updateAriaAttributes();
  }, DEBOUNCE_MS);
}

onMounted(() => {
  uniqueId.value = uniqueString.get();
  if (props.startFocused) {
    inputRef.value?.focus?.();
  }
  if (props.modelValue) {
    isDirty.value = true;
    inputRef.value?.validate?.();
  }
  updateAriaAttributes({ firstUpdate: true });
});

const inputComponentBinds = computed(() => ({
  modelValue: props.modelValue,
  label: label.value,
  hint: fieldHint.value,
  rules: props?.rules || [],
}));

const inputComponentListeners = {
  'update:modelValue': updateValue,
};

const overrideMessages = computed(() => {
  if (hideHints) {
    return '';
  }

  const rules = props?.rules || [];
  const value = props.modelValue;
  const error = rules.some((rule) => !rule(value));

  const required = isRequired.value;

  // If the user has interacted with the field, it's ready for validation
  const readyForValidation =
    wizard?.status?.currentStep?.meta?.isDirty || isDirty.value;

  const errorMessage = error || (required && !value) ? 'Required' : '';

  const message = {
    hint: hideHints ? '' : props.description,
    error: readyForValidation ? errorMessage : '',
  };

  return message;
});
</script>

<template>
  <div
    ref="inputWrapperRef"
    class="q-flex-grow q-py-md"
    :style="{ width: widthPx ? `${widthPx}px` : '', maxWidth: '100%' }"
  >
    <q-input
      v-if="!$slots.override"
      v-bind="{ ...inputComponentBinds, ...$attrs }"
      :ref="!$slots.override ? inputRef : null"
      :stack-label="stackLabel"
      :type="props.type"
      v-on="inputComponentListeners"
    />

    <template v-else>
      <slot
        v-if="inputComponentBinds"
        :listeners="inputComponentListeners"
        :set="updateValue"
        name="override"
      />
      <div class="q-field__bottom q-pl-none">
        <div
          v-if="overrideMessages?.error?.length > 0"
          class="q-field--error text-negative"
          role="alert"
        >
          {{ overrideMessages.error }}
        </div>
        <div
          v-else-if="overrideMessages?.hint?.length > 0"
          class="q-field--error"
          role="alert"
        >
          {{ overrideMessages.hint }}
        </div>
      </div>
    </template>
  </div>
</template>
