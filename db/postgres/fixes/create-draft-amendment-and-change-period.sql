/**
  The provided SQL function creates a draft amendment submission for a person based on their latest actual submission
  for a statement. It sets the new period start/end dates and draft state in the user data JSONB. To use, call the
  function with the statement ID and period dates to amend.

  Will raise an error if it cannot find the statement or if the statement is found but the person has any draft
  submissions. You must delete, or have the user delete, any drafts for this person_id before running.

  The draft is moved to the "IncomeReview" step to prompt them to review all the sections but all of the data is carried
  forward from the statement provided. If you need to change anything about the offices or candidacies, that needs to be
  done after the draft is created.
 */
do $$

    declare
        v_statement_id int;
        v_period_start date;
        v_period_end date;
        v_period_start_json_text text;
        v_period_end_json_text text;
        v_submission record;
        v_user_data jsonb;
        v_new_submission_id int;

    begin

        v_statement_id := $(statement_id int);
        v_period_start := $(period_start 'YYYY-MM-DD');
        v_period_end := $(period_end 'YYYY-MM-DD');

        select
            su.uid, su.realm, su.user_data::jsonb, now() as updated_at, false as verified, false as submitted,
            false as published, 1 as save_count, su.person_id, su.submission_id as amend_id, su.name
        from fa_submission su join fa_statement s on s.submission_id = su.submission_id
        where s.statement_id = v_statement_id
        order by submitted_at desc, su.submission_id desc
        limit 1
        into v_submission;

        if not found then
            raise exception 'Statement %s not found', v_statement_id;
        end if;

        -- Check if there are draft submissions for this person_id and raise an exception if there are
        if exists (select 1 from fa_submission fs
                            where fs.person_id = v_submission.person_id and submitted is distinct from true) then
            raise exception 'Draft submission already exists for person %. They cannot have more than one draft.'
                , v_submission.person_id;
        end if;

        raise notice 'Creating draft amendment for person %...', v_submission.person_id;

        -- The period_start and period_end need to be enclosed in double quotes for jsonb_set
        v_period_start_json_text = '"' || v_period_start::text || '"';
        v_period_end_json_text = '"' || v_period_end::text || '"';

        v_user_data = jsonb_set(v_submission.user_data, '{statement, period_start}', v_period_start_json_text::jsonb);
        v_user_data = jsonb_set(v_user_data, '{statement, period_end}', v_period_end_json_text::jsonb);
        v_user_data = jsonb_set(v_user_data, '{statement, draft_state, step}', '"IncomeReview"'::jsonb);

        insert into fa_submission
        (uid, realm, user_data, updated_at, verified,
         submitted, published, save_count, person_id,
         amend_id, period_start, period_end, name, submitted_at)
        values
        (v_submission.uid, v_submission.realm, v_user_data, v_submission.updated_at, v_submission.verified,
         v_submission.submitted, v_submission.published, v_submission.save_count, v_submission.person_id,
         v_submission.amend_id, v_period_start, v_period_end, v_submission.name, null)
        returning submission_id into v_new_submission_id;

        raise notice 'Draft amendment created with submission id %', v_new_submission_id;
    end
$$ language plpgsql
