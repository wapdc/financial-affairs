# About this folder

This is a temporary development folder for placing library component clones with
modifications to QA test alongside features. After testing, changes will be merge
requested to main `@wapdc/pdc-ui` package library.

# Changelist

### PAccessbileButton

- `v-if="tooltip"` added to button element to allow for conditional rendering of tooltip
- Default styling (flat, rounded, no-caps) applied to match UX/UI design
- Props added so attributes `primary`/`secondary`/`negative` can be passed to the button to change the bg+text colors
- Enter and space keydown events emit click event to allow for keyboard navigation

### PApp

- `addedUserLinks` prop defined to allow for user links to be added to the user menu. Forwarded to component in #default slot
- `title` prop defined to allow for title to be passed to the component

### PUserLayout

- `addedUserLinks` prop defined to allow for user links to be added to the user menu. Forwarded to component in #default slot
- `title` prop defined to allow for title to be passed to the component

### PUserHeader

- `addedUserLinks` prop defined to allow for user links to be added to the user menu. Checked for validity and spread into the user menu `essential_links` array.
- `focus.util.js` imported for modifying tab key navigation order related to user menu.

### PInfoDialog

- Internal state can be overridden by passing `is-open` prop to support custom triggers.
- Added "trigger" slot to allow for custom trigger to open the dialog, replacing the default info/question mark icon.
- Added "actions" slot to allow for custom actions to override (replace, not add to) the default "OK" button.

### focus.util.js

New utility file exports tab key navigation order modification and other focus functions. Used in `PUserHeader` component.


### PWhiteLogo

- Providing accessible aria-labelledby attribute and <title> label to the logo image for screen readers.

### PDatePicker

- Added spaceBar event to input to allow keyboard accessibility for datepicker popup
- Removed unused code

### PEditRows

- Using local PAccessibleButton for Add and Delete buttons
- Using local PInfoDialog for confirmation dialog
- Debouncing addRow to prevent holding enter key from adding multiple rows, support focus on new row in all browsers
