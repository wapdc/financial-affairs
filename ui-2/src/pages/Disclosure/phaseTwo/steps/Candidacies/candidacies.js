import { section, step, lazyLoadWrapper } from 'src/wizard/useWizard';

export const candidaciesSection = section({
  title: 'Candidacies',
  canSave: true,
  steps: [
    step({
      title: 'Candidacies',
      component: lazyLoadWrapper(() => import('./Candidacies.vue')),
    }),
  ],
});
