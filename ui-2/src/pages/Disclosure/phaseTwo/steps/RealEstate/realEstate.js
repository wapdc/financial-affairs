import rules from 'src/utils/rules';
import useWizard from 'src/wizard/useWizard';
import {
  validateCreditorInformation,
  validateLocationObj,
  validatePurchaserInformation,
} from './realEstate.helpers';

const wizard = useWizard();
const { dataPath, getData, getFieldSetUtils, lazyLoadWrapper } = wizard;

export const washingtonRealEstateStep = {
  title: 'Washington Real Estate',
  order: 0,
  component: lazyLoadWrapper(() => import('./RealEstate.vue')),
  fields: {
    isJudgeOrLawEnforcement: {
      rules: [rules.requiredString],
      data() {
        return dataPath(this, 'statement.judge_or_law_enforcement');
      },
      transform: (hasField) =>
        reportingModificationTransforms(hasField, [
          'statement',
          'judge_or_law_enforcement',
        ]),
    },
    hasOtherReportingModification: {
      data() {
        return dataPath(this, 'statement.other_exemption');
      },
      transform: (hasField) =>
        reportingModificationTransforms(hasField, [
          'statement',
          'other_exemption',
        ]),
      requiredIf() {
        return ['false', false].includes(
          getData().statement.judge_or_law_enforcement,
        );
      },
    },
  },
};

export const realEstateOwnershipStep = {
  title: 'Real Estate Ownership',
  order: 1,
  component: lazyLoadWrapper(() =>
    import('./RealEstateOwnership/RealEstateOwnership.vue'),
  ),
  fields: {
    ownsRealEstate: {
      rules: [rules.required],
      data() {
        return dataPath(this, 'statement.real_estate_owned');
      },
      transform: (hasField) =>
        stringBoolTransforms(hasField, ['statement', 'real_estate_owned']),
    },
    propertyArray: {
      blocking: false,
      data() {
        return dataPath(this, 'statement.real_estate');
      },
      transform: (field) => ({
        writeOut: () => convertTemplateStringsToBooleans(field),
        readIn: () => convertBooleansToTemplateStrings(field),
      }),
      requiredIf: () => getData().pendingTransform.real_estate_owned === 'true',
      template: {
        who: {
          as: 'who',
          rules: [rules.requiredNonEmptyArray],
        },
        assessedValue: {
          as: 'assessed_value',
          rules: [rules.required],
        },
        location: {
          as: 'location',
          rules: [validateLocationObj],
        },
        purchased: {
          as: 'purchased',
          rules: [rules.requiredString],
        },
        sold: {
          as: 'sold',
          rules: [rules.requiredString],
        },
        purchaser: {
          as: 'purchaser',
          rules: [validatePurchaserInformation],
        },
        creditors: {
          as: 'creditors',
          rules: [validateCreditorInformation],
        },
        hasMortgage: {
          as: 'has_mortgage',
          rules: [rules.requiredString],
        },
        hasCreditors: {
          as: 'has_creditors',
        },
      },
    },
  },
};

/**
 * For these fields:
 * - On intake, convert boolean values to strings
 * - On output, convert string values to booleans
 */
const templateConvertKeys = [
  'hasMortgage',
  'hasCreditors',
  'purchased',
  'sold',
];

/** Convert API boolean values to strings for quasar-based components */
function convertBooleansToTemplateStrings(field) {
  const arrayUtils = getFieldSetUtils(field);
  const data = arrayUtils.getAllData();
  const length = data?.value?.length;

  if (!length) return;

  for (let i = 0; i < length; i++) {
    const item = arrayUtils.get(i);

    templateConvertKeys.forEach((key) => {
      const fieldValue = item.getValue(key);

      if (fieldValue === true || fieldValue === false) {
        const asString = fieldValue ? 'true' : 'false';
        item.setValue(key, asString);
      }
    });
  }
}

/** Convert template string values to API boolean values */
function convertTemplateStringsToBooleans(field) {
  const arrayUtils = getFieldSetUtils(field);
  const data = arrayUtils.getAllData();
  const length = data?.value?.length;

  for (let i = 0; i < length; i++) {
    const item = arrayUtils.get(i);

    templateConvertKeys.forEach((key) => {
      const fieldValue = item.getValue(key);

      if (fieldValue === 'true' || fieldValue === 'false') {
        item.setValue(key, fieldValue === 'true');
      }
    });
  }
}

function stringBoolTransforms(field, keyChain) {
  return {
    readIn: () => readInBoolAsString(field, keyChain),
    writeOut: () => writeOutStringAsBool(field, keyChain),
  };
}

/** Write out a boolean value as a string in field transform */
function writeOutStringAsBool(field) {
  const fieldInputValue = field.data().value || null;
  const hasInput = ['true', 'false'].includes(fieldInputValue);

  const valueForApi = hasInput ? Boolean(field.data().value === 'true') : null;

  wizard.fieldRaw(field).set(valueForApi);
}

/** Read in a boolean value as a string in field transform */
function readInBoolAsString(hasField, keyChain) {
  const data = getData();

  const apiValue = keyChain.reduce((acc, key) => acc?.[key], data);

  hasField.data().value = [null, undefined, 'null'].includes(apiValue)
    ? null
    : String(apiValue);
}

function reportingModificationTransforms(field, keyChain) {
  return {
    readIn: () => readInBoolAsString(field, keyChain),
    writeOut: () => {
      writeOutStringAsBool(field, keyChain);

      const isJudgeOrSheriff =
        getData()?.statement?.judge_or_law_enforcement === true;

      const hasOtherExmpetion = getData()?.statement?.other_exemption === true;

      if (field.data().value !== null) {
        getData().statement.reporting_modifications = {
          residential_address: isJudgeOrSheriff || hasOtherExmpetion,
        };
      }
    },
  };
}

export const realEstateSection = {
  title: 'Washington Real Estate',
  order: 4,
  steps: {
    washingtonRealEstateStep,
    realEstateOwnershipStep,
  },
};
