import { ref } from 'vue';
import { useApollo } from '@wapdc/pdc-ui/application';

const thresholds = ref(null);

const MINI_REPORTING = 'mini_reporting';

/**
 * Fetches thresholds amounts from the API or returns them from state if already fetched
 * @returns {Promise<Record<string, number>>} Thresholds data object
 */
export default async function useThresholds() {
  if (!thresholds.value) {
    thresholds.value = await fetchThresholds();
  }

  const minimumReporting = getThresholdAmount(MINI_REPORTING);

  return { minimumReporting };
}

/** Returns 'amount' in state for a given threshold by name */
function getThresholdAmount(thresholdName) {
  const threshold = thresholds.value?.find((t) => t.property === thresholdName);
  return Number(threshold?.amount);
}

/**
 * Fetch thresholds from the API
 * @returns {Promise<Object>} Thresholds data object
 */
async function fetchThresholds() {
  try {
    const service = useApollo('service/financial-affairs');
    return (await service.get('/thresholds')).data;
  } catch (error) {
    console.error('Error fetching thresholds:', error);
  }
}
