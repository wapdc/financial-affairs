import { toRaw } from 'vue';
import { collectNestedArraysWithKey } from 'src/utils/object';

export function disclosureStatementHelpers(wizard) {
  /** Maintain related values when the user changes a field */
  function handleUpdateRelatedFields() {
    const statement = wizard.getData()?.statement;

    if (!statement) return;

    _setDependentFields(statement);
  }

  function _setDependentFields(statement) {
    const whoFields = collectNestedArraysWithKey(statement, 'who');
    const hasDependentFields =
      whoFields?.length > 0 &&
      whoFields.some((whoProxy) => {
        const whoArray = toRaw(whoProxy.value); // Nested proxy
        return whoArray.some((who) => who === 'dependent');
      });

    if (hasDependentFields) {
      wizard.getData().statement.has_dependent = true;
      return;
    }

    wizard.getData().statement.has_dependent = false;
    wizard.getData().statement.dependents = [];
  }

  return {
    handleUpdateRelatedFields,
  };
}
