UPDATE fa_submission SET verified = COALESCE(verified,false);
UPDATE fa_submission SET submitted = COALESCE(submitted, false);
UPDATE fa_submission SET published = COALESCE(published, false);
ALTER TABLE fa_submission
  ALTER COLUMN verified  SET DEFAULT FALSE,
  ALTER COLUMN verified SET NOT NULL,
  ALTER COLUMN submitted SET NOT NULL,
  ALTER COLUMN submitted set DEFAULT FALSE,
  ALTER COLUMN published SET DEFAULT FALSE,
  ALTER COLUMN published SET NOT NULL;