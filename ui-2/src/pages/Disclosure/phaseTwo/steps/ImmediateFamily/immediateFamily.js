import useWizard from 'src/wizard/useWizard';
import rules from 'src/utils/rules';

const { lazyLoadWrapper, dataPath, getData } = useWizard();

const immediateFamilyStep = {
  title: 'Immediate Family',
  order: 0,
  component: lazyLoadWrapper(() => import('./ImmediateFamily.vue')),
  fields: {
    hasSpouse: {
      rules: [rules.requiredString],
      blocking: false,
      data() {
        return dataPath(this, 'statement.has_spouse');
      },
      transform: (hasField) => ({
        writeOut: () => {
          const hasSpouseInputValue = hasField.data().value;
          const valueForApi = [null, 'null', undefined].includes(
            hasSpouseInputValue,
          )
            ? null
            : Boolean(hasSpouseInputValue === 'true');

          getData().statement.has_spouse = valueForApi;
        },
        readIn: () => {
          const data = getData();
          const hasSpouseApiValue = data?.statement?.has_spouse;

          hasField.data().value = [null, undefined, 'null'].includes(
            hasSpouseApiValue,
          )
            ? null
            : String(hasSpouseApiValue);
        },
      }),
    },
    spouseName: {
      rules: [validateSpouseName],
      data() {
        return dataPath(this, 'statement.spouse');
      },
    },
    dependentList: {
      data() {
        return dataPath(this, 'statement.dependents');
      },
      rules: [(v) => validateDependentList(v)],
      transform: (dependentListField) => ({
        writeOut: () => {
          const dependentList = dependentListField.data().value;
          const trimmedList = dependentList
            ?.map((dependent) => dependent.trim())
            ?.filter(Boolean);

          getData().statement.dependents = trimmedList;
        },
        readIn: () => void 0, // No-op
      }),
    },
  },
};

/** Validate spouse name, conditional on hasSpouse */
function validateSpouseName(v) {
  if (getData()?.statement?.has_spouse === 'true') {
    return rules.requiredString(v);
  }

  return true;
}

/** Boolean; if dependents are required, return true */
function dependentsRequired() {
  return getData()?.submission?.dependents_entered === true;
}

/** Validate dependent list */
function validateDependentList(v) {
  if (dependentsRequired() === false) return true;

  const arrayIsNotEmpty = v?.length > 0;
  const allElementsHaveLength = v?.every((dependent) => {
    return dependent?.length > 0;
  });

  const isValid = arrayIsNotEmpty && allElementsHaveLength;

  return isValid || 'Please enter at least one dependent';
}

export const immediateFamilySection = {
  title: 'Immediate Family',
  order: 8,
  steps: {
    immediateFamilyStep,
  },
};
