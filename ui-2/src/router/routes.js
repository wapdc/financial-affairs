import { ROUTE_NAMES } from 'src/router/routeNames';

/*
 * `meta.depth` defines the depth of the route in the navigation hierarchy. This is
 * used to determine the subheader's back button target. If no depth is set, no back
 * button will be displayed.
 *
 * `meta.description` is used in the back button tooltip (and optionally elsewhere).
 */

const routes = [
  {
    path: '/styleguide',
    component: () => import('src/pages/StyleGuide/StyleGuide.vue'),
    name: ROUTE_NAMES.STYLEGUIDE,
  },
  {
    path: '/',
    component: () => import('src/library/PUserLayout.vue'),
    meta: { depth: -1 },
    name: 'root',
    children: [
      {
        path: '',
        component: () => import('src/pages/Home/IndexPage.vue'),
        name: ROUTE_NAMES.HOME,
        meta: {
          backButton: false,
          description: 'home page',
          depth: 0,
        },
      },
      {
        path: '/disclosure-determination',
        name: ROUTE_NAMES.DISCLOSURE_DETERMINATION_FORM,
        component: () =>
          import('src/pages/Disclosure/phaseOne/DisclosureDetermination.vue'),
        meta: {
          backButton: false,
          depth: 1,
          confirmLeaveMessage:
            'Are you sure you want to navigate away from the form and lose all unsaved progress?',
        },
      },
      {
        path: '/disclosure-statement/:submissionId?',
        name: ROUTE_NAMES.DISCLOSURE_FORM,
        component: () =>
          import('src/pages/Disclosure/phaseTwo/DisclosureStatement.vue'),
        meta: {
          backButton: false,
          depth: 1,
          confirmLeaveMessage:
            'Are you sure you want to navigate away from the form and lose all unsaved progress?',
        },
        props: (route) => ({ submissionId: route.params?.submissionId }),
      },
      {
        path: '/my-submissions',
        children: [
          {
            path: '/my-submissions',
            name: ROUTE_NAMES.MY_SUBMISSIONS,
            meta: {
              backButton: true,
              description: 'all previous submissions',
              depth: 1,
            },
            component: () =>
              import('src/pages/MySubmissions/MySubmissions.vue'),
          },
          {
            path: '/my-submissions/detail/:submissionId',
            name: ROUTE_NAMES.SUBMISSION_DETAILS,
            meta: { backButton: true, depth: 2 },
            component: () =>
              import('src/pages/MySubmissions/Details/SubmissionDetails.vue'),
            props: (route) => ({ submissionId: route.params.submissionId }),
          },
        ],
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    // @ts-ignore
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
