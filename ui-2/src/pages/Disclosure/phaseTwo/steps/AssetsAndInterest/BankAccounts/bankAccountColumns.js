import { format } from 'quasar';
import { bankAccountStep } from './bankAccountStep';

const { capitalize } = format;

const keys = bankAccountStep.fields.bankArray.template;

const heldBy = {
  key: keys.heldBy.as,
  label: 'Held By',
  align: 'left',
  sortable: false,
  width: '25%',
  format: (value) => capitalize(value?.join(', ')),
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

const bankName = {
  key: keys.bankName.as,
  label: 'Bank Name',
  align: 'left',
  sortable: false,
  width: '25%',
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

const bankState = {
  key: 'state',
  label: 'State',
  align: 'left',
  sortable: false,
  width: '10%',
};

export default [heldBy, bankName, bankState];
