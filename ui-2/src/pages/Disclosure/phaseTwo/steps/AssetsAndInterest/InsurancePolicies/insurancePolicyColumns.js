import { format } from 'quasar';
import { insurancePolicyStep } from './insurancePolicyStep';

const { capitalize } = format;

const keys = insurancePolicyStep.fields.insuranceArray.template;

const heldBy = {
  key: keys.heldBy.as,
  label: 'Held By',
  align: 'left',
  sortable: false,
  width: '25%',
  format: (value) => capitalize(value?.join(', ')),
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

const insuranceName = {
  key: keys.insuranceName.as,
  label: 'Bank Name',
  align: 'left',
  sortable: false,
  width: '25%',
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

const institutionState = {
  key: 'state',
  label: 'State',
  align: 'left',
  sortable: false,
  width: '10%',
};

export default [heldBy, insuranceName, institutionState];
