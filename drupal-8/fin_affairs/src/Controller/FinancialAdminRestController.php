<?php

namespace Drupal\fin_affairs\Controller;

use Drupal\wapdc_core\Controller\CoreControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use WAPDC\Core\SendGrid\Drupal8CMS;
use WAPDC\Core\SendGrid\Mail;
use WAPDC\FinancialAffairs\FinancialAffairs;

class FinancialAdminRestController extends CoreControllerBase
{
  protected $method;
  protected $response_data;
  protected $request_data;
  protected $response_status;

  protected function prepareRequest(Request $request) {
    $this->request_data = new \stdClass;
    $this->response_data = new \stdClass;

    $this->method = $request->getMethod();
    $this->response_status = 200;
    $this->response_data->success = TRUE;
    if ($this->method == 'POST' || $this->method == 'PUT') {
      $raw_data = file_get_contents('php://input');
      $this->request_data = json_decode($raw_data);
    }
    else {
      $this->request_data = NULL;
    }
  }

  protected function getResponse() {
    return new JsonResponse($this->response_data, $this->response_status);
  }

  protected function setError($message) {
    $this->response_data->error = $message;
    $this->response_status = 500;
    $this->response_data->success = false;
  }

  public function query(Request $request, $sql_base) {

    $this->prepareRequest($request);
    $response_data = $this->response_data;

    $sql_file = $sql_base . '.sql';
    $parameters = $_GET;

    try {
      switch($sql_base) {
        case "fa_unverified_submissions":
          $allSubs = FinancialAffairs::service()->adminQuery('fa-unverified-submissions.sql', $parameters) ?? [];
          $this->response_data->data = $allSubs;
          break;
        case "person_search":
          $response_data->data = FinancialAffairs::service()->adminQuery($sql_file, $parameters);
          break;
        case "person_candidacies":
        case "election":
        case "office_types":
        case "position_number":
        case "jurisdiction_names":
          $response_data->data = FinancialAffairs::service()->personQuery($sql_file, $parameters);
          break;
        default:
          $response_data->data = FinancialAffairs::service()->adminQuery($sql_file, $parameters);

      }
    }
    catch (\Exception $e) {
       $this->setError($e->getMessage());
    }
    return $this->getResponse();
  }


  /**
   * Perform administrative processes that are related to a submission.
   * @param Request $request
   * @param $submission_id
   * @param $action
   * @return JsonResponse
   */
  public function processSubmission(Request $request, $submission_id, $action) {
    $request_data = new \stdClass();
    $this->prepareRequest($request);
    $result = new \stdClass();

    if ($this->method == 'POST' || $this->method == 'PUT') {
      $raw_data = file_get_contents('php://input');
      $request_data = json_decode($raw_data);
    }
    try {
      switch (TRUE) {
        case ($action=='verify' && $this->method=='POST'):
          $this->request_data->user_name = $this->currentUser()->getEmail();
          $sp = FinancialAffairs::service()->getStatementProcessor();
          $mail =  new Mail(new Drupal8CMS());
          $mail->setTestModeEmail($this->currentUser->getEmail());
          $result = $sp->verifySubmission($submission_id, $this->request_data->user_name,$this->request_data, $mail);
          $this->response_data->success = !empty($result->person_id);
          if ($this->response_data->success == true) {
            if ($result->verified)  {
              $mail->setMessageTemplate('d-2ed24f36877e44409c031c7a0a4e1fe6');
              $email = $result->certification_email ? $result->certification_email : $this->currentUser->getEmail();
              $mail->addRecipient($email, '', ['repno' => $result->submission_id]);
              $mail->send();
            }
          }
          break;
        case ($this->method == 'POST' && $submission_id=='many' && $action == 'publish'):
          $sp = FinancialAffairs::service()->getStatementProcessor();
          $pdc_user = $this->getCurrentUser();
          $sp->setSubmissionsPublished($request_data->ids, $request_data->publish, $pdc_user->user_name, $request_data->memo);
          break;
        default:
          throw new \Exception("Invalid action: $action");

      }
    } catch (\Exception $e) {
      $this->setError($e->getMessage());
    }

    return $this->getResponse();
  }

  /**
   * Execute request
   * @param $statement_id
   * @param Request $request
   *  The http request passed by drupal
   * @return JsonResponse
   */
  public function statementAPI(Request $request, $statement_id = "") {
    $user = $this->currentUser();
    $data = new \stdClass();
    $data->errors = NULL;
    $data->success = FALSE;
    $request_data = new \stdClass();

    $method = $request->getMethod();
    if ($method == 'POST' || $method == 'PUT') {
      $raw_data = file_get_contents('php://input');
      $request_data = json_decode($raw_data);
    }

    try {
      $sp = FinancialAffairs::service()->getStatementProcessor();
      switch(TRUE) {
        case (empty($statement_id) && $method == 'POST'):
          $data->statement_id = $sp->saveStatement($request_data, $user->getAccountName());
          $data->success = TRUE;
          break;
        case (!empty($statement_id) && $method == 'PUT'):
          $request_data->statement_id = $statement_id;
          $sp->saveStatement($request_data, $user->getAccountName());
          $data->success = TRUE;
          break;
        case (!empty($statement_id) && $method == 'DELETE'):
          $sp->removeStatement($statement_id, $user->getAccountName());
          $data->success = TRUE;
          break;
        default:
          $data->error = 'Unknown Action';
          $data->success = FALSE;
          throw new \Exception("Action not found");
      }
    }
    catch (\Exception $e) {
      $data->data = NULL;
      $data->errors = $e->getMessage();
      $data->success = FALSE;
    }
    $response = new JsonResponse($data);
    return $response;
  }
}