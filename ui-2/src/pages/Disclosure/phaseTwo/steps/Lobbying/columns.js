import { format } from 'quasar';
const { capitalize } = format;

const who = {
  key: 'who',
  label: 'Lobbyist',
  align: 'left',
  sortable: false,
  width: '25%',
  format: (value) => capitalize(value?.join(', ') || ''),
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

const purpose = {
  key: 'purpose',
  label: 'Purpose',
  align: 'left',
  sortable: false,
  width: '30%',
};

const lobbiedFor = {
  key: 'for',
  label: 'For',
  align: 'left',
  sortable: false,
  width: '20%',
};

export default [who, purpose, lobbiedFor];
