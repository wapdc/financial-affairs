<?php

namespace WAPDC\FinancialAffairs;


use DateTime;
use DateInterval;
use Doctrine\DBAL\Connection;
use Exception;
use stdClass;
use WAPDC\Core\EntityProcessor;
use WAPDC\Core\CandidacyProcessor;
use WAPDC\Core\FilerProcessor;
use Symfony\Component\HttpFoundation\JsonResponse;
use WAPDC\Core\Messenger as WAPDCMessenger;
use WAPDC\Core\Model\Election;
use WAPDC\Core\Model\Jurisdiction;
use WAPDC\Core\Model\Office;
use WAPDC\Core\Model\Person;
use WAPDC\Core\Model\EntityAuthorization;
use WAPDC\Core\Model\Position;
use WAPDC\Core\Model\Candidacy;
use WAPDC\Core\Model\Official;
use WAPDC\Core\Model\User;
use WAPDC\Core\OfficialProcessor;
use WAPDC\Core\SendGrid\MailerInterface;
use WAPDC\FinancialAffairs\Model\FAStatement;
use WAPDC\FinancialAffairs\Model\FAStatementLog;
use WAPDC\FinancialAffairs\Model\FASubmission;


/**
 * this polyfill  is added and can be removed when we get to php 7.3
 * it turns out phpStorm adds this polyfill even if 7.2 is set as the current version
 * so tests pass event though the function is not defined in 7.2
 */
if (!function_exists('array_key_first')) {

 function array_key_first(array $arr) {
  foreach($arr as $key => $unused) {
    return $key;
  }
  return NULL;
  }
}

class StatementProcessor
{
  /** @var FADataManager */
  protected $_dataManager;

  /** @var array */
  protected $fas_owed = [];

  protected $fas_cache = [];

  //Used to update the current submission after saving
  /** @var FASubmission */
  protected $submission;

  /** @var MailerInterface */
  private $mailer;

  /** @var string sendGrid template id */
  const TEMPLATE_ID = "d-845240efea034916bcfca43dc14ebaa4";

  public $project_dir;

  /**
   * StatementProcessor constructor.
   * @param FADataManager $dataManager
   * @throws Exception
   */
  public function __construct(FADataManager $dataManager)
  {
    /** @var FADataManager _dataManager */
    $this->_dataManager = $dataManager;
    $this->project_dir = dirname(__DIR__);
  }

  /**
   * Retrieve a statement record from the database in standard object form.
   *
   * @param int $statement_id
   *   The ID of the statement that tp retrieve
   * @return stdClass|null
   *   Submissions data.
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   */
  public function getStatement($statement_id) {
    /** @var FAStatement $statement */
    $data = null;
    $statement = $this->_dataManager->em->find(FAStatement::class, $statement_id);
    if ($statement) {
      $data = new stdClass();
      $data->person_id = $statement->person_id;
      $data->period_start =  StatementProcessor::formatDate($statement->period_start);
      $data->period_end = StatementProcessor::formatDate($statement->period_end);
      $data->first_filed_at = StatementProcessor::formatDate($statement->first_filed_at);

      if (!empty($statement->retain_until) ) {
        $data->retain_until = StatementProcessor::formatDate($statement->retain_until);
      }
      if(!empty($statement->hold_for)) {
        $data->hold_for = $statement->hold_for;
      }

      $data->email = $statement->email;
      $data->statement_id = $statement->statement_id;
    }
    return $data;
  }

  /**
   * @param stdClass $statement
   *   Statement submission object to save
   * @param $method
   *   POST or PUT
   * @param $user
   *  The current Drupal User
   * @return int
   *   ID of statement created.
   * @throws Exception
   */
  public function saveStatement($statement, $user="") {
    $fa_action = null;
    $fa_message = null;
   // $user_name = $user->account->name;


    $currentStatement = null;
    $person = null;
    if (!empty($statement->statement_id)) {
      /** @var FAStatement $currentStatement */
      $currentStatement = $this->_dataManager->em->getRepository(FAStatement::class)
        ->findOneBy(['statement_id' => $statement->statement_id]);
      $person = $this->_dataManager->em->find(Person::class, $currentStatement->person_id);
      $fa_action = 'statement update';
      $fa_message = 'FA Statement updated';
    }
    elseif (!empty($statement->person_id)) {
      $person = $this->_dataManager->em->find(Person::class, $statement->person_id);
      $fa_action = 'statement created';
      $fa_message = 'New FA Statement created';
    }

    if (!$person) {
      throw new Exception("Person required to add a financial statement to.");
    }

    if ($currentStatement) {
      $FAStatement = $currentStatement;
    } else {
      $FAStatement = new FAStatement();
    }


    $FAStatement->person_id = $statement->person_id;
    $FAStatement->name = $person->name;
    $FAStatement->filer_id = $person->filer_id;
    $FAStatement->email = $statement->email;
    $FAStatement->first_filed_at = !empty($statement->first_filed_at) ? new DateTime($statement->first_filed_at) : NULL;
    $FAStatement->period_start = !empty($statement->period_start) ? new DateTime($statement->period_start) : $FAStatement->period_start;
    $FAStatement->period_end = !empty($statement->period_end) ? new DateTime($statement->period_end) : $FAStatement->period_end;

    if (!$FAStatement->period_start || !$FAStatement->period_end) {
      throw new Exception("Period start and end fields are both required. ");
    }
    if ($FAStatement->period_end <= $FAStatement->period_start) {
      throw new Exception("You are not authorized to disrupt the space-time continuum");
    }

    if (isset($statement->retain_until) ) {
      $FAStatement->retain_until = !empty($statement->retain_until) ? new DateTime($statement->retain_until) : $FAStatement->retain_until;
    }
    if(isset($statement->hold_for)) {
      $FAStatement->hold_for = !empty($statement->hold_for) ? $statement->hold_for : $FAStatement->hold_for;
    }

    if($FAStatement->retain_until && !$FAStatement->hold_for) {
      throw new Exception("A Hold for description is required when a retention date is provided.");
    }
    if(!$FAStatement->retain_until && $FAStatement->hold_for) {
      throw new Exception("A retention date is required when a reason for the hold is provided.");
    }

    $this->_dataManager->em->persist($FAStatement);
    $this->_dataManager->em->flush();

    $this->logFAStatementAdminAction($FAStatement->statement_id, null, $FAStatement->person_id, $user, $fa_action, $fa_message);

    return $FAStatement->statement_id;
  }

  public function documentAccess($user, $doc_id, $repno): bool {
    $rows = $this->query('person/legacy-document-authorization.sql', [
      'uid' => $user->uid,
      'realm' => $user->realm,
      'doc_id' => $doc_id,
      'repno' => $repno
    ]);
    return !empty($rows);
  }

  /**
   * @param $response_data
   * @param $user
   * @param $person_id
   *@throws Exception
   */
  public function getHistoricalSubmissions($response_data, $user, $person_id):void {
      $response_data->unverified = $this->query('person/unverified_submission_history.sql', ["uid" => $user->uid, "realm" => $user->realm, 'person_id' => $person_id]);
      if ($person_id) {
        $response_data->prior = $this->query('person/prior_submission_history.sql', ["person_id" => $person_id]);
      }
      if ($person_id) {
        $response_data->legacy = $this->query('person/legacy-statements.sql', ["person_id" => $person_id]);
      }
  }


  public function query($sql_file, $parameters = []) {
    $dir = dirname(__DIR__);
    return $this->_dataManager->executeQueryFromFile("$dir/data", $sql_file, $parameters);
  }

  /**
   * List the matching people for a draft statement.
   *
   * @param int $submission_id
   *   ID of saved submission to use for matching people.
   * @return array|mixed[]
   * @throws Exception
   */
  function matchingPeople($submission_id) {

    /** @var FASubmission $submission */
    $submission= $this->_dataManager->em->find(FASubmission::class, $submission_id);
    if (!$submission) {
     throw new Exception("Submission not found");
    }

    // Get the name and city information
    return $this->matchingPeopleByUser($submission->user_data);
  }

  /**
   * List the matching people for a draft statement.
   *
   * @param stdClass $user_data
   * @return array|mixed[]
   * @throws Exception
   */
  function matchingPeopleByUser($user_data) {

    /** @var FASubmission $submission */

    if (empty($user_data->person->name)) {
      throw new Exception("Name is required to find matching people");
    }

    $parameters = [
      'search' => $user_data->person->name,
    ];

    $matches = $this->query('person/matching_people.sql', $parameters);
    foreach($matches as $match) {
      $match->candidacies = $this->query('person/person_candidacies.sql', ['person_id' => $match->person_id]);
      $match->offices = $this->query('person/person_offices.sql', ['person_id' => $match->person_id ]);
    }

    return $matches;
  }

  /**
   * Remove the record of a statement submission.
   *
   * @param int $statement_id
   *   the ID of statement to remove
   * @param $user
   * @throws Exception
   */
  public function removeStatement($statement_id, $user = "") {
    $statement = $this->_dataManager->em->find(FAStatement::class, $statement_id);
    if (!$statement) {
      throw new Exception("Could not find statement to remove");
    }

    if (isset($statement->retain_until) || isset($statement->hold_for)) {
      throw new Exception("Statement id: $statement_id cannot be removed because the record has a retention hold on it.");
    }

    $this->_dataManager->em->remove($statement);
    $this->_dataManager->em->flush();
    $this->logFAStatementAdminAction($statement->statement_id, null, $statement->person_id, $user, "statement removed", "FA Statement removed");
  }


  /**
   * @param $submission_id
   * @throws Exception
   */
  public function removeSubmission($submission_id, $user = "") {
    /** @var FASubmission $submission */
    $submission = $this->_dataManager->em->find(FASubmission::class, $submission_id);
    if (!$submission) {
      throw new Exception("Submission id: $submission_id does not exist");
    }
    $this->_dataManager->em->remove($submission);
    $this->_dataManager->em->flush();
    $this->logFAStatementAdminAction(NULL, $submission_id, $submission->person_id, $user, "statement removed", "FA Submission removed");
    return;
  }


  /**
   * @param $data
   * @param  User $user
   * @param null $submission_id
   * @param null $save_count
   * @return stdClass $submission
   * @throws Exception
   */
  public function saveSubmission($data, $user, $submission_id=null) {

    $validation = new Validator();
    if ($submission_id) {
      $submission = $this->_dataManager->em->find(FASubmission::class, $submission_id);
    }
    else {
      $submission = new FASubmission();
      if (!empty($data->submission->amend_id)) {
        $submission->amend_id = $data->submission->amend_id;
      }
    }
    if (!$submission) {
      throw new Exception("Submission ID doesn't match any known submissions");
    } elseif ($submission && $submission_id && !isset($data->submission->save_count)) {
      throw new Exception("A save count should be provided when a submission is being saved");
    } elseif ($submission->save_count != $data->submission->save_count){
      throw new Exception("This record has been changed by another user since you started editing it. Please refresh the page.");
    } elseif ($submission->submitted) {
      throw new Exception( "Submission altered after having been submitted.");
    }

    if ($submission->period_start) {
      $this->validateSubmission($data, $validation);
      $this->validateIdentification($data, $validation);
    }
    else {
      $this->validateIdentification($data, $validation);
    }
    if (isset($data->submission->person_id)) {
      $submission->person_id = $data->submission->person_id;
    }
    unset($data->submission);
    $submission->save_count++;
    $submission->updated_at = new DateTime();
    $submission->user_data = $data;
    $submission->realm = $user->realm;
    $submission->uid = $user->uid;
    if (isset($data->person)) {
      $submission->name = $data->person->name ?? null;
    }
    if (isset($data->statement)) {
      $submission->period_start = !empty($data->statement->period_start) ? new DateTime($data->statement->period_start) : NULL;
      $submission->period_end = !empty($data->statement->period_end) ? new DateTime($data->statement->period_end) : NULL;
    }
    if (!$submission->submission_id) {
      $this->_dataManager->em->persist($submission);
    }

    $this->_dataManager->em->flush();
    $this->submission = $submission;
    return $this->submissionData($submission);
  }

  /**
   * @param $data
   * @param User $user
   * @param $submission_id
   *
   * @return stdClass
   * @throws Exception
   */
  public function submitSubmission($data, $user, $submission_id){
    /** @var FASubmission $submission */
    $submission = $this->_dataManager->em->find(FASubmission::class, $submission_id);
    if (!$submission) {
      throw new Exception("Submission ID doesn't match any known submissions");
    }
    $validator = new Validator();
    $this->validateSubmission($submission->user_data, $validator);
    if($validator->getErrors()){
      throw new Exception("Cannot submit, there are still validation errors.");
    }
    if($submission->submitted){
      throw new Exception("This submission has already been submitted");
    }


    $date = new DateTime();
    $submission->updated_at = $date;
    $submission->submitted_at = $date;
    $submission->submitted = true;
    $submission->verified = false;
    $submission->published = false;
    $submission->certification_email = $data->submission->certification_email ?? null;
    $submission->certification_phone = $data->submission->certification_phone ?? null;
    $submission->certification_name = $data->submission->certification_name;
    $submission->certification = $data->submission->certification;
    unset($submission->user_data->submission);
    //log submission action
    $statement_id = null;
    $action = 'submission';
    $message = 'Financial Affairs Disclosure submitted';
    if ($submission->amend_id) {
      $statement = $this->_dataManager->em->getRepository(FAStatement::class)->findOneBy(['submission_id' => $submission->amend_id]);
      if ($statement) {
        $statement_id = $statement->statement_id;
      }
      $action = 'amendment';
      $message = 'Financial Affairs Disclosure amended';
    }
    $this->logFAStatementAdminAction($statement_id, $submission_id, $submission->person_id, $user->user_name, $action, $message);
    $this->_dataManager->em->flush($submission);
    $this->autoVerify($submission,$user);
    return $this->submissionData($submission);
  }

  /**
   * @param $submissionData
   */
  public function setStatementActiveOfficial($submissionData) {
    unset($submissionData->submission->active_official);
    $statement = $submissionData->statement ?? new stdClass();
    if (isset($submissionData->offices) && isset($statement->period_start) && isset($statement->period_end)) {
      $requires = array_filter($submissionData->offices, function ($office) use ($statement) {
        return
             ($office->start_date >= $statement->period_start && $office->start_date <= $statement->period_end)
          || ($office->start_date <= $statement->period_start && (empty($office->end_date) || $office->end_date > $statement->period_end))
          || (!empty($office->end_date) && $office->end_date >= $statement->period_start && $office->end_date <= $statement->period_end);
      });
      if (count($requires) > 0) {
        $submissionData->submission->active_official = true;
      }
    }
  }

  public function setStatementDependentsEntered($submissionData) {
    $statement = $submissionData->statement ?? new stdClass();
    $has_dependents = $this->findInStatement($statement, 'dependent', 'who');
    if ($has_dependents) {
      $submissionData->submission->dependents_entered = true;
    } else {
      $submissionData->submission->dependents_entered = false;
    }
  }

  /**
   *
   */
  protected function mergeHistoricalData(stdClass $data) {
    $dir = $this->project_dir . '/data/person';

    if ($data->person->person_id > 0) {
      $offices = $this->_dataManager->executeQueryFromFile($dir, 'person_offices.sql', ['person_id' => $data->person->person_id]);
      if ($offices && !$data->offices) {
        $data->offices = $offices;
      }
      else {
        $offices = array_filter($offices, function ($e) use ($data) {
          $found = false;
          foreach ($data->offices as $o) {
            $found = $found || $o->office_code == $e->office_code && $o->jurisdiction_id == $e->jurisdiction_id;
          }
          return !$found;
        });
        $data->offices = array_merge($data->offices,$offices);
      }

      $candidacies = $this->_dataManager->executeQueryFromFile($dir, 'person_candidacies.sql', ['person_id' => $data->person->person_id]);
      if ($candidacies && !$data->candidacies) {
        $data->candidacies = $candidacies;
      }
      else {
        // Merge candidacies
        $candidacies = array_filter($candidacies, function ($e) use ($data) {
          $found = false;
          foreach ($data->candidacies as $o) {
            $found = $found || $o->office_code == $e->office_code && $o->jurisdiction_id == $e->jurisdiction_id && $o->election_code = $e->election_code;
          }
          return !$found;
        });
        $data->candidacies = array_merge($data->candidacies, $candidacies);
      }

    }
  }

  /**
   * This function takes a Person and their Candidacies and Offices and
   * determines the time periods for which they owe FAStatements.
   *
   * @param stdClass $data This should contain a key called person that
   * is has a person_id, a key called candidacies that is an array of
   * zero or more objects that contain start and end dates, a key called offices
   * that is an array of zero or more objects that contain a campaign start date.
   *
   * @param User $user This is the logged in user
   *
   * @param null|DateTime $current_date Mostly used for testing. Defaults to current date.
   *
   * @return array Returns an array of objects with period_start and period_end
   * dates that represent the time periods that the Person owes FAStatements for.
   * @throws Exception
   */
  public function getStatementsOwed(stdClass $data, User $user, $current_date = null) {
    $this->faSubmissionCache($data->person->person_id, $user);
    $current_date = $current_date ? new DateTime($current_date) : new DateTime();
    if(!empty($this->fas_owed)) $this->fas_owed = [];

    // Merge in historical data
    $this->mergeHistoricalData($data);

    // Order offices by start date to make sure we process in the right order
    usort($data->offices, function ($a, $b) { return strcmp($a->start_date, $b->start_date);});

    // Check for FA coverage based on office terms
    foreach ($data->offices as $office){

      if (empty($office->start_date)) {
        throw new Exception("Missing start data on office");
      }
      $start_date =  new DateTime($office->start_date);
      if ($start_date <= $current_date) {

        // check for prev 12
        if($start_date->format('m') != '12'){
          $start = date_sub(new DateTime($office->start_date), (new DateInterval('P1Y')))->format('Y-m-d');
          $end = date_sub(new DateTime($office->start_date), (new DateInterval('P1D')))->format('Y-m-d');
          $found = $this->checkForStatement($start);
          if(!$found){
            $this->addToOwed($start, $end);
          }
        }

        //check for annual(s)
        if($office->start_date){
          //check for annuals in all the years between the start and end_date dates
          //work backwards skipping the current year
          $start_date = new DateTime($office->start_date);
          if ($office->start_date > $current_date) {
            $start_date = $current_date;
          }

          if(!empty($office->end_date) && (new DateTime($office->end_date)) < $current_date){
            $end_year = (new DateTime($office->end_date))->format('Y') - 1;
          }else{
            $end_year = $current_date->format('Y') -1;
          }
          $start_year = (int)$start_date->format('Y');
          if ($start_year <= $end_year) {
            $years = range($start_year, $end_year);

            foreach($years as $year){
              $start = $year . '-01-01';
              $end = $year . '-12-31';
              $found = $this->checkForStatement($start);
              if(!$found){
                $this->addToOwed($start, $end);
              }
            }
          }
        }

        // check for final
        if (!empty($office->end_date) && $current_date >= (new DateTime($office->end_date))) {
          $start = (new DateTime($office->end_date))->format('Y') . '-01-01';
          $end = (new DateTime($office->end_date))->format('Y-m-d');
          $found = $this->checkForStatement($start);
          if (!$found) {
            $this->addToOwed($start, $end, null, true);
          }

        }
      }
    }

    // Check for FA coverage based on candidacy start date
    foreach ($data->candidacies as $candidacy) {
      $campaign_start_date = new DateTime($candidacy->campaign_start_date);
      if ($campaign_start_date <= $current_date) {
        $jurisdiction = $this->_dataManager->em->find(Jurisdiction::class, $candidacy->jurisdiction_id);
        $prev12_start = date_sub(new DateTime($candidacy->campaign_start_date), (new DateInterval('P1Y')))->format('Y-m-d');
        $prev12_end = date_sub(new DateTime($candidacy->campaign_start_date), (new DateInterval('P1D')))->format('Y-m-d');
        $found = $this->checkForCandidacyStatements($candidacy->campaign_start_date);
        if (!$found) {
          $this->addToOwed($prev12_start, $prev12_end, $jurisdiction->rpts, true);
        }
      }
    }

    usort($this->fas_owed, function ($a, $b) {
      return strcmp($b->period_end, $a->period_end);

    });
    return $this->fas_owed;
  }

  /**
   * @throws \Doctrine\DBAL\Exception
   */
  public function getDateOverride()
  {
    return $this->_dataManager->db->fetchOne("select value from wapdc.public.wapdc_settings where property = 'date_override'");
  }

  protected function addToOwed($start, $end, $candidateJurisdiction=null, $stretch=false){

    // If candidateJurisdiction is null we still require this statement becuase
    // it is related to an "offical record"
    $required = $candidateJurisdiction != 'N' ? true: false;

    // Determine if addToOwed is being called because we found a candidacy
    // related obligation for a non-N jurisdiction. Null in $canidateJurisdiction
    // implies a reporting obligation due to an offical record, not a candidacy.
    $required_candidate = $candidateJurisdiction && $required;
    // Check if we already have an owed statement for the dates
    $exists = array_filter(
      $this->fas_owed,
      function ($el) use ($start, $end, $stretch)
      {
        $start_day = substr($start, 5);
        $end_day = substr($end, 5);

        if ($start == $el->period_start && $end == $el->period_end) {
          return true;
        }
        elseif ($start_day != '01-01') {
          return (($start >= $el->period_start) && ($start <= $el->period_end));
        }
        elseif ($stretch && $end_day <= '04-15') {
          $new_end = (substr($start,0,4) - 1) . '-12-31';
          return $el->period_end == $new_end;
        }
        else {
          return false;
        }
      }
      );
    if (!$exists) {
      $fas = new stdClass();
      $fas->period_start = $start;
      $fas->period_end = $end;
      $fas->required = $required;
      $fas->legacy_filing = $end < '2019-12-31';
      $this->fas_owed[] = $fas;
    }
    //If already have a statement owed coalesce what is on record with new requirement.
    else {
      $record = $exists[array_key_first($exists)];
      $record->required = $record->required ?? $required;
      if ($stretch && (!$required_candidate || $required_candidate && substr($end, 5) < '04-15')) {
        $record->period_end = $end;
      }
      $this->fas_owed[array_key_first($exists)] = $record;
    }
  }



  /**
   * @param $year
   *
   * @throws Exception
   */
  protected function removeAnnualOwed($year){
    foreach ($this->fas_owed as $idx => $fa){
      if((new DateTime($fa->period_start))->format('Y') == $year &&
        (new DateTime($fa->period_end))->format('Y') == $year){
        unset($this->fas_owed[$idx]);
      }
    }
  }

  /**
   * @param $year
   *
   * @return bool
   * @throws Exception
   */
  protected function owesAnnualForYear($year){
    foreach ($this->fas_owed as $fa){
      if((new DateTime($fa->period_start))->format('Y') == $year){
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Supposed to check for any required statements within a given period.
   *
   * Intentionally using start date instead of first filed at because it
   * makes the case easier to handle the final to allow people to file before
   * it is actually required.
   *
   * @param $start
   * @return bool
   */
  protected function checkForStatement($start){
    $start = substr($start,0,4);
    $found = array_filter($this->fas_cache, function ($fa) use ($start){
      return substr($fa->period_start,0,4) == $start;
    });

    return !empty($found);
  }

  protected function checkForCandidacyStatements($campaign_start_date) {
    $start_year = substr($campaign_start_date, 0, 4);
    $found = array_filter($this->fas_cache, function ($fa) use ($start_year, $campaign_start_date) {
      $max_date = date_add(new DateTime($fa->period_end), (new DateInterval('P1Y')))->format('Y-m-d');
      return $start_year === substr($fa->first_filed_at, 0, 4) && $campaign_start_date < $max_date;
    });

    return !empty($found);
  }

  /**
   * @param $person_id
   * @param $user
   *
   * @throws Exception
   */
  protected function faSubmissionCache($person_id, $user) {
    $this->fas_cache = [];
    if($user === null) {
      $user = new stdClass();
      $user->uid = null;
      $user->realm = null;
    }

    if ($person_id > 0) {
      $this->fas_cache = $this->_dataManager->db->executeQuery(
        "
          SELECT period_start, period_end, first_filed_at
          FROM public.fa_statement WHERE person_id = :person_id
        ",
        ['person_id' => $person_id]
      )->fetchAllAssociative();
      $this->fas_cache = array_map(function($period) { return (object) $period; }, $this->fas_cache);
    }

    $periods = $this->_dataManager->db->executeQuery(
      "
        SELECT period_start FROM public.fa_submission WHERE fa_statement_id IS NULL AND submitted = TRUE
        AND ((person_id = :person_id AND person_id != -1) OR (uid= :uid
        AND realm = :realm
        AND cast(user_data AS json) -> 'person' ->> 'person_id' = :person_id))
      ",
      [
        'person_id' => $person_id,
        'uid' => $user->uid,
        'realm' => $user->realm
      ]
    )->fetchAllAssociative();

    $periods = array_map(function($period) { return (object) $period; }, $periods);

    $this->fas_cache = array_merge($this->fas_cache, $periods);
  }

  /**
   * @param $submissionData
   * @param Validator $validation
   * @throws Exception
   */
  public function validateIdentification($submissionData, $validation) {
    if (!isset($submissionData->person->name) || !$submissionData->person->name) {
      $validation->addError('person.name.required', 'filer name missing');
    }
    $offices = $submissionData->offices ?? [];
    $candidacies = $submissionData->candidacies ?? [];
    $now = time();
    foreach($candidacies as $key=>$c) {
      $election = isset($c->election) ? substr($c->election, 0, 4).' ' : '';
      $office = isset($c->office) ? ' '.$c->office.' candidacy' : 'candidacy';
      $validation->validateAddress($c, 'candidacies.'.$key, $election.$office);
      $validation->validateRequired($c, ['campaign_start_date', 'election_code', 'email', 'jurisdiction_id', 'office_code'],
        'candidacies.'.$key, $election.$office);
      if (!$validation->hasErrorIdPortion('candidacies.')) {
        $validation->validateId(Jurisdiction::class, $c->jurisdiction_id, true, 'candidacies.'.$key.'.jurisdiction', $election.$office.' jurisdiction');
        $validation->validateId(Office::class, $c->office_code, false, 'candidacies.'.$key.'.office', $election.$office.' office');
        $validation->validateId(Election::class, $c->election_code, false,'candidacies.'.$key.'.election', $election.$office.' election');
        if (isset($c->position_id) && $c->position_id) {
          $validation->validateId(Position::class, $c->position_id, true,'candidacies.'.$key.'.position', $election.$office.' position');
        }
        if (!strtotime($c->campaign_start_date)) {
          $validation->addError("candidacies.".$key.".campaign_start_date.invalid", $election.$office.' campaign start date invalid');
        }
        else if (strtotime($c->campaign_start_date) > $now) {
          $validation->addError("candidacies.".$key.".campaign_start_date.invalid", $election.$office.' campaign start date cannot be in the future');
        }
      }
    }
    foreach ($offices as $key=>$o) {
      $office = isset($o->office) ? $o->office : 'public position';
      $validation->validateAddress($o, 'offices.'.$key, $office.' position');
      $validation->validateRequired($o, ['email', 'jurisdiction_id', 'office_code', 'start_date'],
        'offices.'.$key, $office);
      if (!$validation->hasErrorIdPortion('offices.')) {
        $validation->validateId(Jurisdiction::class, $o->jurisdiction_id, true, 'offices.' . $key . '.jurisdiction', $office. ' jurisdiction');
        $validation->validateId(Office::class, $o->office_code, false, 'offices.' . $key . '.office', $office. ' office');
        if (isset($o->position_id) && $o->position_id) {
          $validation->validateId(Position::class, $o->position_id, true, 'offices.' . $key . '.position', $office.' position');
        }
        if (!strtotime($o->start_date)) {
          $validation->addError("offices.".$key.".start_date.invalid", $office.' start date invalid');
        }
        if(isset($o->end_date) && !strtotime($o->end_date)) {
          $validation->addError("offices.".$key.".end_date.invalid", $office.' end date invalid');
        }
        else if (!empty($o->end_date) && $o->end_date > date('Y-m-d', strtotime('+7 days'))) {
          $validation->addError("offices.".$key.".end_date.future", $office.' end date too far in the future');
        }
        else if (isset($o->end_date) && $o->end_date && $o->start_date > $o->end_date) {
          $validation->addError("offices.".$key.".dates.invalid", $office.' start and end date combination invalid');
        }
      }
    }
  }

  /**
   * @param $submissionData
   * @param Validator $validation
   * @throws Exception
   */
  public function validateSubmission($submissionData, $validation) {
    // verify minimally required fields to establish a financial statement
    $statement = $submissionData->statement ?? new stdClass();
    if (!isset($submissionData->submission)) {
      $submissionData->submission = new stdClass();
    }
    $this->setStatementActiveOfficial($submissionData);
    $this->setStatementDependentsEntered($submissionData);
    try {
      $period_start = !empty($statement->period_start) ? new DateTime($statement->period_start) : NULL;
      $period_end = !empty($statement->period_end) ? new DateTime($statement->period_end) : NULL;
    }
    catch(Exception $e) {
      $period_start = NULL;
      $period_end = NULL;
    }
    if (!$period_start || !$period_end) {
      $validation->addError('coverage.required', "Coverage period must be specified for a financial disclosure statement");
    }
    elseif ($period_start > $period_end) {
      $validation->addError('coverage.invalid', "You are not authorized to disrupt the space-time continuum.");
    }
    else {
      $interval = $period_end->diff($period_start);
      if ($interval->y >= 2) {
        $validation->addError('coverage.invalid', "Coverage period cannot be longer than two years. ");
      }
    }

    $person = $submissionData->person ?? new stdClass();
    if (!$person->name) {
      $validation->addError('person.name.required', "Name of person for the statement is required");
    }

    if(isset($submissionData->submission->person_id)) {
      $validate_person = $this->_dataManager->em->find(Person::class, $submissionData->submission->person_id);
      if (!isset($validate_person)) {
        $validation->addError('submission.person.invalid', "Person id invalid");
      }
    }

    // If they listed a spouse there is no reason to check if they indicated that anything is owned by the spouse
    if(!isset($statement->spouse)) {
      $has_spouse = $this->findInStatement($statement, 'spouse', 'who');
      if ($has_spouse && !isset($statement->spouse)) {
        $validation->addError('spouse.required', 'Name of spouse or registered domestic partner is required.');
      }
    }

    $validation->validateRequired($statement, ['has_spouse'], "immediateFamily.required", "Immediate family selection is ");

    if (!empty($statement->has_spouse) && empty($statement->spouse)) {
      $validation->addError('spouse.required', 'Name of spouse or registered domestic partner is required.');
    }
    //The statement->has_dependent is the true false on the front end
    if (!empty($statement->has_dependent) && empty($statement->dependents)) {
      $validation->addError('dependents.required', 'Name of adult dependents required.');
    }
    //This has_dependents is about if any dependents have been added to any items in the submission
    $has_dependents = $this->findInStatement($statement, 'dependent', 'who');
    if ($has_dependents && !isset($statement->has_dependent)) {
      $validation->addError('dependents.required', 'You must indicate if you have adult dependents.');
    }
    //The statement->dependents is the actual list of dependents
    if (!$has_dependents && !empty($statement->dependents)){
      $validation->addError('dependents.overdisclosed', 'Name of dependent over disclosed when not involved in the disclosure.');
    }

    // INCOME Validation
    // Validate that all the required categories are specified.
    $validation->validateCategories($statement, 'income', ['employment', 'retirement', 'uncommon'], 'income', "Income");

    if (!empty($statement->income->data)) {
      foreach ($statement->income->data as $key=>$income) {
        $incomeName = !empty($income->name) ? ' '.$income->name : ' asset';
        if($income->category == 'employment') {
          $validation->validateAddress($income, "income.$key", $this->formatCategoryName($income->category) . " income" . $incomeName);
        }
        $validation->validateWho($income, "income.$key", $this->formatCategoryName($income->category)." income".$incomeName);
        $validation->validateRequired($income, ['name', 'why', 'income'], "income.$key", $this->formatCategoryName($income->category)." income".$incomeName);
        $validation->validateRange($income, 'income', "income.$key",$this->formatCategoryName($income->category)." income".$incomeName);
      }
    }

    // Asset validation
    $validation->validateCategories($statement, 'accounts', ['bank_account', 'investment', 'self_investment', 'stocks', 'insurance', 'other'], "assets", "Asset");
    if (!empty($statement->accounts->data)) {
      foreach ($statement->accounts->data as $key => $account) {
        $accountName = !empty($account->name) ? ' '.$account->name : "";
        $categoryDisplay = $this->formatCategoryName($account->category);
        $validation->validateWho($account, "accounts.$key", $categoryDisplay." asset".$accountName);
        if ($account->category !== 'stocks') {
          $validation->validateAddress($account, "accounts.$key", $categoryDisplay." asset".$accountName);
        }
        $validation->validateRequired($account, ['assets'], "accounts.$key", $categoryDisplay." asset".$accountName);
        if (!empty($account->assets)) foreach ($account->assets as $asset_key => $asset)  {
          $accountDisplay = $accountName ? $accountName : $categoryDisplay;
          $assetName = $asset->nature ?? "asset";
          $required = $account->category == 'stocks' ? ['ticker', 'name', 'value'] : ['nature', 'value'];
          $validation->validateRequired($asset, $required, "accounts.$key.assets.$asset_key", $accountDisplay.' '.$assetName);
          $validation->validateRange($asset, 'value', "accounts.$key.assets.$asset_key", $accountDisplay.' '.$assetName);
          $validation->validateRange($asset, 'income', "accounts.$key.assets.$asset_key", $accountDisplay.' '.$assetName);
        }
      }
    }

    if (isset($submissionData->statement->reporting_modifications->residential_address) && $submissionData->statement->reporting_modifications->residential_address === true) {
      $has_modification = true;
    } else  {
      $has_modification = false;
    }

    // Real estate
    if (!isset($statement->judge_or_law_enforcement)) {
      $validation->addError('judge_or_law_enforcement.required', "Judge or law enforcement officer answer is missing.");
    }

    if (!isset($statement->real_estate_owned) || ($statement->real_estate_owned !== TRUE && $statement->real_estate_owned !== FALSE)) {
      $validation->addError('real_estate_owned.incomplete', "You must indicate if you owned real estate in Washington State");
    }
    if (!empty($statement->real_estate)) {
      $statement->real_estate_owned = TRUE;
      $requiredList = ['who', 'assessed_value'];
      foreach ($statement->real_estate as $key => $estate) {
        $validation->validateRequired($estate, $requiredList, "real_estate.$key", "Real estate");
        $validation->validateRequired($estate, ['purchased', 'sold'], "real_estate.$key.ownership", "Real estate ownership information");
        $validation->validateWho($estate, "real_estate.$key", "Real estate");
        $validation->validateLocation($estate->location, "real_estate.$key", "Real estate", $has_modification);
        $validation->validateRange($estate, "assessed_value", "real_estate.$key", "Real estate");
        $requiredCreditorList = ['name', 'terms', 'original_amount', 'current_amount'];
        if (!empty($estate->creditors)) {
          $estate->has_mortgage = TRUE;
          foreach ($estate->creditors as $creditorKey => $creditor) {
            $validation->validateRequired($creditor, $requiredCreditorList, "real_estate.$key.mortgage.$creditorKey", "Real estate creditor");
            $validation->validateAddress($creditor, "real_estate.$key.mortgage.$creditorKey", "Real estate creditors");
            $validation->validateRange($creditor, "original_amount", "real_estate.$key.mortgage.$creditorKey", "Real estate creditors");
            $validation->validateRange($creditor, "current_amount", "real_estate.$key.mortgage.$creditorKey", "Real estate creditors");
          }
        }
        elseif (empty($estate->creditors) && !empty($estate->has_creditors)) {
          $validation->addError("real_estate.$key.mortgage", "You must add a creditor after indicating the property has a mortgage");
        }
        else {
          $validation->validateRequired($estate, ['has_creditors'], "real_estate.$key.mortgage", "Real estate mortgage information" );
        }
        if (!empty($estate->purchaser)) {
          $estate->sold = TRUE;
          $requiredPurchaserList = ['name', 'sale_amount'];
          $validation->validateRequired($estate->purchaser, $requiredPurchaserList, "real_estate.$key.purchaser", "Real estate purchaser");
          $validation->validateRange($estate->purchaser, "sale_amount", "real_estate.$key.purchaser", "Real estate purchaser");
          $validation->validateAddress($estate->purchaser, "real_estate.$key.purchaser", "Real estate purchaser");
        }
        elseif (empty($estate->purchaser) && !empty($estate->sold)) {
          $validation->addError("real_estate.$key.purchaser.incomplete", 'You must add a purchaser after indicating the property has been sold');
        }
      }
    }
    elseif (empty($statement->real_estate) && (!empty($statement->real_estate_owned))) {
      $validation->addError('real_estate.required', 'You must add real estate after indicating that you own one');
    }

    // Business validation
    $validation->validateCategories($statement, 'business_associations', ['owner', 'officer'], 'business_associations', "Business");

    if (!empty($statement->business_associations->data)) {
      foreach ($statement->business_associations->data as $key=>$business_associations) {
        $businessName = (isset($business_associations->legal_name) && $business_associations->legal_name)? ' '.$business_associations->legal_name :'';
        $requiredList = ['legal_name', 'description', 'who'];
        if ($business_associations->category === "owner") {
          array_push($requiredList,'percentage_of_ownership');
        } else if ($business_associations->category === "officer") {
          array_push($requiredList, "position");
        }
        $validation->validateAddress($business_associations, "business_associations.$key", 'Business'.$businessName);
        $validation->validateWho($business_associations, "business_associations.$key", $businessName);
        $validation->validateRequired($business_associations, $requiredList, "business_associations.$key", 'Business'.$businessName);

        $categoryDisplay = $business_associations->legal_name ?? 'Business';
        $validation->validateRequired($business_associations, ['is_lending_institution'], "business_associations.$key.lending_institution", "Business type for $businessName");
        if (!empty($business_associations->is_lending_institution)) {
          $validation->validateRequired($business_associations, ['reporting_choice'], "business_associations.$key.lending_institution", "Reporting choice for  $businessName");
        }

        if (!empty($business_associations->is_lending_institution)) {
          unset($business_associations->has_agency_payments);
          unset($business_associations->agency_payments);
          unset($business_associations->has_office_payments);
          unset($business_associations->officer_payments);
          if (!empty($business_associations->reporting_choice)) {
            $validation->validateList($business_associations, 'reporting_choice', ['balances', 'interest'], "business_associations.$key.lending_institution.reporting_choice", "$businessName invalid reporting choice.");
          }
          if (($business_associations->reporting_choice ?? NULL) == 'balances') {
            if (!empty($business_associations->officers)) {
              $business_associations->has_officers = TRUE;
              foreach($business_associations->officers as $o => $officer) {
                $validation->validateRequired($officer, ['name', 'occupation', 'address'], "business_associations.$key.officers.$o", $categoryDisplay." officers" );
                $validation->validateAddress($officer, "business_associations.$key.officers.$o", "$categoryDisplay address");
              }
            }
            elseif (($business_associations->has_officers ?? NULL) !== FALSE) {
              $validation->addError("business_associations.$key.officers.incomplete", $categoryDisplay." officers incomplete");
            }

            if (!empty($business_associations->agency_balances)) {
              $business_associations->has_agency_balances = TRUE;
              foreach($business_associations->agency_balances as $o => $balance) {
                $validation->validateRequired($balance, ['account', 'balance'], "business_associations.$key.agency_balances.$o", $categoryDisplay." balance" );
                $validation->validateDollarAmount($balance, 'balance', "business_associations.$key.agency_balances.$o.balance", "$categoryDisplay balances amount");
              }
            }
            elseif (($business_associations->has_agency_balances ?? NULL) !== FALSE) {
              $validation->addError("business_associations.$key.agency_balances.incomplete", $categoryDisplay." agency balances incomplete");
            }

          }
          elseif (($business_associations->reporting_choice ?? NULL) == 'interest') {
            if (!empty($business_associations->business_interest_payments)) {
              $business_associations->has_business_interest_payments = TRUE;
              foreach($business_associations->business_interest_payments as $o => $amount) {
                $validation->validateRequired($amount, ['agency', 'amount'], "business_associations.$key.business_interest_payments.$o", $categoryDisplay." interest payment" );
                $validation->validateDollarAmount($amount, 'amount', "business_associations.$key.business_interest_payments.$o.amount", "$categoryDisplay interest payment amount");
              }
            }
            elseif (($business_associations->has_business_interest_payments ?? NULL) !== FALSE) {
              $validation->addError("business_associations.$key.business_interest_payments.incomplete", $categoryDisplay." interest payments incomplete");
            }
            if (!empty($business_associations->business_interest_receipts)) {
              $business_associations->has_business_interest_receipts = TRUE;
              foreach($business_associations->business_interest_receipts as $o => $amount) {
                $validation->validateRequired($amount, ['agency', 'amount'], "business_associations.$key.business_interest_receipts.$o", $categoryDisplay." interest receipt" );
                $validation->validateDollarAmount($amount, 'amount', "business_associations.$key.business_interest_receipts.$o.amount", "$categoryDisplay interest receipt amount");
              }
            }
            elseif (($business_associations->has_business_interest_receipts ?? NULL) !== FALSE) {
              $validation->addError("business_associations.$key.business_interest_receipts.incomplete", $categoryDisplay." interest receipts incomplete");
            }
          }

        } elseif(($business_associations->is_lending_institution ?? NULL) === FALSE) {
          unset($business_associations->reporting_choice);
          unset($business_associations->has_agency_balances);
          unset($business_associations->agency_blances);
          unset($business_associations->has_officers);
          unset($business_associations->officers);
          unset($business_associations->has_business_interest_payments);
          unset($business_associations->business_interest_payments);
          unset($business_associations->has_business_interest_receipts);
          unset($business_associations->business_interest_receipts);

          // Validate payments from governmental unit for office sought or held
          if (!empty($business_associations->office_payments)) {
            $business_associations->has_office_payments = TRUE;
            foreach($business_associations->office_payments as $payment) {
              $validation->validateRequired($payment, ['amount', 'purpose'], "business_associations.$key.office_payments", $categoryDisplay." payments for governmental unit" );
              $validation->validateDollarAmount($payment, 'amount', "business_associations.$key.office_payments", $categoryDisplay." payments for governmental unit");
            }
          }
          elseif (!isset($business_associations->has_office_payments)
              || $business_associations->has_office_payments !== FALSE) {
            $validation->addError("business_associations.$key.office_payments.incomplete", $categoryDisplay." payments for governmental unit incomplete");
          }

          // Validate other government agency payments:
          if (!empty($business_associations->agency_payments)) {
            $business_associations->has_agency_payments = TRUE;
            foreach($business_associations->agency_payments as $payment) {
              $validation->validateRequired($payment, ['agency', 'purpose'], "business_associations.$key.agency_payments", $categoryDisplay." payments for governmental agency" );
            }
          }
          elseif (!isset($business_associations->has_agency_payments) || $business_associations->has_agency_payments !== FALSE) {
            $validation->addError("business_associations.$key.agency_payments.incomplete", $categoryDisplay." payments for governmental unit incomplete");
          }


          // Validate payments from business customer
          if (!empty($business_associations->business_payments)) {
            $business_associations->has_business_payments = TRUE;
            foreach($business_associations->business_payments as $payment) {
              $validation->validateRequired($payment, ['business', 'purpose'], "business_associations.$key.business_payments", $categoryDisplay." payments for business customers" );
            }
          }
          elseif (!isset($business_associations->has_business_payments) || $business_associations->has_business_payments !== FALSE) {
            $validation->addError("business_associations.$key.business_payments.incomplete", $categoryDisplay." payments for business customers incomplete");
          }
        }

        if ($business_associations->category=='owner') {
          if (!empty($business_associations->wa_property)) {
            $business_associations->has_wa_property = TRUE;
            foreach($business_associations->wa_property as $property) {
              $validation->validateLocation($property, "business_associations.$key.wa_property", $categoryDisplay." owned Washington Real Estate");
            }
          }
          elseif (!isset($business_associations->has_wa_property) || $business_associations->has_wa_property !== FALSE) {
            $validation->addError("business_associations.$key.wa_property.incomplete", $categoryDisplay."  owned Washington Real Estate incomplete");
          }
        }
      }
    }

    $validation->validateRequired($statement, ['has_creditors'], 'creditors', 'Creditors');
    if (!empty($statement->creditors)) {
      $statement->has_creditors = TRUE;
      $requiredList = ['payment_terms', 'original_amount', 'ending_amount', 'name', 'who'];
      foreach ($statement->creditors as $key => $creditors) {
        $creditorName = (isset($creditors->name) && $creditors->name) ? ' '.$creditors->name : '';
        $validation->validateAddress($creditors, "creditors.$key", "Creditors".$creditorName);
        $validation->validateRequired($creditors, $requiredList, "creditors.$key", "Creditors".$creditorName);
        $validation->validateWho($creditors, "creditors.$key", "Creditors".$creditorName);
        $validation->validateRange($creditors, "original_amount", "creditors.$key", "Creditors".$creditorName);
        $validation->validateRange($creditors, "ending_amount", "creditors.$key", "Creditors".$creditorName);
      }
    }
    elseif (empty($statement->creditors) && (!empty($statement->has_creditors))) {
      $validation->addError('creditors.required', 'You must add a creditor after saying you have one.');
    }

    $validation->validateRequired($statement, ['has_lobbies'], 'lobbies', 'Lobbying');
    if (!empty($statement->lobbies)) {
      $statement->has_lobbies = TRUE;
      $requiredList = ['purpose', 'who', 'compensation', 'for'];
      foreach($statement->lobbies as $key => $lobby) {
        $lobbyName = (isset($lobby->for) && $lobby->for) ? ' '.$lobby->for : '';
        $validation->validateRequired($lobby, $requiredList, "lobbies.$key", "Lobbying".$lobbyName);
        $validation->validateWho($lobby, "lobbies.$key", "Lobbying".$lobbyName);
        $validation->validateRange($lobby, "compensation", "lobbies.$key", "Lobbying".$lobbyName);
      }
    }
    elseif (empty($statement->lobbies) && (!empty($statement->has_lobbies))) {
      $validation->addError('lobbies.required', 'You must add a lobby after saying that you have one');
    }

    if (!empty($submissionData->submission->active_official)) {
      $validation->validateRequired($statement, ['has_gifts'], 'gifts', 'Gifts');
    }
    if (!empty($statement->gifts)) {
      $statement->has_gifts = TRUE;
      $requiredList = ['who', 'date_received', 'donor_name', 'city', 'state', 'description', 'value'];
      foreach ($statement->gifts as $key => $gift) {
        $validation->validateWho($gift, "gifts.$key", "Gifts");
        $validation->validateRequired($gift, $requiredList, "gifts.$key", "Gifts");
      }
    }
    elseif (empty($statement->gifts) && (!empty($statement->has_gifts))) {
      $validation->addError('gift.required', 'You must add a gift after saying that you have one');
    }
  }

  /**
   * @param $statement
   * @param $needle (value we are looking for in object)
   * @param null $keyRestriction (used if we only want to find value on a certain key)
   * @return bool
   */
  protected function findInStatement($statement, $needle, $keyRestriction=null){
    $found = FALSE;

    foreach ($statement as $key => $value) {
      if (is_scalar($value) || $value === null) {
        if ((!$keyRestriction || $key == $keyRestriction)) {
          if (is_string($value) ? strtolower($value) === $needle : $value === $needle) {
            $found = TRUE;
            break;
          }
        }
      } else {
        if ($key === $keyRestriction) {
          // If the correct key has been found, we can still descend the tree
          // and it's no longer necessary to look for the key because we expect
          // to find keyMatch->subKey1->subKey2 type matches.
          $keyRestriction = null;
        }
        if ($this->findInStatement($value, $needle, $keyRestriction)) {
          $found = TRUE;
          break;
        }
      }
    }

    return $found;
  }

  protected function formatCategoryName($category) {
    return ucfirst(str_replace('_', ' ', $category));
  }

  protected static function formatDate(DateTime $date) {
    if ($date) {
      return $date->format('Y-m-d');
    }
    else {
      return null;
    }
  }

  /**
   * @param DateTime $date_time
   *   The date/time to format
   * @return string
   *   Formatted date/time object
   */
  protected static function formatDateTime(DateTime $date_time) {
    if ($date_time) {
      return $date_time->format('Y-m-d H:i:s');
    }
    else {
      return null;
    }
  }

  /**
   * Return the data object represented by a loaded submission.
   * @param FASubmission $submission
   * @return stdClass
   * @throws Exception
   */
  protected function submissionData(FASubmission $submission) {
    $data = $submission->user_data;
    $validation = new Validator();
    if ($submission->period_start){
      $this->validateSubmission($data, $validation);
      $this->validateIdentification($data, $validation);
    }
    $data->submission = $sub =  new stdClass();
    $this->setStatementActiveOfficial($data);
    $this->setStatementDependentsEntered($data);

    $sub->updated_at = StatementProcessor::formatDateTime($submission->updated_at);
    $sub->submission_id = $submission->submission_id;
    $sub->verified = $submission->verified;
    $sub->save_count = $submission->save_count;
    $sub->person_id = $submission->person_id;
    $sub->name = $submission->name;
    $sub->fa_statement_id = $submission->fa_statement_id;
    $sub->validation = $validation->getErrors();
    if ($submission->amend_id) {
      $sub->amend_id = $submission->amend_id;
    }
    if ($submission->submitted_at) {
      $sub->submitted_at = StatementProcessor::formatDateTime($submission->submitted_at);
      $sub->certification_name = $submission->certification_name;
      $sub->certification_email = $submission->certification_email;
      $sub->certification_phone = $submission->certification_phone;
      $sub->certification = $submission->certification;
    }
    return $data;
  }

  public function loadSubmissionDraft($submission_id) {
    /** @var FASubmission $draftSubmission */
    $draftSubmission = $this->_dataManager->em->find(FASubmission::class, $submission_id);
    $data = $this->submissionData($draftSubmission);
    if (!empty($data->person->person_id) && $data->person->person_id != -1) {
      $data->statements = $this->query('person/statements.sql', ['person_id' => $data->person->person_id]);
    }
    else {
      $data->statements = [];
    }
    if ($draftSubmission->amend_id !== null) {
      $data->statement->import_selected_id = $draftSubmission->amend_id;
    }
    return $data;
  }

  /**
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   */
  public function getDrafts(User $user, $person_id = null) {
    $drafts = [];
    if (!$person_id || $person_id == -1) {
      $ids = $this->_dataManager->db->executeQuery("SELECT submission_id FROM fa_submission WHERE realm = :realm and uid=:uid and 
                                              (person_id = -1 or person_id is null) and submitted is not true",
        ['realm' => $user->realm, 'uid' => $user->uid] )->fetchAllAssociative();
    }
    else {
      $ids = $this->_dataManager->db->executeQuery("SELECT submission_id FROM fa_submission WHERE realm = :realm and uid=:uid and 
                                               person_id = :person_id and submitted is not true",
        ['realm' => $user->realm, 'uid' => $user->uid, 'person_id' => $person_id] )->fetchAllAssociative();
    }
    foreach($ids as $id){
      array_push($drafts, $this->viewSubmission($id['submission_id']));
    }
    return $drafts;
  }

  /**
   * Retrieve the most recent draft for a user.
   *
   * @param User $user
   *   The user who has the draft.
   * @param $person_id
   * @param $import_id
   * @return stdClass
   *   The draft submission.
   * @throws Exception
   */
  public function draftSubmission(User $user, $person_id = null, $import_id = null) {
    $draftSubmission = NULL;
    if (!$person_id) {
      $submission_id = $this->_dataManager->db->fetchOne("SELECT MAX(submission_id) FROM fa_submission WHERE realm = :realm and uid=:uid and submitted is not true and amend_id IS NULL",
        ['realm' => $user->realm, 'uid' => $user->uid]
      );
    } else {
      $submission_id = $this->_dataManager->db->fetchOne("SELECT MAX(submission_id) FROM fa_submission WHERE realm = :realm and uid=:uid and person_id = :person_id and submitted is not true and amend_id IS NULL",
        ['realm' => $user->realm, 'uid' => $user->uid, 'person_id' => $person_id]
      );
    }
    if ($submission_id) {
      $draftSubmission = $this->_dataManager->em->find(FASubmission::class, $submission_id);
    }

    if (!$draftSubmission) {
      $draftSubmission = new FASubmission();
      $draftSubmission->realm = $user->realm;
      $draftSubmission->uid = $user->uid;
      $draftSubmission->updated_at = new DateTime();
      $draftSubmission->person_id = $person_id;
    }

    $data = $this->submissionData($draftSubmission);
    if (!empty($data->person->person_id) && $data->person->person_id != -1) {
      $data->statements = $this->query('person/statements.sql', ['person_id' => $data->person->person_id]);
    }
    else {
      $data->statements = [];
    }
    if ($import_id !== null) {
      $data->statement = new stdClass();
      $data->statement->import_selected_id = $import_id;
    }

    return $data;
  }

  /**
   * @param $submission_id
   * @param $username
   * @param $admin_data
   * @return stdClass
   * @throws Exception
   */
  public function verifySubmission($submission_id, $username, stdClass $admin_data, MailerInterface $mailer = null) {

    /** @var FASubmission $faSubmission */
    $faSubmission = $this->_dataManager->em->find(FASubmission::class, $submission_id);
    $certification_email = $faSubmission->certification_email;
    if (empty($admin_data->person_id)) {
      $person_id = $faSubmission->person_id;
    } else {
      $person_id = $admin_data->person_id;
    }
    $submission = $faSubmission->user_data;

    $candidacyProcessor = new CandidacyProcessor($this->_dataManager);
    $officialProcessor = new OfficialProcessor($this->_dataManager);

    $period_start = new Datetime($submission->statement->period_start);
    $period_end = new Datetime($submission->statement->period_end);

    if (!$admin_data->file && !empty($admin_data->period_start) && !empty($admin_data->period_end)) {
      try {
        new DateTime($admin_data->period_start);
        new DateTime($admin_data->period_end);
      } catch (Exception $e){
        throw new Exception('Invalid period datetime format');
      }
      if (!empty($admin_data->period_start) && $admin_data->period_start > $admin_data->period_end) {
        throw new Exception('Period end date cannot come before period start date.');
      }
    }

    //Person Matching
    $ep = new EntityProcessor($this->_dataManager);
    if (empty($person_id) || $person_id < 0) {
      $data = (object) ['name' => $submission->person->name, 'is_person' => true];
      $entity_id = $ep->saveEntity($data);
      $person = $this->_dataManager->em->find(Person::class, $entity_id);
    } else {
      $person = $this->_dataManager->em->find(Person::class, $person_id);
    }
    $person->name = $submission->person->name;
    $person->email = $certification_email;
    $filerProcessor = new FilerProcessor($this->_dataManager);
    $person->filer_id = $filerProcessor->ensureFilerExists($person);
    $this->_dataManager->em->flush();

    // Grant access to user.
    $pdc_user_name = $this->_dataManager->db->executeQuery('select user_name from pdc_user where uid = :uid and realm = :realm',["uid" => $faSubmission->uid, "realm" => $faSubmission->realm])->fetchOne();
    $auth = $this->_dataManager->em->getRepository(EntityAuthorization::class)->findOneBy(
      ['entity_id' => $person->person_id, 'uid' => $faSubmission->uid, 'realm' => $faSubmission->realm]);
    if ((!$auth || $auth->role !== 'owner') && empty($faSubmission->user_data->statement->paper_file)) {
      $ep->grantRoleToEmail($person->person_id, $pdc_user_name, 'owner', $mailer, true);
    }
    $faSubmission->person_id = $person->person_id;
    $faSubmission->updated_at = new DateTime();

    //FA Statement
    $file_statement = $admin_data->file ?? true;

    if ($file_statement) {
      $faStatement = $this->_dataManager->em
        ->getRepository(FAStatement::class)
        ->findOneBy(['person_id' => $person_id, 'period_start' => $period_start, 'period_end' => $period_end]);
      if (!$faStatement) {
        $faStatement = new FAStatement();
        $faStatement->name = $submission->person->name;
        $faStatement->first_filed_at = $faSubmission->submitted_at;
        $faStatement->person_id = $person->person_id;
        $faStatement->filer_id = $person->filer_id;
        $faStatement->submission_id = $faSubmission->submission_id;
        $faStatement->period_start = $period_start;
        $faStatement->period_end = $period_end;
        $this->_dataManager->em->persist($faStatement);
        $this->_dataManager->em->flush();
      } else {
        $faStatement->first_filed_at = $faStatement->first_filed_at > $faSubmission->submitted_at
          ? $faSubmission->submitted_at
          : $faStatement->first_filed_at;
        if ($faStatement->submission_id < $faSubmission->submission_id) {
          $faStatement->submission_id = $faSubmission->submission_id;
          $faStatement->name = $submission->person->name;
        }
      }
      $faStatement->updated_at = new DateTime();
      $faSubmission->fa_statement_id = $faStatement->statement_id;
      $faSubmission->verified = true;
    }
    else {
      if (!empty($admin_data->period_start) && !empty($admin_data->period_end)) {
        $faSubmission->period_start = new DateTime($admin_data->period_start);
        $faSubmission->period_end = new DateTime($admin_data->period_end);
        $faSubmission->user_data->statement->period_start =  $admin_data->period_start;
        $faSubmission->user_data->statement->period_end =  $admin_data->period_end;
      }
      $faSubmission->verified = false;
      $faSubmission->submitted_at = null;
      $faSubmission->submitted = false;
      $faSubmission->certification = null;
      $faSubmission->certification_email = null;
      $faSubmission->certification_name = null;
      $faSubmission->published = false;
    }

    //Candidacies
    if ($admin_data->candidacies ?? TRUE) {
      foreach($submission->candidacies as $candidacy) {
        $candidacyInfo = new stdClass();
        $candidacyInfo->declared_email = $candidacy->email;
        $candidacyInfo->person_id = $person->person_id;
        $savedKeys = ['jurisdiction_id', 'office_code', 'position_id', 'election_code', 'address', 'city',
          'state', 'postcode', 'campaign_start_date'];
        $position_id = !empty($candidacy->candidacy_id) ?
          $this->_dataManager->db->executeQuery("
            SELECT position_id FROM candidacy 
            WHERE candidacy_id = :candidacy_id",
            ['candidacy_id' => $candidacy->candidacy_id])
          ->fetchOne() : null;

        foreach ($savedKeys as $key) {
          $candidacyInfo->$key = $candidacy->$key ?? null;
        }
        if (!empty($candidacy->candidacy_id) && !empty($position_id)) {
          $candidacyInfo->position_id = $position_id;
        }
        $modifiedEndDate = (clone $period_end)->modify('+1 year');
        $campaignStartDate = new DateTime($candidacy->campaign_start_date);
        // If the candidacy is within the reporting period set the fa_statement_id
        if ($campaignStartDate > $period_start && $campaignStartDate <= $modifiedEndDate && $faSubmission->verified) {
          $candidacyInfo->fa_statement_id = $faSubmission->fa_statement_id;
        }
        $candidacyProcessor->findAndSaveCandidacy($candidacyInfo);
      }
    }

    //Offices
    if ($admin_data->offices ?? TRUE) {
      foreach($submission->offices as $office) {
        // break out of the loop on empty office entries
        $arr = (array)$office;
        if (!$arr) {
          continue;
        }
        $o = new stdClass();
        $o->person_id = $person->person_id;
        $savedKeys = ['official_id', 'end_date', 'address', 'city', 'state', 'postcode', 'email'];
        if (!isset($office->official_id)) {
          $savedKeys = array_merge($savedKeys, ['official_id', 'jurisdiction_id', 'start_date', 'office_code', 'position_id']);
        }
        foreach ($savedKeys as $key) {
          $o->$key = $office->$key ?? null;
        }
        $officialProcessor->saveOfficial($o);
      }
    }

    // Set publication status.
    if (!$faSubmission->verified ) {
      $faSubmission->published = false;
    }
    else {
      $faSubmission->published = true;
    }

    $this->_dataManager->em->flush();

    $response = $this->submissionData($faSubmission);

    // Log final action
    $message = $admin_data->message ?? "";
    if ($faSubmission->verified) {
      $message = "Verified - $message";
    }
    else {
      $message = "Reverted - $message";
    }
    $this->logFAStatementAdminAction($faSubmission->fa_statement_id, $submission_id, $person->person_id,
      $username, 'verify', $message);
    return $response->submission;
  }

  protected function cloneObject(stdClass $object) {
    return json_decode(json_encode($object));
  }

  /**
   * Create an amendment for a submission.
   * @param $submission_id
   * @param User $user
   * @return stdClass|FASubmission
   * @throws Exception
   */
  public function amendSubmission($submission_id, User $user) {

    /** @var FASubmission $submission */
    $submission = $this->_dataManager->em->find(FASubmission::class, $submission_id);
    if (!$submission) {
      throw new Exception('Unknown submission');
    }

    $new_submission = $this->_dataManager->em->getRepository(FASubmission::class)
      ->findOneBy(['amend_id' => $submission_id, 'submitted' => FALSE]);
    if (!$new_submission) {
      $new_submission = new FASubmission();
    }
    $new_submission->updated_at = new DateTime();
    $new_submission->uid = $user->uid;
    $new_submission->realm = $user->realm;
    $new_submission->fa_statement_id = $submission->fa_statement_id;
    $new_submission->amend_id = $submission->submission_id;
    $new_submission->person_id = $submission->person_id;

    $new_submission->user_data = $this->cloneObject($submission->user_data);
    $new_submission = $this->submissionData($new_submission);

    // Retrieve the statement start and end dates in case they have changed.
    if ($submission->fa_statement_id) {
      $statement = $this->_dataManager->db->fetchAssociative("select * from fa_statement where statement_id = :fa_statement_id",
        ['fa_statement_id' => $submission->fa_statement_id]);
      if ($statement) {
        $new_submission->statement->period_start = $statement['period_start'];
        $new_submission->statement->period_end = $statement['period_end'];
      }
    }



    $new_submission->statement->amends = $submission->submission_id;
    return $new_submission;
  }


  /**
   * @param $new_submission
   * @param $selected_id
   * @param $user
   * @param bool $admin_mode
   *
   * @return object|null
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   */
  public function carryForward($new_submission, $selected_id, $user) {
    $old_submission = $this->_dataManager->em->find(FASubmission::class, $selected_id);

    $new_submission_data = $new_submission->statement;
    $old_submission_data = $old_submission->user_data->statement;
    $new_submission_data->judge_or_law_enforcement = $old_submission_data->judge_or_law_enforcement;
    $new_submission_data->has_lobbies = $old_submission_data->has_lobbies;
    $new_submission_data->has_creditors = $old_submission_data->has_creditors;
    $new_submission_data->has_gifts = false;
    $new_submission_data->has_spouse = $old_submission_data->has_spouse;
    $new_submission_data->real_estate_owned = $old_submission_data->real_estate_owned;
    if (property_exists($old_submission_data, 'spouse')) {
      $new_submission_data->spouse = $old_submission_data->spouse;
    }
    if (property_exists($old_submission_data,'income')) {
      $new_submission_data->income = $old_submission_data->income;
    }
    if (property_exists($old_submission_data,'accounts')) {
      $new_submission_data->accounts = $old_submission_data->accounts;
    }
    if (property_exists($old_submission_data,'business_associations')) {
      $new_submission_data->business_associations = $old_submission_data->business_associations;
    }
    if (property_exists($old_submission_data, 'real_estate')) {
      $new_submission_data->real_estate = $old_submission_data->real_estate;
    }
    if (property_exists($old_submission_data, 'lobbies')) {
      $new_submission_data->lobbies = $old_submission_data->lobbies;
    }
    if (property_exists($old_submission_data, 'gifts')) {
      $new_submission_data->gifts = [];
    }
    if (property_exists($old_submission_data,'creditors')) {
      $new_submission_data->creditors = $old_submission_data->creditors;
    }
    if (property_exists($old_submission_data, 'reporting_modifications')) {
      $new_submission_data->reporting_modifications = $old_submission_data->reporting_modifications;
    }
    $import_from = new stdClass();
    $import_from->period_start = $old_submission_data->period_start;
    $import_from->period_end = $old_submission_data->period_end;
    $import_from->submission_id = $old_submission->submission_id;
    $import_from->submitted_at = $old_submission->submitted_at ? $old_submission->submitted_at->format('Y-m-d') : NULL;

    $new_submission->statement->imported_from = $import_from;
    $this->saveSubmission($new_submission, $user, $new_submission->submission->submission_id);

    return  $this->submissionData($this->submission);
  }

  /**
   * @param $submission_id
   * @return stdClass
   * @throws Exception
   */
  public function viewSubmission($submission_id) {
    /** @var FASubmission $submission */
    $submission = $this->_dataManager->em->find(FASubmission::class, $submission_id);
    $data = $this->submissionData($submission);
    return $data;
  }

  public function lastSubmission($user, $person_id) {
    if ($person_id === -1 || empty($person_id)) {
      $person_id = NULL;
    }
    $submission_data = $this->query('person/last_submission.sql', ['uid' => $user->uid, 'realm' => $user->realm, 'person_id' => $person_id]);
    return $submission_data[0] ?? null;
  }

  /**
   * Log actions performed on a statement.
   *
   * @param ?int $statement_id
   *   ID of statement to log action against
   * @param ?int $submission_id
   *   ID of submission to log action against
   * @param int $person_id
   *   ID of person to log action against
   * @param string $user_name
   *   Name of user performing the action
   * @param string $action
   *   Action the user is performing
   * @param string $message
   *   Additional information regarding the action.
   *
   * @throws Exception
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function logFAStatementAdminAction($statement_id, $submission_id, $person_id, $user_name, $action, $message) {
    $log = new FAStatementLog();
    $log->fa_statement_id = $statement_id;
    $log->submission_id = $submission_id;
    $log->person_id = $person_id;
    $log->user_name = $user_name;
    $log->action = $action;
    $log->message = $message;
    $log->updated_at = new DateTime();
    $this->_dataManager->em->persist($log);
    $this->_dataManager->em->flush();
  }

  /**
   * @param $submission
   * @param $user
   *
   * @return bool
   * @throws Exception
   */
  public function autoVerify($submission, $user) {
    if (!empty($submission->person_id)) {
      $entity_authorization = $this->_dataManager->em->getRepository(EntityAuthorization::class)->findOneBy(['entity_id' =>  $submission->person_id, 'uid' => $user->uid, 'realm' => $user->realm, 'role' => 'owner']);
      if($entity_authorization) {
        $admin_data = (object)['file' => TRUE, 'candidacies' => TRUE, 'offices' => TRUE];
        $this->verifySubmission($submission->submission_id, 'system', $admin_data);
        return true;
      }
    }
    return false;
  }

  /**
   * @param $submission
   * @param $user
   *
   * @throws Exception
   */
  public function saveTestUserFromSubmission($test_data)
  {
    $submission = new FASubmission();
    $statement = new FAStatement();

    $candidacy_ids = [];
    $official_ids = [];
    $dates = ['declared_date', 'campaign_start_date', 'first_filed_at', 'period_start', 'period_end', 'start_date', 'submitted_at', 'updated_at'];
    /** @var Person $person */
    $person = $this->_dataManager->em->getRepository(Person::class)->findOneBy(['filer_id' => $test_data->person->filer_id]);
    if (!$person) {
      $person = new Person();
      $this->_dataManager->em->persist($person);
    }
    // build person
    foreach($test_data->person as $key => $value) {
      $person->$key = $value;
    }
    if (!empty($test_data->candidacies)) {
      // build candidacy
      foreach($test_data->candidacies as $candidacy_data) {
        $candidacy = $this->_dataManager->em->getRepository(Candidacy::class)
          ->findOneBy(['person_id' => $person->person_id, 'election_code' => $candidacy_data->election_code, 'office_code' => $candidacy_data->office_code]);
        if (!$candidacy) {
          $candidacy = new Candidacy();
          $this->_dataManager->em->persist($candidacy);
        }
        $candidacy->person_id = $person->person_id;
        $candidacy->updated_at = new DateTime();
        foreach ($candidacy_data as $key => $value) {
          if (!in_array($key, $dates)) {
            $candidacy->$key = $value;
            continue;
          }
          $candidacy->$key = new DateTime($value);
        }

        $candidacy_ids[] = $candidacy->candidacy_id;
      }
    }

    if (!empty($test_data->offices)) {
      // build officials
      foreach($test_data->offices as $office) {
        $official = $this->_dataManager->em->getRepository(Official::class)
          ->findOneBy(['person_id' => $person->person_id, 'office_code' => $office->office_code, 'jurisdiction_id' => $office->jurisdiction_id] );
        if (!$official) {
          $official = new Official();
          $this->_dataManager->em->persist($official);
        }

        $official->person_id = $person->person_id;
        foreach($office as $key => $value) {
          if (!in_array($key, $dates)) {
            $official->$key = $value;
            continue;
          }
          $official->$key = new DateTime($value);
        }

        $official_ids[] = $official->official_id;
      }

    }

    $this->_dataManager->em->flush();

    $existing_statements = $this->_dataManager->db->executeQuery("SELECT * FROM public.fa_statement where person_id=:person_id", ['person_id' => $person->person_id]);
    if(!empty($test_data->statement) && !$existing_statements) {
      // create statement
      $statement->person_id = $person->person_id;
      $statement->name = $person->name;
      $statement->period_start = new DateTime($test_data->statement->period_start);
      $statement->period_end = new DateTime($test_data->statement->period_end);
      $statement->first_filed_at = new DateTime($test_data->statement->first_filed_at);
      $this->_dataManager->em->persist($statement);
      $this->_dataManager->em->flush();

      // create submission
      $submission->realm = $test_data->submission->realm;
      $submission->uid = $test_data->submission->uid;
      $user_data = new stdClass();
      $user_data->statement = $test_data->statement;
      $submission->user_data = $user_data;
      $submission->person_id = $person->person_id;
      $submission->fa_statement_id = $statement->statement_id;
      foreach($test_data->submission as $key => $value) {
        if (!in_array($key, $dates)) {
          $submission->$key = $value;
        } else {
          $submission->$key = new DateTime($value);
        }
      }
      $this->_dataManager->em->persist($submission);

      $statement->submission_id = $submission->submission_id;
      if (isset($candidacy)) {
        $candidacy->fa_statement_id = !empty($statement->statement_id) ? $statement->statement_id : NULL;
      }
    }
    $this->_dataManager->em->flush();

    if ($test_data->person_authorization) foreach($test_data->person_authorization as $authorization) {

      $q = "INSERT INTO entity_authorization(entity_id, realm, role, uid, approved) 
             SELECT :person_id, realm, 'owner', uid, true FROM wapdc.public.pdc_user
               WHERE user_name = :user_name AND realm=:realm
             ON CONFLICT DO NOTHING ";
      $this->_dataManager->db->executeQuery($q, ['person_id' => $person->person_id, 'realm'=> 'apollo', 'user_name'=>$authorization->user_name]);
    }
  }

  /**
   * @param int[] $submission_ids
   * @param bool $published
   * @param $username
   * @param string $memo
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   */
  public function setSubmissionsPublished($submission_ids, $published, $username, $memo="") {
    foreach ($submission_ids as $id) {
      /** @var FASubmission $submission */
      $submission = $this->_dataManager->em->find(FASubmission::class, $id);
      if (!$submission) {
        throw new Exception('Unknown submission');
      }
      $submission->published = $published;
      $action = $published ? 'published' : 'unpublished';
      $message = strlen($memo) > 0 ? $memo : 'submission ' . $action;
      $this->logFAStatementAdminAction($submission->fa_statement_id, $id, $submission->person_id, $username, $action, $message);
      $this->_dataManager->em->flush();
    }
  }

  /**
   * @param \WAPDC\Core\SendGrid\MailerInterface $m
   *
   * @return $this
   */
  public function setMailer(MailerInterface $m) {
    $this->mailer = $m;
    return $this;
  }


  /**
   * @param $email
   * @param $recipient
   *
   * @return bool
   * @throws Exception
   */
  public function requestAccess($email, $recipient = '') {
    if ($this->mailer) {
      $filer_ids = $this->getFilerIdsFromEmail($email);
      if(empty($filer_ids)){
        return false;
      } else {
        $messenger = new WAPDCMessenger($this->_dataManager);

        $contact = $messenger->getContact($email);
        if (!$contact){
          $messenger->addContact('email', $email);
        }
        if ($recipient){
          $this->mailer->setTestModeEmail($recipient);
        }
        $messenger->sendValidationMessage($email, $this->mailer, self::TEMPLATE_ID);
        return true;
      }
    } else {
      throw new Exception ('Mailer is not set');
    }
  }

  /**
   * @param $email
   * @return array
   * @throws Exception
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function getFilerIdsFromEmail($email) {
    if (!$email) {
      throw new Exception("No email given");
    }
    $email = strtoupper($email);
    //get filer_ids from rds
    $q2 = "select distinct(filer_id) from fa_statement where upper(email) = :email union
        select distinct(filer_id) from person where upper(email) = :email";
    $results = $this->_dataManager->db->executeQuery($q2, ['email' => $email])->fetchAllAssociative();
    $ids = array_unique(array_column($results, 'filer_id'));
    return $ids;
  }

  /**
   * If token is verified, get the persons filerid to gen an authorized person
   * @param \WAPDC\Core\Model\User $pdc_user
   * @param $token
   *
   * @return array | bool
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws Exception
   */
  public function validateRequestToken(User $pdc_user, $token) {
    $persons = [];
    $messenger = new WAPDCMessenger($this->_dataManager);
    $verified_email = $messenger->markVerified($token);
    if ($verified_email) {
      $filerIds = $this->getFilerIdsFromEmail($verified_email);
      if ($filerIds) {
        $this->generatePersonAuthForFilers($pdc_user, $filerIds);
        $persons = $this->getPersonsForFilers($filerIds);
        $faSubmissions = $this->_dataManager->em->getRepository(FASubmission::class)
          ->findBy(['uid' => $pdc_user->uid, 'realm' => $pdc_user->realm, 'person_id' => -1]);
        if ($persons && $faSubmissions) {
          foreach ($faSubmissions as $sub) {
            $subPerson = $sub->user_data->person->person_id ?? -1;
            if (in_array($subPerson, array_map( function($p){return $p->person_id;}, $persons))){
              $sub->person_id = $subPerson;
            }
          }
          $this->_dataManager->em->flush();
        }
      }
    }
    return $verified_email ? $persons : FALSE;
  }

  /**
   * @param $user
   * @return array|object[]
   * @throws Exception
   */
  public function autoGrantPersonAccess(User $user) {
    $filerIds = $this->getFilerIdsFromEmail($user->user_name);
    if ($filerIds) {
      $this->generatePersonAuthForFilers($user, $filerIds);
    }
    return $this->_dataManager->em->getRepository(EntityAuthorization::class)->findBy(['uid' => $user->uid, 'realm' => $user->realm]);
  }

  /**
   * @param $user
   * @param $filer_ids
   * @throws Exception
   */
  public function generatePersonAuthForFilers($user, $filer_ids) {
    if (!isset($user->uid) || !isset($user->realm)) {
      throw new Exception("Invalid user data");
    }
    $query = "insert into entity_authorization (entity_id, realm, role, uid)
        select e.entity_id, :realm as realm, :role as role, :uid as uid from
            (select entity_id from entity where filer_id in (:filer_ids) and is_person = true) e
        on conflict do nothing";
    $this->_dataManager->db->executeQuery($query,
      ['filer_ids' => $filer_ids, 'realm' => $user->realm, 'role' => 'owner', 'uid' => $user->uid],
      ['filer_ids' => Connection::PARAM_STR_ARRAY]);
  }

  /**
   * @param $filer_ids
   *  Returns a list of persons matching filer_ids
   * @return array|object[]
   */
  public function getPersonsForFilers($filer_ids) {
    $persons = $this->_dataManager->em->getRepository(Person::class)
      ->findBy(['filer_id' => $filer_ids]);
    $idsAndNames = [];
    foreach ($persons as $person) {
      $x = new stdClass();
      $x->person_id = $person->person_id;
      $x->name = $person->name;
      $idsAndNames[] = $x;
    }
    return $idsAndNames;
  }
}
