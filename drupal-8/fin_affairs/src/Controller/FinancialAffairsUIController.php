<?php

namespace Drupal\fin_affairs\Controller;


use Drupal\wapdc_core\Controller\SPAControllerBase;
use WAPDC\FinancialAffairs\FinancialAffairs;

class FinancialAffairsUIController extends SPAControllerBase
{
  /**
   * @return string
   * @throws \Exception
   */
  public function getApplicationDirectory()
  {
    return FinancialAffairs::service()->project_dir . '/ui/financial-affairs/dist';
  }
}