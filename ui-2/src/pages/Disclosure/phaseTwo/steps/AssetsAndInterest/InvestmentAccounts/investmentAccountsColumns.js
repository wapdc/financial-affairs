import { format } from 'quasar';
import { investmentStep } from './investmentStep';

const { capitalize } = format;

const keys = investmentStep.fields.investmentAccountArray.template;

const heldBy = {
  key: keys.heldBy.as,
  label: 'Held By',
  align: 'left',
  sortable: false,
  width: '25%',
  format: (value) => capitalize(value?.join(', ')),
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

const institutionName = {
  key: keys.institutionName.as,
  label: 'Bank Name',
  align: 'left',
  sortable: false,
  width: '25%',
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

const institutionState = {
  key: 'state',
  label: 'State',
  align: 'left',
  sortable: false,
  width: '10%',
};

export default [heldBy, institutionName, institutionState];
