#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc <<EOF
ALTER TABLE fa_statement ADD COLUMN IF NOT EXISTS retain_until DATE;
ALTER TABLE fa_statement ADD COLUMN IF NOT EXISTS hold_for VARCHAR;
EOF