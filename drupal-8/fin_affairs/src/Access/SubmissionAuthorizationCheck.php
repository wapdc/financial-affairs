<?php


namespace Drupal\fin_affairs\Access;


use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use WAPDC\Core\CoreDataService;
use WAPDC\Core\UserManager;
use WAPDC\Core\Util\Environment;
use WAPDC\FinancialAffairs\FinancialAffairs;

class SubmissionAuthorizationCheck implements AccessInterface {

  protected $realm = 'apollo';

  /**
   * @param AccountInterface $account
   * @param int $submission_id
   *   ID of submission to check.
   * @return AccessResult
   * @throws \Exception
   */
  public function access(AccountInterface $account, $submission_id) {
    if ($account->hasPermission('access wapdc data')) {
      return AccessResult::allowed();
    }
    $has_access = false;
    // Load environment cause it might not have happened yet.
    $dir = \Drupal::service('file_system')->realpath("private://");
    $env = !empty($_ENV['PANTHEON_ENVIRONMENT']) ? $_ENV['PANTHEON_ENVIRONMENT'] : 'ddev';

    $file = "$dir/environment.$env.yml";
    Environment::loadFromYml($file);

    if (isset($_SESSION['SAW_USER_GUID'])) {
      $this->realm = 'saml_saw';
      $uid = $_SESSION['SAW_USER_GUID'];
    }
    else {
      $uid = $account->id();
    }
    $userManager = new UserManager(CoreDataService::service()
      ->getDataManager(), $this->realm);
    /** @var \WAPDC\Core\Model\User $user */

    $user = $userManager->getRegisteredUser($uid);

    $has_access = FinancialAffairs::service()->submissionAuthorizationCheck($submission_id, $user);
    return AccessResult::allowedIf($has_access);
  }
}