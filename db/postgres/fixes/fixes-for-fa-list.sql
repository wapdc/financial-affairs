SELECT
       count(distinct statement_id) statements,
       count(1) records,
       count(distinct fa.filer_id) filers,
       count(fa.email) as email,
       count(f1.email) as f1_email,
       count(coalesce(f1.email, fa.email, f1.work_email)),
       count(fa.name)
FROM fa_statement fa LEFT JOIN f1 on f1.filer_id=fa.filer_id

  and (fa.first_filed_at = to_date(f1.rpt_date, 'YYYY-MM-DD')
    OR f1.for_year = cast(case when to_char(fa.period_end, 'MM-DD') = '12-31' then (extract(year from period_end)) end  as varchar))
WHERE period_end >= '2018-12-31';

SELECT
  COALESCE(fa.email, f1.email, f1.work_email) as email,
  max(fa.name) as name,
  max(CASE WHEN f1.memo_text like '%ELECTRONIC%' then 'E' else 'P' END) f_type,
  MIN((LEAST(COALESCE(j1.rpts,'N'), COALESCE(j2.rpts,'N')))) as rpts
FROM fa_statement fa LEFT JOIN f1 on f1.filer_id=fa.filer_id
    and (fa.first_filed_at = to_date(f1.rpt_date, 'YYYY-MM-DD')
        OR f1.for_year = cast(case when to_char(fa.period_end, 'MM-DD') = '12-31' then (extract(year from period_end)) end  as varchar))
  LEFT JOIN candidacy ca on ca.person_id = fa.person_id and ca.election_code like '2019%'
  LEFT JOIN jurisdiction j1 ON j1.jurisdiction_id =ca.jurisdiction_id
  LEFT JOIN official o ON o.person_id=fa.person_id
  LEFT JOIN jurisdiction j2 ON j2.jurisdiction_id=o.jurisdiction_id
WHERE period_end >= '2018-12-31'
  AND coalesce(fa.email, f1.email, f1.work_email) is not null
group by coalesce(fa.email, f1.email, f1.work_email)
having min(LEAST(coalesce(j1.rpts, 'N'), COALESCE(j2.rpts,'N'))) < 'N'
ORDER BY 3,1


