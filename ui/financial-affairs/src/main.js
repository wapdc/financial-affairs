import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ServiceContainer  from './ServiceContainer'
import globalFilters from './filters'
import vuetify from './plugins/vuetify';
import browserDetect from "vue-browser-detect-plugin";

Vue.config.productionTip = false;
Vue.use(browserDetect);

export const app = new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
});

// Global filters are defined in filters.js
globalFilters.defineGlobalFilters();

app.$container = new ServiceContainer();

app.$mount('#app');
