update fa_statement fs
set first_filed_at = v.correct_first_filed_at
from (select st.statement_id as                 statement_id,
             st.first_filed_at,
             cast(min(su.submitted_at) as date) min_submitted_date,
             case
                 when (su.submitted_at > st.first_filed_at) then st.first_filed_at
                 when su.submitted_at < st.first_filed_at then su.submitted_at
                 end         as                 correct_first_filed_at
      from fa_statement st
               join fa_submission su
                    on su.fa_statement_id = st.statement_id
      group by st.statement_id, su.submitted_at
      having st.first_filed_at > min(su.submitted_at)::date
      order by st.first_filed_at desc) v
where v.statement_id = fs.statement_id;
