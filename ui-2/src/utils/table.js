import { computed } from 'vue';

/**
 * Produce key-value pairs for each row, e.g `{ columnName: someValue }`
 */
export function getFieldSetAsRows(fieldSetArray) {
  return computed(() => {
    return fieldSetArray.value?.forEach((fieldSet) => {
      console.log(fieldSet);
      return fieldSet;
    });
  });
}
