DO $$
DECLARE
  submissionid int;
BEGIN
INSERT INTO fa_submission
(
    realm,
    uid,
    period_start,
    period_end,
    user_data,
    updated_at,
    submitted_at,
    verified,
    certification_email,
    submitted,
    published,
    fa_statement_id
)
VALUES
(
    'apollo',
    4,
    '2033-01-01',
    '2033-12-31',
    '{
	"statement": {
		"draft_state": {
			"state": {},
			"step": "Submission"
		},
		"isOfficeHolder": null,
		"statementRequired": false,
		"spouse": "Jean Constance Frey Lavallee",
		"income": {
			"categories": [
				{
					"category": "employment",
					"value": true
				},
				{
					"category": "retirement",
					"value": true
				},
				{
					"category": "uncommon",
					"value": true
				}
			],
			"data": [
				{
					"category": "employment",
					"value": null,
					"who": "self",
					"name": "Public Disclosure Commission",
					"why": "Executive Director",
					"income": "200000-499999",
					"address": "711 Capitol Way #206",
					"city": "OLYMPIA",
					"state": "WA",
					"postcode": "98504-8380"
				},
				{
					"category": "retirement",
					"value": null,
					"who": "self",
					"name": "US Government",
					"why": "Social Security",
					"income": "30000-59999",
					"address": "888 Way Place NW",
					"city": "Tempe",
					"state": "AZ",
					"postcode": "78977"
				},
				{
					"category": "uncommon",
					"value": null,
					"who": "self",
					"name": "Kylee Vickerson",
					"why": "Rent",
					"income": "0-29999",
					"address": "8745 Fourth Way Fifth Place",
					"city": "Bremerton",
					"state": "wa",
					"postcode": "89556"
				}
			]
		},
		"accounts": {
			"categories": [
				{
					"category": "bank_account",
					"value": true
				},
				{
					"category": "insurance",
					"value": true
				},
				{
					"category": "investment",
					"value": true
				},
				{
					"category": "self_investment",
					"value": false
				},
				{
					"category": "other",
					"value": true
				}
			],
			"data": [
				{
					"category": "bank_account",
					"value": null,
					"assets": [
						{
							"nature": "Savings",
							"value": "200000-499999",
							"income": "0-29999"
						}
					],
					"who": "self",
					"name": "Heritage Bank",
					"address": "201 5th Ave SW",
					"city": "Olympia",
					"state": "WA",
					"postcode": "85474"
				},
				{
					"category": "bank_account",
					"value": null,
					"assets": [
						{
							"nature": "Savings",
							"value": "60000-99999",
							"income": "0-29999"
						}
					],
					"who": "self",
					"name": "WSECU",
					"address": "330 Union Ave SE",
					"city": "Olympia",
					"state": "WA",
					"postcode": "85471"
				},
				{
					"category": "investment",
					"value": null,
					"assets": [
						{
							"nature": "IRA",
							"value": "100000-199999",
							"income": ""
						}
					],
					"who": "self",
					"name": "Calvert",
					"address": "P.O. Box 219544",
					"city": "Kansas City",
					"state": "KS",
					"postcode": "98557"
				},
				{
					"category": "investment",
					"value": null,
					"assets": [
						{
							"nature": "Pension/Stocks",
							"value": "200000-499999",
							"income": ""
						}
					],
					"who": "self",
					"name": "WA State Dept. Retirement Svcs",
					"address": "6835 Capitol Blvd SE",
					"city": "Tumwater",
					"state": "WA",
					"postcode": "98557"
				},
				{
					"category": "investment",
					"value": null,
					"assets": [
						{
							"nature": "Promissory Note",
							"value": "100000-199999",
							"income": ""
						}
					],
					"who": "self",
					"name": "Around the Circle Midwifery",
					"address": "2120 Pacific Ave SE",
					"city": "Olympia",
					"state": "WA",
					"postcode": "98366"
				},
				{
					"category": "insurance",
					"value": null,
					"assets": [
						{
							"nature": "Life",
							"value": "30000-59999",
							"income": "30000-59999"
						}
					],
					"who": "self",
					"name": "Mutual of Oklahoma",
					"address": "3334 Oklah Way",
					"city": "Lando",
					"state": "OK",
					"postcode": "88888"
				}
			]
		},
		"reporting_modifications": {
			"residential_address": false
		},
		"judge_or_law_enforcement": false,
		"real_estate": [
			{
				"location": {
					"address": "2423 42nd Avenue",
					"city": "Tumwater",
					"zip": "85241"
				},
				"who": "self",
				"assessed_value": "0-29999"
			}
		],
		"real_estate_owned": true,
		"creditors": [
			{
				"name": "CitiBank",
				"who": "self",
				"original_amount": "0-29999",
				"payment_terms": "Monthly, low income plan",
				"security_given": "None",
				"ending_amount": "0-29999",
				"address": "888 Way Place NW",
				"city": "Tempe",
				"state": "AZ",
				"postcode": "78977"
			}
		],
		"hasCreditor": true,
		"business_associations": {
			"categories": [
				{
					"category": "owner",
					"value": true
				},
				{
					"category": "officer",
					"value": true
				}
			],
			"data": [
				{
					"category": "owner",
					"value": null,
					"who": "self",
					"legal_name": "Savvy Slug Investments LLC",
					"description": "Residential real estate",
					"percentage_of_ownership": "100",
					"address": "2919 Lindell Rd. NE",
					"city": "Olympia",
					"state": "WA",
					"postcode": "98506",
					"office_payments": [],
					"has_office_payments": false,
					"agency_payments": [],
					"has_agency_payments": false,
					"business_payments": [],
					"has_business_payments": false,
					"wa_property": [
						{
							"address": "2913 Lindell Rd. NE",
							"city": "Olympia",
							"zip": "98506"
						}
					],
					"has_wa_property": true
				},
				{
					"category": "owner",
					"value": null,
					"who": "self",
					"legal_name": "Sapientiae Consulting LLC",
					"description": "Consulting for individuals",
					"percentage_of_ownership": "10",
					"address": "2129 Lindell Rd NE",
					"city": "Olympia",
					"state": "WA",
					"postcode": "98506",
					"office_payments": [
						{
							"purpose": "GOVERNMENT INSPECTION",
							"amount": "23.00"
						}
					],
					"has_office_payments": true,
					"agency_payments": [
						{
							"agency": "PAYMENT CONSULTING",
							"purpose": "Consulting for payments rendered"
						}
					],
					"has_agency_payments": true,
					"business_payments": [
						{
							"business": "BUSINESS CUSTOMER",
							"purpose": "Portfolio Management"
						}
					],
					"has_business_payments": true,
					"wa_property": [
						{
							"state": "WA",
							"parcel": "871928374",
							"county": "KITSAP",
							"legal_description": "Lot 1, KITSAP COUNTY SHORT PLAT NO. 871928374, according to Plat recorded October 21, 1987, in Kitsap County, Washington"
						}
					],
					"has_wa_property": true
				},
				{
					"category": "owner",
					"value": null,
					"who": "spouse",
					"legal_name": "Sapientiae Consulting LLC",
					"description": "Consulting for individuals",
					"percentage_of_ownership": "90",
					"address": "2919 Lindell Rd NE",
					"city": "Olympia",
					"state": "WA",
					"postcode": "98506",
					"office_payments": [],
					"has_office_payments": false,
					"agency_payments": [],
					"has_agency_payments": false,
					"business_payments": [],
					"has_business_payments": false,
					"wa_property": [],
					"has_wa_property": false
				},
				{
					"category": "officer",
					"value": null,
					"who": "self",
					"legal_name": "Fifth Avenue Commerce",
					"operating_name": "FAC",
					"description": "Financial Management",
					"position": "Chief Officer",
					"address": "345-A Voyager Way",
					"city": "Vancouver",
					"state": "WA",
					"postcode": "98544",
					"office_payments": [
						{
							"purpose": "PDC Purpose",
							"amount": "444.44"
						}
					],
					"has_office_payments": true,
					"agency_payments": [
						{
							"agency": "DOL Government Agency Other",
							"purpose": "Legislative Purpose"
						}
					],
					"has_agency_payments": true,
					"business_payments": [
						{
							"business": "Business Customer",
							"purpose": "Purchases"
						}
					],
					"has_business_payments": true
				}
			]
		},
		"period_start": "2018-01-01",
		"period_end": "2018-12-31",
		"isNJurisdiction": false,
		"lobbies": [
			{
				"who": "self",
				"for": "Initiative to Support Carbonated People",
				"purpose": "Legislative change",
				"compensation": "30000-59999"
			}
		],
		"has_lobbies": true,
		"has_creditors": true
	},
	"person": {
		"person_id": 15056,
		"name": "LAVALLEE PETER J",
		"voter_id": null,
		"voter_name": null,
		"address": "711 CAPITOL WAY S STE 206",
		"premise": null,
		"city_area": null,
		"city": "OLYMPIA",
		"state": "WA",
		"postcode": "985040908",
		"sortcode": null,
		"country": null,
		"email": "peter.lavallee@gmail.com",
		"phone": 3609097655,
		"filer_id": "LAVAP  504"
	},
	"offices": [
		{
			"office_code": "58",
			"office": "STATE AGENCY DIRECTOR",
			"jurisdiction_id": 4495,
			"jurisdiction": "PUBLIC DISCLOSURE COMMISSION",
			"start_date": "2019-03-07",
			"end_date": null
		}
	],
	"candidacies": [
		{
			"office_code": "58",
			"office": "STATE AGENCY DIRECTOR",
			"jurisdiction_id": 4495,
			"jurisdiction": "PUBLIC DISCLOSURE COMMISSION",
			"election": "2017 General",
			"campaign_start_date": "2017-06-14"
		},
		{
			"election_code": "2020S2",
			"election": "2020 February Special",
			"office_code": "15",
			"jurisdiction_id": 4444,
			"jurisdiction": "APPEALS, COURT OF DIV II",
			"position_id": 9,
			"position": "Judge Position 1",
			"office": "APPEALS COURT JUDGE",
			"multiPosition": true,
			"campaign_start_date": "2015-12-01",
			"email": "lavelleforjudge@gmail.com",
			"address": "345345 LAVALLEE LANE S",
			"city": "LAVALLEE",
			"state": "WA",
			"postcode": "85796",
			"phone": "6128963254"
		}
	]
}',
    Now(),
    Now(),
    true,
    'peter.lavallee@pdc.wa.gov',
    true,
    true,
    4377
) returning submission_id INTO submissionid;

Update fa_statement set submission_id = @submissionid where statement_id = 4377;

END$$;

