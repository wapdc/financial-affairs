SELECT * FROM fa_submission
WHERE uid = :uid
AND realm = :realm
AND submitted = TRUE
AND coalesce(verified, false) = false
AND ((person_id = CAST(:person_id AS INTEGER)) OR (CAST(:person_id AS INTEGER) IS NULL AND (person_id IS NULL OR person_id = -1)))