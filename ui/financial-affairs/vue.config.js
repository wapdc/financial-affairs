module.exports = {
  lintOnSave: false,
  publicPath: '/financial-affairs',
  transpileDependencies: ['vuetify'],
  css: {
    loaderOptions: {
      scss: {
        data: `@import "@/variables.scss";`
      }
    }
  }
};
