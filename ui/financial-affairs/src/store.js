import Vue from 'vue'
import Vuex from 'vuex'
import {app} from "./main";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    doSaveSubmissions: true,
    statements: [],
    submissions: [],
    statement: {
      draft_state: {},
    },
    candidacies:[],
    offices: [],
    person: {
    },
    submission: {
    },
    attachments: [],
    validation: {
      minLength3: [ v => (!(!v && v!==false) && v.length >= 3) || 'Type at least three characters' ],
      required: [  v => !(!v && v!==false) || 'Required', v => !v || v.toString().length <= 255 || 'Too long'],
      tooLong: [ v => !v || v.length <= 255 || 'Too long' ],
      selectAtLeastOne: [ v => v.length >= 1 || 'Required' ],
      monetary_ranges: [
        { value: '', text: 'None'},
        { value: '0-29999', text: 'less than $30,000'},
        { value: '30000-59999', text: '$30,000 to $59,999'},
        { value: '60000-99999', text: '$60,000 to $99,999'},
        { value: '100000-199999', text: '$100,000 to $199,999'},
        { value: '200000-499999', text: '$200,000 to $499,999'},
        { value: '500000-749999', text: '$500,000 to $749,999'},
        { value: '750000-999999', text: '$750,000 to $999,999'},
        { value: '1000000-', text: '$1,000,000 or more' },
      ],
      relation_code: [
        { value: 'filer', text: 'Filer', sort: '10'},
        { value: 'spouse', text: 'Spouse or registered domestic partner', sort: '20'},
        { value: 'dependent', text: 'Dependent(s)', sort: '30'},
      ],
    },
    wizard: {},
    user: {
      email: null,
      locked: false,
      rights: {
        enter_wapdc_data: false
      }
    },
    thresholds: []
  },
  mutations: {
    DO_SAVE_SUBMISSIONS(state, doSave) {
      state.doSaveSubmissions = doSave;
    },
    SET_STATEMENT(state, statement) {
      if (statement) {
        state.statement = statement;
        if(!statement.draft_state){
          statement.draft_state = {
            step:"",
            state: {},
          }
        }
      }
      else {
        state.statement = {
          draft_state: {
            step: "",
            state: {},
          }
        };
      }
    },
    SET_CANDIDACIES(state, candidacies) {
      if (candidacies) {
        state.candidacies = candidacies
      }
      else {
        state.candidacies = [];
      }
    },
    SET_OFFICES(state, offices) {
      if (offices) {
        state.offices = offices;
      } else {
        state.offices = [];
      }
    },
    SET_STATEMENTS(state, statements) {
      state.statements = statements;
    },
    SET_SUBMISSIONS(state, submissions) {
      state.submissions = submissions;
    },
    SET_SUBMISSION(state, submissionData) {
      state.submission = submissionData;
    },
    SET_PERSON(state, person) {
      if (person) {
        state.person=person;
      }
      else {
        state.person = {}
      }
    },
    SET_USER(state, user) {
      if (user) {
        if(!user.hasOwnProperty('locked')) {
          user.locked = false;
        }
        state.user = user;

      }
    },
    SET_ATTACHMENTS(state, attachments) {
      state.attachments = attachments;
    },
    SET_THRESHOLDS(state, thresholds){
      state.thresholds = Object.fromEntries(thresholds.map(t => [t.property, t.amount]))
    }
  },
  actions: {
    validateIdentification(context) {
      app.$container.dataService.post('service/financial-affairs/submission/validate-identity', result => {
        const submission = {...context.state.submission, validation: result.data.validation}
        context.commit('SET_SUBMISSION', submission)
        }, {
        'statement': context.state.statement,
        'person': context.state.person,
        'offices': context.state.offices,
        'candidacies': context.state.candidacies,
        'submission': context.state.submission
      });
    },
    doSaveSubmissions(context, doSave) {
      context.commit('DO_SAVE_SUBMISSIONS', doSave);
    },
    getUserInfo(context) {
      app.$container.dataService.getPassive('public/user-info', (result) => {
        let user = result.data;
        context.commit('SET_USER', user);
        app.$container.wire.$emit('app.userInfo', result.data);
      });
    },
    getUnverifiedSubmission (context, params) {
      let url = 'service/financial-affairs/submission/' + params.submission_id + '/view-submission';
      app.$container.dataService.get(url, result => {
        if (result.data.success) {
          context.commit("SET_SUBMISSION", result.data.submission);
          context.commit("SET_STATEMENT", result.data.statement);
          context.commit('SET_PERSON', result.data.person);
          context.commit('SET_OFFICES', result.data.offices);
          context.commit("SET_CANDIDACIES", result.data.candidacies);
          context.commit("SET_ATTACHMENTS", result.data.attachments[0]);
        }
      });
    },
    getDraftSubmission(context, params) {
      let url = null;
      let import_id = null;
      let submission_id = null;
      if (params && params.import_id) {
        import_id = params.import_id
      }
      if (params && params.submission_id) {
        submission_id = params.submission_id
        url = 'service/financial-affairs/submission/' + submission_id;
      }
      if (!submission_id) {
        if (context.state.user.selected_person && context.state.user.selected_person.person_id) {
          url = 'service/financial-affairs/person/' + context.state.user.selected_person.person_id + '/submission';
        } else {
          url = 'service/financial-affairs/submission';
        }
      }
        app.$container.dataService.get(url, result => {
          if (result.data.success) {
            context.commit('SET_STATEMENT', result.data.statement);
            if (result.data.person) {
              context.commit("SET_PERSON", result.data.person);
            }
            context.commit("SET_SUBMISSION", result.data.submission);
            context.commit('SET_OFFICES', result.data.offices);
            context.commit("SET_CANDIDACIES", result.data.candidacies);
            context.commit("SET_STATEMENTS", result.data.statements);
            if (params.start) {
              app.$container.wire.$emit('App.startWizard');
            }
          }
        }, {"import_id": import_id})
    },

    getSubmission(context, submission_id) {
      let url = 'service/financial-affairs/submission/' + submission_id + '/view-submission';
      app.$container.dataService.get(url, result => {
        if (result.data.success) {
          context.commit("SET_SUBMISSION", result.data.submission);
          context.commit('SET_STATEMENT', result.data.statement);
          context.commit('SET_OFFICES', result.data.offices);
          context.commit("SET_CANDIDACIES", result.data.candidacies);
        }
      })
    },

    saveSubmission(context){
      if(!context.state.user.locked && !context.state.submission.saving) {
        if (context.state.submission.submission_id) {
          context.state.submission.saving = true;
          app.$container.dataService.put('service/financial-affairs/submission/' + context.state.submission.submission_id,
              result => {
                if (result.data.success) {
                  context.commit('SET_SUBMISSION', result.data.submission);
                  if (result.data.statement) {
                    context.commit('SET_STATEMENT', result.data.statement);
                  }
                  // app.$container.wire.$emit('StatementWizard.validate');
                }
                else {
                  alert("Error saving submission");
                }
              }, {
                'statement': context.state.statement,
                'person': context.state.person,
                'offices': context.state.offices,
                'candidacies': context.state.candidacies,
                'submission': context.state.submission
              },
              {},
              () => {
                alert("Error saving submission");
                context.state.submission.saving = false;
              }
          )
        }
        else if (context.state.doSaveSubmissions) {
          app.$container.dataService.post('service/financial-affairs/submission/',
              result => {
                if (result.data.success) {
                  context.commit('SET_SUBMISSION', result.data.submission);
                  let person = context.state.user.persons.find(e => e.person_id === result.data.submission.person_id);
                  if (!person) {
                    person = {}
                    context.state.user.persons.push(person)
                  }
                  person.submission_id = result.data.submission.submission_id;
                  let name = result.data.person.name;
                  if (result.data.submission.person_id === -1) {
                    name = name + " (unverified)";
                  }
                  person.name = name;
                  app.$container.wire.$emit('App.SubmissionCreated');
                }
                else {
                  console.log(result.data.error);
                  alert("Error saving submission");
                }
              },
              {
                'statement': context.state.statement,
                'person': context.state.person,
                'offices': context.state.offices,
                'candidacies': context.state.candidacies,
                'submission': context.state.submission
              }
          )
        }
      }
    },
    submitSubmission(context, callback){
      app.$container.dataService.put('service/financial-affairs/submission/' + context.state.submission.submission_id + '/submit',
        result => {
          if (result.data.success) {
            context.commit('SET_SUBMISSION', result.data.submission);
            callback(result);
          } else {
            console.log(result.data.error);
            alert("Error submitting disclosure: " +result.data.error);
          }
        }, {
          'person': context.state.person,
          'submission': context.state.submission
        },
        {},
        () => {
          alert("Error submitting disclosure");
        }
      )
    },
    amendSubmission(context) {
      let submission_id = context.state.submission.submission_id;
      app.$container.dataService.post('service/financial-affairs/submission/' + submission_id + '/amendment',
        (result) => {
          if (result.data.success) {
            context.commit('SET_STATEMENT', result.data.statement);
            context.commit("SET_PERSON", result.data.person);
            context.commit("SET_SUBMISSION", result.data.submission);
            context.commit('SET_OFFICES', result.data.offices);
            context.commit("SET_CANDIDACIES", result.data.candidacies);
            context.commit("SET_STATEMENTS", result.data.statements);
            context.commit("DO_SAVE_SUBMISSIONS", true);
            app.$container.wire.$emit('App.startWizard', 'Spouse');
          }
        }
      );
    },
    resetLocalSubmissionState({state, commit}) {
      let person = {};
      if (! state.user.selected_person || state.user.selected_person.person_id === -1) {
        person = {
          person_id: 0,
          name: "",
          email: ""
        };
      } else {
        person = {
          person_id: state.user.selected_person.person_id,
          name: state.user.selected_person.name,
          email: ""
        }
      }
      let statement = {
        period_start: "",
        period_end: "",
      };
      commit('SET_STATEMENT', statement);
      commit('SET_PERSON', person);
      commit('SET_OFFICES', []);
      commit('SET_CANDIDACIES', []);
      commit('SET_STATEMENTS', []);
      commit('SET_SUBMISSION', {});
    },
    async resetSubmission(context) {
      let p = await context.dispatch('removeSubmission');
      context.dispatch('resetLocalSubmissionState');
      return p;
    },
    async removeSubmission({state}) {
      let submission_id = state.submission.submission_id;
      return await app.$container.dataService.asyncDelete('service/financial-affairs/submission/' + submission_id);
    },
    async removeSubmissionById(context, submission_id) {
      context.commit('SET_PERSON');
      return await app.$container.dataService.asyncDelete('service/financial-affairs/submission/' + submission_id);
    },
    getThresholds(context){
      app.$container.dataService.query(
          'thresholds',
          result => {
            context.commit('SET_THRESHOLDS', result)
          }
      )
    },
    loadPerson (context, person_id) {
      app.$container.dataService.get(
        'service/financial-affairs/person/' + person_id,
        result => {
          context.commit('SET_PERSON', result.data.person);
          context.commit('SET_STATEMENTS', result.data.statements);
          context.commit('SET_SUBMISSIONS', result.data.submissions);
          context.commit('SET_OFFICES', result.data.offices);
          context.commit('SET_CANDIDACIES', result.data.candidacies);
        });
    },
    loadPersonMetadata (context, person_id) {
      app.$container.dataService.get(
        'service/financial-affairs/person/' + person_id,
        result => {
          context.commit('SET_PERSON', result.data.person);
          context.commit('SET_STATEMENTS', result.data.statements);
          context.commit('SET_OFFICES', result.data.offices);
          context.commit('SET_CANDIDACIES', result.data.candidacies);
          if(result.data.offices.length > 0){
            context.state.statement.isOfficeHolder = true;
          }
          if(result.data.candidacies.length > 0){
            context.state.statement.isCandidate = true;
          }
        },
        {current: 'Y'});
    },
    getStatement(context, statement_id) {
      app.$container.dataService.get(
        'service/financial-affairs/statement/' + statement_id,
        result => {
          if (result.data && result.data.success && result.data.statement) {
            context.commit('SET_STATEMENT', result.data.statement);
          }
          else {
            alert("Unable to load statement");
          }
        }
      );
    },
    saveStatement(context) {
      let params = {
        person_id: context.state.person.person_id,
        period_start: context.state.statement.period_start,
        period_end: context.state.statement.period_end,
        retain_until: context.state.statement.retain_until,
        hold_for: context.state.statement.hold_for,
        first_filed_at: context.state.statement.first_filed_at,
      };

      // Determine callback action
      let callback = (result)  => {
        if (result.data.success) {
          app.$container.wire.$emit('DataEntryWizard.close');
          app.$container.wire.$emit('app.refresh-person-statements', params.person_id);
        }
        else {
          alert(result.data.errors);
        }
      };

      // Call appropriate method
      if (context.state.statement.statement_id) {
        app.$container.dataService.put(
          'service/financial-affairs/statement/' + context.state.statement.statement_id,
          callback,
          params
        );
      }
      else {
        app.$container.dataService.post(
          'service/financial-affairs/statement',
          callback,
          params
        );

      }
    },
    removeStatement(context) {
      if (context.state.statement.statement_id) {
        app.$container.dataService.delete(
          'service/financial-affairs/statement/' + context.state.statement.statement_id,
          result => {
            if (result.data && result.data.success) {
              context.state.statement.statement_id = null;
              app.$container.wire.$emit('app.refresh-person-statements', context.state.person.person_id);
            }
            else {
              if (result.data && result.data.error) {
                alert(result.data.error);
              }
              else {
                alert("Unable to remove financial ");

              }
            }
          }
        );
      }
    },
    setSelectedPerson(context, person) {
      context.state.user.selected_person = person;
      if (person) {
        sessionStorage.setItem('person_id', person.person_id);
        sessionStorage.setItem('person_name', person.name);
        app.$container.wire.$emit('SelectedPerson.Changed');
      } else {
        sessionStorage.removeItem('person_id');
        sessionStorage.removeItem('person_name');
      }

    },
    setPerson(context, person) {
      context.commit('SET_PERSON', person);
    },
    setStatement(context, statement) {
      context.commit('SET_STATEMENT', statement);
    },
    setStatements(context, statements) {
      context.commit('SET_STATEMENTS', statements);
    },
    clearOffices(context) {
      context.commit('SET_OFFICES', []);
    },
    clearCandidacies(context) {
      context.commit('SET_CANDIDACIES', []);
    },
    clearRealEstate(context){
      context.commit('SET_REAL_ESTATE',[])
    },
    wizardActivate(context, params) {
      app.$container.wire.$emit('StatementWizard.activate', params.step);
    },
    wizardPrevious(context, options = {}) {
      app.$container.wire.$emit('StatementWizard.previousStep', options);
    },
    wizardNext(context, options = {}) {
      app.$container.wire.$emit('StatementWizard.nextStep', options);
    },
    wizardValidate() {
      app.$container.wire.$emit('StatementWizard.validate');
    }
  }
})
