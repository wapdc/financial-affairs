[readme](./readme.md) / api

# API Integration

<!-- TODO: complete + verify documentation after feature + implementation completion -->

For saving and loading drafts from an API, bindings can be defined between the wizard fields and the payload object.

## Initializing the payload

useWizard returns a method `setApiPayload` that accepts a draft object from the API. Form fields with an `api` property will be updated with the values from the draft payload, and this payload will be kept in sync with the form fields as the user makes changes.

### Example

```javascript
// Define fields and sections
const wiz = useWizard([exampleSection, anotherSection]);

// Load draft from API
const draft = await fetchDraftExampleFn();
wiz.value.setApiPayload(draft);
```

## Mapping fields to API properties

[Fields](#defining-a-field) accept an `api` property that defines how the field value should bind to the payload. There are three possible ways to define this property:

### Basic fields

- For simple fields, use a string that matches the key in the payload object. i.e. `api: "someNumber"` will bind the field to `payload.someNumber`.
- For transformations or nested fields, use a function that returns a writable computed value. This function should accept the payload object and return a writable computed value that maps to the desired payload value. For example:

  ```javascript
  const exampleField = field({
    name: "exampleField",
    api: (payload) =>
      computed({
        get: () => Number(payload.numberAsString), // Cast string to number
        set: (v) => (payload.someNumber = v),
      }),
  });
  ```

### Template fields and subfields of template-derived arrays

> Reminder: A template field is a container of objects derived from the template definition; an array for field-sets. The template field itself does not have a value, but the "subfields" within the derived elements will hold values. A template field will typically correspond to an array in the payload object, and the subfields will correspond to the properties of the objects in that array.

Template fields require logic for accessing elements of the corresponding array in the payload object. `get`, `getOne`, `add`, and `remove` methods can be defined to handle these operations. For example:

```javascript
export function createRealEstateArrayBinds() {
  const targetArray = (payload) => payload?.statement?.real_estate;
  return {
    get: (payload) => targetArray(payload),
    getOne: (payload, index) => targetArray(payload)[index],
    add: (payload) => {
      payload.statement["real_estate"] ??= [];
      payload.statement["real_estate"].push({});
    },
    remove: (payload, index) => {
      targetArray(payload).splice(index, 1);
    },
  };
}
```

- `get(payload)`: Returns the template field's corresponding array from the payload object
- `getOne(payload, index)`: Returns the element at the specified index from the template field's corresponding array in the payload object
- `add(payload)`: Adds a new element to the template field's corresponding array in the payload object
- `remove(payload, index)`: Removes the element at the specified index from the template field's corresponding array in the payload object

#### Subfields of template-derived arrays

Subfields follow the same rules as basic top-level fields, but where the top level field expects the payload object, the subfield expects the element object of the corresponding array in the payload.

```javascript
const exampleArrayField = field({
  name: "exampleArray",
  api: createRealEstateArrayBinds(),
  template: [
    field({
      name: "nestedField1",
      api: (element) =>
        computed({
          get: () => element.someNumber,
          set: (v) => (element.someNumber = v),
        }),
    }),
    field({
      name: "nestedField2",
      api: "someString", // This will bind nestedField2 to the element.someString value
      // `element` is equal to payload.statement.real_estate[someIndex].someString
    }),
  ],
});
```

##### Top level vs subfield API bindings:

|        | Base field                              | Subfield                                                                           |
| -----: | --------------------------------------- | ---------------------------------------------------------------------------------- |
|     fn | (**payload**) => {} : WritableComputed  | (**element**) => {} : WritableComputed (Element is templateDerivedArray[anyIndex]) |
| string | Sets and gets at **payload**[field.api] | Sets and gets at **element**[field.api]                                            |

## Saving the draft

When the user is ready to save the draft, the payload can be extracted from the wizard fields and sent to the API. If the bindings are well defined, the payload should be ready to send as is, or with minimal modifications. `useWizard` returns a method `getApiPayload` function that returns the payload object with the current field values.
