import { assetArrayValidator } from '../common/assetValidator';
import { CategoryHandler } from 'src/utils/CategoryHandler.class';
import rules from 'src/utils/rules';
import useWizard from 'src/wizard/useWizard';

const { lazyLoadWrapper, dataPath, getData } = useWizard();

const accountCategoryHandler = new CategoryHandler('accounts', 'other');

export const otherInvestmentStep = {
  title: 'Other self-directed investments',
  order: 5,
  component: lazyLoadWrapper(() =>
    import('../Other/OtherInvestmentAccounts.vue'),
  ),
  fields: {
    hasOtherInvestments: {
      rules: [rules.required],
      blocking: false,
      data() {
        return dataPath(this, 'pendingTransform.hasOtherInvestments');
      },
      transform: (field) => ({
        writeOut: () => accountCategoryHandler.writeOutHas(field),
        readIn: () => accountCategoryHandler.readInHas(field),
      }),
    },
    investmentTypeArray: {
      blocking: false,
      data() {
        return dataPath(this, 'pendingTransform.investmentTypeArray');
      },
      transform: (field) => ({
        writeOut: () => accountCategoryHandler.writeOutData(field),
        readIn: () => accountCategoryHandler.readInData(field),
      }),
      requiredIf: () => [true, 'true'].includes(getData().pendingTransform.hasOtherInvestments),
      template: {
        heldBy: {
          as: 'who',
          rules: [rules.requiredNonEmptyArray],
        },
        investmentName: {
          as: 'name',
          rules: [
            rules.requiredString,
            rules.maxLength(100),
            rules.alphbetical,
          ],
        },
        investmentArray: {
          as: 'assets',
          rules: [
            rules.requiredNonEmptyArray,
            (v) => assetArrayValidator(v, ['nature']),
          ],
        },
        streetAddress: {
          as: 'address',
          rules: [
            rules.requiredString,
            rules.maxLength(120),
          ],
        },
        city: {
          rules: [
            rules.requiredString,
            rules.alphbetical,
            rules.maxLength(120),
          ],
        },
        state: {
          rules: [rules.required, rules.state],
        },
        zip: {
          rules: [rules.required, rules.exactLength(5), rules.numbersOnly],
        },
      },
    },
  },
};
