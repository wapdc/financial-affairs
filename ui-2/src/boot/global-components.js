import Head1 from 'src/components/Headers/Head1.vue';
import PAccessibleButton from 'src/library/PAccessibleButton.vue';
import PApp from 'src/library/PApp.vue';
import PDatePicker from 'src/library/PDatePicker.vue';
import PInfoDialog from 'src/library/PInfoDialog.vue';
import PToolbar from 'src/library/PToolbar.vue';
import PUserHeader from 'src/library/PUserHeader.vue';
import PUserLayout from 'src/library/PUserLayout.vue';
import PWhiteLogo from 'src/library/PWhiteLogo.vue';

export default async ({ app }) => {
  // pdc-ui library overrides
  app.component('head1', Head1);
  app.component('p-accessible-button', PAccessibleButton);
  app.component('p-app', PApp);
  app.component('p-date-picker', PDatePicker);
  app.component('p-info-dialog', PInfoDialog);
  app.component('p-toolbar', PToolbar);
  app.component('p-user-header', PUserHeader);
  app.component('p-user-layout', PUserLayout);
  app.component('p-white-logo', PWhiteLogo);

};
