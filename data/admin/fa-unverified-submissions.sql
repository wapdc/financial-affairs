select *, 'apollo' as source
from fa_submission
where submitted is true
  and verified is false
  and (
    (cast(:year as INTEGER) is null and submitted_at >= (now() - interval '90 days'))
      or cast(:year as INTEGER) = date_part('year', submitted_at)
  )