import { computed, ref } from 'vue';
import { useApollo } from '@wapdc/pdc-ui/application';

const occupations = ref(null);

/**
 * Fetch and return occupations
 * @returns {Promise<Array>} Computed options list of occupations for a select input
 */
export default async function useOccupations() {
  if (!occupations.value) {
    occupations.value = (await fetchOccupations()) ?? [];
  }

  return computed(() => {
    return occupations.value.map((el) => ({
      value: el.occupation,
      label: el.occupation,
    }));
  });
}

/**
 * Fetch occupations from the API
 * @returns {Promise<Object>} Occupations data object
 */
async function fetchOccupations() {
  try {
    const service = useApollo('service/financial-affairs');
    return (await service.get('/occupations')).data;
  } catch (error) {
    console.error('Error fetching occupations:', error);
  }
}
