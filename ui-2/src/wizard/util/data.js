import { computed } from 'vue';
import { validateField, _validateStep } from './step';
/**
 * @module wizard/util/data
 * @description Logic for managing wizard data (user input)
 */

/** Get data at a specific location in an object, creating it if needed */
export function _dataPath(state, field, accessString) {
  const keys = accessString.split('.');

  return keys.reduce((acc, key, index) => {
    if (index === keys.length - 1) {
      // For the last key, return a computed property that can get and set the value
      return computed({
        get: () => acc[key] ?? null,
        set: (value) => {
          acc[key] = value;
          validateField(field);
        },
      });
    }
    // Create the nested object if it does not exist
    acc[key] = acc[key] || {};
    return acc[key];
  }, state.data);
}

/** onSave callback defined by instantiating module */
export async function _save(state) {
  if (
    !state.eventCallbacks?.onSave ||
    typeof state.eventCallbacks.onSave !== 'function'
  ) {
    console.error('No valid onSave callback provided');
    return;
  }

  const sections = Object.values(state.form);

  const readIns = [];

  sections.forEach((section) => {
    const steps = Object.values(section.steps);

    steps.forEach((step) => {
      if (!step.fields) return;
      const fields = Object.values(step.fields);

      fields.forEach((field) => {
        if (!field.transform) return;
        const transforms = field.transform(field);

        if (!transforms.writeOut || !transforms.readIn) {
          console.error('Transforms must have writeOut and readIn methods');
          return;
        }

        transforms.writeOut();

        readIns.push(transforms.readIn);
      });
    });
  });

  await state.eventCallbacks.onSave();

  readIns.forEach((readIn) => readIn());
}

export function _setData(state, payload) {
  state.data = { ...payload };

  // Update step and field meta values
  Object.values(state.form).forEach((section) => {
    const affectedSteps = new Set();
    const steps = Object.values(section.steps);

    steps.forEach((step) => {
      if (!step.fields) return;

      const fields = Object.values(step.fields);

      fields.forEach((field) => {
        // First run the readIn transform to get the data from the payload to the field
        if (typeof field.transform === 'function') {
          field.transform(field).readIn?.();
        }

        const notSet = [undefined, null, ''].includes(field.data()?.value);
        const emptyArray =
          Array.isArray(field.data()?.value) && !field.data()?.value.length;
        if (notSet || emptyArray) return;

        field.meta.isDirty = true;
        affectedSteps.add(step);
      });
    });

    affectedSteps.forEach((step) => {
      step.meta.isDirty = true;
      _validateStep(step);
    });
  });
}
