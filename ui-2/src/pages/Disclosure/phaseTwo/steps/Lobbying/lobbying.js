import useWizard from 'src/wizard/useWizard';
import rules from 'src/utils/rules';

const { lazyLoadWrapper, dataPath, getData } = useWizard();

export const lobbyingStep = {
  title: 'Lobbying',
  order: 0,
  component: lazyLoadWrapper(() => import('./Lobbying.vue')),
  fields: {
    hasLobbies: {
      rules: [rules.requiredString],
      blocking: false,
      data() {
        return dataPath(this, 'statement.has_lobbies');
      },
      transform: (hasField) => ({
        writeOut: () => {
          const hasLobbiesInputValue = hasField.data().value;
          const valueForApi = [null, 'null', undefined].includes(
            hasLobbiesInputValue,
          )
            ? null
            : Boolean(hasLobbiesInputValue === 'true');

          getData().statement.has_lobbies = valueForApi;
        },
        readIn: () => {
          const data = getData();
          const hasLobbiesApiValue = data?.statement?.has_lobbies;
          hasField.data().value = [null, undefined, 'null'].includes(
            hasLobbiesApiValue,
          )
            ? null
            : String(hasLobbiesApiValue);
        },
      }),
    },

    lobbies: {
      blocking: false,
      data() {
        return dataPath(this, 'statement.lobbies');
      },
      requiredIf: () =>
        [true, 'true'].includes(getData().statement.has_lobbies),
      template: {
        who: {
          as: 'who',
          rules: [rules.requiredNonEmptyArray],
        },
        compensation: {
          rules: [rules.required],
        },
        lobbiedFor: {
          as: 'for',
          rules: [rules.requiredString],
        },
        purpose: {
          as: 'purpose',
          rules: [rules.requiredString],
        },
      },
    },
  },
};

export const lobbyingSection = {
  title: 'Lobbying',
  order: 6,
  steps: {
    lobbyingStep,
  },
};
