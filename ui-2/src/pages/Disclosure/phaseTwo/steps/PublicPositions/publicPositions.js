import { section, step, lazyLoadWrapper } from 'src/wizard/useWizard';

export const publicPositionsSection = section({
  title: 'Public Positions',
  canSave: true,
  steps: [
    step({
      title: 'Public Positions',
      component: lazyLoadWrapper(() => import('./PublicPositions.vue')),
    }),
  ],
});
