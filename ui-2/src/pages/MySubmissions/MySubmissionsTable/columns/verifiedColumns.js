import { sortDateTimeStrings } from './sort';

export const verifiedColumns = [
  {
    name: 'period',
    label: 'Reporting Period',
    field: 'period',
    required: true,
    align: 'left',
    sortable: true,
    sort: sortDateTimeStrings,
  },
  {
    name: 'Submitted',
    label: 'Submitted',
    required: true,
    align: 'left',
    field: 'submitted',
    sortable: true,
    sort: sortDateTimeStrings,
  },
];
