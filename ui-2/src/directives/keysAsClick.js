/**
 * Vue directive to simulate a click event on a focused element when the Enter or Space key is pressed.
 * Useful for quickly making prefab components accessible to keyboard users.
 * @example
 * ```
 * <ComponentWithClickHandler v-keys-as-click />
 * ```
 */

const keysAsClick = {
  beforeMount(el) {
    el.__handleKeydown__ = (e) => {
      if (e.key === 'Enter' || e.key === ' ') {
        e.preventDefault();
        el.click();
      }
    };
    el.addEventListener('keydown', el.__handleKeydown__);
  },
  unmounted(el) {
    el.removeEventListener('keydown', el.__handleKeydown__);
    delete el.__handleKeydown__;
  },
};

export default keysAsClick;
