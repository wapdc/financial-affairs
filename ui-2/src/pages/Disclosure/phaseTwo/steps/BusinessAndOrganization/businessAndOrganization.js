import useWizard from 'src/wizard/useWizard';
import { businessesOwnedStep } from './ownership/businessesOwnedStep';
import { businessesOtherStep } from './otherBusinessRelationship/businessesOtherStep';

const { lazyLoadWrapper } = useWizard();

export const intro = {
  title: 'Business and Organizational Interests',
  order: 0,
  component: lazyLoadWrapper(() =>
    import('./BusinessAndOrganizationIntro.vue'),
  ),
};

export const businessAndOrganizationSection = {
  title: 'Business Interests',
  order: 7,
  canSave: true,
  steps: {
    intro,
    businessesOwnedStep,
    businessesOtherStep,
  },
};
