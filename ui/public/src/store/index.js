import Vue from 'vue'
import Vuex from 'vuex'
import {app} from '../main'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    statement_info: {},
    statement: {},
    person: {},
    submission: {},
    candidacies: [],
    offices: [],
    validation: {
      relation_code: [
        { value: 'filer', text: 'Filer', sort: '10'},
        { value: 'spouse', text: 'Spouse or registered domestic partner', sort: '20'},
        { value: 'dependent', text: 'Dependent(s)', sort: '30'},
      ],
    },
  },
  mutations: {
    SET_STATEMENT_INFO(state, statement_info){
      state.statement_info = statement_info;
    },
    SET_STATEMENT(state, statement){
      state.statement = statement;
    },
    SET_PERSON(state, person){
      if (person) {
        state.person=person;
      }
      else {
        state.person = {};
      }
    },
    SET_CANDIDACIES(state, candidacies) {
      if (candidacies) {
        state.candidacies = candidacies;
      } else {
        state.candidacies = [];
      }
    },
    SET_OFFICES(state, offices) {
      if (offices) {
        state.offices = offices;
      } else {
        state.offices = [];
      }
    },
    SET_SUBMISSION(state, submission){
      state.submission = submission;
    }
  },
  actions: {
    getStatementData(context, params) {
      app.$container.SocrataDataService.getRecord(params.statement_id, (data) => {
        // Unless there is an actual error (non 200 response) Socrata always
        //   returns an array, even if it is empty.

        let s = data[0];
        s.json = JSON.parse(data[0].json);

        let temp_submission = {
          active_official: true,
          submitted_at: s.receipt_date,
          certification_name: s.certification_name,
          certification_email: s.certification_email,
          certification_phone: s.certification_phone,
          certification: s.certification,
          name: s.name
        };

        temp_submission.history = JSON.parse(s.history);

        context.commit('SET_STATEMENT_INFO', s);
        context.commit('SET_STATEMENT', s.json.statement);
        context.commit('SET_PERSON', {'name': s.name});
        context.commit('SET_SUBMISSION', temp_submission);
        context.commit('SET_CANDIDACIES', JSON.parse(s.candidacies));
        context.commit('SET_OFFICES', JSON.parse(s.offices));
        app.$container.wire.$emit('data.loaded');
      });
    }
  },
  modules: {
  }
})
