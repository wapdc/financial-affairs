import { computed, inject, onMounted, provide, reactive } from 'vue';
import { LEGACY, UNVERIFIED, VERIFIED } from 'src/constants/submissions.js';
import { useApollo } from '@wapdc/pdc-ui/application';
import { Notify } from 'quasar';

const SubmissionsAccessorSymbol = Symbol('SubmissionsAccessorSymbol');

/**
 * Composable function to provide submission data accessors to descendent components.
 * Must be instantiated in the nearest common ancestor of components that import the
 * useSubmissions composable.
 * @returns {void}
 */
export function useSubmissionsProvider() {
  const faService = useApollo('service/financial-affairs');

  const state = reactive({
    drafts: [],
  });

  async function newDraft(personId) {
    personId ??= -1;
    return await faService.get(`/person/${personId}/submission`);
  }

  /**
   * Function to retrieve submissions by a specific filer ID, returning a plain object with
   * separate arrays for unverified, verified, and legacy submissions.
   * @param {string} personId The person ID of the filer to retrieve submissions for.
   * @returns {Promise<Submissions[]>} The submissions for the specified filer ID.
   */
  async function getSubmissionsByPersonId(personId) {
    const personIdQuery = personId ? `?person_id=${personId}` : '';
    const result = await faService.get(`/submissions${personIdQuery}`);

    if (!result?.success) return;

    const filerSubmissions = {
      [UNVERIFIED]: result?.[UNVERIFIED] || [],
      [VERIFIED]: result?.[VERIFIED] || [],
      [LEGACY]: result?.[LEGACY] || [],
    };

    return filerSubmissions;
  }

  /**
   * Function to save a draft form
   * @param {Object} payload The draft form payload
   * @returns {Promise<Object>}
   * @example
   * ```javascript
   *  const apiPayload = wiz.value.getApiPayload();
   *  await submissions.saveDraft(apiPayload);
   * ```
   */
  async function saveDraft(payload, id) {
    try {
      let response;

      if (id) {
        response = await faService.put(`/submission/${id}`, payload);
      } else {
        response = await faService.post('/submission', payload);
      }

      if (!response?.success) {
        console.error(response);
        return Notify.create({
          message: response.error ?? 'Could not save draft',
          color: 'negative',
          position: 'top',
        });
      }

      return response.submission;
    } catch (error) {
      console.error('saveDraft error', error);
    }
  }

  /**
   * Function to load the draft form
   * @returns {Promise<*>} The draft form
   */
  async function loadDraftForm() {
    try {
      const response = await faService.get('/drafts');
      return response?.drafts || [];
    } catch (error) {
      console.error('loadDraftForm error', error);
    }
  }

  /**
   * Function to delete the draft form
   * @param {string} draftId The draft ID
   * @returns {Promise<void>}
   * @throws {Error} Throws an error if the draft form cannot be deleted.
   */
  async function deleteDraft(draftId) {
    try {
      await faService.delete(`/submission/${draftId}`);
      await reloadDrafts();
    } catch (error) {
      console.error('deleteDraftForm error', error);
    }
  }

  async function getSubmissionById(submissionId) {
    return await faService.get(`/submission/${submissionId}`);
  }

  async function reloadDrafts() {
    state.drafts = await loadDraftForm();
  }

  onMounted(async () => {
    await reloadDrafts();
  });

  provide(SubmissionsAccessorSymbol, {
    newDraft,
    drafts: computed(() => state.drafts),
    saveDraft,
    deleteDraft,
    getSubmissionById,
    getSubmissionsByPersonId,
    reloadDrafts,
  });
}

/**
 * Composable function to return submissions data accessors. Must be used within a component where
 * SubmissionsProvider has been instantiated in an ancestor component.
 * @returns {{
 *   newDraft: (personId: string) => Promise<Object>,
 *   saveDraft: (payload: Object, id: string) => Promise<Object>,
 *   deleteDraft: (draftId: string) => Promise<void>,
 *   getSubmissionsByPersonId: (filerId: string) => Promise<Submissions[]>,
 *   getSubmissionById: (filerId: string, submissionId: string) => Promise<ComputedRef<Submission>>,
 *   drafts: ComputedRef<Submission[]>, // Computed reference to the submissions drafts.
 *   reloadDrafts: () => Promise<void>,
 * }} submissionsAccessor:
 *   - getSubmissionsByPersonId: Function to retrieve submissions by a specific filer ID
 *   - getSubmissionById: Function to retrieve a specific submission by filer ID and submission ID
 *   - drafts: Computed submissions drafts array
 * @throws {Error} Throws an error if useSubmissionsProvider() has not been instantiated in an ancestor component.
 */
export default function useSubmissions() {
  const submissionsAccessor = inject(SubmissionsAccessorSymbol);
  if (!submissionsAccessor) {
    throw new Error(
      'useSubmissions must be used within a descendant of a component instantiating' +
        ' useSubmissionsProvider',
    );
  }

  return submissionsAccessor;
}
