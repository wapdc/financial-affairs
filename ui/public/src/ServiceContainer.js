/**
 * The ServiceContainer class allows injection of dependent services
 * to the class.
 */
import SocrataDataService from "./SocrataDataService";
import Vue from 'vue';

export default class ServiceContainer {
  SocrataDataService = new SocrataDataService();
  wire = new Vue();
  utils = {
    titleCase(phrase) {
      phrase = phrase.toLowerCase().split(' ');

      for (var i = 0; i < phrase.length; i++) {
        phrase[i] = phrase[i].charAt(0).toUpperCase() + phrase[i].slice(1);
      }
      return phrase.join(' ');
    }

  }
}