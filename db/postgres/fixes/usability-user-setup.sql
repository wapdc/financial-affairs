do $$
begin
insert into pdc_user (uid, realm, user_name) select '93', 'apollo', 'filer'
where not exists(select uid, realm, user_name from pdc_user where uid = '93' and realm = 'apollo');

delete from entity_authorization where realm = 'apollo' and uid = '93';

insert into entity_authorization (uid, realm, entity_id, role) select '93', 'apollo', :person_id, 'owner'
where not exists(select uid, realm, entity_id from entity_authorization where uid = '93' and realm = 'apollo');

update fa_submission set uid = '-99' where realm = 'apollo' and uid = '93';

update fa_submission set uid = '93', realm = 'apollo' where person_id = :person_id;


delete from wapdc_settings where property = 'date_override' and stage is null;

insert into wapdc_settings (property, stage, value) values ('date_override', null, '2021-01-05');
end $$