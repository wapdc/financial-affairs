import { format } from 'quasar';
import { publicInvestmentStep } from './publicInvestmentStep';

const { capitalize } = format;

const keys = publicInvestmentStep.fields.institutionArray.template;

const heldBy = {
  key: keys.heldBy.as,
  label: 'Held By',
  align: 'left',
  sortable: false,
  width: '60%',
  format: (value) => capitalize(value?.join(', ')),
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

const nature = {
  key: keys.natureOfHoldings.as,
  label: 'Institution Name',
  align: 'left',
  sortable: false,
  width: '40%',
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

export default [heldBy, nature];
