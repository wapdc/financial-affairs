SELECT
  docid,
  d.batch_number as repno
  FROM entity_authorization ea
    join entity e on e.entity_id = ea.entity_id
    join ax_document d on d.filer_id = e.filer_id
WHERE
  ea.uid = :uid and ea.realm = :realm
AND
  (batch_number = :repno OR docid = :doc_id)