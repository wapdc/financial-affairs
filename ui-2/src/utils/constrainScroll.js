/**
 * Scrolls the viewport to ensure the given element is in view
 * @param {HTMLElement} element The element to scroll into view
 * @param {number} [topBufferPx=100] The buffer in pixels from the top of the viewport
 * @param {number} [btmBufferPx=100] The buffer in pixels from the bottom of the viewport
 * @returns {void}
 */
export default function constrainScroll(
  element,
  topBufferPx = 100,
  btmBufferPx = 100,
) {
  if (!(element instanceof HTMLElement) || element.tabIndex === -1) {
    return;
  }

  const rect = element.getBoundingClientRect();

  // Scroll the element into view if it is near the bottom of the viewport
  if (rect.bottom + btmBufferPx > window.innerHeight) {
    window.scrollBy({
      top: rect.bottom - window.innerHeight + btmBufferPx,
      behavior: 'smooth',
    });
  }

  // Scroll the element into view if it is near the top of the viewport
  else if (rect.top < topBufferPx) {
    window.scrollBy({
      top: rect.top - topBufferPx,
      behavior: 'smooth',
    });
  }
}
