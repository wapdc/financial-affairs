import { renderIncomeRange, capitalize } from 'src/utils/format';
import { realEstateOwnershipStep } from 'src/pages/Disclosure/phaseTwo/steps/RealEstate/realEstate.js';

const keys = realEstateOwnershipStep.fields.propertyArray.template;

const owner = {
  key: keys.who.as,
  label: 'Received By',
  align: 'left',
  sortable: false,
  width: '50%',
  format: (value) => capitalize(value?.join(', ')),
};

const assessedValue = {
  key: keys.assessedValue.as,
  label: 'Assessed Value',
  align: 'left',
  sortable: false,
  width: '50%',
  format: (value) => renderIncomeRange(value),
};

export default [owner, assessedValue];
