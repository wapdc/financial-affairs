import { Notify } from 'quasar';
import { useApollo } from '@wapdc/pdc-ui/application';

export default function useRequestAccess(filerStateHandler = null) {
  const service = useApollo('service/financial-affairs');

  return {
    submitAccessRequest: (email) => submitAccessRequest(service, email),
    submitToken: (token) => submitToken(service, filerStateHandler, token),
  };
}

async function submitAccessRequest(service, email = null) {
  try {
    const result = await service.post('/request-access', { email });
    return result.success;
  } catch (error) {
    console.error('Error in submitAccessRequest:', error);
  }
}

async function submitToken(service, filerStateHandler, token = null) {
  try {
    const verificationResult = await service.post('/verify-access-request', {
      token,
    });

    if (!verificationResult.success) {
      return false;
    }

    const addedPersonId =
      verificationResult?.authorizationsAdded?.person?.[0]?.person_id;

    if (!addedPersonId) {
      console.error('No person_id found in verificationResult');
      return codeSuccessButPersonError();
    }

    const persons = await filerStateHandler.fetchAvailableFilers();

    if (!persons) {
      console.error('No authorizations found in auth check');
      return codeSuccessButPersonError();
    }

    const person = persons?.find((p) => p.person_id === addedPersonId);

    if (!person) {
      console.error('Person not found in auth persons array');
      return codeSuccessButPersonError();
    }

    filerStateHandler.setSelectedFilerByPersonId(person.person_id, persons);

    return true;
  } catch (error) {
    console.error('Error in useRequestAccess submitToken', error);
  }
}

function codeSuccessButPersonError() {
  Notify.create({
    message: 'Security code matched, but the filer was not found',
    color: 'negative',
  });
}
