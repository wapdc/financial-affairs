export const ROUTE_NAMES = {
  HOME: 'home',
  STYLEGUIDE: 'styleguide',
  DISCLOSURE_FORM: 'disclosureForm',
  DISCLOSURE_DETERMINATION_FORM: 'disclosureDeterminationForm',
  MY_SUBMISSIONS: 'mySubmissions',
  SUBMISSION_DETAILS: 'submissionDetails',
};
