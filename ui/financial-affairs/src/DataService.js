
import axios from 'axios';
import {app} from "./main";
export default class DataService {

  service_host = 'https://apollod8.lndo.site';

  pendingRequests = 0;

  errorReason = {};

  /**
   * Constructor
   *
   * Sets an appropriate url handling our development environment.
   */
  constructor() {

    if (location.host.search('localhost') >= 0) {
      // Assume ports that start iwth 818 (e.g. 8180-8189 will connect to demo environment)
      // so that devs with no lando work.
      if (location.host.search('818') > 0) {
        this.service_host = 'https://demo-apollo-d8.pantheonsite.io/';
      }
      else {
        this.service_host = 'https://apollod8.lndo.site/';
      }
    } else {
      this.service_host = '/';
    }
  }

  /**
   * Handle errors in all axios calls.
   * @param r
   */
  handleError(r) {
    if(r.response && r.response.hasOwnProperty('status') && r.response.status === 403) {
      app.$container.wire.$emit('authenticate');
    }
    if(r.response && r.response.hasOwnProperty('data') && r.response.data.hasOwnProperty('error')) {
      alert (r.response.data.error);
      console.log(r.response.data.error);
    }
    else {
      alert('An error occurred while trying to communicate with the PDC servers. ');
      console.log(r);
    }
    this.errorReason = r;

    this.resolvePending();
  }

  /**
   * Call a web service that expects to return json.
   * This is used for axios call when you dont want
   * the progess bar to popup.
   */
  getPassive(path, callback, parms = {}) {
    let url = this.service_host + path;
    axios.get(url, {withCredentials: true, params: parms})
      .then(result => {
        callback(result);
      })
      .catch(r => {
        this.handleError(r)
      });
  }

  setPending() {
    if (this.pendingRequests === 0) {
      app.$container.wire.$emit('DataService.pending', true);
    }
    this.pendingRequests++;
  }

  resolvePending() {
    if (this.pendingRequests > 0) {
      this.pendingRequests--;
    }
    if (this.pendingRequests === 0 ) {
      app.$container.wire.$emit('DataService.pending', false);
    }
  }

  /**
   * Call a web service that expects to return json.
   * @param path
   *   Indicates the path of the webservice to be called.
   * @param callback
   *   A callable that processes parsed json data from axios
   * @param parms
   *   An object containing URL parameters to send with request.
   * @param handleErrorCallBack
   */
  get(path, callback, parms = {}, handleErrorCallBack = null) {
    let url = this.service_host + path;
    this.setPending();
    axios.get(url, {withCredentials: true, params: parms})

      .then(result => {
        callback(result);
        this.resolvePending();
      })
      .catch(r => {
        if (handleErrorCallBack) {
          handleErrorCallBack(r);
          this.resolvePending();
        } else {
          this.handleError(r);
        }
      });
  }

  post(path, callback, post_data, get_data = {}, handleErrorCallBack = null) {
    let url = this.service_host + path;
    this.setPending();
    axios.post(url, JSON.stringify(post_data), {withCredentials: true, params : get_data})
      .then(result => {
        callback(result);
        this.resolvePending();
      })
      .catch(r => {
        if (handleErrorCallBack) {
          handleErrorCallBack(r);
          this.resolvePending();
        } else {
          this.handleError(r);
        }
      });
  }

  put(path, callback, post_data, get_data = {}, handleErrorCallBack = null) {
    let url = this.service_host + path;
    this.setPending();
    axios.put(url, JSON.stringify(post_data), {withCredentials: true, params : get_data})
      .then(result => {
        callback(result);
        this.resolvePending();
      })
      .catch(r => {
        if (handleErrorCallBack) {
          handleErrorCallBack(r);
          this.resolvePending();
        } else {
          this.handleError(r);
        }
      });
  }

  /**
   * Make a delete web service call.
   * @param path
   *   Indicates the path of the webservice to be called.
   * @param callback
   *   A callable that processes parsed json data from axios
   * @param parms
   *   An object containing URL parameters to send with request.
   * @param handleErrorCallback
   */
  delete(path, callback, parms = {}, handleErrorCallBack = null) {
    let url = this.service_host + path;
    this.setPending();
    axios.delete(url, {withCredentials: true, params: parms})

      .then(result => {
        callback(result);
        this.resolvePending();
      })
      .catch(r => {
        if (handleErrorCallBack) {
          handleErrorCallBack(r);
          this.resolvePending();
        } else {
          this.handleError(r);
        }
      });
  }

  /**
   * Make a delete web service call using async/await. Returns result
   * @param path
   *   Indicates the path of the webservice to be called.
   * @param parms
   *   An object containing URL parameters to send with request.
   */
  async asyncDelete (path, parms = {}) {
    this.setPending();
    let url = this.service_host + path;
    let result = await axios.delete(url, {withCredentials: true, params: parms})
      .catch(r => {
        this.handleError(r);
      });
    this.resolvePending();
    return result;
  }

  /**
   * Run a sql query from the data admin.
   *
   * @param sql_base
   *   The name of the query to run without .sql
   * @param callback
   *   The function to call when query results are returned.
   * @param parms
   *   Parameters to pass to the query.
   */
  adminQuery(sql_base, callback, parms = {}) {
    this.get(
      'service/financial-affairs/admin-query/' + sql_base,
      result => {
        if (result.data.success) {
          callback(result.data.data);
        }
        else {
          alert(result.data.errors);
        }
      }, parms);
  }

  /**
   * Run a sql query from the data admin.
   *
   * @param sql_base
   *   The name of the query to run without .sql
   * @param callback
   *   The function to call when query results are returned.
   * @param parms
   *   Parameters to pass to the query.
   */
  query(action, callback, parms = {}) {
    this.get(
      'service/financial-affairs/' + action,
      result => {
        if (result.data.success) {
          callback(result.data.data);
        }
        else {
          alert(result.data.errors);
        }
      }, parms);
  }

  /**
   * Run a sql query from the data admin.
   *
   * @param sql_base
   *   The name of the query to run without .sql
   * @param callback
   *   The function to call when query results are returned.
   * @param parms
   *   Parameters to pass to the query.
   */
  coreAdminQuery(sql_base, callback, parms = {}) {
    this.get(
      'service/data-admin/query/' + sql_base,
      result => {
        if (result.data.success) {
          callback(result.data.data);
        }
        else {
          alert(result.data.errors);
        }
      }, parms);
  }

}