
import {app} from "./main.js";
import axios from 'axios';

export default class SocrataDataService {

  service_host = 'https://data.wa.gov';

  dataset_id = '';

  pendingRequests = 0;

  errorReason = {};

  /**
   * Constructor
   *
   * Sets an appropriate url handling our development environment.
   */
  constructor() {
    if ((location.host.search('demo') >= 0) || (location.host.search('lndo') >= 0) || (location.host.search('localhost') >= 0)) {
      this.dataset_id = 'ehbc-shxw';
    } else {
      this.dataset_id = 'ehbc-shxw';
    }
    this.path = `/resource/${this.dataset_id}.json`;
  }

  /**
   * Handle errors in all axios calls.
   * @param r
   */
  handleError(e) {
    if (e === 404) {
      app.$container.wire.$emit('data.not_found');
    } else {
      app.$container.wire.$emit('data.fetch_failed');
    }
    this.resolvePending();
  }

  setPending() {
    if (this.pendingRequests === 0) {
      app.$container.wire.$emit('DataService.pending', true);
    }
    this.pendingRequests++;
  }

  resolvePending() {
    if (this.pendingRequests > 0) {
      this.pendingRequests--;
    }
    if (this.pendingRequests === 0 ) {
      app.$container.wire.$emit('DataService.pending', false);
    }
  }

  /**
   * Get's a single statement
   * @param id
   * @param callback
   */
  getRecord(id, callback) {
    const url = this.service_host + this.path;
    const query = `select * where id='${id}'`;
    const config = {
      params: {
        $query: query
      }
    };

    this.setPending();
    axios.get(url, config)
        .then(result => {
          if (result.data.length === 0) {
            throw 404;
          }
          callback(result.data);
          app.$container.SocrataDataService.resolvePending();
        })
        .catch((e) => {
          app.$container.SocrataDataService.handleError(e);
        });
  }
}
