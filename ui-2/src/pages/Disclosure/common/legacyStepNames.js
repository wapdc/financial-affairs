/* eslint-disable camelcase */

/**
 * @module legacyStepNames
 * @description Map and functions to convert between new and old step names as used in the API.
 */

const defaultStep = {
  section: 'Overview',
  step: 'Overview',
};

/** Step identification */
const apiStepToSectionStepDictionary = {
  StatementsOwed: {
    section: 'Overview',
    defaultStep: 'Overview',
  },
  OfficesHeld: {
    section: 'Public Positions',
    defaultStep: 'Public Positions',
  },
  Candidacies: {
    // section: 'Candidacies',
    // defaultStep: 'Candidacies',
    section: 'Overview',
    defaultStep: 'Overview',
  },
  StatementOverview: {
    section: 'Overview',
    defaultStep: 'Overview',
  },
  CarryForward: {
    section: 'Import information',
    defaultStep: 'Import information',
  },
  IncomeReview: {
    section: 'Income',
    defaultStep: 'Income Disclosure',
    categoryMap: {
      employment: 'Employment Income',
      retirement: 'Retirement Income',
      uncommon: 'Uncommon Income',
    },
  },
  AccountsReview: {
    section: 'Assets and Interest',
    defaultStep: 'Financial assets and interest income',
    categoryMap: {
      bank_account: 'Bank accounts',
      insurance: 'Insurance Policy',
      investment: 'Investment accounts',
      stocks: 'Publicly traded investments',
      self_investment: 'Other self-directed investments',
      other: 'Other less common assets',
    },
  },
  RealEstateReview: {
    section: 'Washington Real Estate',
    defaultStep: 'Real Estate Ownership',
  },
  CategoricalExemption: {
    section: 'Washington Real Estate',
    defaultStep: 'Washington Real Estate',
  },
  CreditorReview: {
    section: 'Debt',
    defaultStep: 'Debt',
  },
  BusinessReview: {
    section: 'Business Interests',
    defaultStep: 'Business and Organizational Interests',
    categoryMap: {
      owner: 'Businesses owned',
      officer: 'Other relationships',
    },
  },
  LobbyingIndication: {
    section: 'Lobbying',
    defaultStep: 'Lobbying',
  },
};

/**
 * Provide API's submission draft state in exchange for the internal step identifier (section and step titles).
 * @param {Object} draftState API's submission draft state.
 * @returns {{sectionTitle: string, stepTitle: string}} Wizard's internal step identifier
 */
export function getActiveStepFromDraft(draftState) {
  const step = draftState?.step;
  const state = draftState?.state;
  const definition = apiStepToSectionStepDictionary?.[step];

  if (!step || !definition) {
    console.error('Invalid step or definition:', { step, definition });
    return defaultStep;
  }

  const category = state?.category;

  if (!category) {
    const { section, defaultStep: step } = definition;
    return { sectionTitle: section, stepTitle: step };
  }

  return {
    sectionTitle: definition.section,
    stepTitle: definition?.categoryMap?.[category] ?? definition.defaultStep,
  };
}

/** Create a draftState object using legacy step names for the API */
export function getLegacyStepFromActiveStep({ sectionTitle, stepTitle }) {
  const draftState = {
    step: null,
    state: {
      category: null,
    },
  };

  const [sectionKey] = Object.entries(apiStepToSectionStepDictionary).find(
    ([, { section }]) => section === sectionTitle,
  );

  if (!sectionKey) {
    console.error('Invalid section title:', sectionTitle);
    return null;
  }

  draftState.step = sectionKey;

  const definition = apiStepToSectionStepDictionary[sectionKey];
  const category = Object.entries(definition.categoryMap ?? {}).find(
    ([, step]) => step === stepTitle,
  )?.[0];

  draftState.state.category = category ?? null;

  return draftState;
}
