import { noneOpt, relationCode } from 'src/pages/Disclosure/common/options.js';
import { computed } from 'vue';

/**
 * @module utils/api
 * @desc Utility functions for binding wizard data to the financial
 * affairs API
 */

export function createStdHasCtgryBinding(payload, sectionName, categoryName) {
  const categoryObject = findOrCreateApiCategory(
    payload,
    sectionName,
    categoryName,
  );

  console.log('categoryObject', categoryObject);

  return computed({
    get: () => categoryObject?.value,
    set: (value) => {
      categoryObject.value = value;
    },
  });
}

/**
 * Find a category object within an array of categories, creating and inserting it if it doesn't exist
 * @param {Object} payload Object to traverse
 * @param {Array} keyChain Sequence of nested keys to use to traverse payload
 * @param {String} category Category value (e.g. 'employment') to find or create
 * @returns {{ category: string, value: boolean | null }} Reference to category object
 */
export function findOrCreateApiCategory(payload, sectionName, categoryName) {
  if (!payload?.statement?.[sectionName]) {
    payload.statement = { ...payload.statement, [sectionName]: {} };
    payload.statement[sectionName].categories = [];
  }
  const categoryArray = payload.statement[sectionName].categories;

  let categoryObject = categoryArray.find(
    (catObj) => catObj.category === categoryName,
  );

  if (!categoryObject) {
    const newIndex =
      categoryArray.push({
        category: categoryName,
        value: null,
      }) - 1;

    return categoryArray[newIndex];
  }

  return categoryObject;
}

/**
 * Factory for creating an object with get, add, and remove functions
 * expected by the wizard for template fields (arrays of field objects)
 * @param {String} sectionName Section name (e.g. 'income')
 * @param {String} categoryName Category name (e.g. 'employment')
 * @returns {Object}
 * - get(payload): Get function to retrieve data for a specific category
 * - getOne(payload, index): GetOne function to retrieve a single category object
 * - add(payload): Add function to insert a new category object
 * - remove(payload, index): Remove function to delete a category object
 */
export function createCategoryDataApiBinds(sectionName, categoryName) {
  const targetObj = (payload) =>
    traverseObject(payload, ['statement', sectionName, 'data']);

  return {
    get: (payload) => {
      return targetObj(payload)?.filter((ic) => ic.category === categoryName);
    },
    getOne: (payload, index) => {
      return targetObj(payload)?.filter((ic) => ic.category === categoryName)[
        index
      ];
    },
    add: (payload) => {
      payload.statement[sectionName].data ??= [];
      payload.statement[sectionName].data.push({
        category: categoryName,
      });
    },
    remove: (payload, index) => {
      const arrayHasElements = targetObj(payload)?.length > 0;

      if (!arrayHasElements) {
        return console.error(
          `Attempted to remove element at index ${index} from empty categoryName array`,
        );
      }

      const filteredArray = targetObj(payload).filter(
        (ic) => ic.category === categoryName,
      );

      const element = filteredArray[index];
      const originalIndex = targetObj(payload).indexOf(element);

      targetObj(payload).splice(originalIndex, 1);
    },
  };
}

export function createRealEstateArrayBinds() {
  const accessKey = 'real_estate';
  const targetArray = (payload) =>
    traverseObject(payload, ['statement', accessKey]);

  return {
    get: (payload) => targetArray(payload),
    getOne: (payload, index) => {
      return targetArray(payload)[index];
    },
    add: (payload) => {
      payload.statement[accessKey] ??= [];
      payload.statement[accessKey].push({});
    },
    remove: (payload, index) => {
      targetArray(payload).splice(index, 1);
    },
  };
}

/** Old client uses empty string ('') for none (as in, no income/no value), new client uses 'none' */
export function createAssetApiBinding(element) {
  const setNone = (value) => (value === '' ? noneOpt.value : value);
  return computed({
    get: () => {
      return {
        value: setNone(element.value),
        income: setNone(element.income),
        nature: element.nature,
      };
    },
    set: (options) => {
      element.value = options.value;
      element.income = options.income;
      element.nature = options.type;
    },
  });
}

/** Standard reusable to bind to 'who' values, e.g. for `heldBy` opt array */
export function createStdWhoApiBinding(element) {
  return createOptionsApiBinding({
    element,
    key: 'who',
    mapFn: (whoVal) => relationCode.find((rc) => rc.value === whoVal),
  });
}

/**
 * Factory for binding option arrays for select fields to an API array
 * @example
 * ```javascript
 * // In a field object
 * {
 *   name: 'heldBy',
 *   api: createOptionsApiBinding({
 *    element,
 *    key: 'who',
 *    mapFn: (whoVal) => whoOptions.find((o) => o.value === whoVal)
 *   });
 * }
 * ```
 */
export function createOptionsApiBinding({ element, key, mapFn = null }) {
  return computed({
    get: () =>
      !!mapFn && !!element[key] ? element[key].map(mapFn) : element[key],
    set: (selections) => {
      const validSelections = selections.filter(
        (opt) => opt && opt.value && opt.label,
      );
      element[key] = validSelections.map((o) => o.value);
    },
  });
}

/** Traverse an object using a sequence of keys, returning the value at the end of the chain */
function traverseObject(obj, keyChain) {
  return keyChain.reduce((acc, key) => acc[key], obj);
}
