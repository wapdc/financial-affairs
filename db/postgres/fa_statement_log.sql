create table fa_statement_log
(
    log_id           serial  not null
        constraint fa_statement_log_pkey
            primary key,
    fa_statement_id     integer,
    submission_id       integer,
    person_id           integer,
    repno               integer,
    action              varchar(255),
    message             varchar,
    user_name           varchar,
    updated_at          timestamp with time zone
);