import { formatDate } from '@wapdc/pdc-ui/util';
import { sortDate } from 'src/library/date.util';

export const legacyColumns = [
  {
    name: 'filerId',
    label: 'Filer ID',
    required: true,
    align: 'left',
    field: 'filerId',
    sortable: true,
  },
  {
    name: 'Submitted',
    label: 'Submitted',
    required: true,
    align: 'left',
    field: 'submitted',
    sortable: true,
    format: (val) => formatDate(val),
    sort: sortDate,
  },
];
