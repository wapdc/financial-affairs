export function deepClone(obj) {
  if (obj === null || typeof obj !== 'object') {
    return obj;
  }

  if (Array.isArray(obj)) {
    return obj.map(deepClone);
  }

  return Object.fromEntries(
    Object.entries(obj).map(([key, value]) => [key, deepClone(value)]),
  );
}

export function shallowClone(obj) {
  return JSON.parse(JSON.stringify(obj));
}

/** Traverse an object using a sequence of keys, returning the value at the end of the chain */
export function traverseObject(obj, keyChain) {
  return keyChain.reduce((acc, key) => acc[key], obj);
}

/** Get all nested arrays with a given key in an object */
export function collectNestedArraysWithKey(obj, targetKey, path = []) {
  let results = [];
  for (let key in obj) {
    const newPath = [...path, key];
    if (key === targetKey && Array.isArray(obj[key])) {
      results.push({ path: newPath.join('.'), value: obj[key] });
    }
    if (obj[key] && typeof obj[key] === 'object') {
      results = results.concat(
        collectNestedArraysWithKey(obj[key], targetKey, newPath),
      );
    }
  }
  return results;
}
