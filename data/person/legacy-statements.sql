select
  d.form_type as type,
  p.filer_id as filer_id,
  d.date_received as submitted_at,
  d.batch_number,
  d.batch_number as repno,
  d.docid,
  case
    when d.append ilike '%EFILE%' then 'electronic'
    else 'paper'
  end as filing_type,
  d.append
from entity p join ax_document d on
    d.filer_id = p.filer_id
where d.form_type in  ('F1', 'F1A', 'F1 AMENDED', 'F1A AMENDED')
  and d.election_year >= 2014
  and entity_id = :person_id
order by submitted_at desc