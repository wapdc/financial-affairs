import { publicPositionsSection } from './publicPositions';

const keys =
  publicPositionsSection.steps.publicPositionsStep.fields
    .publicOfficePositionArray.template;

const office = {
  key: keys.officeName.as,
  label: 'Office',
  align: 'left',
  sortable: false,
  width: '30%',
  responsiveBehavior: {
    xs: { width: '33%' },
  },
};
const jurisdiction = {
  key: keys.jurisdictionName.as,
  label: 'Jurisdiction',
  align: 'left',
  sortable: false,
  width: '25%',
  responsiveBehavior: {
    xs: { occlude: true },
  },
};
const position = {
  key: keys.positionName.as,
  label: 'Position',
  align: 'left',
  sortable: false,
  width: '20%',
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};
const startDate = {
  key: keys.startDate.as,
  label: 'Start Date',
  align: 'left',
  sortable: false,
  width: '100px',
};
const endDate = {
  key: keys.endDate.as,
  label: 'End Date',
  align: 'left',
  sortable: false,
  width: '100px',
  responsiveBehavior: {
    sm: { occlude: true },
  },
};

export default [office, jurisdiction, position, startDate, endDate];
