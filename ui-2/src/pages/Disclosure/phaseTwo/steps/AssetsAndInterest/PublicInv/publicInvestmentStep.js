import { assetArrayValidator } from '../common/assetValidator';
import { CategoryHandler } from 'src/utils/CategoryHandler.class';
import rules from 'src/utils/rules';
import useWizard from 'src/wizard/useWizard';

const { lazyLoadWrapper, dataPath, getData } = useWizard();

const accountCategoryHandler = new CategoryHandler('accounts', 'stocks');

export const publicInvestmentStep = {
  title: 'Publicly traded investments',
  order: 4,
  component: lazyLoadWrapper(() =>
    import('../PublicInv/PubliclyTradedInvestments.vue'),
  ),
  fields: {
    hasPubliclyTradedInvestments: {
      rules: [rules.required],
      blocking: false,
      data() {
        return dataPath(this, 'pendingTransform.hasPubliclyTradedInvestments');
      },
      transform: (field) => ({
        writeOut: () => accountCategoryHandler.writeOutHas(field),
        readIn: () => accountCategoryHandler.readInHas(field),
      }),
    },
    institutionArray: {
      blocking: false,
      data() {
        return dataPath(this, 'pendingTransform.institutionArray');
      },
      transform: (field) => ({
        writeOut: () => accountCategoryHandler.writeOutData(field),
        readIn: () => accountCategoryHandler.readInData(field),
      }),
      requiredIf: () =>
        [true, 'true'].includes(
          getData().pendingTransform.hasPubliclyTradedInvestments,
        ),
      template: {
        heldBy: {
          as: 'who',
          rules: [rules.requiredNonEmptyArray],
        },
        natureOfHoldings: {
          as: 'name',
          rules: [rules.requiredString],
        },
        investmentArray: {
          as: 'assets',
          rules: [
            rules.requiredNonEmptyArray,
            (v) => assetArrayValidator(v, ['name']),
          ],
        },
      },
    },
  },
};
