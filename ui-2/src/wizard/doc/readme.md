# Wizard Readme

! This is a work in progress and may not be complete or accurate.

## Defining form sections, steps, and fields

The form is defined using a basic JavaScript object. The wizard expects a certain hierarchy and supports a set of properties for each element:

- Fields are properties of steps
- Steps are properties of sections
- An array of sections is passed to useWizard to create a form object with state, validation, and navigation handling

Example:

```javascript
import { rules, lazyLoadWrapper } from "src/wizard/defineWizardForm";

const exampleSection = {
  title: "Example Section",
  steps: {
    firstStep: {
      title: "First Step",
      component: lazyLoadWrapper(() => import("./FirstStep.vue")),
      fields: {
        firstField: {
          rules: [rules.required],
        },
        secondField: {
          rules: [rules.required, rules.maxLength(100)],
        },
      },
    },
    secondStep: {
      title: "Second Step",
      component: lazyLoadWrapper(() => import("./SecondStep.vue")),
      fields: {
        thirdField: {
          rules: [rules.required],
        },
      },
    },
  },
};
```

> The idea is to define the form structure in a way that is easy to read and understand. The wizard form definition is a simple object that can be easily modified and is extensible. The wizard will not use the object for direct data storage, but will assign `meta` properties to each step and field. If the step or field objects have a `meta` property, there may be conflicts.

### Defining a Section

A **section** can contain one or multiple steps. The wizard expects an object with the following properties:

- **title**: The title of the section; must be unique per form
- **steps**: An object containing steps as properties

### Defining a Step

A **step** represents a single stage or view. The wizard expects an object with the following properties:

- **title**: The title of the step; must be unique per section
- **component**: A Vue component that will be dynamically loaded when the step is active. Use `lazyLoadWrapper`, which returns the component marked as 'raw' and defined as an asynchronous component (per Vue best practices).
- **fields** (optional): An object containing fields as properties

### Defining a Field

A **field** represents a single input field, or in some cases, an array of inputs. The wizard expects an object with the following properties:

- **name**: The unique name of the field.
- **rules**: An array of validation rules that apply to the field.

#### More supported field properties

- **name**: The unique (per step) name of the field, used to access the field's state
- **rules**: An array of validation callbacks `<Function<value: any>: string>` that return an error message if the validation fails
  to apply to the field's value
- **blocking**: A boolean indicating whether the field is required to progress to the next step. Defaults to `false`.
- **initialValue**: The initial value of the field. Defaults to `null`. This can be either a function which accepts the form context and returns the initial value, or, if not a function, the initial value itself.
- **api**: See the [API Integration Reference](./api.md) for information about accepting, modifying, and sending payloads.
- **template**: An object containing child fields as properties. This is used to define nested fields or arrays of fields. It is a 'template' in the sense that it defines the structure of the fields, but the actual fields are created dynamically based on the template.

#### Field Values

At the instantiation of the wizard, the field values are initialized to their `initialValue`.

If `setApiPayload` is used, field's `api` property is used to update the field value from the given object. The payload is kept in sync with the field value based on the `api` property as well. See the [API Integration Reference](./api.md) for information about accepting, modifying, and sending payloads.

The wizard adds a `value` property to each field object, which is a reactive Vue proxy that holds the current value of the field.

This value can be accessed with:

- `wizard.activeFields.fieldName.value`,
- `wizard.activeStep.fields.fieldName.value`,
- or `wizard.findField({sectionName, stepName, fieldName}).value`.

And it can be updated with:

- `wizard.activeFields.fieldName.value = newValue`,
- `wizard.activeStep.fields.fieldName.value = newValue`,
- or `wizard.findField({sectionName, stepName, fieldName}).value = newValue`.

## Defining Form and Step Components

The wizard form and step components are defined as Vue components. The wizard form component is a container for the wizard steps, and the wizard step components are the individual views that are loaded dynamically as the user progresses through the form.

`useWizard` is imported into both. If useWizard is not given a form object as an argument, it will return functions and reactive properties that can be used to interact with the wizard. If useWizard is given a form object, it returns an instantiated wizard object with the form state, validation, and navigation handling.

## Slotting components

The wizard core is decoupled from the form components. `WizardActions` or `WizardTableOfContents` can be replaced with custom components using `table-of-contents` and `wizard-actions` slots:

```vue
<template>
  <wizard-container
    v-bind="wiz"
    @on-step-change="() => console.log('Step changed')"
  >
    <!-- Custom Table of Contents -->
    <template #table-of-contents>
      <div class="custom-toc">
        <h3>Custom Table of Contents</h3>
        <ul>
          <!-- Custom ToC items -->
        </ul>
      </div>
    </template>

    <!-- Custom Actions Toolbar -->
    <template #wizard-actions>
      <div class="custom-actions">
        <!-- Custom actions -->
      </div>
    </template>
  </wizard-container>
</template>
```

#### Slotted primary actions in default wizard actions

You can also slot in primary actions in the default wizard actions component:

```vue
<template>
  <wizard-container v-bind="wiz" @on-step-change="handleStepChange">
    <template #wizard-actions>
      <wizard-actions
        v-bind="wiz"
        :show-next="!isLastStep"
        @on-step-change="handleStepChange"
      >
        <!-- Use the primary slot to conditionally show a submit button -->
        <template v-if="isLastStep" #primary>
          <SubmitButton
            :disabled="!readyToSubmit"
            @on-activate="handleSubmit"
          />
        </template>
      </wizard-actions>
    </template>
  </wizard-container>
</template>
```
