import libRules from '@wapdc/pdc-ui/rules';

/**
 * Validation rules
 * @property {Function} required - Value must be present
 * @property {Function} requiredString - Value must be present and not just whitespace
 * @property {Function} minLength - Value must be at least a certain length; Curried function
 * accepts the minimum length as an argument, and returns a function that accepts the value to validate
 * @property {Function} exactLength - Value must be exactly a certain length; Curried function
 * accepts the exact length as an argument, and returns a function that accepts the value to validate
 * @property {Function} numbersOnly - Value must contain only numbers
 * @property {Function} phoneNumber - Value must be a valid phone number
 * @property {Function} positiveAmount - Value must be a positive number
 * @property {Function} email - Value must be a valid email address
 * @property {Function} state - Value must be a valid US state abbreviation (e.g. 'WA', 'NY', 'ME')
 */
export default {
  alphbetical,
  alphaNumeric,
  constrainNumber,
  email: libRules.validEmail,
  exactLength,
  minLength,
  maxLength,
  numbersOnly,
  phoneNumber,
  positiveAmount: libRules.positiveAmount,
  required,
  requiredString,
  requiredNonEmptyArray,
  requiredOptionObject,
  state,
};

function alphbetical(value) {
  return (
    /^[A-Za-zÀ-ÖØ-öø-ÿ\s\-'.]*$/.test(value) ||
    'Please enter letters, periods, and hyphens only'
  );
}

function alphaNumeric(value) {
  return (
    /^[A-Za-zÀ-ÖØ-öø-ÿ0-9\s\-'.]*$/.test(value) ||
    'Please enter letters, numbers, periods, and hyphens only'
  );
}

function constrainNumber(min, max) {
  return (value) =>
    (value >= min && value <= max) ||
    `Please enter a number between ${min} and ${max}`;
}

function required(value) {
  return ![null, undefined].includes(value) || 'Required';
}

function requiredString(value) {
  return (typeof value === 'string' && value?.trim().length > 0) || 'Required';
}

function requiredNonEmptyArray(value) {
  return value?.length > 0 || 'Required';
}

function requiredOptionObject(value) {
  return ![undefined, null, ''].includes(value?.value) || 'Required';
}

function minLength(min) {
  return (value) =>
    value.length >= min || `Please enter at least ${min} characters`;
}

function maxLength(max) {
  return (value) =>
    value?.length <= max || `Please enter no more than ${max} characters`;
}

function exactLength(length) {
  return (value) =>
    value?.length === length || `Please enter exactly ${length} characters`;
}

function numbersOnly(value) {
  return /^\d+$/.test(value) || 'Please enter numbers only';
}

function phoneNumber(phoneNumber) {
  return (
    /^(\+?1[-\s]?)?(\(?\d{3}\)?)[-\s.]?\d{3}[-\s.]?\d{4}$/.test(phoneNumber) ||
    'Valid formats: 3215551234 or 321-555-1234'
  );
}

function state(value) {
  return (
    libRules.states.includes(value) ||
    'Please enter a valid US state abbreviation (e.g. "WA" or "CA")'
  );
}
