select sub.submitted_at, fs.submission_id
from fa_statement fs
       join fa_submission sub on fs.statement_id = sub.fa_statement_id
       where fs.statement_id = :statement_id
order by sub.submitted_at