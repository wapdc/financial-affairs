import { defineAsyncComponent, markRaw, ref } from 'vue';
import { validateFieldSet } from './template';
/**
 * @module wizard/util/step
 * @description Validation and metadata logic for wizard steps
 */

/** Set field.meta.isValid and isBlocking for field */
export function validateField(field) {
  if (field.template) {
    validateFieldSet(field);
    return;
  }

  const fieldHasRules = field?.rules?.length > 0;
  let isValid = fieldHasRules ? true : false;

  if (fieldHasRules) {
    const fieldValue = field?.data?.()?.value;

    isValid = field.rules
      .filter((rule) => typeof rule === 'function')
      .every((rule) => rule(fieldValue) === true);
  } else if (typeof field.requiredIf === 'function') {
    const isRequired = field.requiredIf();
    const fieldValueExists = field?.data?.()?.value;

    isValid = isRequired ? fieldValueExists : true;
  }

  let isBlocking = isValid ? false : field.blocking === true;

  field.meta.isValid = isValid;
  field.meta.isBlocking = isBlocking;
}

/**
 * Derive and set step validation state by validating all step fields
 */
export function _validateStep(step) {
  if (!step.fields) {
    step.meta.isValid = true;
    step.meta.isBlocking = false;
    return;
  }

  for (const field of Object.values(step.fields)) {
    validateField(field);
  }

  const fieldAsArray = Object.values(step.fields);

  step.meta.isValid = fieldAsArray.every((field) => field.meta.isValid);
  step.meta.isBlocking = fieldAsArray.some((field) => field.meta.isBlocking);
}

/** Initialize meta object for field recursively */
export function addMetaToFields(fields, stepRef) {
  for (const fieldName in fields) {
    const field = fields[fieldName];

    field.key = fieldName;

    field.meta ??= {
      isDirty: false,
      isValid: false,
      isBlocking: field.blocking === true,
      stepRef,
    };
  }
}

/** Initializes metadata to the steps and fields of a form */
export function addMetaToStepsAndFields(form) {
  if (!form) {
    return;
  }

  for (const [sectionIndex, section] of Object.entries(form)) {
    if (!section.steps) {
      continue;
    }

    section.meta ??= {
      isDirty: false,
      isValid: false,
      sectionIndex,
    };

    let stepIndex = 0;
    for (const step of Object.values(section.steps)) {
      step.meta ??= {
        isBlocking: true,
        isHidden: false,
        isDirty: false,
        isValid: false,
        sectionIndex: section.order,
        stepIndex: stepIndex++,
      };

      if (step.fields) {
        addMetaToFields(step.fields, ref(step));
      } else {
        step.meta.isValid = true;
        step.meta.isBlocking = false;
      }

      _validateStep(step);
    }
  }
}

/**
 * Wraps a callback that imports a Vue component, marking it as raw to prevent
 * Vue from proxying it inside the wizard state
 * @param {function(): Promise<ModuleType>} importCb Import callback for target component
 * @returns {ComponentPublicInstance} Lazy-loaded Vue component
 *
 * @example
 * ```
 * import { lazyLoadWrapper } from './defineWizardForm';
 * const MyComponent = lazyLoadWrapper(() => import('./MyComponent.vue'));
 * ```
 */
export function lazyLoadWrapper(importCb) {
  return markRaw(defineAsyncComponent(importCb));
}
