import { assetArrayValidator } from '../common/assetValidator';
import { CategoryHandler } from 'src/utils/CategoryHandler.class';
import rules from 'src/utils/rules';
import useWizard from 'src/wizard/useWizard';

const { lazyLoadWrapper, dataPath, getData } = useWizard();

const accountCategoryHandler = new CategoryHandler('accounts', 'insurance');

export const insurancePolicyStep = {
  title: 'Insurance Policy',
  order: 2,
  component: lazyLoadWrapper(() =>
    import('../InsurancePolicies/InsurancePolicy.vue'),
  ),
  fields: {
    hasInsurance: {
      rules: [rules.required],
      blocking: false,
      data() {
        return dataPath(this, 'pendingTransform.hasInsurance');
      },
      transform: (field) => ({
        writeOut: () => accountCategoryHandler.writeOutHas(field),
        readIn: () => accountCategoryHandler.readInHas(field),
      }),
    },
    insuranceArray: {
      blocking: false,
      data() {
        return dataPath(this, 'pendingTransform.insuranceArray');
      },
      transform: (field) => ({
        writeOut: () => accountCategoryHandler.writeOutData(field),
        readIn: () => accountCategoryHandler.readInData(field),
      }),
      requiredIf: () =>
        [true, 'true'].includes(getData().pendingTransform.hasInsurance),
      template: {
        heldBy: {
          as: 'who',
          rules: [rules.requiredNonEmptyArray],
        },
        insuranceName: {
          as: 'name',
          rules: [
            rules.requiredString,
            rules.maxLength(100),
            rules.alphbetical,
          ],
        },
        insurancePolicyArray: {
          as: 'assets',
          rules: [
            rules.requiredNonEmptyArray,
            (v) => assetArrayValidator(v, ['nature']),
          ],
        },
        streetAddress: {
          as: 'address',
          rules: [
            rules.requiredString,
            rules.maxLength(120),
          ],
        },
        city: {
          rules: [
            rules.requiredString,
            rules.alphbetical,
            rules.maxLength(120),
          ],
        },
        state: {
          rules: [rules.required, rules.state],
        },
        zip: {
          as: 'postcode',
          rules: [rules.required, rules.exactLength(5), rules.numbersOnly],
        },
      },
    },
  },
};
