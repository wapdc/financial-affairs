import { CategoryHandler } from 'src/utils/CategoryHandler.class';
import rules from 'src/utils/rules';
import useWizard from 'src/wizard/useWizard';

const { lazyLoadWrapper, dataPath, getData } = useWizard();
const incomeCategoryHandler = new CategoryHandler('income', 'uncommon');

export const uncommonIncomeStep = {
  title: 'Uncommon Income',
  order: 3,
  component: lazyLoadWrapper(() => import('./UncommonIncome.vue')),
  fields: {
    hasUncommonIncome: {
      rules: [rules.requiredString],
      data() {
        return dataPath(this, 'pendingTransform.hasUncommonIncome');
      },
      transform: (field) => ({
        writeOut: () => incomeCategoryHandler.writeOutHas(field),
        readIn: () => incomeCategoryHandler.readInHas(field),
      }),
    },
    uncommonIncomeArray: {
      blocking: false,
      data() {
        return dataPath(this, 'pendingTransform.uncommonIncomeArray');
      },
      transform: (field) => ({
        writeOut: () => incomeCategoryHandler.writeOutData(field),
        readIn: () => incomeCategoryHandler.readInData(field),
      }),
      requiredIf: () =>
        [true, 'true'].includes(getData().pendingTransform.uncommonIncomeArray),
      template: {
        receivedBy: {
          as: 'who',
          rules: [rules.requiredNonEmptyArray],
        },
        sourceName: {
          as: 'name',
          rules: [
            rules.requiredString,
            rules.maxLength(100),
          ],
        },
        natureOfIncome: {
          as: 'why',
          rules: [rules.requiredString],
        },
        compensation: {
          as: 'income',
          rules: [rules.required],
        },
        streetAddress: {
          as: 'address',
          rules: [
            rules.requiredString,
            rules.maxLength(120),
          ],
        },
        city: {
          rules: [
            rules.requiredString,
            rules.alphbetical,
            rules.maxLength(120),
          ],
        },
        state: {
          rules: [rules.required, rules.state],
        },
        zip: {
          as: 'postcode',
          rules: [rules.required, rules.exactLength(5), rules.numbersOnly],
        },
      },
    },
  },
};
