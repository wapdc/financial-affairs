/** Validates the asset array, returning an array of validation results */
export function assetArrayValidator(assetArray, keys) {
  const errorMsg = 'Missing required asset values';

  if (!assetArray || assetArray.length === 0) {
    return errorMsg;
  }

  const validations = [];
  for (let i = 0; i < assetArray.length; i++) {
    const asset = assetArray[i];
    const assetValidationResults = (validations[i] = {});

    assetValidationResults.hasError = false;

    const requiredMsg = () => {
      assetValidationResults.hasError = true;
      return 'Required';
    };

    [...keys, 'value', 'income'].forEach((prop) => {
      const property = asset[prop];
      if (property) {
        assetValidationResults[prop] =
          property.trim().length > 1 || requiredMsg();
      } else assetValidationResults[prop] = requiredMsg();
    });
  }

  return validations.every((v) => !v.hasError) || errorMsg;
}
