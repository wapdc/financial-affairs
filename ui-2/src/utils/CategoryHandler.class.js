import useWizard from 'src/wizard/useWizard';

/**
 * Syncronize the data between the form objects and the API for sections
 * using the category and data structure:
 * ```
 *    statementSection: {
 *       categories: { category: <string>, value: <boolean> }[],
 *       data: { category: <string>, ...data }[],
 *    },
 * ```
 */
export class CategoryHandler {
  constructor(statementSection, categoryName) {
    this.statementSection = statementSection;
    this.categoryName = categoryName;

    this.wizard = useWizard();
  }

  /** Create the category object as the API expects */
  createCategoryObject(value) {
    return {
      category: this.categoryName,
      value,
    };
  }

  /** Write the 'has' field value to the wizard data as the API expects */
  writeOutHas(hasField) {
    const categoryObject = this.wizard
      .getData()
      .statement[this.statementSection].categories.find(
        (c) => c.category === this.categoryName,
      );

    // Cast strings to boolean, but keep null values as null
    const formValue = hasField.data().value;
    const apiValue = [null, 'null', undefined].includes(formValue)
      ? null
      : Boolean(formValue === 'true');

    if (categoryObject) {
      categoryObject.value = apiValue;
      return;
    }

    this.wizard
      .getData()
      .statement[this.statementSection].categories.push(
        this.createCategoryObject(apiValue),
      );
  }

  /** Read the 'has' field value from the API data and set it to the field */
  readInHas(hasField) {
    const categoryObject = this.wizard
      .getData()
      .statement[this.statementSection].categories.find(
        (c) => c.category === this.categoryName,
      );

    if (categoryObject) {
      const v = categoryObject.value;
      hasField.data().value = [null, undefined, 'null'].includes(v)
        ? null
        : String(categoryObject.value);
    }
  }

  /** Write the array data to the payload as the API expects */
  writeOutData(arrayField) {
    const syncData = (mappedData) => {
      const newData = this.wizard
        .getData()
        .statement[this.statementSection].data.filter(
          (d) => d.category !== this.categoryName,
        );

      if (mappedData) newData.push(...mappedData);

      this.wizard.getData().statement[this.statementSection].data = newData;
    };

    const sourceArrayData = arrayField.data().value;

    if (!sourceArrayData?.length) {
      // Make sure the data is removed from the wizard data or deleting the field will not remove the data
      syncData(null);
      return;
    }

    const mappedData = sourceArrayData.map((data) => {
      return {
        ...data,
        category: this.categoryName,
      };
    });

    if (!this.wizard.getData().statement[this.statementSection].data?.length) {
      this.wizard.getData().statement[this.statementSection].data = mappedData;

      return;
    }

    syncData(mappedData);
  }

  /** Read the array data from the API data and set it to the field */
  readInData(arrayField) {
    const sourceArrayData =
      this.wizard.getData().statement[this.statementSection].data;

    if (!sourceArrayData?.length) {
      return;
    }

    const filteredArrayData = sourceArrayData.filter(
      (d) => d.category === this.categoryName,
    );

    arrayField.data().value = []; // Clear the array before adding refreshed data

    filteredArrayData.forEach((data) => {
      const fieldSetUtils = useWizard().getFieldSetUtils(arrayField);
      const newIndex = fieldSetUtils.add(); // Using the add method to instantiate the meta objects

      const fieldDataArrayUtils = fieldSetUtils.get(newIndex);

      Object.entries(data).forEach(([key, value]) => {
        const realKey = fieldDataArrayUtils.realKey(key);
        if (!realKey) return; // Skip if the key is not found in the template

        fieldDataArrayUtils.setValue(realKey, value);
      });
    });
  }
}
