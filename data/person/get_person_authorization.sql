-- Gets the most recent draft for each person_id.
-- Adds a psudo person with id = -1 and name set to the name from the most recent draft if the user has one.
select entity_id as person_id, name, submission_id from (
   SELECT e.entity_id,
          e.name,
          f.submission_id as submission_id,
          1               as ordering
   FROM entity_authorization ea
            JOIN entity e on ea.entity_id = e.entity_id
            left join (select fs.person_id, max(fs.submission_id) as submission_id
                       from fa_submission fs
                       where fs.submitted = false
                       and fs.amend_id is null
                       group by fs.person_id) f on f.person_id = e.entity_id
   WHERE uid = :uid
     AND realm = :realm
     AND role = 'owner'
   union
   select fs.person_id,
          coalesce(
                      nullif(
                              trim(cast(f.user_data as json) -> 'person' ->> 'name'),
                              '') || ' (Unverified)',
                      'New filer') as name,
          case
              when submitted = false then f.submission_id
              end                  as submission_id,
          0                        as ordering
   from (
            select -1 as person_id, max(submission_id) as submission_id
            from fa_submission fs
            where uid = :uid
              AND realm = :realm
              and person_id = -1
              AND (fs.submitted = false or (fs.submitted = true and fs.verified = false))) fs
            left join fa_submission f
                      on f.submission_id = fs.submission_id
) pas
ORDER BY ordering desc, name;