<?php


namespace Drupal\fin_affairs\Controller;

use Drupal;
use Drupal\wapdc_core\Controller\CoreControllerBase;
use Exception;
use stdClass;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WAPDC\Core\CoreDataService;
use WAPDC\Core\SendGrid\Drupal8CMS;
use WAPDC\Core\SendGrid\Mail;
use WAPDC\FinancialAffairs\FinancialAffairs;
use Symfony\Component\HttpFoundation\JsonResponse;
use WAPDC\FinancialAffairs\Validator;

class FinancialAffairsRestController extends CoreControllerBase
{

  /**
   * Submission REST API
   *
   * @param Request $request
   * @param int $submission_id
   *   The submission id of he submission to be operated on.
   * @param int $person_id
   * @param string $action
   *   The action to perform on the submission
   * @return JsonResponse
   *
   * @throws Exception
   */
  public function submissionAPI(Request $request, $submission_id=null, $person_id=-1, $action="") {
    $person_id = (int) $person_id;
    $request_data = new stdClass();
    $response_data = new stdClass();
    $response_data->errors = [];
    $response_data->success = TRUE;
    $parameters = $_GET;
    $method = $request->getMethod();
    $parameters['submission_id'] = $submission_id;
    $sp = FinancialAffairs::service()->getStatementProcessor();
    $pdc_user = $this->getCurrentUser();

    if ($method == 'POST' || $method == 'PUT') {
      $raw_data = file_get_contents('php://input');
      $request_data = json_decode($raw_data);
    }

    try {
       switch(TRUE) {
        case ($method == 'POST' && $action == 'person-match'):
          $response_data->matches = $sp->matchingPeople($submission_id);
          break;
        case ($method == 'POST' && $action == 'amendment' && !empty($submission_id)):
          $response_data = $sp->amendSubmission($submission_id, $pdc_user);
          $response_data->success = TRUE;
          break;
        case ($method == 'POST' && $action == 'validate-identity'):
          $validator = new Validator();
          $sp->validateIdentification($request_data, $validator);
          $response_data = new stdClass();
          $response_data->validation = $validator->getErrors();
          $response_data->success = TRUE;
          break;
        case ($method == 'POST' && $submission_id==""):
          $response_data = $sp->saveSubmission($request_data, $pdc_user);
          $response_data->success = TRUE;
          break;
        case ($method == 'PUT' && $action == 'submit'):
          $response_data = $sp->submitSubmission($request_data, $pdc_user, $submission_id);
          $response_data->success = TRUE;

          if ($response_data->submission->verified)  {
            $mail = new Mail(new Drupal8CMS());
            $mail->setTestModeEmail($this->currentUser->getEmail());
            $mail->setMessageTemplate('d-2ed24f36877e44409c031c7a0a4e1fe6');
            $email = $response_data->submission->certification_email ? $response_data->submission->certification_email : $this->currentUser->getEmail();
            $mail->addRecipient($email, '', ['repno' => $response_data->submission->submission_id]);
            $mail->send();
          }
          break;
        case ($method == 'PUT' && !empty($request_data->submission->import_from)):
          $response_data = $sp->carryForward($request_data, $request_data->submission->import_from, $pdc_user);
          $response_data->success = TRUE;
          break;
        case ($method == 'PUT'):
          $response_data->submission = $sp->saveSubmission($request_data, $pdc_user, $submission_id)->submission;
          $response_data->success = TRUE;
          break;
        case ($method == 'GET' && $action=="" && !$submission_id):
          $import_id = null;
          if (!empty($parameters['import_id'])) {
            $import_id = $parameters['import_id'];
          }
          $parms['current'] = 'Y';
          $response_data = $sp->draftSubmission($pdc_user, $person_id, $import_id);
          if ($person_id > 0 && !$response_data->submission->submission_id) {
            $parms['person_id'] = $person_id;
            $response_data->candidacies = FinancialAffairs::service()->personQuery('person_candidacies.sql', $parms);
            $response_data->offices = FinancialAffairs::service()->personQuery(('person_offices.sql'), $parms);
          }
          $response_data->success = TRUE;
          break;
        case ($method == 'GET' && $action=="" && $submission_id):
          $response_data = $sp->loadSubmissionDraft($submission_id);
          $response_data->success = TRUE;
          break;
        case ($method == 'GET' && $action == "view-submission" && $submission_id):
          $user_authorization = $sp->query('person/submission_authorization_uid_realm.sql', ['submission_id' => $submission_id, 'uid' => $pdc_user->uid, 'realm' => $pdc_user->realm]);
          $person_authorization = $sp->query('person/submission_authorization_person_id.sql', ['submission_id' => $submission_id, 'uid' => $pdc_user->uid, 'realm' => $pdc_user->realm]);

          /** @var \Drupal\user\Entity\User $user */
          $admin_user = $this->currentUser();
          if ($admin_user->hasPermission("access wapdc data") || count($user_authorization) > 0 || count($person_authorization) > 0) {
            $response_data = $sp->viewSubmission($submission_id);
            if ($response_data->submission->verified) {
              $parms['person_id'] = $response_data->submission->person_id;
              $parms['period_start'] = $response_data->statement->period_start;
              $parms['period_end'] = $response_data->statement->period_end;
              $response_data->candidacies = FinancialAffairs::service()->personQuery('updatedCandidacies.sql', $parms);
              $response_data->offices = FinancialAffairs::service()->personQuery(('updatedOffices.sql'), $parms);
              $response_data->submission->history = FinancialAffairs::service()->personQuery(('submission_history.sql'), ['statement_id' => $response_data->submission->fa_statement_id]);
            }
            $response_data->attachments = $sp->query('admin/attachments.sql', ['target_id' => $submission_id, 'target_type' => 'fa_submission']);
            $response_data->success = TRUE;
          } else {
            throw new Exception("Access Denied");
          }

          break;
        case ($method == 'GET' && $action == "prior-submission"):
          $response_data->last_submission = $sp->lastSubmission($pdc_user, $person_id);
          $response_data->success = TRUE;
          break;
        case ($method == 'POST' && $action=="get-owed"):
          $user = $this->getCurrentUser();
          $response_data->statements_owed = $sp->getStatementsOwed($request_data, $user, $sp->getDateOverride());
          break;
        case ($method == 'DELETE' && $submission_id):
          $user = $this->getCurrentUser();
          $sp->removeSubmission($submission_id, ""); //$user);
          $response_data->success = true;
          break;
        default:
          throw new Exception("Unsupported submission action $action with method $method");
      }
    }
    catch (Exception $e) {
      $response_data->error = $e->getMessage();
      $response_data->success = FALSE;
    }
    $response = new JsonResponse($response_data);
    return $response;
  }

  /**
   * @param Request $request
   * @param $person_id
   * @return JsonResponse
   * @throws Exception
   */
  public function personAPI(Request $request, $person_id) {
    $data = new stdClass();
    $data->errors = [];
    $data->success = TRUE;
    $parms = $_GET;
    $current_user = $this->getCurrentUser();
    $parms['current_user'] = $current_user->uid;
    $parms['current_realm'] = $current_user->realm;
    $parms['person_id'] = $person_id;
    $method = $request->getMethod();

    try {
      switch (TRUE) {
        case ($method == 'GET'):
          $data->person = CoreDataService::service()->getPersonProcessor()->getPerson($person_id);
          $data->statements = FinancialAffairs::service()->personQuery("statements.sql", $parms);
          $data->submissions = FinancialAffairs::service()->personQuery("prior_submission_history.sql", $parms);
          $data->candidacies = FinancialAffairs::service()->personQuery('person_candidacies.sql', $parms);
          $data->offices = FinancialAffairs::service()->personQuery('person_offices.sql', $parms);
          break;
        default:
          throw new Exception("Invalid person action");
      }
    }
    catch (Exception $e) {
      $data->errors[] = $e->getMessage();
      $data->success = FALSE;
    }
    $response = new JsonResponse($data);
    return $response;
  }

  public function auth_check(){
    $response = new stdClass();
    $http_status = 200;
    $response->errors = [];
    $response->success = TRUE;

    try {
      /** @var \Drupal\user\Entity\User $user */
      $user = $this->currentUser();
      $response->authenticated = !empty($user->id());
      $response->permissions = new stdClass();
      $response->permissions->enter_wapdc_data = $user->hasPermission("enter wapdc data");
      $response->permissions->access_wapdc_data = $user->hasPermission("access wapdc data");
      $response->permissions->fin_affairs = $user->hasPermission("fin affairs");
      // Make sure PDC user is registered.
      $this->getCurrentUser();
      if(!empty($user->id())){
        $response->email = $user->getEmail();
      }
    }
    catch (Exception $exception) {
      $response->errors['exception.unknown'] = $exception->getMessage();
      Drupal::logger('pdc_financial_affairs')->error("An error has occurred: " . $exception->getMessage());
      $response->success = FALSE;
      $http_status = 500;
    }
    try {
      if (!empty($user->id())) {
        $pdc_user = $this->getCurrentUser();
        $response->authorizations = FinancialAffairs::service()->getUserAuthorizations($pdc_user);
        if (sizeof($response->authorizations->person)==0) {
          FinancialAffairs::service()->getStatementProcessor()->autoGrantPersonAccess($pdc_user);
          $response->authorizations = FinancialAffairs::service()->getUserAuthorizations($pdc_user);
        }
      }
    }
    catch (Exception $exception) {
      Drupal::logger('pdc_financial_affairs')->error("An error has occurred: " . $exception->getMessage());

    }
    return new JsonResponse($response, $http_status);
  }

  /**
   * History API
   * @param Request $request
   * @param $person_id
   * @throws Exception
   * @return JsonResponse
   */
  public function historyAPI(Request $request) {
    $person_id = $_GET['person_id'] ?? null;
    $response_data = new stdClass();
    $response_data->errors = [];
    $response_data->success = TRUE;
    $http_status = 200;
    $person = null;
    $method = $request->getMethod();
    $sp = FinancialAffairs::service()->getStatementProcessor();
    try {
      $user = $this->getCurrentUser();
      /** @var \Drupal\user\Entity\User $user */
      $drupal_user = $this->currentUser();
      if ($person_id) {
        if (!$drupal_user->hasPermission("access wapdc data")) {
          $person = $sp->query('person/get_authenticated_filer_id.sql', ['person_id' => $person_id]);
        } else {
          $person = $sp->query('person/person.sql', ['person_id' => $person_id]);
        }
      }
      switch(TRUE) {
        case ($method == 'GET'):
          $sp->getHistoricalSubmissions($response_data, $user, $person_id);
          break;
        default:
      }
    } catch (Exception $exception) {
      $response_data->errors = $exception->getMessage();
      $response_data->success = FALSE;
      $http_status = 500;
    }
    return new JsonResponse($response_data, $http_status);
  }

  /**
   * @throws Exception
   */
  public function requestAccess() {
    $raw_data = file_get_contents('php://input');
    $request_data = json_decode($raw_data);
    $response_data = new stdClass();
    $response_data->errors = [];
    $response_data->success = true;
    $mail = new Mail(new Drupal8CMS());
    $sp = FinancialAffairs::service()->getStatementProcessor();
    $sp->setMailer($mail);

    $recipient = '';

    if ($this->currentUser()->hasPermission("access wapdc data")) {
      $recipient = $this->currentUser()->getEmail();
    }

    try {
      $response_data->success = $sp->requestAccess($request_data->email, $recipient);
      $httpResponse = Response::HTTP_OK;
    } catch (Exception $e) {
      $response_data->success = false;
      $response_data->errors = $e->getMessage();
      $httpResponse = Response::HTTP_INTERNAL_SERVER_ERROR;
    }
    return new JsonResponse($response_data, $httpResponse);
  }

  /**
   * Statement API.
   *
   * @param int $statement_id
   * @return JsonResponse
   * @throws Exception
   */
  public function statementAPI($statement_id) {
    $response_data = new stdClass();
    $response_data->errors = [];
    $response_data->success = TRUE;
    $http_status = 200;
    $fa = FinancialAffairs::service();
    try {
      $response_data->statement = $fa->getStatementProcessor()->getStatement($statement_id);
    }
    catch (Exception $exception) {
      $response_data->error = $exception->getMessage();
      Drupal::logger('pdc_financial_affairs')->error("An error has occurred: " . $exception->getMessage());
      $response_data->success = FALSE;
      $http_status = 500;

    }
    return new JsonResponse($response_data, $http_status);
  }

  /**
   * General API
   * @param Request $request
   *   HTTP Request
   * @param $action
   *   Action to be checked.
   * @return JsonResponse
   * @throws \Exception
   */
  public function API(Request $request, $action) {
    $request_data = new stdClass();
    $response_data = new stdClass();
    $response_data->errors = [];
    $response_data->success = TRUE;
    $method = $request->getMethod();
    $parameters = $_GET;
    $fs = FinancialAffairs::service();
    $sp = $fs->getStatementProcessor();
    $pdc_user = $this->getCurrentUser();

    if ($method == 'POST' || $method == 'PUT') {
      $raw_data = file_get_contents('php://input');
      $request_data = json_decode($raw_data);
    }

    try {
      switch(TRUE) {
        case ($method == 'POST' && $action=="request-missing-statements"):
          $user = $this->getCurrentUser();
          $response_data->statements_owed = $sp->getStatementsOwed($request_data, $user, $sp->getDateOverride());
          break;
        case ($method == 'POST' && $action=="person-match"):
          $response_data->matches = $sp->matchingPeopleByUser($request_data);
          break;
        case ($method == 'GET' && $action=="drafts"):
          $person_id = $parameters['person_id'];
          $response_data->drafts = $sp->getDrafts($pdc_user, $person_id);
          $response_data->success = TRUE;
          break;
        case ($method == 'GET'):
          $response_data->data = $fs->personQuery("$action.sql", $_GET);
          break;
        default:
          throw new Exception("Unsupported financial affairs action $action with method $method");
      }
    }
    catch (Exception $e) {
      $response_data->error = $e->getMessage();
      $response_data->success = FALSE;
    }
    $response = new JsonResponse($response_data);
    return $response;
  }

  /**
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function validateAccessToken(){
    $validationResponse = new stdClass();
    $sp = FinancialAffairs::service()->getStatementProcessor();
    try {
      $json = file_get_contents('php://input');
      $data = json_decode($json);

      if (empty($data->token)) {
        throw new Exception('Missing token.');
      }

      $user = $this->getCurrentUser();
      $data->token = trim($data->token);

      $persons = $sp->validateRequestToken($user, $data->token);

      if (!$persons) {
        $data->error = 'Invalid token provided';
      } else {
        $validationResponse->personsAdded = $persons;
      }

      $validationResponse->success = false;
      $httpResponse = empty($data->error) ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST;
      $validationResponse->error = $data->error ?? null;

      if (empty($data->error)) {
        $validationResponse->success = true;
        $authorizationsAdded = new stdClass();
        $authorizationsAdded->person = $persons;
        $validationResponse->authorizationsAdded = $authorizationsAdded;

        $validationResponse->authorizations = FinancialAffairs::service()
          ->getUserAuthorizations($user);
      }
    } catch (Exception $e) {
      $validationResponse->success = FALSE;
      $httpResponse = Response::HTTP_INTERNAL_SERVER_ERROR;
      $validationResponse->error = $e->getMessage();
    }
    return new JsonResponse($validationResponse, $httpResponse);
  }

}
