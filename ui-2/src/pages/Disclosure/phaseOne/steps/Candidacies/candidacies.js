import { positionTemplate } from 'src/pages/Disclosure/common/positionTemplate';
import rules from 'src/utils/rules';
import useWizard from 'src/wizard/useWizard';

const { dataPath, lazyLoadWrapper, getData } = useWizard();

export const candidaciesSection = {
  title: 'Candidacies',
  order: 3,
  steps: {
    candidacies: {
      title: 'Candidacies',
      order: 0,
      component: lazyLoadWrapper(() => import('./Candidacies.vue')),
      fields: {
        hasCandidacy: {
          rules: [rules.required],
          blocking: false,
          data() {
            return dataPath(this, 'statement.isCandidate');
          },
        },
        candidaciesArray: {
          blocking: false,
          data() {
            return dataPath(this, 'candidacies');
          },
          requiredIf: () => getData().statement.isCandidate === 'true',
          template: {
            ...positionTemplate,
            electionId: {
              rules: [rules.required],
              blocking: false,
              as: 'election_code',
            },
            electionName: {
              rules: [rules.required],
              blocking: false,
              as: 'election',
            },
            startDate: {
              rules: [rules.required],
              blocking: false,
              as: 'campaign_start_date',
            },
          },
        },
      },
    },
  },
};
