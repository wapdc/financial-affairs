import useWizard from 'src/wizard/useWizard';
import rules from 'src/utils/rules';

const wizard = useWizard();
const { lazyLoadWrapper, dataPath, getData } = wizard;

export const giftsAndTravelStep = {
  title: 'Gifts and travel',
  order: 0,
  component: lazyLoadWrapper(() => import('./GiftsAndTravelStep.vue')),
  fields: {
    hasGifts: {
      rules: [rules.requiredString],
      blocking: false,
      data() {
        return dataPath(this, 'statement.has_gifts');
      },
      transform: (field) => ({
        readIn: () => readInBoolAsString(field, ['statement', 'has_gifts']),
        writeOut: () => writeOutStringAsBool(field),
      }),
    },
    gifts: {
      blocking: false,
      data() {
        return dataPath(this, 'statement.gifts');
      },
      requiredIf: () => getData().statement.has_gifts === 'true',
      template: {
        who: {
          as: 'who',
          rules: [rules.requiredNonEmptyArray],
        },
        dateReceived: {
          as: 'date_received',
          rules: [rules.requiredString],
        },
        donorName: {
          as: 'donor_name',
          rules: [rules.requiredString],
        },
        city: {
          rules: [rules.requiredString],
        },
        state: {
          rules: [rules.state],
        },
        description: {
          rules: [rules.requiredString],
        },
        monetaryValue: {
          as: 'value',
          rules: [rules.requiredString],
        },
      },
    },
  },
};

/** Write out a boolean value as a string in field transform */
function writeOutStringAsBool(field) {
  const fieldInputValue = field.data().value || null;
  const hasInput = ['true', 'false'].includes(fieldInputValue);

  const valueForApi = hasInput ? Boolean(field.data().value === 'true') : null;

  wizard.fieldRaw(field).set(valueForApi);
}

/** Read in a boolean value as a string in field transform */
function readInBoolAsString(hasField, keyChain) {
  const data = getData();

  const apiValue = keyChain.reduce((acc, key) => acc?.[key], data);

  hasField.data().value = [null, undefined, 'null'].includes(apiValue)
    ? null
    : String(apiValue);
}

export const giftsAndTravelSection = {
  title: 'Gifts and Travel',
  order: 7,
  steps: {
    giftsAndTravelStep,
  },
};
