<?php


namespace WAPDC\FinancialAffairs\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class FAStatement
 * @Entity
 * @Table(name="fa_statement")
 */
#[ORM\Entity]
#[ORM\Table(name: 'fa_statement')]
class FAStatement {
  /**
   * @Column @Id @GeneratedValue
   */
  #[ORM\Column]
  #[ORM\Id]
  #[ORM\GeneratedValue]
  public $statement_id;

  /**
   * @Column(type="integer")
   */
  #[ORM\Column(type: 'integer')]
  public $person_id;

  /**
   * @Column
   */
  #[ORM\Column]
  public $name;

  /**
   * @var \DateTime
   * @Column(type="date")
   */
  #[ORM\Column(type:'date')]
  public $period_start;

  /**
   * @var \DateTime
   * @Column(type="date")
   */
  #[ORM\Column(type: 'date')]
  public $period_end;

  /**
   * @var \DateTime
   * @Column(type="date")
   */
  #[ORM\Column(type: 'date')]
  public $retain_until;

  /**
   * @Column
   */
  #[ORM\Column]
  public $hold_for;

  /**
   * @var \DateTime
   * @Column(type="date")
   */
  #[ORM\Column(type: 'date')]
  public $first_filed_at;

  /**
   * @Column
   */
  #[ORM\Column]
  public $filer_id;

  /**
   * @Column
   */
  #[ORM\Column]
  public $email;

  /**
   * @Column(type="integer")
   */
  #[ORM\Column]
  public $repno;

  /**
   * @Column(type="integer")
   */
  #[ORM\Column(type: 'integer')]
  public $old_repno;

  /**
   * @var \DateTime
   * @Column(type="datetimetz")
   */
  #[ORM\Column(type: 'datetimetz')]
  public $updated_at;

  /**
   * @Column(type="integer")
   */
  #[ORM\Column(type: 'integer')]
  public $submission_id;
}