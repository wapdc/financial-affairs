import { assetArrayValidator } from '../common/assetValidator';
import { CategoryHandler } from 'src/utils/CategoryHandler.class';
import rules from 'src/utils/rules';
import useWizard from 'src/wizard/useWizard';

const { lazyLoadWrapper, dataPath, getData } = useWizard();

const accountCategoryHandler = new CategoryHandler(
  'accounts',
  'self_investment',
);

export const uncommonAssetsStep = {
  title: 'Other less common assets',
  order: 6,
  component: lazyLoadWrapper(() => import('../Uncommon/UncommonAssets.vue')),
  fields: {
    hasOtherAssets: {
      rules: [rules.required],
      blocking: false,
      data() {
        return dataPath(this, 'pendingTransform.hasOtherAssets');
      },
      transform: (field) => ({
        writeOut: () => accountCategoryHandler.writeOutHas(field),
        readIn: () => accountCategoryHandler.readInHas(field),
      }),
    },
    assetArray: {
      blocking: false,
      data() {
        return dataPath(this, 'pendingTransform.assetArray');
      },
      transform: (field) => ({
        writeOut: () => accountCategoryHandler.writeOutData(field),
        readIn: () => accountCategoryHandler.readInData(field),
      }),
      requiredIf: () => [true, 'true'].includes(getData().pendingTransform.hasOtherAssets),
      template: {
        heldBy: {
          as: 'who',
          rules: [rules.requiredNonEmptyArray],
        },
        assetName: {
          as: 'name',
          rules: [
            rules.requiredString,
            rules.maxLength(100),
            rules.alphbetical,
          ],
        },
        assetTypeArray: {
          as: 'assets',
          rules: [
            rules.requiredNonEmptyArray,
            (v) => assetArrayValidator(v, ['type']),
          ],
        },
        streetAddress: {
          as: 'address',
          rules: [
            rules.requiredString,
            rules.maxLength(120),
          ],
        },
        city: {
          rules: [
            rules.requiredString,
            rules.alphbetical,
            rules.maxLength(120),
          ],
        },
        state: {
          rules: [rules.required, rules.state],
        },
        zip: {
          rules: [rules.required, rules.exactLength(5), rules.numbersOnly],
        },
      },
    },
  },
};
