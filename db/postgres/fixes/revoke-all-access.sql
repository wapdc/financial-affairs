-- Use this query to remove entity authorization records from pdc staff
delete from entity_authorization where (uid, realm) in (select uid,realm from pdc_user where user_name ilike :user_name)
  and entity_id not in (select entity_id from entity where filer_id ilike 'test.%');

