import { formatMoney, formatDate } from '@wapdc/pdc-ui/util';
import { ABBREVIATIONS } from '../constants/abbreviations.js';

/**
 * Renders a list of owners into a human-readable format.
 * The function expects an array of owner strings like ['spouse', 'filer'].
 * @param {Array<string>} who An array of owner strings.
 * @param {Object} submission The submission object.  Used to get the filer's name.
 * @returns {string} A human-readable string with the list of owners.
 * @example
 * ```javascript
 * renderWho(['spouse', 'filer'], { parsedUserData: { person: { name: 'John Doe' } } });
 * // Returns 'Spouse, John Doe'
 * ```
 */
export function renderWho(who, submission) {
  const labeledOwners = who.map((owner) => {
    let text = owner;
    if (owner === 'filer') {
      text = submission?.parsedUserData?.person.name;
    } else {
      text = ABBREVIATIONS[owner] || owner;
    }
    return text;
  });

  return labeledOwners.join(', ');
}

/**
 * Parses an income range string into a human-readable format where the values are formatted as money.
 * The function expects an income range string like '10000-20000'. The minimum value is formatted as-is,
 * and the maximum value is treated as inclusive unless the minimum value is 0, in which case the maximum
 * value is treated as exclusive.
 *
 * @param {string} income - A string representing an income range (e.g., '10000-20000').
 * @returns {string|null} A human-readable formatted string with the income range, or null if the input is invalid.
 */
export function renderIncomeRange(income) {
  if (!income) return null;
  if (typeof income !== 'string') {
    console.warn('renderIncomeRange argument must be a string');
    return null;
  }

  let [min, max] = income.split('-');

  const positiveMin = min > 0;

  if (max) {
    max = positiveMin ? max + '.99' : +max + 1;
  }

  const left = positiveMin ? `${formatMoney(min)}` : 'Less than';
  const right = max ? `${formatMoney(max)}` : 'or more';
  const middle = positiveMin && max ? ' - ' : ' ';

  return `${left}${middle}${right}`;
}

/** Capitalizes the first letter of a string */
export function capitalize(str) {
  return str?.charAt(0).toUpperCase() + str?.slice(1);
}

/** Converts camelCase string to spaced words ('camelCase' -> 'Camel case') */
export function prettyFromCamelCase(str) {
  if (!str) return '';
  return str.replace(/([A-Z])/g, ' $1').replace(/^./, (s) => s.toUpperCase());
}

/** Formats a date string to a human-readable format */
export function formatPrettyDate(dateStr) {
  const options = { year: 'numeric', month: 'long', day: 'numeric' };
  return new Date(dateStr).toLocaleDateString('en-US', options);
}

/** Constrains number (converting type if needed) to given range */
export function constrainNumber(min, max, val) {
  const num = Number(val);
  if (isNaN(num)) return;

  const numFloor = Math.min(max, num);
  return Math.max(numFloor, min);
}

export { formatDate, formatMoney };
