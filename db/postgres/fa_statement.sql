CREATE TABLE fa_statement (
  statement_id SERIAL PRIMARY KEY,
  person_id INTEGER NOT NULL,
  name VARCHAR(255) NOT NULL,
  period_start DATE NOT NULL,
  period_end DATE NOT NULL,
  first_filed_at DATE,
  filer_id VARCHAR(12),
  email VARCHAR(255),
  repno INTEGER,
  old_repno INTEGER,
  updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NOW(),
  FOREIGN KEY (person_id) references person(person_id)
);
CREATE INDEX idx_fa_statement_person ON fa_statement(person_id);
CREATE INDEX idx_fa_statement_period ON fa_statement(period_start);
