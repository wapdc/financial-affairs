#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc <<EOF
ALTER TABLE fa_statement ADD COLUMN IF NOT EXISTS submission_id INTEGER;
EOF