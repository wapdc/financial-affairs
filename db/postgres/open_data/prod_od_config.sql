Delete from open_data_config where job_name = 'financial_affairs_statements' and stage = 'live';
insert into open_data_config (job_name, stage, config, is_enabled)
values ('financial_affairs_statements', 'live', '{
      "changes": [
        {
          "schema" : "public",
          "table": "fa_statement",
          "changes": ["insert", "update"],
          "columns": ["statement_id"],
          "event": "updateStatement"
        },
        {
          "schema" : "public",
          "table": "fa_statement",
          "changes": ["delete"],
          "columns": ["statement_id"],
          "event": "deleteStatement"
        },
        {
          "schema" : "public",
          "table": "candidacy",
          "changes": ["update", "insert"],
          "columns": ["fa_statement_id"],
          "event": "updateStatementFromCandidacy",
          "filter": {
            "fa_statement_id" : ".+"
          }
        }
      ],
      "events": {
        "updateStatement": {
          "type": "upsert",
          "dataset_id": "ehbc-shxw",
          "source": "select * from fa_open_data where id=$1",
          "criteria": ["statement_id"]
        },
        "deleteStatement": {
          "type": "delete",
          "dataset_id": "ehbc-shxw",
          "source": "select id where id=$1",
          "criteria": ["statement_id"]
        },
        "updateStatementFromCandidacy": {
          "type": "upsert",
          "dataset_id": "ehbc-shxw",
          "source": "select * from fa_open_data where id=$1",
          "criteria": ["fa_statement_id"]
        }
      }
    }', true);

Delete from open_data_config where job_name = 'fa_imaged_documents_and_reports_financial_affairs' and stage = 'live';
insert into open_data_config (job_name, stage, config, is_enabled)
values ('fa_imaged_documents_and_reports_financial_affairs', 'live', '{
      "changes": [
        {
          "schema" : "public",
          "table": "fa_statement",
          "changes": ["insert", "update"],
          "columns": ["statement_id"],
          "event": "updateStatement"
        },
        {
          "schema" : "public",
          "table": "fa_statement",
          "changes": ["delete"],
          "columns": ["statement_id"],
          "event": "deleteStatement"
        }
      ],
      "events": {
        "updateStatement": {
          "type": "upsert",
          "dataset_id": "j78t-andi",
          "source": "select * from od_imaged_documents_and_reports_financial_affairs where id = concat(cast($1 as text), ''.financial_affairs'')",
          "criteria": ["statement_id"]
        },
        "deleteStatement": {
          "type": "delete",
          "dataset_id": "j78t-andi",
          "source": "select id where id=\"$1.financial_affairs\"",
          "criteria": ["statement_id"]
        }
      }
    }', true)