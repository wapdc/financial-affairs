CREATE TABLE fa_submission (
  submission_id SERIAL PRIMARY KEY,
  realm VARCHAR(255) NOT NULL,
  uid VARCHAR(255) NOT NULL,
  user_data VARCHAR,
  updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NOW(),
  fa_statement_id VARCHAR,
  submitted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NOW(),
  verified BOOLEAN NOT NULL DEFAULT FALSE,
  certification_email VARCHAR(255),
  submitted BOOLEAN NOT NULL DEFAULT FALSE,
  published BOOLEAN NOT NULL DEFAULT FALSE
);
CREATE INDEX idx_fa_submission_uid ON fa_submission(uid);
CREATE INDEX idx_fa_submission_published ON fa_submission(published);