import rules from 'src/utils/rules';

export const positionTemplate = {
  officeId: {
    as: 'office_code',
    rules: [rules.required],
  },
  officeName: {
    as: 'office',
    rules: [rules.required],
  },
  jurisdictionId: {
    as: 'jurisdiction_id',
    rules: [rules.required],
  },
  jurisdictionName: {
    as: 'jurisdiction',
    rules: [rules.required],
  },
  positionId: {
    as: 'position_id',
  },
  positionName: {
    as: 'position',
  },
  startDate: {
    as: 'start_date',
    rules: [rules.required],
  },
  email: {
    rules: [rules.requiredString, rules.email, rules.maxLength(120)],
  },
  phone: {
    rules: [rules.required, rules.phoneNumber],
  },
  streetAddress: {
    as: 'address',
    rules: [rules.requiredString, rules.maxLength(120)],
  },
  city: {
    as: 'city',
    rules: [rules.requiredString, rules.alphbetical, rules.maxLength(120)],
  },
  state: {
    rules: [rules.required, rules.state],
  },
  zip: {
    as: 'postcode',
    rules: [rules.required, rules.exactLength(5), rules.numbersOnly],
  },
  isMultiPosition: {
    initialValue: false,
  },
};
