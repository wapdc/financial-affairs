<?php


namespace WAPDC\FinancialAffairs\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Profile
 *
 * @package WAPDC\FinancialAffairs\Model
 * @Entity
 * @Table(name="aspnetdb..aspnet_Profile")
 * @HasLifecycleCallbacks
 * @property string $FirstName;
 * @property string $LastName;
 * @property string $AccountStatus;
 * @property string $Filerid;
 * @property string $Street;
 * @property string $City;
 * @property string $Zip;
 * @property string $State;
 * @property string $Phone;
 * @property string $EmailAddress;
 * @property string $EmailM;
 * @property string $County;
 * @property string $Office;
 * @property string $Position;
 * @property string $Jurisdiction;
 * @property string $EndTerm;
 * @property string $BeginTerm;
 * @property string $FilingStatus;
 * @property string $ElectionMY;
 * @property string $Mandatory;
 * @property string $SignaturecardStatus;
 * @property string $user_id;
 */
#[ORM\Entity]
#[ORM\Table(name: 'aspnetdb..aspnet_Profile')]
#[ORM\HasLifecycleCallbacks]
class Profile {

  /**
   * @Id
   * @Column(name="UserId")
   */
  #[ORM\Column(name: 'UserId')]
  public $user_id;

  /**
   * @Column(name="PropertyNames")
   */
  #[ORM\Column(name: 'PropertyNames')]
  protected $_property_names;

  /**
   * @Column(name="PropertyValuesString")
   */
  #[ORM\Column(name: 'PropertyValuesString')]
  protected $_property_values = '';

  /**
   * @Column(name="PropertyValuesBinary")
   */
  #[ORM\Column(name: 'PropertyValuesBinary')]
  protected $_property_values_binary;


  protected $_custom_properties = [];

  /**
   * @Column(name="LastUpdatedDate")
   */
  #[ORM\Column(name: 'LastUpdatedDate')]
  public $updated_at;


  /**
   * @PrePersist
   * @PreFlush
   */
  #[ORM\PrePersist]
  #[ORM\PreFlush]
  public function encodeValues(){
    $property_names = '';
    $property_values = '';
    $start = 0;

    foreach ($this->_custom_properties as $field => $value){
      $property_names .= "$field:" . 'S' . ":$start:" . strlen($value) . ":";
      $property_values .= $value;
      $start = strlen($property_values);
    }

    $this->_property_names = $property_names;
    $this->_property_values = $property_values;
    $this->_property_values_binary = pack("nvc*", 0x1234, 0x5678, 65, 66);
  }

  /**
   * @PostLoad
   */
  #[ORM\PostLoad]
  public function decodeValues() {
    if ($this->_property_names) {

      $names = explode(":", rtrim($this->_property_names, ' :'));
      $values = $this->_property_values;

      for ($i = 0; $i <= sizeof($names) - 1; $i += 4) {
        $value = substr($values, (int) $names[$i + 2], (int) $names[$i + 3]);
        $this->_custom_properties[$names[$i]] = !empty($value) ? $value : null;
      }

    }
  }

  public function __get($name) {
    if(isset($this->_custom_properties[$name])){
      $value = $this->_custom_properties[$name];
    }else{
      $value = null;
    }
    return $value;
  }

  public function __set($name, $value) {
    $this->_custom_properties[$name] = $value;
  }

}