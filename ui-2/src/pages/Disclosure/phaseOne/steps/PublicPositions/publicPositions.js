import useWizard from 'src/wizard/useWizard';
import rules from 'src/utils/rules';
import { positionTemplate } from 'src/pages/Disclosure/common/positionTemplate';

const { dataPath, lazyLoadWrapper } = useWizard();

export const publicPositionsSection = {
  title: 'Public Positions',
  order: 2,
  steps: {
    publicPositionsStep: {
      title: 'Public Positions',
      order: 0,
      component: lazyLoadWrapper(() => import('./PublicPositions.vue')),
      fields: {
        hasPublicPositionsHeld: {
          rules: [rules.required],
          blocking: false,
          data() {
            return dataPath(this, 'statement.isOfficeHolder');
          },
        },
        publicOfficePositionArray: {
          blocking: false,
          data() {
            // Array of objects will be found at useWizard().state.data.offics
            return dataPath(this, 'offices');
          },
          template: {
            endDate: {
              as: 'end_date',
            },
            isCurrentlyHeld: {
              initialValue: false,
            },
            ...positionTemplate,
          },
        },
      },
    },
  },
};
