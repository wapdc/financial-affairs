#!/usr/bin/env bash
export PATH=/app/bin:$PATH
## If no CI assume a lando container so environment comes from lando
if [[ -z "$CI_COMMIT_REF_NAME" ]] ; then
  . vendor/wapdc/core/ci/ci_build_environment.sh
if [ ! -d vendor ] ; then
  echo "Cache directory not found. Running composer install..."
  composer config -g gitlab-token.gitlab.com "$COMPOSER_ACCESS_TOKEN"
  composer install --quiet --no-progress
else
  echo "Found cached vendor directory"
fi
## Otherwise we assume that we're building in the devops image and we can get the environment
## from that project
else
  . ci_build_environment.sh
fi
mkdir -p local
vendor/bin/phpunit --bootstrap vendor/autoload.php -c tests/phpunit.xml --testsuite rds