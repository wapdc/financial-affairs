import useWizard from 'src/wizard/useWizard';

import { businessesOtherStep } from './otherBusinessRelationship/businessesOtherStep';
import { businessesOwnedStep } from './ownership/businessesOwnedStep';
import { validateLocationObj } from '../RealEstate/realEstate.helpers';

const wizard = useWizard();

const balances = 'balances';
const interest = 'interest';

export function validateReportingChoice(
  reportingChoiceInputVal,
  index,
  stepName,
) {
  return requiredIf({
    dependentValue: reportingChoiceInputVal,
    determiningFieldKey: 'isLendingInstitution',
    stepName,
    index,
    requiringValue: 'true',
  });
}

export function validateHasOfficers(hasOfficersValue, index, stepName) {
  return requiredIf({
    dependentValue: hasOfficersValue,
    determiningFieldKey: 'reportingChoice',
    stepName,
    index,
    requiringValue: balances,
  });
}

export function validateHasAgencyBalances(hasAgenciesValue, index, stepName) {
  return requiredIf({
    dependentValue: hasAgenciesValue,
    determiningFieldKey: 'reportingChoice',
    stepName,
    index,
    requiringValue: balances,
  });
}

export function validateHasBusinessInterests(
  hasBusinessInterestsValue,
  index,
  stepName,
) {
  return requiredIf({
    dependentValue: hasBusinessInterestsValue,
    determiningFieldKey: 'reportingChoice',
    stepName,
    index,
    requiringValue: interest,
  });
}

export function validateHasBusinessInterestReceipts(
  hasBusinessInterestReceiptsValue,
  index,
  stepName,
) {
  return requiredIf({
    dependentValue: hasBusinessInterestReceiptsValue,
    determiningFieldKey: 'reportingChoice',
    stepName,
    index,
    requiringValue: interest,
  });
}

export function validateHasOfficePayments(
  hasOfficePaymentsValue,
  index,
  stepName,
) {
  return requiredIf({
    dependentValue: hasOfficePaymentsValue,
    determiningFieldKey: 'isLendingInstitution',
    stepName,
    index,
    requiringValue: 'false',
  });
}

export function validateOfficePayments(officePaymentsArray, index, stepName) {
  if ([null, undefined, -1].includes(index)) {
    if (!officePaymentsArray?.length) return true;
    const firstElement = officePaymentsArray?.[0];
    index = firstElement?.parentIndex;
  }

  if ([null, undefined, -1].includes(index)) return true;

  const hasOfficePaymentsValue = getSubfieldValue(
    stepName,
    index,
    'hasOfficePayments',
  );

  if (hasOfficePaymentsValue !== 'true') return true;

  if (!officePaymentsArray?.length) return 'required';

  for (const paymentObj of officePaymentsArray) {
    if (!chkStr(paymentObj?.purpose)) return 'Purpose is required';
    if (!chkStr(paymentObj?.amount)) return 'Amount is required';
  }

  return true;
}

export function validateHasAgencyPayments(
  hasAgencyPaymentsValue,
  index,
  stepName,
) {
  return requiredIf({
    dependentValue: hasAgencyPaymentsValue,
    determiningFieldKey: 'isLendingInstitution',
    stepName,
    index,
    requiringValue: 'false',
  });
}

export function validateHasBusinessPayments(
  hasBusinessPaymentsValue,
  index,
  stepName,
) {
  return requiredIf({
    dependentValue: hasBusinessPaymentsValue,
    determiningFieldKey: 'isLendingInstitution',
    stepName,
    index,
    requiringValue: 'false',
  });
}

export function validateBusinessPayments(
  businessPaymentsArray,
  index,
  stepName,
) {
  if ([null, undefined, -1].includes(index)) {
    if (!businessPaymentsArray?.length) return true;
    const firstElement = businessPaymentsArray?.[0];
    index = firstElement?.parentIndex;
  }

  if ([null, undefined, -1].includes(index)) return true;

  const hasBusinessPaymentsValue = getSubfieldValue(
    stepName,
    index,
    'hasBusinessPayments',
  );

  if (hasBusinessPaymentsValue !== 'true') return true;

  if (!businessPaymentsArray?.length) return 'required';

  for (const paymentObj of businessPaymentsArray) {
    if (!chkStr(paymentObj?.business)) return 'Business is required';
    if (!chkStr(paymentObj?.purpose)) return 'Purpose is required';
  }

  return true;
}

export function validateAgencyPayments(
  agencyPaymentsArray = [],
  index,
  stepName,
) {
  if ([null, undefined, -1].includes(index)) {
    if (!agencyPaymentsArray?.length) return true;
    const firstElement = agencyPaymentsArray?.[0];
    index = firstElement?.parentIndex;
  }

  if ([null, undefined, -1].includes(index)) return true;

  const hasAgencyPaymentsValue = getSubfieldValue(
    stepName,
    index,
    'hasAgencyPayments',
  );

  if (hasAgencyPaymentsValue !== 'true') return true;

  if (!agencyPaymentsArray?.length) return 'required';

  for (const paymentObj of agencyPaymentsArray) {
    if (!chkStr(paymentObj?.agency)) return 'Agency is required';
    if (!chkStr(paymentObj?.purpose)) return 'Purpose is required';
  }

  return true;
}

export function validateWaProperty(waProperty, index, stepName) {
  const hasWaProperty = getSubfieldValue(stepName, index, 'hasWaProperty');
  if (hasWaProperty !== 'true') return true;
  if (!waProperty?.length) return 'Required';

  for (const property of waProperty) {
    const validationResult = validateLocationObj(property);
    if (validationResult !== true) return validationResult;
  }

  return true;
}

/** Validates the officer arrays in steps: businessesOtherStep, businessesOwnedStep */
export function validateOfficerArray(officersArray, index, stepName) {
  if ([null, undefined, -1].includes(index)) {
    if (!officersArray?.length) return true;
    const firstElement = officersArray?.[0];
    index = firstElement?.parentIndex;
  }

  if ([null, undefined, -1].includes(index)) return true;

  const hasOfficersValue = getSubfieldValue(stepName, index, 'hasOfficers');

  if (hasOfficersValue !== 'true') return true;

  if (!officersArray?.length) return 'required';

  for (const officerObj of officersArray) {
    if (!chkStr(officerObj?.name)) return 'Name is required';
    if (!chkStr(officerObj?.occupation)) return 'Occupation is required';
    if (!chkStr(officerObj?.address)) return 'Address is required';
    if (!chkStr(officerObj?.city)) return 'City is required';
    if (!chkStr(officerObj?.state)) return 'State is required';
    if (!chkStr(officerObj?.postcode)) return 'Zip is required';
  }

  return true;
}

/** Validates the officer arrays in steps: businessesOtherStep, businessesOwnedStep */
export function validateAgencyArr(agencyArray, index, stepName) {
  if ([null, undefined, -1].includes(index)) {
    if (!agencyArray?.length) return true;
    const firstElement = agencyArray?.[0];
    index = firstElement?.parentIndex;
  }

  if ([null, undefined, -1].includes(index)) return true;

  const hasAgenciesValue = getSubfieldValue(stepName, index, 'hasAgencies');

  if (hasAgenciesValue !== 'true') return true;

  if (!agencyArray?.length) return 'required';

  for (const agencyObj of agencyArray) {
    if (!chkStr(agencyObj?.agency)) return 'Agency is required';
    if (!chkStr(agencyObj?.purpose)) return 'Purpose is required';
  }

  return true;
}

export function validateBusinessInterestPayments(
  businessInterestPaymentsArray,
  index,
  stepName,
) {
  if ([null, undefined, -1].includes(index)) {
    if (!businessInterestPaymentsArray?.length) return true;
    const firstElement = businessInterestPaymentsArray?.[0];
    index = firstElement?.parentIndex;
  }

  if ([null, undefined, -1].includes(index)) return true;

  const hasBusinessInterestPaymentsValue = getSubfieldValue(
    stepName,
    index,
    'hasBusinessInterestPayments',
  );

  if (hasBusinessInterestPaymentsValue !== 'true') return true;

  if (!businessInterestPaymentsArray?.length) return 'required';

  for (const paymentObj of businessInterestPaymentsArray) {
    if (!chkStr(paymentObj?.agency)) return 'Agency is required';
    if (!chkStr(paymentObj?.amount)) return 'Amount is required';
  }

  return true;
}

export function validateBusinessInterestReceiptsArray(
  hasBusinessInterestReceiptsArray,
  index,
  stepName,
) {
  if ([null, undefined, -1].includes(index)) {
    if (!hasBusinessInterestReceiptsArray?.length) return true;
    const firstElement = hasBusinessInterestReceiptsArray?.[0];
    index = firstElement?.parentIndex;
  }

  if ([null, undefined, -1].includes(index)) return true;

  const hasBusinessInterestReceiptsValue = getSubfieldValue(
    stepName,
    index,
    'hasBusinessInterestReceipts',
  );

  if (hasBusinessInterestReceiptsValue !== 'true') return true;

  if (!hasBusinessInterestReceiptsArray?.length) return 'required';

  for (const receiptObj of hasBusinessInterestReceiptsArray) {
    if (!chkStr(receiptObj?.agency)) return 'Agency is required';
    if (!chkStr(receiptObj?.amount)) return 'Amount is required';
  }

  return true;
}

/** Generic conditionally required validator */
function requiredIf({
  dependentValue,
  determiningFieldKey,
  stepName,
  index = null,
  requiringValue = null,
}) {
  const invalidMessage = 'Required';
  if ([null, undefined].includes(index)) return invalidMessage;

  const determiningFieldValue = getSubfieldValue(
    stepName,
    index,
    determiningFieldKey,
  );

  const isNotRequired =
    requiringValue === null
      ? ![null, undefined, 'false'].includes(determiningFieldValue)
      : determiningFieldValue !== requiringValue;

  if (isNotRequired) return true;

  if (!chkStr(dependentValue)) return invalidMessage;

  return true;
}

/** Gets the subfield from the template of the given step */
function getSubfieldValue(stepName, index, subfieldKey) {
  const arrayUtils = wizard.getFieldSetUtilsByPointer(
    stepName === 'businessesOwnedStep'
      ? businessesOwnedStep.fields.ownedBusinessesArray
      : businessesOtherStep.fields.otherBusinessesArray,
  );

  return arrayUtils.get(index)?.getValue(subfieldKey);
}

/** Checks if the string is not empty */
function chkStr(str) {
  return str?.trim?.()?.length > 0 ?? false;
}
