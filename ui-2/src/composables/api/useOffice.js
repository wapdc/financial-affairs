import { computed, inject, provide, reactive } from 'vue';
import { useApollo } from '@wapdc/pdc-ui/application';

/*
  We are violating a convention: Accessing computed properties should not produce side
  effects. `elections` and `offices` place values in reactive state. This provides a better
  interface to the components, isolating psuedo-caching behavior to this module.
*/

const OfficeAccessorSymbol = Symbol('OfficeAccessorSymbol');

/**
 * Composable function to provide office/election data accessors to descendent components.
 * Must be instantiated in the nearest common ancestor of components that import the
 * useSubmissions composable.
 * @returns {void}
 */
export function useOfficeProvider() {
  const faService = useApollo('service/financial-affairs');

  const state = reactive({
    elections: [],
    offices: [],
  });

  function asOption(label, value) {
    return { label, value };
  }

  /** Prefer state, fallback to new API fetch for elections array */
  const elections = computed(() => {
    if (state.elections?.length > 0) {
      return state.elections;
    }

    _fetchElectionsData()
      .then((data) => {
        return (state.elections = data.map((election) => {
          return {
            ...election,
            option: asOption(election.title, election.election_code),
          };
        }));
      })
      .catch((err) => {
        console.error(err);
        return () => [];
      });
  });

  /** Prefer state, fallback to new API fetch for offices array */
  const offices = computed(() => {
    if (state.offices?.length > 0) {
      return state.offices;
    }

    _fetchOfficesData()
      .then((data) => {
        return (state.offices = data.map((office) => {
          return {
            ...office,
            jurisdictions: [],
            option: asOption(office.offtitle, office.offcode),
          };
        }));
      })
      .catch((err) => {
        console.error(err);
        return () => [];
      });
  });

  /**
   * Prefer state, fallback to API fetch + set state for following calls
   * Stores jurisdictions in corresponding state.office array element
   *
   * Note: where offices and elections are loaded wholly, jurisdictions
   * are loaded one set at a time, based on given officeCode parameter
   */
  async function getJurisdictions(officeCode) {
    try {
      if (!offices.value?.length) return;

      const office = _getOfficeFromState(officeCode);
      if (!office) {
        return [];
      }

      if (office.jurisdictions?.length > 0) {
        // Already fetched
        return office.jurisdictions;
      }

      const response = await faService.get(
        `/jurisdictions?office_code=${officeCode}`,
      );

      if (!response.success) {
        throw new Error('Unable to fetch office jurisdictions');
      }

      const jurisdictionOptions = response.data.map((j) => {
        return {
          ...j,
          option: asOption(j.name, j.jurisdiction_id),
          positions: [],
        };
      });

      office.jurisdictions = jurisdictionOptions;

      return office.jurisdictions;
    } catch (error) {
      console.error('getJurisdictions error', error);
      return [];
    }
  }

  /**
   * Preferring state data if available, falling back to new API call,
   * returns positions for a given office jurisdiction
   */
  async function getOfficeJurisdictionPositions(officeCode, jurisdictionId) {
    try {
      const jurisdictions = await getJurisdictions(+officeCode);
      if (!jurisdictions?.length) return;

      const jurisdiction = jurisdictions.find(
        (j) => +j.jurisdiction_id === +jurisdictionId,
      );
      if (!jurisdiction) return;

      const response = await faService.get(
        `/positions?jurisdiction_id=${jurisdictionId}&office_code=${officeCode}`,
      );
      if (!response.success) {
        throw new Error('Unable to fetch office jurisdiction positions');
      }

      const positionOptions = response.data.map((p) => {
        return {
          ...p,
          option: asOption(p.title, p.position_id),
        };
      });

      jurisdiction.positions = positionOptions;

      return jurisdiction.positions;
    } catch (error) {
      console.error('getJurisdictionOfficePositions error', error);
      return [];
    }
  }

  /** Internal/unprovided helper fn; Make API call to load elections data */
  async function _fetchElectionsData() {
    const response = await faService.get('/elections');
    if (!response.success) {
      throw new Error('Unable to fetch election data');
    }
    return response.data;
  }

  /** Internal/unprovided helper fn; Make API call to load office data */
  async function _fetchOfficesData() {
    const response = await faService.get('/offices');
    if (!response.success) {
      throw new Error('Unable to fetch office data');
    }
    return response.data;
  }

  /** Internal/unprovided helper fn; return office by code (id) from internal state */
  function _getOfficeFromState(officeCode) {
    return state.offices.find((off) => +off.offcode === +officeCode);
  }

  provide(OfficeAccessorSymbol, {
    // Computed
    offices,
    elections,

    // Functions
    getJurisdictions,
    getOfficeJurisdictionPositions,
  });
}

/**
 * Composable function to return election/office data accessors. Must be used within a component where
 * useOfficeProvider has been instantiated in an ancestor component.
 * @returns {{
 *   elections: () => ComputedGetter<Record<string, any>[]>,
 *   offices: () => ComputedGetter<Record<string, any>[]>,
 *   getJurisdictions: (officeCode: string) => Promise<Record<string, any>[]>
 *   getOfficeJurisdictionPositions: (officeCode: string, jurisdictionId: string) => Promise<Record<string, any>[]>
 * }} submissionsAccessor:
 *   - elections: Elections array; access sets value in state if fetched from API
 *   - offices: Offices array; access sets value in state if fetched from API
 *   - getJurisdictions: Retrieves the list of office positions by office code
 *   - getOfficeJurisdictionPositions: Retrieves office jurisdiction positions by jurisdiction ID and office code
 * @throws {Error} Throws an error if useOfficeProvider() has not been instantiated in an ancestor component.
 */
export default function useOffice() {
  const submissionsAccessor = inject(OfficeAccessorSymbol);
  if (!submissionsAccessor) {
    throw new Error(
      'useOffice must be used within a descendant of a component instantiating' +
        ' useOfficeProvider',
    );
  }

  return submissionsAccessor;
}
