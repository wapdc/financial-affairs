#!/usr/bin/env bash
set -e
# Include pantheon build commands from dev ops
if [[ -f "/app/bin/ci_build_environment.sh" ]] ; then
. /app/bin/ci_build_environment.sh
fi
#
# Determine if we are working on a the qa-pww branch and if so change the deploy branch for apollo
#
# Save the current working directory so that we can restore it later.
if [[ $CI_COMMIT_REF_NAME = "qa-pww" ]] ; then
  export DEPLOY_BRANCH="qa-pww"
  echo "Changed deployment branch to $DEPLOY_BRANCH"
fi
start_dir=`pwd`
# Pull a copy of the apollo site
pull_site apollo
site_dir=`pwd`
fa_dir=vendor/wapdc/financial-affairs
composer clearcache
rm -rf "$fa_dir"
composer update --with-all-dependencies --no-progress wapdc/financial-affairs
# This remove is required because the .git files are removed after the composer update.
rm -rf web/modules/custom/fin_affairs
# Build the qaasar app
cd "$fa_dir/ui-2"
npm install -q --yes
npm run build
echo "Quasar application build complete!"
cd "$site_dir"
# Copy drupal module files fresh
echo "Copying drupal 8 files ........"
cp -r $fa_dir/drupal-8/. web/modules/custom/
#
# Build Vue  financial-affairs UI
echo "Building Financial Affairs Application"
mkdir -p "$fa_dir/ui/financial-affairs"
cd "$fa_dir/ui/financial-affairs"
cp -f ../.gitignore_production ./.gitignore
npm install -q --yes
npm run build
#
# Build Vue public UI
echo "Building Financial Affairs Public Application"
mkdir -p "../public"
cd "../public"
cp -f ../.gitignore_production ./.gitignore
npm install -q --yes
npm run build
#
# Commit and push to master
commit_push
