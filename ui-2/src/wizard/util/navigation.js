import { nextTick } from 'vue';

/**
 * @module wizard/util/navigation
 * @description Step/section navigation logic for the wizard
 */

/** Set the current step and section */
export function _setCurrentStep({ state, stepIndex, sectionIndex }) {
  state.status.currentStepIndex = stepIndex;
  state.status.currentSectionIndex = sectionIndex;

  const section = Object.values(state.form).find(
    (section) => section.order === sectionIndex,
  );
  state.status.currentSection = section;
  state.status.currentSectionTitle = section.title;

  const targetStep = Object.values(section.steps).find(
    (step) => step.order === stepIndex,
  );
  state.status.currentStep = targetStep;
  state.status.currentStepTitle = targetStep.title;

  if (state.eventCallbacks.onStepChange) {
    state.eventCallbacks.onStepChange();
  }

  setNewAccessibleSteps(state);

  const stepHasFields = targetStep && targetStep.fields;

  if (stepHasFields) return;

  // Loading a step with no fields automatically marks it as valid and dirty
  state.status.currentStep.meta.isDirty = true;
  state.status.currentStep.meta.isValid = true;
}

/** Navigates to next or previous available step */
export function _navigateOneTick(state, direction) {
  const { nextAccessibleStep, prevAccessibleStep } = state.status;

  const targetStep = direction < 0 ? prevAccessibleStep : nextAccessibleStep;

  if (!targetStep) {
    console.error('No accessible step found');
    return;
  }

  const { sectionIndex, stepIndex } = targetStep;
  _setCurrentStep({ state, sectionIndex, stepIndex });

  nextTick(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  });
}

/** Sets next and previous accessible step state */
export function setNewAccessibleSteps(state) {
  const forwardStep = getNextAccessibleStep(1, state);
  const backwardStep = getNextAccessibleStep(-1, state);

  state.status.nextAccessibleStep = forwardStep;
  state.status.prevAccessibleStep = backwardStep;
}

/** Mark step as hidden for exclusion in TOC and nav buttons */
export function hideStep(step) {
  step.meta.isHidden = true;
}

/**
 * Returns next (forward (1) or backward (-1)) accessible
 * step as { nextSectionIndex, nextStepIndex }
 * Note: Logic is easier to understand if you skip to the
 * loop at the bottom
 */
function getNextAccessibleStep(navDirection, state) {
  const { currentSectionIndex, currentStepIndex } = state.status;

  const cursor = [currentSectionIndex, currentStepIndex];

  const maximumSectionIndex = Object.values(state.form).reduce(
    (acc, section) => {
      return section.order > acc ? section.order : acc;
    },
    0,
  );

  const getSectionMaximunStepIndex = (section) => {
    return Object.values(section.steps).reduce((acc, step) => {
      return step.order > acc ? step.order : acc;
    }, 0);
  };

  const tickSection = () => (cursor[0] += navDirection);
  const tickStep = () => (cursor[1] += navDirection);

  const getSectionAtCursor = () =>
    Object.values(state.form).find((section) => section.order === cursor[0]);

  const getStepAtCursor = () => {
    const section = getSectionAtCursor();
    if (!section) return null;
    return Object.values(section.steps).find(
      (step) => step.order === cursor[1],
    );
  };

  let stepAtCursor = getStepAtCursor();

  const rolloverCursor = () => {
    tickSection();
    const section = getSectionAtCursor();
    const maxStep = section ? getSectionMaximunStepIndex(section) : null;
    const directionIsPositive = navDirection > 0;
    cursor[1] = directionIsPositive ? 0 : maxStep;
  };

  const sectionCursorIsInBounds = () => {
    return cursor[0] <= maximumSectionIndex && cursor[0] >= 0;
  };

  const stepCursorIsInBounds = () => {
    const section = getSectionAtCursor();
    return cursor[1] <= getSectionMaximunStepIndex(section) && cursor[1] >= 0;
  };

  tickStep();
  while (sectionCursorIsInBounds()) {
    while (stepCursorIsInBounds()) {
      stepAtCursor = getStepAtCursor();

      if (stepAtCursor && !stepAtCursor.meta.isHidden) {
        return { sectionIndex: cursor[0], stepIndex: cursor[1] };
      }

      tickStep();
    }
    rolloverCursor();
  }

  return null;
}

/**
 * Computed a table of contents for the form in the form
 * of an array of sections and steps
 */
export function getTableOfContents(form) {
  const sections = Object.values(form);
  if (!sections) return [];

  return sections.map((section) => {
    const visibleSteps = Object.values(section.steps).filter(
      (step) => !step.meta.isHidden,
    );
    const steps = visibleSteps.map((step) => {
      return {
        title: step.title,
        order: step.order,
        meta: step.meta,
      };
    });

    return {
      title: section.title,
      order: section.order,
      nest: steps.length > 1,
      steps,
    };
  });
}
