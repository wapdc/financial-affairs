<?php


namespace WAPDC\FinancialAffairs;


use Doctrine\DBAL\Connection;
use Exception;
use stdClass;
use WAPDC\Core\Model\EntityAuthorization;
use WAPDC\Core\Model\User;
use WAPDC\Core\SchemaManager;
use WAPDC\FinancialAffairs\Model\FASubmission;

class FinancialAffairs {

  /** @var FinancialAffairs */
  static $_service;

  /**
   * @var FADataManager
   */
  protected $_dataManager;

  /** @var StatementProcessor */
  protected $_statementProcessor;

  public $project_dir = '.';

  /** @var bool  */
  protected $migration_mode;

  /**
   * CampaignFinance constructor.
   * @param bool $migration
   *   Indicates whether the data mover is being used to migrate data.
   * @throws Exception
   */
  public function __construct($migration = FALSE, $dataManager = null) {
    $this->migration_mode = $migration;
    $this->_dataManager = $dataManager ?? new FADataManager($migration);
    $this->project_dir = dirname(__DIR__);
  }

  /**
   * Singleton Factory
   *
   * @param bool $migration
   *   Indicates whether the service is being used to do migration.
   * @return FinancialAffairs
   * @throws Exception
   */
  static public function service($migration = FALSE) {
    if (!static::$_service) {
      static::$_service = new static($migration);
    }
    return static::$_service;
  }

  public function getDataManager() {
    return $this->_dataManager;
  }

  /**
   * @return StatementProcessor
   * @throws Exception
   */
  public function getStatementProcessor() {
    if (!$this->_statementProcessor) {
      $this->_statementProcessor = new StatementProcessor($this->_dataManager);
    }
    return $this->_statementProcessor;
  }

  /**
   * Generate orm proxy files.
   */
  public function generateProxyFiles() {
    $this->_dataManager->generateProxyFiles();
  }

  /**
   * Perform schema upgrades.
   * @throws Exception
   */
  public function upgrade() {

    // Run the postgres RDS schema revisions
    $revision_dir = dirname(__DIR__) . '/db/postgres/revisions';
    $this->generateProxyFiles();
    $s = new SchemaManager($revision_dir, $this->_dataManager);
    $s->runRevScripts();
  }

  public function personQuery($sql_file, $parms=[]) {
    $dir = $this->project_dir . '/data/person';
    return $this->_dataManager->executeQueryFromFile($dir, $sql_file, $parms);
  }

  public function adminQuery($sql_file, $parms=[]) {
    $dir = $this->project_dir . '/data/admin';
    return $this->_dataManager->executeQueryFromFile($dir, $sql_file, $parms);
  }

  /**
   * @param $entity_id
   * @param User $user
   * @return bool
   */
  public function entityAuthorizationCheck($entity_id, User $user) : bool
  {
    if ($entity_id == -1) {
      return true;
    }
    $entity_auth = $this->_dataManager->em->getRepository(EntityAuthorization::class)->findBy(['entity_id' => (int)$entity_id, 'uid' => $user->uid, 'realm' => $user->realm, 'role' => 'owner']);
    return (!empty($entity_auth));
  }

  /**
   * Determine if a user has access to a submission.
   *
   * @param int $submission_id
   *   ID of the submission to check
   * @param User $user
   *   User to test acess with.
   * @return bool
   *
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   */
  public function submissionAuthorizationCheck($submission_id, User $user) {
    if (!$submission_id) return true;
    /** @var FASubmission $submission */
    $submission = $this->_dataManager->em->find(FASubmission::class, $submission_id);

    if ($user->uid == $submission->uid && $user->realm == $submission->realm) {
      return true;
    }else if ($submission->person_id !== -1) {
      $auth = $this->_dataManager->em->getRepository(EntityAuthorization::class)->findBy(['entity_id' =>  (int)$submission->person_id, 'uid' => $user->uid, 'realm' => $user->realm, 'role' => 'owner']);
      if ($auth) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param User $user
   * @return stdClass
   */
  public function getUserAuthorizations(User $user) {
    $authorizations = new stdClass();
    $dir = $this->project_dir . '/data/person';
    $authorizations->person = $this->_dataManager->executeQueryFromFile($dir, '/get_person_authorization.sql', ['uid' => $user->uid, 'realm' => $user->realm]);
    return $authorizations;
  }

  /**
   * @param $user
   * @param $filer_ids
   * @throws Exception
   */
  public function generatePersonAuthForFilers($user, $filer_ids) {
    if (!isset($user->uid) || !isset($user->realm)) {
      throw new Exception("Invalid user data");
    }
    $query = "insert into entity_authorization (entity_id, realm, role, uid)
        select e.entity_id, :realm as realm, :role as role, :uid as uid from
            (select entity_id from entity where filer_id in (:filer_ids) and is_person = true) e
        on conflict do nothing";
    $this->_dataManager->db->executeQuery($query,
      ['filer_ids' => $filer_ids, 'realm' => $user->realm, 'role' => 'owner', 'uid' => $user->uid],
      ['filer_ids' => Connection::PARAM_STR_ARRAY]);
  }
}