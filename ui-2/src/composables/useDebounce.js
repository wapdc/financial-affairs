import { ref } from 'vue';

/**
 * Debounce function to delay the execution of a function
 * @param {Function} func - Callback Function to be executed
 * @param {Number} wait - Time in milliseconds to wait before executing the function
 *
 * @example
 * import useDebounce from '../composables/useDebounce';
 * const debouncedFunction = useDebounce();
 * debouncedFunction(() => {
 *   console.log('This will be debounced');
 * }, 500);
 */
export default function useDebounce() {
  const timeout = ref(null);

  function debounce(func, wait) {
    if (timeout.value !== null) {
      clearTimeout(timeout.value);
    }

    timeout.value = setTimeout(func, wait);
  }

  return debounce;
}
