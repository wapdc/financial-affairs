import { ABBREVIATIONS } from '../../../constants/abbreviations.js';
import { renderIncomeRange } from '../../../utils/format.js';

export const monetaryRanges = [
  '0-29999',
  '30000-59999',
  '60000-99999',
  '100000-199999',
  '200000-499999',
  '500000-749999',
  '750000-999999',
  '1000000-',
].map((range) => {
  return {
    label: renderIncomeRange(range),
    value: range,
  };
});

export const noneOpt = {
  label: 'None',
  value: 'none',
};

export const relationCode = ['filer', 'spouse', 'dependent'].map((relation) => {
  return {
    label: ABBREVIATIONS[relation],
    value: relation,
  };
});
