import { computed } from 'vue';
import { useQuasar } from 'quasar';

export default function useViewport() {
  const quasar = useQuasar();

  const isNarrowViewport = computed(() => quasar.screen.lt.sm);
  const isShortViewport = computed(() => quasar.screen.height < 600);

  const isSmViewport = computed(
    () => quasar.screen.lt.md || isShortViewport.value,
  );

  const isXsViewport = computed(
    () => isNarrowViewport.value || isShortViewport.value,
  );

  const width = computed(() => quasar.screen.width);

  return {
    isNarrowViewport,
    isShortViewport,
    isXsViewport,
    isSmViewport,
    width,
  };
}
