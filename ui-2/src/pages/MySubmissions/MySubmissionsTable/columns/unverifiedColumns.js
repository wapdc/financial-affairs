import { formatDate } from '@wapdc/pdc-ui/util';
import { sortDateTimeStrings } from './sort';

export const unverifiedColumns = [
  {
    name: 'lastUpdated',
    label: 'Last Updated',
    required: true,
    align: 'left',
    field: 'lastUpdated',
    format: (val) => formatDate(val),
    sortable: true,
    sort: sortDateTimeStrings,
  },
  {
    name: 'submitted',
    label: 'Submitted',
    required: true,
    align: 'left',
    field: 'submitted',
    format: (val) => formatDate(val),
    sortable: true,
    sort: sortDateTimeStrings,
  },
];
