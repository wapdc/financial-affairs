import { CategoryHandler } from 'src/utils/CategoryHandler.class';
import rules from 'src/utils/rules';
import useWizard from 'src/wizard/useWizard';

const { lazyLoadWrapper, dataPath, getData } = useWizard();
const incomeCategoryHandler = new CategoryHandler('income', 'employment');

export const employmentIncomeStep = {
  title: 'Employment Income',
  order: 1,
  component: lazyLoadWrapper(() => import('./EmploymentIncome.vue')),
  fields: {
    hasEmploymentIncome: {
      rules: [rules.required, rules.requiredString],
      data() {
        return dataPath(this, 'pendingTransform.hasEmploymentIncome');
      },
      transform: (field) => ({
        writeOut: () => incomeCategoryHandler.writeOutHas(field),
        readIn: () => incomeCategoryHandler.readInHas(field),
      }),
    },
    employmentIncomeArray: {
      blocking: false,
      data() {
        return dataPath(this, 'pendingTransform.employmentIncomeArray');
      },
      transform: (field) => ({
        writeOut: () => incomeCategoryHandler.writeOutData(field),
        readIn: () => incomeCategoryHandler.readInData(field),
      }),
      requiredIf: () =>
        [true, 'true'].includes(getData().pendingTransform.hasEmploymentIncome),
      template: {
        receivedBy: {
          as: 'who',
          rules: [rules.requiredNonEmptyArray],
        },
        employerName: {
          as: 'name',
          rules: [
            rules.requiredString,
            rules.maxLength(100),
          ],
        },
        occupation: {
          as: 'why',
          rules: [rules.required],
        },
        compensation: {
          as: 'income',
          rules: [rules.required],
        },
        streetAddress: {
          as: 'address',
          rules: [
            rules.requiredString,
            rules.maxLength(120),
          ],
        },
        city: {
          rules: [
            rules.requiredString,
            rules.alphbetical,
            rules.maxLength(120),
          ],
        },
        state: {
          rules: [rules.required, rules.state],
        },
        zip: {
          as: 'postcode',
          rules: [rules.required, rules.exactLength(5), rules.numbersOnly],
        },
      },
    },
  },
};
