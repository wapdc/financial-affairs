import { computed } from 'vue';
import { STEP_STATUS } from './constants';

/**
 * Accepts reactive wizard form object which is used to compute
 * a simplified structure for use in rendering sections and steps
 * as table of contents
 */
export default function useTableOfContents(form) {
  return computed(() => mapFormToTableOfContents(form));
}

/**
 * Maps the form data to the array of objects that the WizardTableOfContents component expects
 * @param {Object} form Form object from useWizard composable
 * returns {Array<Object>} Array of objects representing the table of contents
 */
export function mapFormToTableOfContents(form) {
  const sections = Object.values(form);

  return sections.map((section, sectionIndex) => {
    const steps = section.steps ? Object.values(section.steps) : [];

    const mappedSteps = steps.map((step, stepIndex) => {
      const { hideToc, title, meta } = step;
      let status;

      if (meta) {
        const { isDirty, isValid } = meta;

        if (!isDirty) {
          return (status = STEP_STATUS.NOT_STARTED);
        } else {
          status = isValid ? STEP_STATUS.COMPLETED : STEP_STATUS.INCOMPLETE;
        }
      }

      return {
        title,
        sectionIndex,
        stepIndex,
        hideToc,
        status,
      };
    });

    const visibleSteps = mappedSteps.filter((step) => !step.hideToc);

    const sectionItem = {
      nest: visibleSteps.length > 1,
      title: section.title,
    };

    if (sectionItem.nest) {
      sectionItem.steps = visibleSteps;
    } else if (visibleSteps.length === 1) {
      sectionItem.step = visibleSteps[0];
    }

    return sectionItem;
  });
}
