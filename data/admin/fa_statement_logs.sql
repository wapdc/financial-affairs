select * from fa_statement_log
    where (:id_type = 'submission_id' and submission_id = :id)
    or (:id_type = 'fa_statement_id' and fa_statement_id = :id)
    or (:id_type = 'person_id' and person_id = :id)
order by updated_at desc;