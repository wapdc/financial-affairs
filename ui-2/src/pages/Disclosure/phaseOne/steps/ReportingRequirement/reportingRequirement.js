import useGetRequiredStatements from 'src/composables/api/useGetRequiredStatement.js';
import useWizard from 'src/wizard/useWizard';
import useFiler from 'src/composables/api/useFiler.js';

const wizard = useWizard();
const { lazyLoadWrapper } = wizard;

// Requirement step definition
export const reportingRequirementSection = {
  title: 'Reporting Requirement',
  order: 4,
  steps: {
    reportingRequirement: {
      title: 'Reporting Requirement',
      order: 0,
      component: lazyLoadWrapper(() => import('./ReportingRequirement.vue')),
    },
  },
};

/**
 * Determines the reporting requirement for the filer based on the form data
 */
export async function determineReportingRequirement() {
  const { selectedFiler } = useFiler();

  const payload = {
    offices: wizard.getData().offices || [],
    statement: wizard.getData().statement || {},
    candidacies: wizard.getData().candidacies || [],
    submission: wizard.getData().submission || {},
    person: selectedFiler.value,
  };

  const knownInvalidSteps = wizard.getInvalidSteps();
  const serverValidation = await useGetRequiredStatements(payload);

  return {
    isValid: serverValidation.isValid && knownInvalidSteps.length === 0,
    invalidSteps: [...serverValidation.invalidSteps, ...knownInvalidSteps],
    statementRequired: serverValidation.statementRequirement,
    periodStart: serverValidation.periodStart,
    periodEnd: serverValidation.periodEnd,
  };
}
