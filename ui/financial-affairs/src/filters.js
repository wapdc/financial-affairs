import Vue from 'vue'
import moment from 'moment'

function formatMoney(amount) {
  let decimalCount = 0, decimal = ".", thousands = ",";
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 0 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return '$' + negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
}

export default {
  filters: {
    // Yes or NO from boolean
    yesNo:  value  => {
      if (value) {
        return 'Yes'
      }
      else {
        return 'No'
      }
    },
    dateFormatShort: value => {
      if (value) {
        return moment(value).format('M/D/YYYY');
      }
      return '';
    },
    dateFormat: value => {
      if (value) {
        return moment(value).format('MMMM D, YYYY');
      }
      return '';
    },

    dateTimeFormat: value => {
      if (value) {
        return moment(value).format('M/D/YYYY HH:mm');
      }
      return '';
    },

    defaultText: (value, text) => {
      if (!value) {
        return text;
      }
      return value;
    },

    upperCase: value => {
      if (value) {
        return value.toUpperCase();
      }
      return value;
    },

    lowerCase: value => {
      if (value) {
        return value.toLowerCase();
      }
      return value;
    },

    // Title Case a field or code.
    titleCase: value => {
      if (value) {
        let str = value.toLowerCase().split(' ');
        for (var i = 0; i < str.length; i++) {
          str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
        }
        return str.join(' ');
      }
      return '';
    },

    // Create a (value) but only if the value exists.
    parenthetical: value => {
      if (value){
        return '(' + value + ')';
      }
      return '';
    },

    // Display a label only if true.
    ifTrue: (value, label) => {
      if (value) {
        return label
      }
      return '';
    },

    // Display value only if equals comparator.
    ifEquals: (value, comparator) => {
      if (value===comparator){
        return value;
      }
      return '';
    },

    // Display value only if not equal to comparator.
    ifNotEquals: (value, comparator) => {
      if (value!==comparator){
        return value;
      }
      return '';
    },

    // Suffix the value with a label if it is present.
    suffix: (value, label) => {
      if (value) {
        return value + label;
      }
      return '';
    },


    // Prefix the value with a label if it is present.
    prefix: (value, label) => {
      if (value) {
        return label + value;
      }
      return '';
    },


    // Return the proper values for ministerial and treasurer in the proper formatting
    treasurerFormat: (treasurer, ministerial, title) => {
      let compareTitle = title ? title.toLowerCase() : '';
      if (compareTitle !== "treasurer" && treasurer && ministerial) {
        return ' (ministerial treasurer)';
      }
      else if (compareTitle === "treasurer" && treasurer && ministerial) {
        return ' (ministerial)';
      }
      else if (compareTitle !== "treasurer" && treasurer && !ministerial) {
        return ' (treasurer)';
      }
      else {
        return '';
      }
    },

    money(value) {
      return formatMoney(value);
    },

    whoDisplay(value, validation) {
      if (!value) {
        return "";
      }
      const validationCodes = [...validation.relation_code];
      validationCodes.sort((a, b) => {

        if (a.sort < b.sort) {
          return -1;
        }
        if (a.sort > b.sort) {
          return 1;
        }
        return 0;
      });

      let displayValues = validationCodes.map((validationEntry, index, theArray) => {
        if (value.includes(validationEntry.value)) {
          return theArray[index].text;
        }
      });

      return displayValues.filter(Boolean).join("; ");
    },

    monetaryRange(value) {
      let v = '';
      if (value) {
        let range = value.split('-');
        let low = range[0];
        let high = range[1];
        if (!low) {
          v = "less than " + formatMoney(low);
        }
        else if (!high) {
          v = "more than " + formatMoney(low);
        }
        else {
          v =formatMoney(low) + " to " + formatMoney(high);
        }
      }

      return v;
    }

  },
  defineGlobalFilters () {

    for (let f in this.filters ) {
      Vue.filter(f, this.filters[f]);
    }

  }
}