<?php


namespace Tests\WAPDC\FinancialAffairs;


use DateTime;
use PHPUnit\Framework\TestCase;
use stdClass;
use WAPDC\Core\Model\Person;
use WAPDC\FinancialAffairs\FADataManager;
use WAPDC\FinancialAffairs\Model\FAStatement;
use WAPDC\FinancialAffairs\Model\FASubmission;

class FADataManagerTest extends TestCase {

  /** @var FADataManager */
  protected $dataManager;

  /** @var Person */
  protected $person;

  /**
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();
    $this->dataManager = new FADataManager();
    $this->dataManager->beginTransaction();
    $this->person = $this->createPerson();
  }

  /**
   * @return Person
   * @throws \Exception
   */
  protected function createPerson() {
    $person = new Person();
    $person->name = "Test Person";
    $this->dataManager->em->persist($person);
    $this->dataManager->em->flush();
    return $person;
  }

  /**
   * @throws \Exception
   */
  public function testStatementModel() {
    $statement = new FAStatement();
    $statement->person_id = $this->person->person_id;
    $statement->period_start = new DateTime('2019-01-01');
    $statement->period_end = new DateTime('2019-12-31');
    $statement->first_filed_at = new DateTime('2020-02-15');
    $statement->filer_id = 'TEST FILER';
    $statement->name = "John Doe";
    $statement->repno = 1234;
    $statement->old_repno = 4321;
    $statement->updated_at = new DateTime();
    $statement->email = "test@example.com";

    $this->dataManager->em->persist($statement);
    $this->dataManager->em->flush();

    $this->dataManager->em->clear();
    $ss = $this->dataManager->em->find(FAStatement::class, $statement->statement_id);

    $this->assertEquals($statement->person_id, $ss->person_id);
    $this->assertEquals($statement->period_start, $ss->period_start);
    $this->assertEquals($statement->period_end, $ss->period_end);
    $this->assertEquals($statement->first_filed_at, $ss->first_filed_at);
    $this->assertEquals($statement->filer_id, $ss->filer_id);
    $this->assertEquals($statement->repno, $ss->repno);
    $this->assertEquals($statement->old_repno, $ss->old_repno);
    $this->assertEquals($statement->email, $ss->email);
  }

  /**
   * @throws \Doctrine\Common\Persistence\Mapping\MappingException
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Doctrine\ORM\TransactionRequiredException
   */
  public function testSubmissionModel(){

    $statement = new FAStatement();
    $statement->person_id = $this->person->person_id;
    $statement->period_start = new DateTime('2019-01-01');
    $statement->period_end = new DateTime('2019-12-31');
    $statement->first_filed_at = new DateTime('2020-02-15');
    $statement->filer_id = 'TEST FILER';
    $statement->name = "John Doe";
    $statement->repno = 1234;
    $statement->old_repno = 4321;
    $statement->updated_at = new DateTime();
    $statement->email = "test@example.com";

    $this->dataManager->em->persist($statement);
    $this->dataManager->em->flush();

    $submission = new FASubmission();
    $this->assertInstanceOf(stdClass::class, $submission->user_data);
    $submission->realm = 'saw';
    $submission->uid = '2h4n2-3hj34-n23uin23j2n';
    $submission->user_data->statement = new stdClass();
    $submission->user_data->statement->statement_id = 1245214;
    $submission->user_data->person = new stdClass();
    $submission->user_data->person->name = 'Joe Bob';
    $submission->user_data->offices = [];
    $office = new stdClass();
    $office->office_code = 2458;
    $submission->user_data->offices[] = $office;
    $submission->updated_at = new DateTime('2019-06-06');
    $submission->period_start= new DateTime('2020-01-01');
    $submission->period_end = new DateTime('2020-12-31');
    $submission->fa_statement_id = $statement->statement_id;
    $submission->submitted_at = new DateTime('2019-06-06');
    $submission->verified = FALSE;
    $submission->certification_email = 'asdf@a.invalid';
    $submission->certification_name = 'John W Smith';
    $submission->certification = 'asdf@a.invalid';
    $submission->submitted = TRUE;
    $submission->published = FALSE;
    $this->assertSame(0, $submission->save_count);
    $submission->save_count = 1;

    $this->dataManager->em->persist($submission);
    $this->dataManager->em->flush();
    $this->dataManager->em->clear();

    $ss = $this->dataManager->em->find(FASubmission::class, $submission->submission_id);
    $this->assertEquals(1, $ss->save_count);
    $this->assertEquals($submission->realm, $ss->realm);
    $this->assertEquals($submission->uid, $ss->uid);
    $this->assertEquals($submission->user_data->statement->statement_id, $ss->user_data->statement->statement_id);
    $this->assertEquals($submission->user_data->person->name, $ss->user_data->person->name);
    $this->assertEquals($submission->user_data->offices[0]->office_code, $ss->user_data->offices[0]->office_code);
    $this->assertEquals($submission->updated_at, $ss->updated_at);
    $this->assertEquals($submission->fa_statement_id, $ss->fa_statement_id);
    $this->assertEquals($submission->submitted_at, $ss->submitted_at);
    $this->assertEquals($submission->verified, $ss->verified);
    $this->assertEquals($submission->certification_email, $ss->certification_email);
    $this->assertEquals($submission->certification_name, $ss->certification_name);
    $this->assertEquals($submission->certification_phone, $ss->certification_phone);
    $this->assertEquals($submission->certification, $ss->certification);
    $this->assertEquals($submission->submitted, $ss->submitted);
    $this->assertEquals($submission->published, $ss->published);
  }

  protected function tearDown(): void {
    $this->dataManager->rollback();
    parent::tearDown();
  }


}
