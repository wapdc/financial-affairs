select entity_id as person_id,
       e.name,
       e.email,
       t.filer_id,
       fs.submission_id,
       e.updated_at,
       fs.period_start,
       fs.period_end
from entity e
         join fa_submission fs on e.entity_id = fs.person_id
         join fa_statement t on t.submission_id = fs.submission_id
where published is distinct from true
  and verified = true
order by submitted_at desc