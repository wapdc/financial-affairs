import { format } from 'quasar';
import { retirementIncomeStep } from './retirementStep';

const { capitalize } = format;

const keys = retirementIncomeStep.fields.retirementIncomeArray.template;

const receivedBy = {
  key: keys.receivedBy.as,
  label: 'Received By',
  align: 'left',
  sortable: false,
  width: '25%',
  format: (value) => capitalize(value?.join(', ')),
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

const natureOfIncome = {
  key: keys.natureOfIncome.as,
  label: 'Nature of Income',
  align: 'left',
  sortable: false,
  width: '25%',
};

const providerName = {
  key: keys.providerName.as,
  label: 'Provider Name',
  align: 'left',
  sortable: false,
  width: '25%',
};

export default [receivedBy, natureOfIncome, providerName];
