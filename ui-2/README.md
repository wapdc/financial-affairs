# Quasar App (ui-2)

A Quasar Project

## Install the dependencies

```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

Bookmarklet to set the Apollo URL in local storage, for use in development without needing to run pantheon locally:

```javascript
javascript: (function () {
  localStorage.setItem("apollo_url", "https://demo-apollo-d8.pantheonsite.io/");
})();
```

### Lint the files

```bash
yarn lint
# or
npm run lint
```

### Format the files

```bash
yarn format
# or
npm run format
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).

# Application Notes

Most files in the `src` directory fall into one of three categories, defined below. This is intended to help maintain a simple and consistent codebase.

1. [Templates](#1-templates)
2. [Composables](#2-composables)
3. [Utility Functions & Helpers](#3-utility-functions--helpers)

## 1. Templates

**`.vue` files, consisting of templates, scripts, and styles**

These files define the UI. They are the visual representation of the application. Within this group, we can further break down the files into three subcategories:

### Layout

```
  ┌───────────────────────────────────────────────────┐
  │ App.vue                                           │
  │                                                   │
  │   Providers                                       │
  │  instantiated ┌────────────────────────────────┐  │
  │    here are   │ PApp                           │  │
  │  available to │  ┌──────────────────────────┐  │  │
  │   whole app   │  │ PUserLayout              │  │  │
  │               │  │  ┌─────────────────────┐ │  │  │
  │               │  │  │ PUserHeader         │ │  │  │
  │ x─┬───────────┴──┴──┴───────────────┬─►   │ │  │  │
  │   │ GlobalSubHeader.vue             │     │ │  │  │
  │   │                                 │     │ │  │  │
  │   │ (slot-drilled to header)        │     │ │  │  │
  │   └───────────┬──┬──┬───────────────┘     │ │  │  │
  │               │  │  └─────────────────────┘ │  │  │
  │               │  │                          │  │  │
  │               │  │  ┌─────────────────────┐ │  │  │
  │               │  │  │ <router-view>       │ │  │  │
  │    Path  ─────┼──┼► │ [Page rendered here]│ │  │  │
  │  determines   │  │  │ </router-view>      │ │  │  │
  │   rendered    │  │  └─────────────────────┘ │  │  │
  │     page      │  └──────────────────────────┘  │  │
  │               └────────────────────────────────┘  │
  └───────────────────────────────────────────────────┘
```

There is a single layout file used in the app, defined in the `@wapdc/pdc-ui` library at `quasar-app-extension-pdc-ui/src/components/PUserLayout.vue`. This layout is used to wrap all pages of the application. It contains the header, side menu, and footer components. This works by defining routes as children of the root layout in the `src/router/routes.js` file.

```yaml
Note: PUserLayout is being temporarily overridden by `src/library/PUserLayout.vue` to support a secondary header slot and added user links in the side menu.
```

### Pages

**`.vue` files in the `src/pages` directory**

A page represents a unique view in the application. Ideally, each page is attached to a unique route. Pages are composed of components, and may also use composables to fetch data or manage state. Splitting a page into smaller components can help to keep the codebase organized and maintainable.

### Generic Components

**`.vue` files in the `src/components` directory**, in the `@wapdc/pdc-ui` library, or from Quasar.

Pieces of the UI that are used in multiple places throughout the application, or pieces of UI that are used in a single place, but divided into files for convenience.

- [Vue.js Component Basics](https://vuejs.org/guide/essentials/component-basics)
- [Quasar Components](https://quasar.dev/components/)
- [wapdc/pdc-ui repo component dir](https://gitlab.com/wapdc/pdc-ui/-/tree/main/quasar-app-extension-pdc-ui/src/components)

### Pages vs. Generic Components

Pages are essentially top-level components, and can be split into smaller pieces. Folders within the `src/pages` directory should be named after the route they represent. If a page is composed of multiple components, each component should be placed in a separate file within the page's directory. If a component is used in multiple places, it probably should be placed in the `src/components` directory.

## 2. Composables

**`.js` files beginning with `use` in the `src/composables` directory**

### Context Composables

These modules primarily serve the purpose of offloading logic, specifically state management and synchronization (whether it be with the server or between components), allowing components to remain UI-focused and free of business logic.

> **Pattern outline:** JavaScript modules which provide state accessors (getters and setters), sharable across multiple components by importing Vue's `provide` and `inject` functions. Each module of this type exports a provider to be instantiated in the nearest common ancestor of components that will make use of the second and default export: a function that injects the provided state accessors into the component it is called in. A Symbol is used as a key to prevent name collisions in the `provide` and `inject` functions. For API connected modules, using useApollo, syncronizing state with the server is entirely encapsulated within the module. See `src/compsables/useTitle.js`, which serves as a straightforward example of this pattern.

### Standard Composables

These modules provide a single function or object that can be imported and used in a component. They are not tied to a specific component or context, and are intended to be reusable across the application. Distinct from utility functions, these modules may rely on Vue's reactivity system and may be used to manage state or perform side effects.

## 3. Utility & Helper Functions

**`util.js` files**

These files contain utility functions that are not tied to a specific component or context. They are intended to be reusable across the application.
