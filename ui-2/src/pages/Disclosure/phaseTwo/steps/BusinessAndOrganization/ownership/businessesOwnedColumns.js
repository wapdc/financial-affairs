import { format } from 'quasar';
import { businessesOwnedStep } from './businessesOwnedStep';

const { capitalize } = format;

const keys = businessesOwnedStep.fields.ownedBusinessesArray.template;

const owner = {
  key: 'who',
  label: 'Owned By',
  align: 'left',
  sortable: false,
  width: '50%',
  format: (value) => capitalize(value?.join(', ')),
};

const businessLegalName = {
  key: keys.legalName.as,
  label: 'Business Legal Name',
  align: 'left',
  sortable: false,
  width: '50%',
};

export default [owner, businessLegalName];
