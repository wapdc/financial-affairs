select o.offtitle, o.offcode, o.jurisdiction_category
from foffice o
where offcode in
  (select jo.offcode
   from jurisdiction_office jo
     join jurisdiction j on j.jurisdiction_id = jo.jurisdiction_id
   where (jo.valid_from is null or jo.valid_from <= now()::date)
     and (jo.valid_to is null or jo.valid_to >= now()::date)
)
and on_off_code=true
order by o.offtitle