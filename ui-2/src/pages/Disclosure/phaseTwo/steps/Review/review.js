import useWizard from 'src/wizard/useWizard';

const { lazyLoadWrapper } = useWizard();

const reviewStep = {
  title: 'Review',
  order: 0,
  component: lazyLoadWrapper(() => import('./Review.vue')),
};

export const reviewSection = {
  title: 'Review',
  order: 9,
  steps: {
    reviewStep,
  },
};

export async function determineFormValidity() {
  const wizard = useWizard();

  const knownInvalidSteps = wizard.getInvalidSteps();

  return {
    isValid: knownInvalidSteps.length === 0,
    invalidSteps: knownInvalidSteps,
  };
}
