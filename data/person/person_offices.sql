SELECT
  oh.office_code,
  p.position_id,
  o.offtitle as office,
  oh.address,
  oh.city,
  oh.state,
  oh.postcode,
  oh.email,
  j.jurisdiction_id,
  j.name as jurisdiction,
  oh.start_date,
  oh.end_date
FROM official oh
  JOIN foffice o ON o.offcode = oh.office_code
  JOIN jurisdiction j ON j.jurisdiction_id = oh.jurisdiction_id
  left join position p on p.position_id = oh.position_id
WHERE oh.person_id = :person_id
  AND ((oh.end_date is null OR oh.end_date > now() - interval '1 year') OR (COALESCE(CAST(:current as VARCHAR), 'N') = 'N'))
order by oh.start_date desc