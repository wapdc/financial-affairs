UPDATE fa_statement t
SET submission_id = u.submission_id
FROM (SELECT *
      FROM (SELECT submission_id,
                   fa_statement_id,
                   count(*) OVER ( PARTITION BY fa_statement_id ) as c,
                   row_number() OVER ( PARTITION BY fa_statement_id ORDER BY submitted_at DESC, submission_id DESC ) r
            FROM fa_submission
            WHERE verified = true) v
      WHERE r = 1 and c > 1) u
WHERE u.fa_statement_id = t.statement_id
