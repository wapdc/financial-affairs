<?php

namespace Drupal\fin_affairs\Controller;


use Drupal\wapdc_core\Controller\SPAControllerBase;
use WAPDC\FinancialAffairs\FinancialAffairs;

class FinancialAffairsPublicUIController extends SPAControllerBase
{
  /**
   * @return string
   * @throws \Exception
   */
  public function getApplicationDirectory()
  {
    return FinancialAffairs::service()->project_dir . '/ui/public/dist';
  }
}