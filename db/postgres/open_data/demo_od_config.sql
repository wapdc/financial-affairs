Delete from open_data_config where job_name = 'financial_affairs_statements' and stage = 'demo';
insert into open_data_config (job_name, stage, config, is_enabled)
values ('financial_affairs_statements', 'demo', '{
      "changes": [
        {
          "schema" : "public",
          "table": "fa_statement",
          "changes": ["insert", "update"],
          "columns": ["statement_id"],
          "event": "updateStatement"
        },
        {
          "schema" : "public",
          "table": "fa_statement",
          "changes": ["delete"],
          "columns": ["statement_id"],
          "event": "deleteStatement"
        },
        {
          "schema" : "public",
          "table": "candidacy",
          "changes": ["update", "insert"],
          "columns": ["fa_statement_id"],
          "event": "updateStatementFromCandidacy",
          "filter": {
            "fa_statement_id": ".+"
          }
        }
      ],
      "events": {
        "updateStatement": {
          "type": "upsert",
          "dataset_id": "4fxd-dirj",
          "source": "select * from fa_open_data where id=$1",
          "criteria": ["statement_id"]
        },
        "deleteStatement": {
          "type": "delete",
          "dataset_id": "4fxd-dirj",
          "source": "select id where id=$1",
          "criteria": ["statement_id"]
        },
        "updateStatementFromCandidacy": {
          "type": "upsert",
          "dataset_id": "4fxd-dirj",
          "source": "select * from fa_open_data where id=$1",
          "criteria": ["fa_statement_id"]
        }
      }
    }', true)