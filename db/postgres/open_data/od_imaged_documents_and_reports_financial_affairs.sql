drop view if exists od_imaged_documents_and_reports_financial_affairs cascade;
create or replace view od_imaged_documents_and_reports_financial_affairs as
select st.statement_id::text || '.financial_affairs'                as id,
       ''                                                           as report_number,
       case
           when amended = true then 'F1 AMENDED'
           else 'F1' end                                            as origin,
       case
           when amended = true then 'Amended Form F1, personal financial affairs statement'
           else 'Form F1, personal financial affairs statement' end as document_description,
       st.filer_id                                                  as filer_id,
       ''                                                           as type,
       st.name                                                      as filer_name,
       ''                                                           as office,
       ''                                                           as legislative_district,
       ''                                                           as party,
       TO_CHAR(st.period_end :: DATE, 'yyyy')                       as election_year,
       TO_CHAR(st.first_filed_at :: DATE, 'yyyy-MM-dd')             as receipt_date,
       TO_CHAR(st.first_filed_at :: DATE, 'yyyy-MM-dd')             as processed_date,
       'Electronic'                                                 as filing_method,
       TO_CHAR(st.period_start :: DATE, 'yyyy-MM-dd')               as report_from,
       TO_CHAR(st.period_end :: DATE, 'yyyy-MM-dd')                 as report_to,
       concat('<a href="', apollo_url(), '/public/financial-affairs/statement/', st.statement_id,
              '">View submission</a>')                              as url
from fa_statement st
         join (select fa_statement_id,
                      case
                          when count(1) > 1 then true
                          else false end as amended
               from fa_submission s
               where published = true
                 and verified = true
               group by fa_statement_id) su
              on st.statement_id = su.fa_statement_id;