import { computed, ref } from 'vue';
import { useApollo } from '@wapdc/pdc-ui/application';

const counties = ref(null);

/**
 * Fetch and return county data
 * @returns {Promise<Array>} Computed options list ({value, label} for a dropdown) of counties
 */
export default async function useCounties() {
  if (!counties.value) {
    counties.value = (await fetchCounties()) ?? [];
  }

  return computed(() => {
    return counties.value.map((el) => ({
      value: el.county,
      label: el.county,
    }));
  });
}

/**
 * Fetch counties from the API
 * @returns {Promise<Object>} Counties data object
 */
async function fetchCounties() {
  try {
    const service = useApollo('service/financial-affairs');
    return (await service.get('/counties')).data;
  } catch (error) {
    console.error('Error fetching counties:', error);
  }
}
