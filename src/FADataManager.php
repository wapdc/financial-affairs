<?php


namespace WAPDC\FinancialAffairs;

use WAPDC\Core\RDSDataManager;

class FADataManager extends RDSDataManager {

  public function __construct($migration_mode = FALSE) {
    parent::__construct('financial-affairs', $migration_mode);
  }

}