select s.name, u.user_name from fa_submission s join pdc_user u on u.uid = s.uid
where certification is null and updated_at > now() - interval '1 day'