import rules from 'src/utils/rules';
import useWizard from 'src/wizard/useWizard';

const { lazyLoadWrapper, dataPath, getData } = useWizard();

export const debtStep = {
  title: 'Debt',
  order: 0,
  component: lazyLoadWrapper(() => import('./Debt.vue')),
  fields: {
    hasDebt: {
      rules: [rules.requiredString],
      blocking: false,
      data() {
        return dataPath(this, 'statement.has_creditors');
      },
      transform: (hasField) => ({
        writeOut: () => {
          const hasCreditorsInputValue = hasField.data().value;
          const valueForApi = [null, 'null', undefined].includes(
            hasCreditorsInputValue,
          )
            ? null
            : Boolean(hasCreditorsInputValue === 'true');

          getData().statement.has_creditors = valueForApi;
        },
        readIn: () => {
          const data = getData();
          const hasCreditorsApiValue = data?.statement?.has_creditors;

          hasField.data().value = [null, undefined, 'null'].includes(
            hasCreditorsApiValue,
          )
            ? null
            : String(hasCreditorsApiValue);
        },
      }),
    },
    debtArray: {
      blocking: false,
      data() {
        return dataPath(this, 'statement.creditors');
      },
      requiredIf: () => [true, 'true'].includes(getData().statement.hasDebt),
      template: {
        creditorName: {
          as: 'name',
          rules: [rules.requiredString],
        },
        who: {
          rules: [rules.requiredNonEmptyArray],
        },
        originalAmount: {
          as: 'original_amount',
          rules: [rules.required],
        },
        paymentTerms: {
          as: 'payment_terms',
          rules: [rules.requiredString],
        },
        typeOfDebt: {
          as: 'security_given',
          rules: [rules.requiredString],
        },
        endingAmount: {
          as: 'ending_amount',
          rules: [rules.required],
        },
        streetAddress: {
          as: 'address',
          rules: [rules.requiredString, rules.maxLength(120)],
        },
        city: {
          rules: [rules.requiredString, rules.maxLength(120)],
        },
        state: {
          rules: [rules.required, rules.state],
        },
        zip: {
          as: 'postcode',
          rules: [rules.required, rules.exactLength(5), rules.numbersOnly],
        },
      },
    },
  },
};

export const debtSection = {
  title: 'Debt',
  order: 5,
  steps: {
    debtStep,
  },
};
