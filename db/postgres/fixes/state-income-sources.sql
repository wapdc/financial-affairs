/*
  I manually created a person collection in production database which was collection 202.
  From here the following scripts were used to populate the list of concerned officials
 */

delete from collection_member where collection_id=202;
insert into collection_member(collection_id, target_id, label)
select distinct 202, person_id, name || ' - ' || title from
(select p.person_id, p.name, COALESCE(fo1.offtitle, fo2.offtitle) as title, ca.general_result, o.start_date
  from person p
   left join official o
     on o.person_id=p.person_id
     and '2021-01-30' between o.start_date and coalesce(o.end_date, now())
       and (o.jurisdiction_id = 1000 or o.jurisdiction_id in (select jurisdiction_id from jurisdiction j2 where category=4))
   left join foffice fo1 ON fo1.offcode = o.office_code
   left join candidacy ca on ca.election_code='2022' and ca.person_id = p.person_id and ca.general_result in ('W','L')
    and (ca.jurisdiction_id = 1000 or ca.jurisdiction_id in (select jurisdiction_id from jurisdiction j2 where category=4))
   left join foffice fo2 on fo2.offcode = ca.office_code
where (o.official_id is not null or (ca.candidacy_id is not null and ca.general_result='W'))
order by 3,2) pop;

-- Income sources
select target_id as person_id, label, income->>'category' as category, income->>'who' as who, income->>'name' as name, income->>'income' as value from (
select target_id, label, json_array_elements(json->'statement'->'income'->'data') income
from (SELECT m.target_id, m.label, CAST(fs.user_data AS json) AS json
               FROM collection_member m
                      JOIN fa_statement f ON m.target_id = f.person_id
                      JOIN fa_submission fs ON fs.submission_id = f.submission_id
               WHERE '2021-12-31' BETWEEN f.period_start AND f.period_end
                 AND collection_id = 202) v ) i;;

-- Business interests
select target_id as person_id, label, business->>'category' as category, business->>'who' as who, business->>'legal_name' as name, business->>'operating_name' as operating_name, business->>'description' as description from (
   select target_id, label, json_array_elements(json->'statement'->'business_associations'->'data') business
   from (SELECT m.target_id, m.label, CAST(fs.user_data AS json) AS json
         FROM collection_member m
                JOIN fa_statement f ON m.target_id = f.person_id
                JOIN fa_submission fs ON fs.submission_id = f.submission_id
         WHERE '2021-12-31' BETWEEN f.period_start AND f.period_end
           AND collection_id = 202) v ) i;

-- Spouses
select target_id as person_id, label, statement->'person'->>'name' as name, statement->'statement'->>'spouse' as spouse, statement->'statement'->>'dependents' as dependents
   from (SELECT m.target_id, m.label, CAST(fs.user_data AS json) AS statement
         FROM collection_member m
                JOIN fa_statement f ON m.target_id = f.person_id
                JOIN fa_submission fs ON fs.submission_id = f.submission_id
         WHERE '2021-12-31' BETWEEN f.period_start AND f.period_end
           AND collection_id = 202) v ;



