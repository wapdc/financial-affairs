<?php

namespace WAPDC\FinancialAffairs;

use Exception;
use WAPDC\Core\CoreDataService;

class Validator {

  private $errors = [];

  public function addError($id, $message) {
    $this->errors[$id] = $message;
  }

  public function hasErrors() {
    return !empty($this->errors);
  }

  public function getErrors() {
    return ($this->errors);
  }

  /**
   * Validate an address setting errors if parts are missing.
   * @param \stdClass $address
   *   Object containing address data
   * @param string $error_prefix
   *   Prefix of the incomplete error message to set
   * @param string $message_prefix
   *   Message prefix for user error message.
   */
  public function validateAddress($address, $error_prefix, $message_prefix) {
    if (empty($address->address)) {
      $this->addError("$error_prefix.incomplete", "$message_prefix is incomplete");
    }
    if (empty($address->city)) {
      $this->addError("$error_prefix.incomplete", "$message_prefix is incomplete");
    }
    if (empty($address->state)) {
      $this->addError("$error_prefix.incomplete", "$message_prefix is incomplete");
    }
    if (empty($address->postcode)) {
      $this->addError("$error_prefix.incomplete", "$message_prefix is incomplete");
    }
  }


  /**
   * Code to name
   * @param string $code
   * @return string
   */
  protected function codeToName($code) {
    return str_replace('_', ' ', $code);
  }

  /**
   * Validate that all categories of data are answered and that each category has some corresponding data
   * @param \stdClass $object
   *   object containing the category styled property (usually $statement)
   * @param string $property
   *   name of the property containing the data/categories
   * @param array $categories_required
   *   categories required to be present
   * @param string $error_prefix
   *   prefix of the error code
   * @param string $message_prefix
   *   prefix of the error message
   */
  public function validateCategories($object, $property, $categories_required, $error_prefix, $message_prefix) {

    if (empty($object->$property)) {
      $object->$property = new \stdClass();
    }
    if (empty($object->$property->categories)) {
      $object->$property->categories = [];
    }
    if (empty($object->$property->data)) {
      $object->$property->data = [];
    }
    sort($categories_required);

    $message_lower = strtolower($message_prefix);
    $categories = $object->$property->categories;
    $data = $object->$property->data;

    $found_categories = [];
    foreach ($categories as $category) {
      $key = $category->category;
      $category_name = $this->codeToName($key);
      if (!isset($category->value)  && $category->value !== null) {
        $this->addError("$error_prefix.required", "The $category_name section under $message_lower is incomplete");
      }
      elseif ($category->value===TRUE) {
        $found_categories[] = $key;
        $itemForSelectedType = false;
        foreach ($data as $item) {
          if ($item->category === $key) {
            $itemForSelectedType = true;
          }
        }
        if (!$itemForSelectedType) {
          $this->addError("$error_prefix.$key.required", "The $message_lower $category_name section is incomplete");
        }
      } elseif ($category->value === FALSE) {
        $found_categories[] = $key;
        foreach ($data as $item) {
          if ($item->category === $key) {
            $this->addError("$error_prefix.$key.extra", "Invalid data found in the $message_lower $category_name section");
          }
        }
      }
      elseif ($category->value !== null) {
        $this->addError("$error_prefix.required", "The $category_name section under $message_lower is incomplete");
      }
    }
    sort($found_categories);
    if ($found_categories !== $categories_required) {
      $missing_categories = array_diff($categories_required, $found_categories);
      foreach ($missing_categories as $key) {
        $category_name = $this->codeToName($key);
        $this->addError("$error_prefix.$key.required", "The $category_name section under $message_lower is incomplete");
      }
    }
  }

  /**
   * If $property of $object is iterable then $values will be searched for
   *   the individual items of $property
   *
   * @param $object
   * @param $property
   * @param $values
   * @param $error_prefix
   * @param $message_prefix
   */
  public function validateList($object, $property, $values, $error_prefix, $message_prefix) {
    if (!empty($object->$property)) {
      if(is_iterable($object->$property)) {
        foreach ($object->$property as $item) {
          if (array_search($item, $values) === FALSE) {
            $this->addError("$error_prefix.invalid", "$message_prefix invalid value for $property");
          }
        }
      } elseif (array_search($object->$property, $values) === FALSE) {
        $this->addError("$error_prefix.invalid", "$message_prefix invalid value for $property");
      }
    }

  }

  /**
   * @param \stdClass $object
   *   Object containing who property
   * @param string $error_prefix
   *   Error code prefix
   * @param string $message_prefix
   *   Error message prefix
   */
  public function validateWho($object, $error_prefix, $message_prefix) {
    if (isset($object->who) && !is_array($object->who)) {
      $this->addError("$error_prefix.invalid", "$message_prefix property who must be specified as an array.");
    }
    $this->validateRequired($object, ['who'] , $error_prefix, $message_prefix);
    $this->validateList($object, 'who',  ['spouse', 'filer', 'dependent'], $error_prefix, $message_prefix);
  }

  /**
   * @param \stdClass $object
   *   $data
   * @param array $properties
   *   Array of property names to require.
   * @param string $error_prefix
   *   Error code prefix
   * @param string $message_prefix
   *   Message prefix
   */
  public function validateRequired($object, $properties, $error_prefix, $message_prefix) {
    $missing_fields = FALSE;
    foreach ($properties as $property) {
      if (!empty($object->$property) && is_string($object->$property)) {
        $object->$property = trim($object->$property);
        if (strlen($object->$property) > 255) {
          $this->addError("$error_prefix.invalid", "$message_prefix exceeds length limit of 255 characters");
        }
      }
      if (empty($object->$property) && (($object->$property ?? null) !== FALSE) ) {
        $missing_fields = TRUE;
      }
    }
    if ($missing_fields) {
      $this->addError("$error_prefix.incomplete", "$message_prefix incomplete");
    }
  }

  /**
   * Make sure amounts/values are in the properly specified ranges.
   *
   * @param \stdClass $object
   *   object containting property with range
   * @param string $property
   *   name of the property to validate
   * @param string $error_prefix
   *   error code prefix
   * @param string $message_prefix
   *   message code prefix
   */
  public function validateRange($object, $property, $error_prefix, $message_prefix) {

    $ranges = [
      '0-29999',
      '30000-59999',
      '60000-99999',
      '100000-199999',
      '200000-499999',
      '500000-749999',
      '750000-999999',
      '1000000-',
      ];

    $this->validateList($object, $property, $ranges, $error_prefix, $message_prefix);

  }

  /**
   * @param $id
   * @return bool
   */
  public function hasErrorId($id){
    return array_key_exists($id, $this->errors);
  }

  /**
   * @param $string
   * @return bool
   */
  public function hasErrorIdPortion($string) {
    return sizeof(preg_grep("/$string/i", array_keys($this->errors))) > 0;
  }

  /**
   * Validate dollar amount entry.
   * @param $object
   * @param $property
   * @param $error_prefix
   * @param $message_prefix
   */
  public function validateDollarAmount($object, $property, $error_prefix, $message_prefix) {
    if (isset($object->$property)) {

      if (!(float) $object->$property && $object->$property) {
        $this->addError("$error_prefix.invalid", "invalid value for $message_prefix" );
      }
    }
  }

  public function validateLocation($location, $error_prefix, $message_prefix, $allow_exempt = false ) {
    if (!empty($location->exempt_from_disclosure) &&
      (!empty($location->address) || !empty($location->zip)
        || !empty($location->parcel) || !empty($location->legal_description))) {
      $this->addError("$error_prefix.invalid", "Location information not removed after status indicated an exception");
    }

    // Skip validation of required fields when we have a residential address reporting modification and are working on a residential real estate
    if (!$allow_exempt || empty($location->exempt_from_disclosure)) {

      if (!empty($location->county) || !empty($location->parcel) || !empty($location->legal_description)) {
        if (empty($location->county) || (empty($location->parcel) && empty($location->legal_description))) {
          $this->addError("$error_prefix.incomplete", "$message_prefix incomplete");
        }
      }
      else {
        if ((empty($location->address) || empty($location->city) || empty($location->zip))) {
          $this->addError("$error_prefix.incomplete", "$message_prefix incomplete");
        }
      }
    } else {
      if (empty($location->city) || empty($location->residence_type)) {
        $this->addError("$error_prefix.incomplete", "City and residence type are required when submitting with a reporting modification");
      }
    }

  }

  /**
   * @param $class
   * @param $id
   * @param $numeric_key
   * @param $error_prefix
   * @param $message_prefix
   * @throws Exception
   */
  public function validateId($class, $id, $numeric_key, $error_prefix, $message_prefix) {
    $dm = CoreDataService::service()->getDataManager();
    if ($numeric_key && !is_numeric($id)){
      $this->addError($error_prefix.".invalid", $message_prefix." invalid");
    }
    else {
      $object = $dm->em->find($class, $id);
      if (!$object) {
        $this->addError($error_prefix . ".invalid", $message_prefix . " invalid");
      }
    }
  }

}