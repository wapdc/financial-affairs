#!/usr/bin/env php
<?php
require_once ('vendor/autoload.php');

// Instantiate a service
echo "Running upgrade scripts\n";
date_default_timezone_set('America/Los_Angeles');
$app = \WAPDC\FinancialAffairs\FinancialAffairs::service();

switch($argv[1]) {
  case "ormProxy":
    $app->generateProxyFiles();
    break;
  case "rds":
    $app->upgrade();
    break;
  default:
    echo "Unknown option!";
    exit(1);
}
