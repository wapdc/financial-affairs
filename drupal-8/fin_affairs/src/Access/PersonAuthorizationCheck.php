<?php


namespace Drupal\fin_affairs\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use WAPDC\Core\CoreDataService;
use WAPDC\Core\UserManager;
use WAPDC\FinancialAffairs\FinancialAffairs;
use WAPDC\Core\Util\Environment;

class PersonAuthorizationCheck implements AccessInterface {

  protected $realm = 'apollo';

  /**
   * @param AccountInterface $account
   * @param $person_id
   * @return AccessResult|\Drupal\Core\Access\AccessResultAllowed
   * @throws \Exception
   */
  public function access(AccountInterface $account, $person_id) {
    if ($account->hasPermission('access wapdc data')) {
      return AccessResult::allowed();
    }
    $has_access = false;
    // Load environment cause it might not have happened yet.
    $dir = \Drupal::service('file_system')->realpath("private://");
    $env = !empty($_ENV['PANTHEON_ENVIRONMENT']) ? $_ENV['PANTHEON_ENVIRONMENT'] : 'ddev';

    $file = "$dir/environment.$env.yml";
    Environment::loadFromYml($file);

    if (isset($_SESSION['SAW_USER_GUID'])) {
      $this->realm = 'saml_saw';
      $uid = $_SESSION['SAW_USER_GUID'];
    }
    else {
      $uid = $account->id();
    }
    $userManager = new UserManager(CoreDataService::service()
      ->getDataManager(), $this->realm);
    /** @var \WAPDC\Core\Model\User $user */
    $user = $userManager->getRegisteredUser($uid);
    $has_access = FinancialAffairs::service()->entityAuthorizationCheck($person_id, $user);
    return AccessResult::allowedIf($has_access);
  }
}