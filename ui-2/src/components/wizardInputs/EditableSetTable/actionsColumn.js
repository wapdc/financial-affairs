const actionsColumn = {
  name: 'actions',
  align: 'center',
  field: 'actions',
  sortable: false,
  style: 'min-width: 122px',
  headerStyle: 'min-width: 122px',
};

export default actionsColumn;
