import { format } from 'quasar';
const { capitalize } = format;

const who = {
  key: 'who',
  label: 'Gift Receiver',
  align: 'left',
  sortable: false,
  width: '25%',
  format: (value) => capitalize(value?.join(', ')),
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

const amount = {
  key: 'value',
  label: 'Amount',
  align: 'left',
  sortable: false,
  width: '25%',
  format: (value) => (value?.length ? `$${value}` : '-'),
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

export default [who, amount];
