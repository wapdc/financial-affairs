import useWizard from 'src/wizard/useWizard';
import rules from 'src/utils/rules';
import { CategoryHandler } from 'src/utils/CategoryHandler.class';
import {
  validateOfficerArray,
  validateAgencyArr,
  validateBusinessInterestPayments,
  validateHasBusinessInterests,
  validateHasBusinessInterestReceipts,
  validateBusinessInterestReceiptsArray,
  validateReportingChoice,
  validateHasOfficers,
  validateHasAgencyBalances,
  validateWaProperty,
  validateHasOfficePayments,
  validateOfficePayments,
  validateHasAgencyPayments,
  validateAgencyPayments,
  validateHasBusinessPayments,
  validateBusinessPayments,
} from 'src/pages/Disclosure/phaseTwo/steps/BusinessAndOrganization/validators';

const wizard = useWizard();
const { dataPath, getData, lazyLoadWrapper } = wizard;

const businessCategoryHandler = new CategoryHandler(
  'business_associations',
  'officer',
);

const stepName = 'businessesOtherStep';

export const businessesOtherStep = {
  title: 'Other relationships',
  order: 2,
  component: lazyLoadWrapper(() => import('./BusinessesOther.vue')),
  fields: {
    hasBusinessesOther: {
      rules: [rules.required],
      data() {
        return dataPath(this, 'pendingTransform.hasBusinessesOther');
      },
      transform: (field) => ({
        writeOut: () => businessCategoryHandler.writeOutHas(field),
        readIn: () => businessCategoryHandler.readInHas(field),
      }),
    },
    otherBusinessesArray: {
      blocking: false,
      data() {
        return dataPath(this, 'pendingTransform.otherBusinessesArray');
      },
      transform: (field) => ({
        writeOut: () => {
          convertStringsToBooleans(field);
          businessCategoryHandler.writeOutData(field);
        },
        readIn: () => {
          businessCategoryHandler.readInData(field);
          convertBooleansToStrings(field);
        },
      }),
      requiredIf: () =>
        [true, 'true'].includes(getData().pendingTransform.hasBusinessesOther),
      template: {
        who: {
          rules: [rules.requiredNonEmptyArray],
        },
        legalName: {
          as: 'legal_name',
          rules: [rules.requiredString],
        },
        operatingName: {
          as: 'operating_name',
          rules: [rules.requiredString],
        },
        description: {
          rules: [rules.requiredString],
        },
        percentageOfOwnership: {
          as: 'percentage_of_ownership',
          rules: [
            rules.required,
            rules.numbersOnly,
            rules.constrainNumber(1, 100),
          ],
        },
        streetAddress: {
          as: 'address',
          rules: [rules.requiredString],
        },
        city: {
          rules: [rules.requiredString],
        },
        state: {
          rules: [rules.required, rules.state],
        },
        zip: {
          as: 'postcode',
          rules: [rules.required, rules.exactLength(5), rules.numbersOnly],
        },
        isLendingInstitution: {
          as: 'is_lending_institution',
          rules: [rules.requiredString],
        },
        reportingChoice: {
          as: 'reporting_choice',
          rules: [(v, i = null) => validateReportingChoice(v, i, stepName)],
        },
        hasBusinessInterestPayments: {
          as: 'has_business_interest_payments',
          rules: [
            (v, i = null) => validateHasBusinessInterests(v, i, stepName),
          ],
        },
        businessInterestPayments: {
          as: 'business_interest_payments',
          rules: [
            (v, i = null) => validateBusinessInterestPayments(v, i, stepName),
          ],
        },
        hasBusinessInterestReceipts: {
          as: 'has_business_interest_receipts',
          rules: [
            (v, i = null) =>
              validateHasBusinessInterestReceipts(v, i, stepName),
          ],
        },
        businessInterestReceipts: {
          as: 'business_interest_receipts',
          rules: [
            (v, i = null) =>
              validateBusinessInterestReceiptsArray(v, i, stepName),
          ],
        },
        hasOfficers: {
          as: 'has_officers',
          rules: [(v, i = null) => validateHasOfficers(v, i, stepName)],
        },
        officers: {
          rules: [(v, i = null) => validateOfficerArray(v, i, stepName)],
        },
        hasAgencyBalances: {
          as: 'has_agency_balances',
          rules: [(v, i = null) => validateHasAgencyBalances(v, i, stepName)],
        },
        agencyBalances: {
          as: 'agency_balances',
          rules: [(v, i = null) => validateAgencyArr(v, i, stepName)],
        },
        hasWaProperty: {
          as: 'has_wa_property',
          rules: [rules.requiredString],
        },
        waProperties: {
          as: 'wa_property',
          rules: [(v, i = null) => validateWaProperty(v, i, stepName)],
        },
        hasOfficePayments: {
          as: 'has_office_payments',
          rules: [(v, i = null) => validateHasOfficePayments(v, i, stepName)],
        },
        // add interest payments and receipts
        officePayments: {
          as: 'office_payments',
          rules: [(v, i = null) => validateOfficePayments(v, i, stepName)],
        },
        hasAgencyPayments: {
          as: 'has_agency_payments',
          rules: [(v, i = null) => validateHasAgencyPayments(v, i, stepName)],
        },
        agencyPayments: {
          as: 'agency_payments',
          rules: [(v, i = null) => validateAgencyPayments(v, i, stepName)],
        },
        hasBusinessPayments: {
          as: 'has_business_payments',
          rules: [(v, i = null) => validateHasBusinessPayments(v, i, stepName)],
        },
        businessPayments: {
          as: 'business_payments',
          rules: [(v, i = null) => validateBusinessPayments(v, i, stepName)],
        },
      },
    },
  },
};

/**
 * For these owned businesses fields:
 * - On intake, convert boolean values to strings
 * - On output, convert string values to booleans
 */
const stringBoolConversionKeys = [
  'isLendingInstitution',
  'hasOfficers',
  'hasAgencyBalances',
  'hasWaProperty',
  'hasOfficePayments',
  'hasAgencyPayments',
  'hasBusinessPayments',
  'hasBusinessInterestPayments',
  'hasBusinessInterestReceipts',
];

/**
 * Convert boolean values to strings for owned businesses,
 * since the old client expects boolean values.
 */
function convertBooleansToStrings(field) {
  const ownedBusinessesUtils = wizard.getFieldSetUtils(field);
  const data = ownedBusinessesUtils.getAllData();
  const length = data?.value?.length;

  for (let i = 0; i < length; i++) {
    const ownedBusiness = ownedBusinessesUtils.get(i);

    stringBoolConversionKeys.forEach((key) => {
      const fieldValue = ownedBusiness.getValue(key);

      if (fieldValue === true || fieldValue === false) {
        ownedBusiness.setValue(key, String(fieldValue));
      }
    });
  }
}

/**
 * Convert boolean values to strings for owned businesses,
 * since the old client expects boolean values, but we expect string
 * values in our radio buttons.
 */
function convertStringsToBooleans(field) {
  const ownedBusinessesUtils = wizard.getFieldSetUtils(field);
  const data = ownedBusinessesUtils.getAllData();
  const length = data?.value?.length;

  for (let i = 0; i < length; i++) {
    const ownedBusiness = ownedBusinessesUtils.get(i);

    stringBoolConversionKeys.forEach((key) => {
      const fieldValue = ownedBusiness.getValue(key);

      if (fieldValue === 'true' || fieldValue === 'false') {
        ownedBusiness.setValue(key, fieldValue === 'true');
      }
    });
  }
}
