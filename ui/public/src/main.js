import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import ServiceContainer from "./ServiceContainer";
import globalFilters from './filters'

Vue.config.productionTip = false

export const app = new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
})

// Global filters are defined in filters.js of the financial affairs ui project
globalFilters.defineGlobalFilters();

app.$container = new ServiceContainer();

app.$mount('#app');
