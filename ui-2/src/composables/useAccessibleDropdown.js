import { computed, nextTick, onBeforeUnmount, onMounted, ref } from 'vue';

/**
 * Accessible dropdown composable, handles open/close, keyboard navigation, and focus management
 * @param {Object} options
 * @param {Ref} options.triggerRef Ref for dropdown trigger element
 * @param {Ref} options.listRef Ref for dropdown list element containing focusable items
 * @param {Function} [options.onOpen] Callback function to run when dropdown opens
 * @param {Function} [options.onClose] Callback function to run when dropdown closes
 */
export default function useAccessibleDropdown({
  triggerRef,
  listRef,
  onOpen = null,
  onClose = null,
}) {
  const _isOpen = ref(false);
  const isOpen = computed({
    get: () => _isOpen.value,
    set: (newValue) => {
      _isOpen.value = newValue;
      !!newValue ? nextTick(openDropdown) : closeDropdown();
    },
  });

  onMounted(() => document.addEventListener('keydown', handleKeydown));
  onBeforeUnmount(() => document.removeEventListener('keydown', handleKeydown));

  /** Calls appropriate function based on keydown event defined by key-action map */
  function handleKeydown(event) {
    const keyActionMap = {
      ArrowUp: () => handleFocusChange(event, 'prev'),
      ArrowDown: () => handleArrowDown(event),
      Tab: () => handleFocusChange(event, 'next', false),
      Escape: () => (isOpen.value = false),
      Shift: {
        // Shift + Tab
        Tab: () => handleFocusChange(event, 'prev', false),
      },
    };

    if (event.shiftKey) {
      keyActionMap.Shift[event.key]?.();
    } else {
      keyActionMap[event.key]?.();
    }
  }

  /** Arrow down on trigger opens menu; on menu item, moves focus to next */
  function handleArrowDown(event) {
    if (triggerRef.value?.$el?.contains(document.activeElement)) {
      event.preventDefault();
      isOpen.value = !isOpen.value;
    } else {
      handleFocusChange(event, 'next');
    }
  }

  /** Open dropdown and call provided onOpen callback */
  function openDropdown() {
    triggerRef.value?.$el?.setAttribute('aria-expanded', true);
    focusFirstDropdownItem();
    if (typeof onOpen === 'function') {
      onOpen();
    }
  }

  /** Close dropdown and call provided onClose callback */
  function closeDropdown() {
    triggerRef.value?.$el?.setAttribute('aria-expanded', false);
    if (typeof onClose === 'function') {
      onClose();
    }
  }

  /** @returns {NodeListOf<HTMLElement>} */
  function fetchFocusableDropdownItems() {
    const containerElement = listRef.value?.$el;
    return containerElement?.querySelectorAll('[tabindex="0"]');
  }

  /** Focuses the first dropdown item */
  function focusFirstDropdownItem() {
    const firstItem = fetchFocusableDropdownItems()?.[0];
    nextTick(() => firstItem?.focus());
  }

  /**
   * Executes focus change (if needed), and closes dropdown if new focus target is out of range of dropdown items
   * @param {KeyboardEvent} event
   * @param {'next'|'prev'} direction Direction to move focus, either 'next' or 'prev'
   * @param {Boolean} [execute=true] Whether to execute the focus change (false lets caller actuate focus functionality)
   */
  function handleFocusChange(event, direction, execute = true) {
    if (direction !== 'next' && direction !== 'prev') {
      return console.warn(
        `Invalid direction given to handleFocusChange: ${direction}, must be 'next' or 'prev'`,
      );
    }

    if (execute) {
      // Prevents, for example, the page from scrolling when pressing arrow keys
      event.preventDefault();
    }

    const focusableElements = Array.from(fetchFocusableDropdownItems() || []);
    const currentIndex = focusableElements?.indexOf(document.activeElement);

    if (currentIndex === -1) {
      isOpen.value = false;
      return;
    }

    const targetIndex =
      direction === 'next' ? currentIndex + 1 : currentIndex - 1;

    const targetIndexIsInRange =
      targetIndex > -1 && targetIndex < focusableElements.length;

    if (targetIndexIsInRange) {
      if (execute) focusableElements[targetIndex].focus();
    } else {
      isOpen.value = false;
    }
  }

  return isOpen;
}
