#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc <<EOF
alter table fa_submission add column if not exists certification_phone varchar(20);
EOF
