SELECT j.name, jo.jurisdiction_office_id, j.jurisdiction_id, o.offtitle, jo.partisan
FROM jurisdiction j
  JOIN jurisdiction_office jo
    ON j.jurisdiction_id = jo.jurisdiction_id
      and (jo.valid_from is null or jo.valid_from <= now()::date)
      and (jo.valid_to is null or jo.valid_to >= now()::date)
  JOIN foffice o ON jo.offcode = o.offcode
    and o.on_off_code = true
WHERE o.offcode = :office_code
  and j.inactive is null
  and (j.valid_from is null or j.valid_from <= now()::date)
  and (j.valid_to is null or j.valid_to >= now()::date)
ORDER BY j.name