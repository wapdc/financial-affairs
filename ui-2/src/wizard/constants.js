export const STEP_STATUS = {
  NOT_STARTED: 'not-started',
  INCOMPLETE: 'incomplete',
  COMPLETED: 'completed',
};

export const ACTIVE_STEP_CLASSNAME = 'current-active-step';
