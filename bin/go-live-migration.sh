#!/usr/bin/env bash
set -e
export PATH=./bin:$PATH
## If no CI assume a lando container so environment comes from lando
if [[ -z "$CI_COMMIT_REF_NAME" ]] ; then
  . vendor/wapdc/core/ci/ci_build_environment.sh
## Otherwise we assume that we're building in the devops image and we can get the environment
## from that project
else
  . ci_build_environment.sh
fi
if [[ -f "local/environment.sh" ]] ; then
  . ./local/environment.sh
fi

echo "running f1 migration..."

php -d memory_limit=512M bin/pdc_fa.php migrate
