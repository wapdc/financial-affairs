import { format } from 'quasar';
import { otherInvestmentStep } from './otherInvestmentStep';

const { capitalize } = format;

const keys = otherInvestmentStep.fields.investmentTypeArray.template;

const heldBy = {
  key: keys.heldBy.as,
  label: 'Held By',
  align: 'left',
  sortable: false,
  width: '50%',
  format: (value) => capitalize(value?.join(', ')),
};

const investmentName = {
  key: keys.investmentName.as,
  label: 'Investment Name',
  align: 'left',
  sortable: false,
  width: '50%',
};

export default [heldBy, investmentName];
