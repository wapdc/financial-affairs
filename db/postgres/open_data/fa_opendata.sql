drop view if exists fa_open_data cascade;
create or replace view fa_open_data as
select statement.statement_id                                  as id,
       statement.submission_id                                 as submission_id,
       statement.repno                                         as report_number,
       statement.filer_id                                      as filer_id,
       statement.person_id                                     as person_id,
       statement.name                                          as name,
       TO_CHAR(statement.first_filed_at :: DATE, 'yyyy-MM-dd') as receipt_date,
       TO_CHAR(statement.period_start :: DATE, 'yyyy-MM-dd')   as period_start,
       TO_CHAR(statement.period_end :: DATE, 'yyyy-MM-dd')     as period_end,
       sub.user_data::varchar                                  as json,
       coalesce(can.candidacies, '[]') ::varchar               as candidacies,
       coalesce(of.offices, '[]') ::varchar                    as offices,
       sub.certification,
       sub.certification_name,
       sub.certification_email,
       sub.certification_phone,
       CASE
           WHEN sub.submission_id is null THEN
               concat('<a href="', pdc_url(), '/browse/request-agency-records">Request record</a>')
           ELSE
               concat('<a href="', apollo_url(), '/public/financial-affairs/statement/', statement.statement_id,
                      '">View submission</a>')
           END                                                 as url,
       coalesce(sub2.history, '[]') ::varchar                  as history
from fa_statement statement
         left join fa_submission sub on sub.submission_id = statement.submission_id and published = true
         left join(
    select fs2.statement_id,
           array_to_json(array_agg(json_build_object(
                   'person_name', coalesce(p.name, ''),
                   'office', coalesce(fo.offtitle, ''),
                   'jurisdiction', coalesce(j.name, ''),
                   'position', coalesce(po.title, ''),
                   'campaign_start_date', coalesce(ca.campaign_start_date::varchar, ''),
                   'election', coalesce(e.title, ''),
                   'address', coalesce(ca.address, ''),
                   'city', coalesce(ca.city, ''),
                   'state', coalesce(ca.state, ''),
                   'postcode', coalesce(ca.postcode, '')
               ))) as candidacies
    from fa_statement fs2
             left join person p on p.person_id = fs2.person_id
             left join candidacy ca on ca.person_id = fs2.person_id
             left join election e on ca.election_code = e.election_code
             left join foffice fo on fo.offcode = ca.office_code
             left join jurisdiction j on ca.jurisdiction_id = j.jurisdiction_id
             left join position po on po.position_id = ca.position_id
    where ca.campaign_start_date - interval '1 day' >= fs2.period_start
      and ca.campaign_start_date - interval '1 day' <= fs2.period_end
    group by fs2.statement_id
) can on can.statement_id = statement.statement_id
         left join (
    select fs3.statement_id,
           array_to_json(array_agg(json_build_object(
                   'office', coalesce(fo.offtitle, ''),
                   'jurisdiction', coalesce(j.name, ''),
                   'position', coalesce(po.title, ''),
                   'start_date', coalesce(o.start_date::varchar, ''),
                   'end_date', coalesce(o.end_date::varchar, ''),
                   'appointed_until', coalesce(o.appointed_until::varchar, ''),
                   'address', coalesce(o.address, ''),
                   'city', coalesce(o.city, ''),
                   'state', coalesce(o.state, ''),
                   'postcode', coalesce(o.postcode, '')
               ))) as offices
    from fa_statement fs3
             join official o on o.person_id = fs3.person_id and
                                     fs3.period_end >= o.start_date - interval '1 year' and
                                     fs3.period_start <= coalesce(o.end_date, fs3.period_start)
             join foffice fo on fo.offcode = o.office_code
             join jurisdiction j on o.jurisdiction_id = j.jurisdiction_id
             left join position po on po.position_id = o.position_id
    where fs3.period_end between o.start_date - interval '1 day' and coalesce(o.end_date, fs3.period_end)
    group by fs3.statement_id
) of on of.statement_id = statement.statement_id
         left join (
    select fs4.statement_id,
           array_to_json(array_agg(json_build_object(
                                           'submitted_at', coalesce(sub2.submitted_at::varchar, ''),
                                           'submission_id', coalesce(sub2.submission_id, 0::integer)
                                       ) order by sub2.submitted_at)) as history
    from fa_statement fs4
             join fa_submission sub2 on fs4.statement_id = sub2.fa_statement_id
    group by fs4.statement_id) sub2 on ((sub2.statement_id = statement.statement_id))
