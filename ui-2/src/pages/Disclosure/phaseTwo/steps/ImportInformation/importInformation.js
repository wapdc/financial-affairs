import useWizard from 'src/wizard/useWizard';

const { lazyLoadWrapper } = useWizard();

export const importInformationSection = {
  title: 'Import information',
  order: 1,
  steps: {
    overview: {
      title: 'Import information',
      order: 0,
      component: lazyLoadWrapper(() => import('./ImportInformation.vue')),
    },
  },
};
