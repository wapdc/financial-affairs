SELECT ea.uid, ea.realm, sub.submission_id
FROM fa_submission sub
  JOIN fa_statement sta ON sub.fa_statement_id = sta.statement_id
  JOIN entity e on sta.person_id = e.entity_id
  JOIN entity_authorization ea on e.entity_id = ea.entity_id
WHERE sub.submission_id = :submission_id
AND ea.uid = :uid AND ea.realm = :realm