declare module "@wapdc/pdc-ui/application" {
  export interface User {
    name: string;
    uid: string;
    admin: boolean;
    roles: string[];
    email: string;
    auth?: {
      token: string;
      expires: number;
    };
    rights?: Record<string, boolean>;
    appInfo?: {
      expires: number;
    };
  }

  export interface Application {
    user: User;
    hasAccessToContent: (criteria: any) => boolean;
    userHasRight: (right: string) => boolean;
    userHasPermission: (permission: string) => boolean;
    getSetting: (setting: string) => any;
    refresh: () => Promise<void>;
  }

  export interface Gateway {
    init: (url: string, token?: string) => Promise<void>;
  }

  export interface Apollo {
    init: (url: string) => void;
  }

  export function useApollo(serviceName: string): Apollo;
  export async function useApplication(): Promise<Application>;
  export function useGateway(name: string, port?: number): Promise<Gateway>;
}
