import { computed, inject, onMounted, provide, reactive } from 'vue';
import { useApollo } from '@wapdc/pdc-ui/application';

const FilerAccessorSymbol = Symbol('filerContext');

/**
 * Composable function to provide filer data accessors to descendent components.
 * Must be instantiated in the nearest common ancestor of components that import the
 * useFiler composable.
 * @returns {void}
 */
export function useFilerProvider() {
  const state = reactive({
    availableFilers: [],
    selectedFiler: null,
    locked: false,
  });

  const publicService = useApollo('public/service/financial-affairs');
  const service = useApollo('service/financial-affairs');

  const defaultPersonObject = { ['person_id']: -1, name: 'New Filer' };

  async function fetchAvailableFilers() {
    const result = await publicService.get('/auth-check');
    const auth = result?.authorizations;
    return auth?.person || [];
  }

  function setAvailableFilersState(availableFilers) {
    state.availableFilers = availableFilers.length
      ? availableFilers
      : [defaultPersonObject];
    state.selectedFiler = state.availableFilers.find(
      (p) =>
        p.person_id === state.selectedFiler?.person_id || defaultPersonObject,
    );
  }

  async function searchFilers(searchTerm) {
    const results = await service.post('person-match/', {
      person: { name: searchTerm },
    });
    return _mapFilers(results?.matches) || [];
  }

  function _mapFilers(filers) {
    return filers.map((f) => {
      const { person_id: personId, name, candidacies } = f;
      const { office, jurisdiction, election } = candidacies[0] || {};
      return {
        personId,
        name,
        office,
        jurisdiction,
        election,
      };
    }, []);
  }

  function setLock(isLocked = false) {
    state.locked = isLocked;
  }

  /**
   * Set the selected filer by their person ID and return the ID
   * @param {number} personId - The ID of the person to select
   * @param {Array} [filersArray] - An optional array of filers to search, defaults to state.availableFilers
   * @returns {number} The ID of the selected filer
   */
  function setSelectedFilerByPersonId(personId = -1, filersArray = []) {
    const availableFilers = filersArray.length
      ? filersArray
      : state.availableFilers;

    const matchPredicate = (p) => +(p.person_id || p.personId) === +personId;

    const match = availableFilers.find(matchPredicate);

    // If a filers array is given, we should make sure the selected filer is in the state
    if (filersArray.length) {
      const matchIsInState = state.availableFilers.find(matchPredicate);
      if (!matchIsInState) {
        state.availableFilers.push(match);
      }
    }

    state.selectedFiler = match || defaultPersonObject;
    return state.selectedFiler.person_id;
  }

  function updateSelectedFilerName(name) {
    state.selectedFiler.name = name;
  }

  onMounted(async () => {
    const availableFilers = await fetchAvailableFilers();
    setAvailableFilersState(availableFilers);
  });

  const accessors = {
    availableFilers: computed(() => state.availableFilers),
    fetchAvailableFilers,
    locked: computed(() => state.locked),
    searchFilers,
    selectedFiler: computed(() => state.selectedFiler),
    selectedFilerPersonId: computed(() => state.selectedFiler?.person_id),
    setLock,
    setSelectedFilerByPersonId,
    updateSelectedFilerName,
  };

  provide(FilerAccessorSymbol, accessors);

  return accessors;
}

/**
 * Composable function to return filer data accessors. Must be used within a component where
 * useFilerProvider has been instantiated in an ancestor component.
 * @returns {{
 *   availableFilers: ComputedRef<Array>,
 *   fetchAvailableFilers: () => Promise<Array>,
 *   selectedFiler: ComputedRef<Object>,
 *   selectedFilerPersonId: ComputedRef<string | null>,
 *   locked: ComputedRef<boolean>,
 *   setSelectedFilerByPersonId: (personId: string, filersArray?: Array<Object>) => number,
 *   setLock: (isLocked: boolean) => void,
 *   searchFilers: (searchTerm: string) => Promise<Array>,
 *   updateSelectedFilerName: (name: string) => void,
 * }} filerAccessor:
 *   - availableFilers: Computed list of available filers.
 *   - fetchAvailableFilers: Function to fetch available filers from the API.
 *   - selectedFiler: Computed currently selected filer.
 *   - selectedFilerPersonId: Computed ID of the selected filer.
 *   - setSelectedFilerByPersonId: Function to set the selected filer by their ID. Optionally accepts an
 * array of filers to search if the selected filer is not in the state yet (and adds it if not).
 *   - searchFilers: Function to search for filers by name.
 *   - updateSelectedFilerName: Function to update the name of the selected filer.
 * @throws {Error} Throws an error if useFilerProvider() has not been instantiated in an ancestor component.
 */
export default function useFiler() {
  const filerContext = inject(FilerAccessorSymbol);
  if (!filerContext) {
    throw new Error('useFiler must be used within a provider');
  }
  return filerContext;
}
