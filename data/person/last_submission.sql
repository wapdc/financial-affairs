SELECT fa.*
FROM fa_submission fa
WHERE
  submitted = TRUE
  AND ((uid = :uid AND realm = :realm
  AND (CAST(:person_id AS INTEGER) IS NULL AND (person_id IS NULL OR person_id = -1)))
  OR (person_id = CAST(:person_id AS INTEGER)))
ORDER BY submitted_at DESC LIMIT 1
