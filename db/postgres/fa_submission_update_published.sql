UPDATE fa_submission
SET published = TRUE
WHERE published = false
and submitted = true
and verified = true