import moment from 'moment';

export function sortDate(a, b) {
  const dateA = moment(a);
  const dateB = moment(b);
  return dateA.isBefore(dateB) ? -1 : dateA.isAfter(dateB) ? 1 : 0;
}
