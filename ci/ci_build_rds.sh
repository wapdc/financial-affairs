#!/usr/bin/env bash
# determine environment
set -e
export PATH=/app/bin:$PATH
export PHP_IDE_CONFIG="serverName=financial-affairs"

echo "Current working directory...."
pwd
echo "Working directory contents...."
ls -l
echo "Vendor directory..."
composer config -g gitlab-token.gitlab.com "$COMPOSER_ACCESS_TOKEN"
composer install --quiet --no-progress
## If no CI assume a lando container so environment comes from lando
if [[ -z "$CI_COMMIT_REF_NAME" ]] ; then
  . vendor/wapdc/core/ci/ci_build_environment.sh
## Otherwise we assume that we're building in the devops image and we can get the environment
## from that project
else
  . ci_build_environment.sh
fi

## make sure revision scripts are runnable:
chmod a+x db/postgres/revisions/*

## Execute the build script.
ci/financial-affairs-upgrade.php rds
if [[ "$STAGE" != "live" ]] ; then
  echo "Running test environment scripts..."
  $PSQL_CMD wapdc -f db/postgres/fa_statement.insert.sql
  bin/pdc_fa.php import-test-submissions /ci/data
fi