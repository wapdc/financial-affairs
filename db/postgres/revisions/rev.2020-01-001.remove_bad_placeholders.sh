#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc <<EOF
DELETE FROM fa_statement where statement_id in (select statement_id from fa_statement
  where first_filed_at = '2019-08-05'
  and
 filer_id not in
 ('BOLED  029',
'BUTTE  362',
'DEGLR  233',
'DEMOJ--264',
'DESMM--370',
'DICKD  568',
'FOSSJ  198',
'FRANC  201',
'FRIEA  382',
'GORDF  520',
'GREAL  383',
'HOCHP  837',
'JOHNJ  277',
'KAPPJ  033',
'KEGER  520',
'LAWTJ  589',
'MARTA--346',
'MARTR--063',
'MCCLD--802',
'MCKEB  141',
'MCLAK  212',
'MITCC  338',
'ONEIJ--284',
'ORTEC--901',
'ROSEE  611',
'TAYLJ  648',
'VILLL  932'));
EOF
