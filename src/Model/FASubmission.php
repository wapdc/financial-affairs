<?php


namespace WAPDC\FinancialAffairs\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class FASubmission
 *
 * @package WAPDC\FinancialAffairs\Model
 * @Entity
 * @Table(name="fa_submission")
 * @HasLifeCycleCallbacks
 */
#[ORM\Entity]
#[ORM\Table(name: 'fa_submission')]
#[ORM\HasLifecycleCallbacks]
class FASubmission {

  /**
   * @Column
   * @Id
   * @GeneratedValue
   */
  #[ORM\Column]
  #[ORM\Id]
  #[ORM\GeneratedValue]
  public $submission_id;

  /**
   * @Column
   */
  #[ORM\Column]
  public $realm;

  /**
   * @Column
   */
  #[ORM\Column]
  public $uid;

  /**
   * @Column(name="user_data")
   */
  #[ORM\Column(name: 'user_data')]
  private $user_data_serialized;

  /**
   * @var \stdClass
   */
  public $user_data;

  /**
   * @Column(type="datetimetz")
   * @var \DateTime
   */
  #[ORM\Column(type: 'datetimetz')]
  public $updated_at;

  /**
   * @Column
   */
  #[ORM\Column]
  public $fa_statement_id;

  /**
   * @Column(type="datetimetz")
   * @var \DateTime
   */
  #[ORM\Column(type: 'datetimetz')]
  public $submitted_at;

  /**
   * @Column(type="boolean")
   */
  #[ORM\Column(type: 'boolean')]
  public $verified = FALSE;

  /**
   * @Column
   */
  #[ORM\Column]
  public $certification_email;

  /**
   * @Column
   */
  #[ORM\Column]
  public $certification_phone;

  /**
   * @Column
   */
  #[ORM\Column]
  public $certification_name;

  /**
   * @Column
   */
  #[ORM\Column]
  public $certification;

  /**
   * @Column(type="boolean")
   */
  #[ORM\Column(type: 'boolean')]
  public $submitted = FALSE;

  /**
   * @Column(type="boolean")
   */
  #[ORM\Column(type: 'boolean')]
  public $published = FALSE;

  /**
   * @Column(type="integer")
   */
  #[ORM\Column(type: 'integer')]
  public $save_count = 0;

  /**
   * @Column(type="integer")
   */
  #[ORM\Column(type: 'integer')]
  public $person_id;

  /**
   * @var \DateTime
   * @Column(type="date")
   */
  #[ORM\Column(type: 'date')]
  public $period_start;

  /**
   * @var \DateTime
   * @Column(type="date")
   */
  #[ORM\Column(type: 'date')]
  public $period_end;

  /**
   * @Column
   */
  #[ORM\Column]
  public $name;

  /**
   * @Column(type="integer")
   */
  #[ORM\Column(type: 'integer')]
  public $amend_id;

  /**
   * FASubmission constructor.
   */
  public function __construct() {
    $this->user_data = new \stdClass();
  }

  /**
   * @postLoad
   */
  #[ORM\PostLoad]
  public function onLoad() {
    $this->user_data = json_decode($this->user_data_serialized);
  }

  /**
   * @prePersist
   * @preUpdate
   */
  #[ORM\PrePersist]
  #[ORM\PreUpdate]
  public function onUpdate(){
    if (!empty($this->user_data)) {
      $this->user_data_serialized = json_encode($this->user_data);
    }
  }
}