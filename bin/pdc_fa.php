#!/usr/bin/env php
<?php

use Symfony\Component\Yaml\Yaml;
use WAPDC\FinancialAffairs\StatementProcessor;
use WAPDC\FinancialAffairs\FADataManager;

$memory_limit = ini_get('memory_limit');

echo "Memory limit $memory_limit\n";
require_once('vendor/autoload.php');
date_default_timezone_set('America/Los_Angeles');

if ($argc < 2) {
  $cmd = $argv[0];
  echo "Usage: $cmd <command>\n";
  echo "   where command is: ";
  echo "     fa-data-dump  - Generates open data dump file and curls to Socrata. \n";
  echo "     import-test-submissions - Used to create (testing) FA submissions from YML files.\n; ";
  exit(1);
}


if ($argc < 2) {
  $cmd = $argv[0];
  echo "Usage: $cmd <migrate>\n";
  exit(1);
}
else {
  // Instantiate a service
  $command = strtolower($argv[1]);

  echo "Running $command\n";
  $service = \WAPDC\FinancialAffairs\FinancialAffairs::service();
  switch ($command) {
    case 'import-test-submissions':
      if (empty($argv[2])) {
        echo "Arguments required: \n 1. dir : Directory where test data yml files are kept (e.g., /ci/data).\n";
        echo "Example: $ php bin/pdc_fa.php import-test-submissions /ci/data\n";
        exit(1);
      }
      $dir = dirname(__DIR__) . $argv[2];
      $processor = new StatementProcessor(new FADataManager());
      $fileList = array_diff(scandir($dir), array('.', '..'));
      foreach ($fileList as $file) {
        $submission = Yaml::parseFile("$dir/$file", Yaml::PARSE_OBJECT_FOR_MAP);
        $processor->saveTestUserFromSubmission($submission);
      }
      break;

    default:
      echo "Unknown command $command";
  }
}
