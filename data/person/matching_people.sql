SELECT
  p.person_id,
  p.name,
  p.email,
  ROUND((similarity(normalized_name(p.name), normalized_name(CAST(:search AS VARCHAR)))) * 100) AS score
FROM person p
  WHERE
    similarity(normalized_name(p.name), normalized_name(CAST(:search AS VARCHAR))) >= 0.3
ORDER BY score DESC LIMIT 5
