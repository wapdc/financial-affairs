import { ref } from 'vue';

/**
 * Combined focus and hover state
 * @returns {Object} hovered, focusBinds
 * @example
 * ```vue
 *  import useFocusAsHover from 'src/composables/useFocusAsHover.js';
 *  const { hovered, focusBinds } = useFocusAsHover();
 *  <input
 *    :class="{ 'border-blue-500': hovered }"
 *    v-bind="focusBinds"
 *  />
 * ```
 */
export default function useFocusAsHover() {
  const hovered = ref(false);

  const onHighlight = () => {
    hovered.value = true;
  };

  const onExit = () => {
    hovered.value = false;
  };

  const focusBinds = {
    onFocus: onHighlight,
    onBlur: onExit,
    onMouseover: onHighlight,
    onMouseleave: onExit,
  };

  return {
    hovered,
    focusBinds,
  };
}
