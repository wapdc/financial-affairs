import useWizard from 'src/wizard/useWizard';
import rules from 'src/utils/rules';

const { dataPath, lazyLoadWrapper } = useWizard();

const selectFiler = {
  title: 'Select Filer',
  order: 0,
  component: lazyLoadWrapper(() => import('./Identification.vue')),
  hideToc: true,
  fields: {
    selectedMatchedFiler: {
      rules: [rules.required],
      blocking: true,
      data() {
        return dataPath(this, 'selectedFiler.selectedMatchedFiler');
      },
    },
  },
};

const nameConfirmation = {
  title: 'Name Confirmation',
  order: 1,
  component: lazyLoadWrapper(() => import('./NameConfirmation.vue')),
  fields: {
    fullName: {
      blocking: true,
      rules: [rules.requiredString, rules.maxLength(100), rules.alphbetical],
      data() {
        return dataPath(this, 'person.name');
      },
    },
  },
};

export const identificationSection = {
  title: 'Identification',
  order: 1,
  steps: {
    selectFiler,
    nameConfirmation,
  },
};
