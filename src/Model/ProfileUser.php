<?php


namespace WAPDC\FinancialAffairs\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ProfileUser
 * @Entity
 * @Table(name="aspnetdb..aspnet_Users")
 * @HasLifeCycleCallbacks
 */
#[ORM\Entity]
#[ORM\Table(name: 'aspnetdb..aspnet_Users')]
#[ORM\HasLifecycleCallbacks]
class ProfileUser
{
  /**
   * @Id
   * @Column(name="userId")
   */
  #[ORM\Column(name: 'userId')]
  public $user_id;

  /**
   * @Column(name="applicationId")
   */
  #[ORM\Column(name: 'applicationId')]
  public $application_id;

  /**
   * @Column(name="UserName")
   */
  #[ORM\Column(name: 'UserName')]
  public $username;

  /**
   * @Column(name="LastActivityDate", type="date")
   */
  #[ORM\Column(name: "LastActivityDate", type: "date")]
  public $last_activity_date;

  /**
   * @Column(name="LoweredUserName")
   */
  #[ORM\Column(name: 'LoweredUserName')]
  private $lowered_username;

  /**
   * @PrePersist
   */
  #[ORM\PrePersist]
  public function preSave() {
    $this->lowered_username = strtolower($this->username);
  }

}