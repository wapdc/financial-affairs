import { format } from 'quasar';
import { uncommonAssetsStep } from './uncommonAssetsStep';

const { capitalize } = format;

const keys = uncommonAssetsStep.fields.assetArray.template;

const heldBy = {
  key: keys.heldBy.as,
  label: 'Held By',
  align: 'left',
  sortable: false,
  width: '50%',
  format: (value) => capitalize(value?.join(', ')),
};

const investmentName = {
  key: keys.assetName.as,
  label: 'Investment Name',
  align: 'left',
  sortable: false,
  width: '50%',
};

export default [heldBy, investmentName];
