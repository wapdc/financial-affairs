module.exports = {
  // https://eslint.org/docs/user-guide/configuring#configuration-cascading-and-hierarchy
  // This option interrupts the configuration hierarchy at this file
  // Remove this if you have an higher level ESLint config file (it usually happens into a monorepos)
  root: true,
  parserOptions: {
    ecmaVersion: 2022,
    sourceType: "module", // Allows for the use of imports/exports
  },

  env: {
    node: true,
    browser: true,
  },

  // Rules order is important, please avoid shuffling them
  extends: [
    // Base ESLint recommended rules
    "eslint:recommended",

    // Uncomment any of the lines below to choose desired strictness,
    // but leave only one uncommented!
    // See https://eslint.vuejs.org/rules/#available-rules

    // 'plugin:vue/vue3-essential', // Priority A: Essential (Error Prevention)
    // 'plugin:vue/vue3-strongly-recommended', // Priority B: Strongly Recommended (Improving Readability)
    "plugin:vue/vue3-recommended", // Priority C: Recommended (Minimizing Arbitrary Choices and Cognitive Overhead)

    // https://github.com/prettier/eslint-config-prettier#installation
    // usage with Prettier, provided by 'eslint-config-prettier'.
    "prettier",
  ],

  plugins: [
    // https://eslint.vuejs.org/user-guide/#why-doesn-t-it-work-on-vue-files
    // required to lint *.vue files
    "vue",

    // https://github.com/typescript-eslint/typescript-eslint/issues/389#issuecomment-509292674
    // Prettier has not been included as plugin to avoid performance impact
    // add it as an extension for your IDE
  ],

  globals: {
    ga: "readonly", // Google Analytics
    cordova: "readonly",
    __statics: "readonly",
    __QUASAR_SSR__: "readonly",
    __QUASAR_SSR_SERVER__: "readonly",
    __QUASAR_SSR_CLIENT__: "readonly",
    __QUASAR_SSR_PWA__: "readonly",
    process: "readonly",
    Capacitor: "readonly",
    chrome: "readonly",
  },

  // add your custom rules here
  rules: {
    // Disable typescript-eslint rules
    "@typescript-eslint/*": "off",

    "prefer-promise-reject-errors": "off",

    // allow debugger during development only
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
  },

  overrides: [
    {
      files: ["src/**/*"],
      rules: {
        "no-tabs": "error",
        "space-before-blocks": "error",
        "func-call-spacing": ["error", "never"],
        "space-in-parens": ["error", "never"],
        "comma-spacing": ["error", { before: false, after: true }],
        "space-infix-ops": ["error"],
        "key-spacing": ["error"],
        "no-trailing-spaces": "error",
        "no-multiple-empty-lines": [
          "error",
          {
            max: 1,
            maxEOF: 0,
            maxBOF: 0,
          },
        ],
        "object-curly-spacing": ["error", "always"],
        semi: ["error", "always"],
        "comma-dangle": ["error", "always-multiline"],
        "max-len": [
          "error",
          {
            code: 120,
            ignorePattern: "^import .*",
          },
        ],
        "no-undef": ["warn"],
        "no-unused-vars": ["warn", { argsIgnorePattern: "^_?$" }],
        camelcase: [0],
        "keyword-spacing": ["error", { before: true, after: true }],
        "no-redeclare": "warn",
        "max-params": ["warn", 4],
        "no-case-declarations": "warn",
        "no-global-assign": "warn",
        "no-nested-ternary": "warn",
        "no-func-assign": "warn",
        "no-prototype-builtins": "warn",
        "no-unsafe-finally": "warn",
        "no-empty": "warn",
        "no-extra-boolean-cast": "off",
        "no-setter-return": "warn",
        "no-control-regex": "warn",
        "no-useless-escape": "warn",
        "no-cond-assign": "warn",
        "no-sparse-arrays": "warn",
        "no-dupe-keys": "warn",
        "space-before-function-paren": "off",
        indent: "off",
        quotes: ["warn", "single"],
        "no-irregular-whitespace": "warn",
        "vue/multi-word-component-names": 0,
        "vue/html-indent": ["warn", 2],
        "vue/require-prop-types": 0,
        "vue/no-unused-components": 0,
        "vue/require-v-for-key": 1,
        "vue/no-use-v-if-with-v-for": 0,
        "vue/require-explicit-emits": 0,
        "vue/require-default-prop": 0,
        "vue/no-mutating-props": 0,
        "vue/no-dupe-keys": 0,
        "vue/v-on-event-hyphenation": 0,
        "vue/valid-define-props": 0,
        "vue/no-setup-props-destructure": 0,
        "vue/return-in-computed-property": 0,
        "vue/no-parsing-error": 0,
        "vue/no-unused-vars": 1,
        "vue/component-definition-name-casing": ["error", "kebab-case"],
      },
    },
  ],
};
