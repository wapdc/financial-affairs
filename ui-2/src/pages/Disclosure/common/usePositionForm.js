import { computed, ref, onMounted, nextTick } from 'vue';

import useOffice from 'composables/api/useOffice';
import useWizard from 'src/wizard/useWizard.js';

/**
 * Helper composable to manage reactive state for the PositionForm component office fields, including
 * fetching, reacting to changes, and setting values in the wizard.
 * @param {Object} props - The props object passed to the PositionForm component
 * @param {Function} positionDataUtils.setValue - The function to set the value of a position field in the wizard
 * @param {Function} positionDataUtils.getValue - The function to get the value of a position field in the wizard
 * @returns {Object} Object with writable computed properties for the selected jurisdiction, office, and position
 * selections, as well as the computed available options for each dropdown
 */
export default function usePositionForm({
  fetchedElections,
  fetchedOffices,
  templateKey,
  dataIndex,
}) {
  const { getJurisdictions, getOfficeJurisdictionPositions } = useOffice();
  const wizard = useWizard();

  const positionDataUtils = wizard.fieldSet(templateKey).get(dataIndex);

  const emptyOption = () => ({ label: '', value: '' });
  const tbdOption = () => ({ label: 'TBD', value: 'TBD' });

  const electionArray = computed(() => {
    return fetchedElections;
  });

  const electionOptions = computed(() => {
    return electionArray.value?.map((election) => {
      return election.option;
    });
  });

  const electionSelection = computed({
    get: () => {
      const electionId = positionDataUtils.getValue('electionId');
      if (!electionId) return emptyOption();

      return electionOptions.value.find((e) => e.value === electionId);
    },
    set: (v) => {
      positionDataUtils.setValue('electionId', v.value);
      positionDataUtils.setValue('electionName', v.label);
    },
  });

  const officeArray = computed(() => {
    return fetchedOffices;
  });

  const officeOptions = computed(() => {
    return officeArray.value?.map((office) => {
      return office.option;
    });
  });

  const officeSelection = computed({
    get: () => {
      const officeId = positionDataUtils.getValue('officeId');
      if (!officeId) return emptyOption();

      return officeOptions.value.find((o) => o.value === officeId);
    },
    set: (v) => {
      positionDataUtils.setValue('officeId', v.value);
      positionDataUtils.setValue('officeName', v.label);
      clearJurisdiction();
      loadJurisdictions();
    },
  });

  const jurisdictionArray = ref([]);

  const jurisdictionOptions = computed(() => {
    return jurisdictionArray.value?.map((j) => {
      return j.option;
    });
  });

  const jurisdictionSelection = computed({
    get: () => {
      const jurisdictionId = positionDataUtils.getValue('jurisdictionId');
      if (!jurisdictionId) return emptyOption();

      return jurisdictionOptions.value.find((j) => j.value === jurisdictionId);
    },
    set: (v) => {
      positionDataUtils.setValue('jurisdictionId', v.value);
      positionDataUtils.setValue('jurisdictionName', v.label);
      clearPosition();
      loadPositions();
    },
  });

  function clearJurisdiction() {
    positionDataUtils.setValue('jurisdictionId', '');
    positionDataUtils.setValue('jurisdictionName', '');
    clearPosition();
  }

  function loadJurisdictions() {
    const officeId = positionDataUtils.getValue('officeId');
    if (!officeId) {
      jurisdictionArray.value = [];
      return;
    }

    getJurisdictions(officeId)
      .then((jurisdictions) => {
        jurisdictionArray.value = jurisdictions;
      })
      .catch((error) => {
        console.error('Error loading jurisdictions', error);
      });
  }

  const positionArray = ref([]);

  const positionOptions = computed(() => {
    const options = positionArray.value?.map((p) => {
      return p.option;
    });

    if (!options?.length || options.length === 0) {
      return [];
    }

    if (electionOptions.value.length > 0) {
      return [tbdOption(), ...options];
    }

    return options;
  });

  const positionSelection = computed({
    get: () => {
      const positionId = positionDataUtils.getValue('positionId');
      if (!positionId) return emptyOption();

      return positionOptions.value.find((p) => p.value === positionId);
    },
    set: (v) => {
      positionDataUtils.setValue('positionId', v.value);
      positionDataUtils.setValue('positionName', v.label);
    },
  });

  function clearPosition() {
    positionArray.value = [];
    positionDataUtils.setValue('positionId', '');
    positionDataUtils.setValue('positionName', '');
  }

  function loadPositions() {
    const officeId = positionDataUtils.getValue('officeId');
    const jurisdictionId = positionDataUtils.getValue('jurisdictionId');
    if (!officeId || !jurisdictionId) {
      positionArray.value = [];
      return;
    }

    getOfficeJurisdictionPositions(officeId, jurisdictionId)
      .then((positions) => {
        positionArray.value = positions;
        positionDataUtils.setValue('isMultiPosition', positions?.length > 1);
      })
      .catch((error) => {
        console.error('Error loading positions', error);
      });
  }

  onMounted(() => {
    nextTick(() => {
      loadJurisdictions();
      loadPositions();
    });
  });

  return {
    electionOptions,
    electionSelection,
    officeOptions,
    officeSelection,
    jurisdictionOptions,
    jurisdictionSelection,
    positionOptions,
    positionSelection,
  };
}
