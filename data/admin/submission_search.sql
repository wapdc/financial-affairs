SELECT * from (
SELECT
       CASE
         WHEN char_length(:search) > 2 THEN ROUND(cast(similarity(normalized_name(:search), normalized_name(p.name)) as numeric), 3)
         ELSE 1 END AS similarity_name,
       p.person_id,
       p.name,
       s.submission_id
FROM person p
  LEFT JOIN fa_submission S on p.person_id = s.person_id and not s.submitted
WHERE
  similarity(normalized_name(:search), normalized_name(p.name)) >= .3
  ) people
UNION ALL (
  SELECT
    CASE
      WHEN char_length(:search) > 2 THEN ROUND(cast(similarity(normalized_name(:search), normalized_name(s2.name)) as numeric), 3)
      ELSE 1 END AS similarity_name,
    s2.person_id,
    s2.name || ' (Unverified)',
    s2.submission_id
  FROM fa_submission s2
    WHERE
      coalesce(s2.submitted, false) = false
      and s2.updated_at > now() - interval '1 YEAR'
      and similarity(normalized_name(:search), normalized_name(s2.name)) >= .3
      and s2.person_id = -1


)
order by 1 desc, 3
limit 50