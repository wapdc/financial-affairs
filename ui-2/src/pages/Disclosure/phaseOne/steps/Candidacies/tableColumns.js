import { candidaciesSection } from './candidacies';

const keys =
  candidaciesSection.steps.candidacies.fields.candidaciesArray.template;

const election = {
  key: keys.electionName.as,
  label: 'Election',
  align: 'left',
  sortable: false,
  width: '20%',
};
const office = {
  key: keys.officeName.as,
  label: 'Office',
  align: 'left',
  sortable: false,
  width: '30%',
};
const jurisdiction = {
  key: keys.jurisdictionName.as,
  label: 'Jurisdiction',
  align: 'left',
  sortable: false,
  width: '25%',
  responsiveBehavior: {
    xs: { occlude: true },
  },
};
const startDate = {
  key: keys.startDate.as,
  label: 'Start Date',
  align: 'left',
  sortable: false,
  width: '120px',
};

export default [election, office, jurisdiction, startDate];
