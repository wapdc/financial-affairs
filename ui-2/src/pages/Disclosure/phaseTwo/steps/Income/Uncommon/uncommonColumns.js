import { format } from 'quasar';
import { uncommonIncomeStep } from './uncommonStep';

const { capitalize } = format;

const keys = uncommonIncomeStep.fields.uncommonIncomeArray.template;

const receivedBy = {
  key: keys.receivedBy.as,
  label: 'Received By',
  align: 'left',
  sortable: false,
  width: '25%',
  format: (value) => capitalize(value?.join(', ')),
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

const natureOfIncome = {
  key: keys.natureOfIncome.as,
  label: 'Nature of Income',
  align: 'left',
  sortable: false,
  width: '25%',
};

const sourceName = {
  key: keys.sourceName.as,
  label: 'Source',
  align: 'left',
  sortable: false,
  width: '25%',
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

export default [receivedBy, natureOfIncome, sourceName];
