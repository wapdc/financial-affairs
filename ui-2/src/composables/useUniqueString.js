import { ref } from 'vue';

/**
 * Generate a unique string suitable for use as an HTML ID,
 * but not suitable for use as a secure token.
 */
export default function useUniqueString() {
  const used = ref(new Set());

  return {
    get() {
      let str = generate();
      while (used.value.has(str)) {
        str = generate();
      }
      used.value.add(str);
      return str;
    },
  };
}

function generate() {
  const timestamp = Date.now();
  const rndString = Math.random().toString(36).substring(2);
  return `s${rndString}${timestamp}`;
}
