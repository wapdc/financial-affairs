import { Loading } from 'quasar';
import { ROUTE_NAMES } from 'src/router/routeNames';

/**
 * Sets up global navigation guards (routing middleware)
 * @param {import('vue-router').Router} router
 */
export function setupNavigationGuards(router) {
  router.beforeEach((to, from, next) => {
    useLeaveGuard(to, from, next);
  });

  router.afterEach((to) => {
    handleConfirmedLeave(to, router);
    handleHardRefresh(to);
  });

  // Handles non-router navigation (e.g. PDC Website link)
  window.addEventListener('beforeunload', (event) => {
    const leaveMsg = router.currentRoute.value.meta.confirmLeaveMessage;
    if (leaveMsg) {
      event.preventDefault();
      event.returnValue = '';
      return leaveMsg;
    }
  });
}

/**
 * If the route has a confirmLeaveMessage meta field, it will prompt the user
 * with a confirmation dialog before navigating away from the route, unless
 * the query `{ confirmedLeave: true }` is present in the next route (for
 * example, if a custom modal is used to confirm the leave).
 */
export async function useLeaveGuard(to, from, next) {
  const alwaysAllowList = [
    {
      from: ROUTE_NAMES.DISCLOSURE_DETERMINATION_FORM,
      to: ROUTE_NAMES.DISCLOSURE_FORM,
    },
  ];

  if (
    alwaysAllowList.some(
      (pair) => pair.from === from.name && pair.to === to.name,
    )
  ) {
    next();
    return;
  }

  if (from.meta.confirmLeaveMessage && !to.query.confirmedLeave) {
    const answer = window.confirm(from.meta.confirmLeaveMessage);
    if (answer === false) {
      next(false);
      return;
    }
  }

  next();
}

/**
 * If the query `{ confirmedLeave: true }` is present in the next route, remove
 * it from the query and replace the route without the query.
 */
function handleConfirmedLeave(to, router) {
  if (to.query.confirmedLeave) {
    // eslint-disable-next-line no-unused-vars
    const { confirmedLeave, ...query } = to.query;
    router.replace({ query });
  }
}

/**
 * If the query `{ hardRefresh: true }` is present in the next route, perform a
 * hard refresh of the page to clear the state.
 */
function handleHardRefresh(to) {
  if (to.query.hardRefresh) {
    const href = to.href.split('?')[0];
    const appPath = process.env.VUE_APP_BASE_PATH || '/financial-affairs-q/';
    const targetUrl = `${window.location.origin}${appPath}${href}`;

    Loading.show();

    window.location.replace(targetUrl);

    setTimeout(() => {
      window.location.reload();
    }, 100);
  }
}
