SELECT p.person_id,p.name,
CASE WHEN char_length(:search) > 2 THEN ROUND(CAST(similarity(normalized_name(:search), normalized_name(p.name)) AS numeric), 3) ELSE 1 END AS similarity_name,
       p.voter_id,p.voter_name,p.updated_at,p.address,p.city,p.state,p.postcode,p.country,p.filer_id,p.email
FROM person p
WHERE similarity(normalized_name(:search), normalized_name(p.name)) >= .3
ORDER BY similarity_name DESC, p.name