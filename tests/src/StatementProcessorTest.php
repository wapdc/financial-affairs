<?php

namespace Tests\WAPDC\FinancialAffairs;

use DateTime;
use Drupal\Core\Render\Element\Date;
use Exception;
use stdClass;
use Symfony\Component\Yaml\Yaml;
use Tests\WAPDC\Core\Mock\MockMailer;
use WAPDC\Core\Model\Candidacy;
use WAPDC\Core\Model\MessengerContact;
use WAPDC\Core\Model\Official;
use WAPDC\Core\Model\Person;
use PHPUnit\Framework\TestCase;
use WAPDC\Core\Model\EntityAuthorization;
use WAPDC\Core\Model\User;
use WAPDC\FinancialAffairs\FinancialAffairs;
use WAPDC\FinancialAffairs\Model\FAStatement;
use WAPDC\FinancialAffairs\Model\FAStatementLog;
use WAPDC\FinancialAffairs\Model\FASubmission;
use WAPDC\FinancialAffairs\StatementProcessor;
use WAPDC\FinancialAffairs\FADataManager;
use WAPDC\FinancialAffairs\Validator;


/**
 * Class StatementProcessorTest
 * @coversDefaultClass \WAPDC\FinancialAffairs\StatementProcessor
 */
class StatementProcessorTest extends TestCase
{

  /** @var FADataManager */
  protected $dataManager;

  /** @var Person */
  protected $person;

  /** @var StatementProcessor */
  protected $statementProcessor;

  /** @var FASubmission */
  protected $submission;

  /** @var FAStatement */
  protected $statement;

  /** @var MockMailer */
  private $mailer;

  /**
   * @throws Exception
   */
  protected function setUp(): void {
    parent::setUp();
    date_default_timezone_set('America/Los_Angeles');
    $this->dataManager = new FADataManager();
    $this->dataManager->beginTransaction();
    $this->person = $this->createPerson();
    $this->mailer = new MockMailer();
    $this->statementProcessor = new StatementProcessor($this->dataManager);
    $this->statementProcessor->setMailer($this->mailer);
  }

  /**
   * @param stdClass $data
   * @return Person
   * @throws Exception
   */
  protected function createPerson(stdClass $data = null) {
    if (!$data) $data = new stdClass();

    $this->person = $person = new Person();
    $person->name = $data->name ?? "Test Person";
    $person->filer_id = isset($data->filer_id) ? $data->filer_id  : "AAAA  0000" ;
    $person->address = $data->address ?? NULL;
    $person->city = $data->city ?? NULL;
    $person->state = $data->state ?? NULL;
    $person->postcode = $data->postcode ?? NULL;
    $this->dataManager->em->persist($person);
    $this->dataManager->em->flush();
    $this->person = $person;
    return $person;
  }

  /**
   * @param $person_id
   * @param array $data
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  protected function createCandidacies($person_id, array $data) {
    foreach ($data as $cd) {
      $candidacy = new Candidacy();
      $candidacy->person_id = $person_id;
      $candidacy->election_code = $cd->election_code;
      $candidacy->office_code = $cd->office_code;
      $candidacy->jurisdiction_id = $cd->jurisdiction_id;
      if (!empty($cd->position_id)) $candidacy->position_id = $cd->position_id;
      $candidacy->campaign_start_date = isset($cd->campaign_start_date) ? new DateTime($cd->campaign_start_date) :  NULL;
      $this->dataManager->em->persist($candidacy);
    }
    $this->dataManager->em->flush();
  }

  /**
   * @param $person_id
   * @param array $data
   * @throws Exception
   */
  protected function createOfficials($person_id, array $data) {
    foreach ($data as $od) {
      $official = new Official();
      $official->person_id = $person_id;
      $official->start_date = isset($od->start_date) ? new DateTime($od->start_date) :  NULL;
      $official->end_date = isset($od->end_date) ? new DateTime($od->end_date) : NULL;
      $official->appointed_until = isset($od->appointed_until) ? new DateTime($od->appointed_until) : NULL;

      $official->office_code = $od->office_code;
      $official->jurisdiction_id = $od->jurisdiction_id;
      $this->dataManager->em->persist($official);
    }
    $this->dataManager->em->flush();
  }

  /**
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws Exception
   */
  public function testRequestAccess() {
    $expected_email = "requestlegacyAccessTest@a.invalid";
    $sendGridTemplateId = $this->statementProcessor::TEMPLATE_ID;
    $invalid_email = "example@m.invalid";

    //Make a new person that has an email address matching the test email.
    $person = new Person();
    $person->name = "Test Person";
    $person->filer_id = "TESTP  504";
    $person->email = "requestlegacyAccessTest@a.invalid";
    $this->dataManager->em->persist($person);
    $this->dataManager->em->flush($person);

    //make a PDC user with the email specified. realm= test uid = email user_name = email.
    $pdc_user = new User();
    $pdc_user->realm = 'TEST';
    $pdc_user->uid = $expected_email;
    $pdc_user->user_name = $expected_email;
    $this->dataManager->em->persist($pdc_user);
    $this->dataManager->em->flush($pdc_user);

    $mail_sent = $this->statementProcessor->requestAccess($invalid_email);
    $this->assertFalse($mail_sent); // false

    // set up mock mailer for the legacy processor in constructor;
    // Call requestLegacyAccess with the email
    $mail_sent = $this->statementProcessor->requestAccess($expected_email);
    $this->assertTrue($mail_sent); // false

    /** @var MockMailer $message */
    $message = $this->mailer->getMailSentTo($expected_email);
    $this->assertEquals($sendGridTemplateId, $message->message_template);

    // call validateRequestToken and get back invalid token
    $wrong_token = 'BKdOQ0sGHgbXVYhuWxvvvAL';
    $val = $this->statementProcessor->validateRequestToken($pdc_user, $wrong_token);
    $this->assertFalse($val);

    // call validateRequestToken
    /** @var MessengerContact $contact */
    $contact = $this->dataManager->em
      ->getRepository(MessengerContact::class)
      ->findOneBy(['target' => $expected_email, 'protocol' => 'email']);
    $token = $contact->token;
    $valid = $this->statementProcessor->validateRequestToken($pdc_user, $token);
    $this->assertNotEmpty($valid);

    // verify that the person authorization records exist for the pdc we created.
    // ORM loads of person auth by pdc user realm and uid.
    $authorizations = $this->dataManager->em->getRepository(EntityAuthorization::class);
    $p1Auth = $authorizations->findOneBy(['entity_id' => $person->person_id,
      'realm' => $pdc_user->realm, 'uid' => $pdc_user->uid]);
    $this->assertNotEmpty($p1Auth);


  }

  // TODO: Refactor test to use RDS rather than legacy F1 db
  /**
   * @throws Exception
   */
  public function testGetFilerIdsFromEmail() {
    $email = 'ihavethebestinvalidemailzzz';
    $filer_ids = ['person_id', 'fa_id'];

    $data = (object) ['filer_id' => $filer_ids[0], 'name' => 'Test Person'];
    $person = $this->createPerson($data);
    $person->email = $email;

    $data->person_id = $this->person->person_id;
    $data->name = $this->person->name;
    $data->repno = 123456789;
    $data->filer_id = "AAAA  0000";
    $data->period_start = '2019-01-01';
    $data->period_end = '2019-12-31';
    $data->first_filed_at = '2020-02-15';

    //add an id to an fa_statement
    $statement = $this->createFAStatement($data);
    $statement->filer_id = $filer_ids[1];
    $statement->email = $email;
    $this->dataManager->em->persist($statement);
    $this->dataManager->em->flush();

    $results = $this->statementProcessor->getFilerIdsFromEmail($email);
    //make sure we are getting data from all sources.
    foreach ($filer_ids as $filer_id) {
      $this->assertContains($filer_id, $results);
    }
  }

  /**
   * @throws Exception
   */
  public function testGeneratePersonAuthForFilers() {
    $user = new User();
    $user->realm = 'testland';
    $user->uid = 14;
    $p1 = new Person();
    $p1->name = 'P1';
    $p1->filer_id = 'NOPE nope';
    $this->dataManager->em->persist($p1);
    $p2 = new Person();
    $p2->name = 'P2';
    $p2->filer_id = 'Nap-time';
    $this->dataManager->em->persist($p2);
    $this->dataManager->em->flush();
    $filer_ids =[$p1->filer_id, $p2->filer_id];
    $fas = new FinancialAffairs(false, $this->dataManager);
    $fas->generatePersonAuthForFilers($user, $filer_ids);
    $authorizations = $this->dataManager->em->getRepository(EntityAuthorization::class);
    $p1Auth = $authorizations->findOneBy(['entity_id' => $p1->person_id]);
    $this->assertNotEmpty($p1Auth);
    $this->assertEquals($user->uid, $p1Auth->uid);
    $this->assertEquals($user->realm, $p1Auth->realm);
    $p2Auth = $authorizations->findOneBy(['entity_id' => $p2->person_id]);
    $this->assertNotEmpty($p2Auth);
  }

  /**
   * @param stdClass|null $data
   *
   * @return FAStatement
   * @throws Exception
   */
  protected function createFAStatement(stdClass $data = null): FAStatement {
    $this->statement = $statement = new FAStatement();
    $statement->name = $data->name ?? "A Name";
    $statement->period_start = new DateTime($data->period_start);
    $statement->period_end = new DateTime($data->period_end);
    $statement->first_filed_at = !empty($data->first_filed_at) ? new DateTime($data->first_filed_at) : null;
    $statement->person_id = $data->person_id ?? $this->person->person_id;
    $statement->filer_id = $data->filer_id ?? $this->person->filer_id ?? "AAAA  0000";
    $this->dataManager->em->persist($statement);
    $this->dataManager->em->flush();
    return $statement;
  }

  /**
   * Save Submission
   * @param stdClass $data
   * @return FASubmission
   * @throws Exception
   */
  protected function createSubmission(stdClass $data, $markSubmitted, $submitted_at=""): FASubmission {
    $this->submission = $submission =  new FASubmission();
    $submission->updated_at = new DateTime();
    $submission->user_data = $data;
    $submission->save_count = 0;
    $submission->realm = 'test';
    $submission->uid = 'test';
    if (isset($data->statement) && isset($data->statement->period_start)) {
      $submission->period_start = new DateTime($data->statement->period_start);
    }
    if (isset($data->statement) && isset($data->statement->period_end)) {
      $submission->period_end = new DateTime($data->statement->period_end);
    }
    if ($markSubmitted) {
      $submission->submitted = TRUE;
      $submission->submitted_at = new DateTime($submitted_at ?? '');
      $submission->certification_email = 'test@a.invalid';
      $submission->certification = 'I Certify';
      $submission->certification_name = $data->person->name;
    }
    $this->dataManager->em->persist($submission);
    $this->dataManager->em->flush();
    return $submission;
  }

  /**
   * @param null $rel_dir
   * @param null $file_name
   * @return array
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  protected function setupSaveSubmission($rel_dir = null, $file_name = null): array {
    $u = new User();
    $u->realm = 'test';
    $u->uid = 'test please work!';
    $u->user_name = 'Rene Descartes';
    $this->dataManager->em->persist($u);
    $this->dataManager->em->flush();
    $dir = dirname(__DIR__) . ($rel_dir ?? '/data/submission');
    $file = $file_name ??  "candidate.yml";
    $submission_data = Yaml::parseFile("$dir/$file", Yaml::PARSE_OBJECT_FOR_MAP);

    return [$submission_data, $u];
  }


  /**
   * @throws Exception
   */
  public function testSaveSubmission(){
    $now = new DateTime();
    list($submission_data, $u) = $this->setupSaveSubmission();

    // This should create a new submission
    $saved = $this->statementProcessor->saveSubmission(json_decode(json_encode($submission_data)), $u, null);
    $this->assertNotEmpty($saved->submission->submission_id);
    $this->assertGreaterThanOrEqual($now->format('Y-m-d H:i:s'), $saved->submission->updated_at);
    $this->dataManager->em->clear();
    /** @var FASubmission $savedSubmission */
    $savedSubmission = $this->dataManager->em->find(FASubmission::class, $saved->submission->submission_id);
    $this->assertNotEmpty($savedSubmission->user_data);
    $this->assertEquals($u->realm, $savedSubmission->realm);
    $this->assertEquals($u->uid, $savedSubmission->uid);
    $this->assertEquals(1, (int)$savedSubmission->save_count);
    $this->assertEquals($saved->submission->person_id, (int)$savedSubmission->person_id);
    $this->assertObjectNotHasAttribute('submission', $savedSubmission->user_data);
    $this->assertEquals($submission_data->person->name, $savedSubmission->name);
    $this->assertEquals($submission_data->statement->period_start, $savedSubmission->period_start->format('Y-m-d'));
    $this->assertEquals($submission_data->statement->period_end, $savedSubmission->period_end->format('Y-m-d'));
    $this->dataManager->em->clear();
    $new_data = json_decode(json_encode($savedSubmission->user_data));
    $new_data->submission = new stdClass();
    $new_data->submission->save_count = 1;
    $new_data->submission->person_id = null;

    // Saving an existing submission
    $this->statementProcessor->saveSubmission(json_decode(json_encode($new_data)), $u, $savedSubmission->submission_id);
    /** @var FASubmission $savedSubmission */
    $reSavedSubmission = $this->dataManager->em->find(FASubmission::class, $saved->submission->submission_id);
    $this->assertEquals($savedSubmission->save_count+1, $reSavedSubmission->save_count);

    // Saving an existing submission but not providing a save count
    $exceptionThrown = false;
    try {
        $new_data = json_decode(json_encode($savedSubmission->user_data));
        unset($new_data->submission->save_count);
        $this->statementProcessor->saveSubmission(json_decode(json_encode($new_data)), $u, $savedSubmission->submission_id);
    } catch (Exception $e) {
      $exceptionThrown = true;
    } finally {
      $this->assertTrue($exceptionThrown);
    }

    // Make sure we cannot save a submission after it has been submitted.
    $exceptionThrown = false;
    $this->dataManager->db->executeStatement("UPDATE fa_submission set submitted=true where submission_id = :submission_id", ['submission_id' => $savedSubmission->submission_id]);
    $this->dataManager->em->clear();
    try {
      $new_data = json_decode(json_encode($savedSubmission->user_data));
      $this->statementProcessor->saveSubmission(json_decode(json_encode($new_data)), $u, $savedSubmission->submission_id);
    } catch (Exception $e) {
      $exceptionThrown = true;
    } finally {
      $this->assertTrue($exceptionThrown);
    }
  }



  /**
   * @throws Exception
   */
  public function testRemoveSubmission(){
    list($submission_data, $u) = $this->setupSaveSubmission();

    // This should create a new submission
    $saved = $this->statementProcessor->saveSubmission(json_decode(json_encode($submission_data)), $u, null);
    $savedSubmission = $this->dataManager->em->find(FASubmission::class, $saved->submission->submission_id);
    $submissionId = $savedSubmission->submission_id;

    $this->statementProcessor->removeSubmission($submissionId);
    $this->dataManager->em->clear();

    $retSubmission = $this->dataManager->em->find(FASubmission::class, $submissionId);
    $this->assertNull($retSubmission);

    $this->expectException(Exception::class);
    $this->statementProcessor->removeSubmission($submissionId);

  }


  /**
   * @throws Exception
   */
  public function testSubmitSubmission(){
    list($submission_data, $u) = $this->setupSaveSubmission();
    $sub_data = clone $submission_data->submission;
    unset($submission_data->submission);
    $submission = $this->createSubmission($submission_data, false);
    $this->assertEmpty($submission->submitted_at);
    $this->assertEmpty($submission->certification_email);
    $this->assertEmpty($submission->certification_name);
    $this->assertEmpty($submission->certification_phone);
    $this->assertEmpty($submission->certification);
    $preTestTime = new DateTime();
    $save_count = $submission->save_count;
    $submit_data = new stdClass();
    $submit_data->submission = clone $sub_data;
    $submit_data->person = clone $submission_data->person;
    $this->dataManager->em->clear();

    $this->statementProcessor->submitSubmission($submit_data, $u, $submission->submission_id);

    $this->dataManager->em->clear();
    /** @var FASubmission $submitted */
    $submitted = $this->dataManager->em->find(FASubmission::class, $submission->submission_id);
    $this->assertGreaterThanOrEqual($preTestTime->getTimestamp(), $submitted->submitted_at->getTimestamp());
    $this->assertEquals($submitted->updated_at, $submitted->submitted_at);
    $this->assertFalse($submitted->verified);
    $this->assertTrue($submitted->submitted);
    $this->assertEquals($submit_data->submission->certification_email, $submitted->certification_email);
    $this->assertEquals($submit_data->submission->certification_name, $submitted->certification_name);
    $this->assertEquals($submit_data->submission->certification_phone, $submitted->certification_phone);
    $this->assertEquals($submit_data->submission->certification, $submitted->certification);
    $this->assertEquals($save_count, $submitted->save_count);
    $this->assertObjectNotHasAttribute('submission', $submitted->user_data);

    $this->expectException(Exception::class); // Cannot submit twice
    $this->statementProcessor->submitSubmission($submit_data, $u, $submission->submission_id);
  }

  /**
   * @covers \WAPDC\FinancialAffairs\StatementProcessor::saveStatement
   * @param $scenario
   * @dataProvider statementScenarios
   * @throws Exception
   */
  public function testSaveStatement($scenario) {
    $dir = dirname(__DIR__) . '/data/statement';
    $submission = Yaml::parseFile("$dir/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    $expected_statement = $submission->expected_statement;
    $statement = $submission->statement;
    $statement->person_id = $this->person->person_id;

    $statement_id = $this->statementProcessor->saveStatement($statement);

    $this->dataManager->em->clear();

    $saved_statement = $this->dataManager->em->find(FAStatement::class, $statement_id);
    $person = $this->dataManager->em->find(Person::class, $statement->person_id);

    $this->assertEquals($person->name, $saved_statement->name);
    $this->assertEquals(new DateTime($expected_statement->first_filed_at), $saved_statement->first_filed_at);
    $this->assertEquals(new DateTime($expected_statement->period_start), $saved_statement->period_start);
    $this->assertEquals(new DateTime($expected_statement->period_end), $saved_statement->period_end);

    if(isset($expected_statement->retain_until)) {
      $this->assertEquals(new DateTime($expected_statement->retain_until), $saved_statement->retain_until);
    } else {
      $this->assertEquals($expected_statement->retain_until, $saved_statement->retain_until);
    }

    $this->assertEquals($expected_statement->hold_for, $saved_statement->hold_for);
    $this->assertEquals($expected_statement->email, $saved_statement->email);

    $log_entry = $this->dataManager->em->getRepository(FAStatementLog::class)
      ->findBy(['fa_statement_id' => $statement_id]);
    $this->assertEquals($statement_id, $log_entry[0]->fa_statement_id);

  }

  /**
   * @covers \WAPDC\FinancialAffairs\StatementProcessor::removeStatement\
   * @throws Exception
   */
  public function testRemoveStatement() {
    $dir = dirname(__DIR__) . '/data/statement';
    $submission = Yaml::parseFile("$dir/annual.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $statement = $submission->statement;
    $statement->person_id = $this->person->person_id;
    $statement_id = $this->statementProcessor->saveStatement($statement);
    $this->dataManager->em->clear();
    $check_statement = $this->dataManager->em->find(FAStatement::class, $statement_id);
    $this->assertNotEmpty($check_statement);

    $this->expectException(Exception::class);
    $this->expectExceptionMessage("Statement id: $statement_id cannot be removed because the record has a retention hold on it.");

    $this->statementProcessor->removeStatement($statement_id);
    $this->dataManager->em->clear();
    $check_statement= $this->dataManager->em->find(FAStatement::class, $statement_id);
    $this->assertEmpty($check_statement);

    $log_entry = $this->dataManager->em->getRepository(FAStatementLog::class)
      ->findBy(['fa_statement_id' => $statement_id]);
    $this->assertEquals($statement_id, $log_entry[0]->fa_statement_id);

  }

  /**
   * @throws Exception
   */
  public function testGetStatement() {
    $dir = dirname(__DIR__) . '/data/statement';
    $submission = Yaml::parseFile("$dir/annual.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Save a statement
    $submission->statement->person_id = $this->person->person_id;
    $statement_id = $this->statementProcessor->saveStatement($submission->statement);

    $this->dataManager->em->clear();

    // Retrieve
    $statement = $this->statementProcessor->getStatement($statement_id);

    foreach($submission->statement as $field=>$value) {
      $this->assertSame($statement->$field, $value);
    }
    $this->assertNotEmpty($statement->statement_id);

  }

  public function statementScenarios() {
    return [
      ['annual'],
      ['prior12'],
      ['final']
    ];
  }

  /**
   * @param string $scenario
   * @throws Exception
   */
  public function testMatchingPeople($scenario = "incumbent_candidate") {
    $dir = dirname(__DIR__) . '/data/submission';
    $submission = Yaml::parseFile("$dir/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    $this->createSubmission($submission, false);
    if (isset($submission->person)) {
      $this->createPerson($submission->person);
    }
    if (isset($submission->candidacies)) {
      $this->createCandidacies($this->person->person_id, $submission->candidacies);
    }
    if (isset($submission->offices)) {
      $this->createOfficials($this->person->person_id, $submission->offices);
    }

    // Make sure the matches include the test data.
    $matches = $this->statementProcessor->matchingPeople($this->submission->submission_id);
    $this->assertGreaterThan(0, count($matches));

    // Make sure the person we created was in the list.
    $test_persons = array_filter($matches, function ($person) {
      return $person->person_id == $this->person->person_id;
    });
    $person = reset($test_persons);
    $this->assertInstanceOf(stdClass::class, $person, 'Finding match');

    // Verify that the candidacies were added.
    if (isset($submission->candidacies)) {
      foreach($submission->candidacies as $candidacy) {
        $matches = array_filter($person->candidacies, function ($o) use ($candidacy) {
          return $candidacy->office_code == $o->office_code;
        });
        $this->assertNotEmpty($matches, "Search for office ".  $candidacy->office_code);
      }
    }

    // Verify that the official entries were added
    if (isset($submission->offices)) {
      foreach($submission->offices as $office) {
        $matches = array_filter($person->candidacies, function ($o) use ($office) {
          return $office->office_code == $o->office_code;
        });
        $this->assertNotEmpty($matches, "Search for office ".  $office->office_code);
      }
    }

    // Make sure the matches include the test data.
    $matches = $this->statementProcessor->matchingPeopleByUser($this->submission->user_data);
    $this->assertGreaterThan(0, count($matches));

    // Make sure the person we created was in the list.
    $test_persons = array_filter($matches, function ($person) {
      return $person->person_id == $this->person->person_id;
    });

    $person = reset($test_persons);
    $this->assertInstanceOf(stdClass::class, $person, 'Finding match');

  }

  public function statementOwedProvider(){
    return [
      ['staffer-candidate-no-annual-after-tax'],
      ['staffer-candidate-no-annual-april'],
      ['candidate-with-prior-12'],
      ['candidate-december'],
      ['staffer-old-candidate'],
      ['staffer-candidate'],
      ['staffer-candidate-historical'],
      ['final-quick-quitter'],
      ['candidate-future'],
      ['staffer-december'],
      ['staffer-new'],
      ['staffer-future'],
      ['candidate-multi'],
      ['candidate-new'],
      ['candidate-new-annual-filed'],
      ['candidate-no-final-or-annual-filed'],
      ['candidate-with-fa'],
      ['final-with-annual'],
      ['final-with-out-annual'],
      ['pro-staff-file-final-no-annual'],
      ['pro-staff-multi-year-nothing-filed'],
      ['staff-submit-unverified'],
      ['staff-submit-unverified-with-draft'],
      ['pro-staff-in-the-past'],
      ['multi-year-annual'],
    ];

  }

  /**
   * @param $scenario
   *
   * @throws Exception
   * @dataProvider statementOwedProvider
   */
  public function testGetStatementsOwed($scenario){
    $dir = dirname(__DIR__) . '/data/owed_statements';
    $scenario_file = Yaml::parseFile("$dir/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    $submission = $scenario_file->submission;
    $expected_statements_owed = $scenario_file->statements_owed;

    if (isset($scenario_file->create_person)) {
      $this->createPerson($submission->person);
      $submission->person->person_id = $this->person->person_id;
    }

    if (isset($scenario_file->statements_create)) {
      foreach ($scenario_file->statements_create as $fas) {
        $this->createFAStatement($fas);
      }
    }

    // Premake offices
    if (isset($scenario_file->create_offices)) {
      $this->createOfficials($this->person->person_id, $scenario_file->create_offices);
    }

    // Premake candidacies
    if (isset($scenario_file->create_candidacies)) {
      $this->createCandidacies($this->person->person_id, $scenario_file->create_candidacies);
    }

    $user = new User();
    $user->uid = 'test';
    $user->realm = 'test';

    if (isset($scenario_file->submissions_create)) {
      foreach ($scenario_file->submissions_create as $sub) {
        $data = new stdClass();
        $data->statement = new stdClass();
        $data->statement->period_start = $sub->period_start;
        $data->statement->period_end = $sub->period_end;
        $data->person = new stdClass();
        $data->person->name = 'test';
        if ($this->person->person_id) {
          $data->person->person_id = $this->person->person_id;
        }
        if ($sub->submit) {
          $this->createSubmission($data, true);
        } else {
          $this->createSubmission($data, false);
        }
      }

    }

    $current_date = $scenario_file->current_date ?? null;
    $statements_owed = $this->statementProcessor->getStatementsOwed($submission, $user, $current_date);

    if(count($expected_statements_owed) > 0){
      foreach ($expected_statements_owed as $fa){
        $found = $this->statement_owed_found($statements_owed, $fa);
        $this->assertNotNull($found, "did not find fa statement owed for dates " . $fa->period_start . " to " . $fa->period_end . " for $scenario");
        if(new DateTime($fa->period_end) < new DateTime('2019-12-31')){
          $this->assertTrue($found->legacy_filing);
        }else{
          $this->assertFalse($found->legacy_filing);
        }
      }
    }
    $this->assertEquals(count($expected_statements_owed), count($statements_owed), "Found a different number of expected statements than expected.");

  }

  public function officialActiveProvider(){
    return [
      ['candidate-new'],
      ['candidate-no-final-or-annual-filed'],
      ['final-with-annual'],
      ['final-with-out-annual'],
      ['pro-staff-file-final-no-annual'],
      ['pro-staff-multi-year-nothing-filed'],
      ['staffer-new'],
      ['staffer-candidate'],
    ];

  }

  /**
   * @param $scenario
   *
   * @throws Exception
   * @dataProvider officialActiveProvider
   */
  public function testSetStatementActiveOfficial($scenario) {
    $dir = dirname(__DIR__) . '/data/owed_statements';
    $scenario_file = Yaml::parseFile("$dir/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    $submission = $scenario_file->submission;
    $statementsOwed = $scenario_file->statements_owed;
    $active_official = ($scenario_file->active_official) ?? null;
    $submission->submission = new stdClass();
    $submission->submission->save_count = 0;
    $submission->statement = new stdClass();
    $submission->statement->period_start = $statementsOwed[0]->period_start;
    $submission->statement->period_end = $statementsOwed[0]->period_end;
    $validator = new Validator();

    $this->statementProcessor->validateSubmission($submission, $validator);
    if ($active_official) {
      $this->assertEquals($active_official, $submission->submission->active_official, $scenario);
    }
    else {
      $this->assertFalse(isset($submission->submission->active_official), $scenario);
    }
  }

  protected function statement_owed_found($statements_create, $fa){
    $found = null;
    foreach ($statements_create as $expected){
      if(isset($expected->period_start, $expected->period_end) &&
        $expected->period_start == $fa->period_start &&
        $expected->period_end == $fa->period_end){
        $found = $expected;
      }
    }
    return $found;
  }

  /**
   * Sets the property of an object or removes the property if the value is null.
   *
   * @param $data
   * @param $property
   * @param $value
   */
  protected function setProperty($data, $property, $value) {
    // Determine if the property has a '.' so we know if we need to drill further.
    if (strpos($property, '.')) {
      list($outer, $inner) = explode('.', $property, 2);
      // See if we have an array index.
      if ($outer === '0' || (int)$outer) {
        $this->setProperty($data[$outer], $inner, $value);
      }
      else {
        $this->setProperty($data->$outer, $inner, $value);
      }
    }
    else {
      if (isset($data->$property) && $value === NULL) {
        unset($data->$property);
      }
      else {
        $data->$property = $value;
      }
    }

  }

  protected function incomeSourceSetter($object, $property, $value) {
    foreach ($object as $source) {
      if (strpos($property, '.')) {
        list($outer, $inner) = explode('.', $property, 2);
        if (isset($source->$outer->$inner) && $value === NULL) {
          $source->$outer->$inner = NULL;
        }
        else {
          $source->$outer->$inner = $value;
        }
      }
      else {
        if (isset($source->$property) && $value === NULL) {
          $source->$property = NULL;
        } else {
          $source->$property = $value;
        }
      }
    }
  }

  protected function sourceTypesSetter($object, $category, $value) {
    foreach ($object as $type) {
      if ($type->category === $category) {
        if (isset($type->income) && $value === NULL) {
          $type->income = NULL;
        }
        else {
          $type->income = $value;
        }
      }
    }
  }

  /**
   * Sets values of conditions of the form object.property to the value defined.
   * @param stdClass $submission
   * @param string $condition
   * @param mixed $value
   */
  protected function setSubmissionValue($submission, $condition, $value) {
    if (strpos($condition, '.')) {
      list($object, $property) = explode('.', $condition, 2);
    }
    else {
      $object = "";
      $property = $condition;
    }
    switch ($object){
      case 'income_sources':
        $this->incomeSourceSetter($submission->statement->income_sources, $property, $value);
        break;
      case 'income_source_types-employment':
      case 'income_source_types-retirement':
      case 'income_source_types-uncommon':
        list($o, $category) = explode('-', $object, 2);
        $this->sourceTypesSetter($submission->statement->income_source_types, $category, $value);
        break;
      default:
        $this->setProperty($submission, $condition, $value);
    }
  }


  public function testIdentificationValidation() {
    $dir = dirname(__DIR__) . "/data/submission";
    $validations = Yaml::parseFile("$dir/identification.validations.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    foreach($validations->tests as $test) {
      // Start with the same initial data.
      $submission = Yaml::parseFile("$dir/identification.yml", Yaml::PARSE_OBJECT_FOR_MAP);
      $expected_errors = [];

      // Each condition either an assertion of an error or and assertion of a particular initial state modified from
      // the base $submission
      foreach ($test as $condition => $value) {

        switch (true) {
          case ($condition == 'name'):
            break;
          case (strpos($condition, '.') !== FALSE):
            $this->setSubmissionValue($submission, $condition, $value);
            break;
          case ($condition == 'error'):
            if (is_array($value)) {
              foreach ($value as $error) {
                $expected_errors [] = $error;
              }
            } else {
              $expected_errors [] = $value;
            }
            break;
        }

      }

      $errorBus = new Validator();
      $this->statementProcessor->validateIdentification($submission->submission, $errorBus);
      $errors = $errorBus->getErrors();
      $conditions = print_r($test, 1);
      if ($expected_errors && $expected_errors != ['']) {
        foreach ($expected_errors as $expected_error) {
          $this->assertArrayHasKey($expected_error, $errors, "Missing expected errors for identification: $conditions");
        }
      } else {
        $error = implode("; ", $errors);
        $this->assertEmpty($errors, "Unexpected error(s) $error for identification : $conditions.");
      }
    }


  }

  public function validationProvider() {
    return [
      ['assets'],
      ['candidate'],
      ['real_estate'],
      ['income'],
      ['business_associations'],
      ['business_financial'],
      ['creditor'],
      ['lobbying_indication'],
      ['gifts']
    ];
  }

  /**
   * @dataProvider validationProvider
   * @param $scenario
   *
   * @throws Exception
   */
  public function testSubmissionValidation($scenario) {
    $dir = dirname(__DIR__) . "/data/submission";
    $validations = Yaml::parseFile("$dir/$scenario.validations.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Object has tests property which contains an array of test condition.
    foreach($validations->tests as $test) {
      // Start with the same initial data.
      $submission = Yaml::parseFile("$dir/$scenario.yml", Yaml::PARSE_OBJECT_FOR_MAP);
      $expected_errors = [];

      // Each condition either an assertion of an error or and assertion of a particular initial state modified from
      // the base $submission
      foreach ($test as $condition => $value) {

        switch (true) {
          case ($condition == 'name'):
            break;
          case (strpos($condition, '.') !== FALSE):
            $this->setSubmissionValue($submission, $condition, $value);
            break;
          case ($condition == 'error'):
            if (is_array($value)) {
              foreach ($value as $error) {
                $expected_errors [] = $error;
              }
            } else {
              $expected_errors [] = $value;
            }
            break;
        }

      }

      $errorBus = new Validator();
      $this->statementProcessor->validateSubmission($submission, $errorBus);
      $errors = $errorBus->getErrors();

      if ($expected_errors && $expected_errors != ['']) {
        foreach ($expected_errors as $expected_error) {
          $error_found = implode(', ', array_keys($errors));
          $this->assertArrayHasKey($expected_error, $errors, "Missing expected errors for $scenario: $error_found");
        }
      } else {
        $error = implode("; ", array_keys($errors));
        $this->assertEmpty($errors, "Unexpected error(s) $error for $scenario.");
      }
    }
  }

  public function verificationProvider() {
    // Gets all the files from the verification folder and runs them.
    $dir = dirname(__DIR__) . "/data/verification";
    $files = scandir($dir);
    $tests = [];
    foreach($files as $file) {
      if (strpos($file, '.') !== 0) {
        $tests[] = [$file];
      }
    }
    return $tests;
  }

  /**
   * @dataProvider verificationProvider
   * @throws Exception
   */
  public function testVerifySubmission($file) {
    $dir = dirname(__DIR__) . "/data/verification";
    $s = Yaml::parseFile("$dir/$file", Yaml::PARSE_OBJECT_FOR_MAP);

    if ($s->admin_data->person_id) {
      $person = $this->createPerson($s->person);
      $s->admin_data->person_id = $person->person_id;
    }

    // Manufacture the submission
    $unverifiedSubmission = $this->createSubmission($s, TRUE, $s->submitted_at ?? NULL);
    $currentDate = new DateTime();
    $periodStart = new DateTime($s->statement->period_start);
    $periodEnd = new DateTime($s->statement->period_end);
    if (!empty($s->tests->person_authorized)) {
      $this->submission->person_id = $this->person->person_id;
    }
    $this->dataManager->em->flush();

    // Manufacture user requesting access
    $user = new User();
    $user->uid = 'test';
    $user->realm = 'test';
    $user->user_name = 'rebekkatest@a.invalid';
    $this->dataManager->em->persist($user);
    $this->dataManager->em->flush();

    // Perform the verification
    $mailer = new MockMailer;
    $this->dataManager->db->executeStatement("insert into entity_authorization (entity_id, realm, role, uid) select :entity_id, :realm, 'finance_reporter', :uid",
      ["entity_id" => $s->admin_data->person_id, "realm" => $user->realm, "uid" => $user->uid]);
    $this->statementProcessor->verifySubmission($unverifiedSubmission->submission_id, $user->user_name, $s->admin_data, $mailer);
    // Clear after verification.
    $this->dataManager->em->clear();

    //Fa statement submission
    /** @var FASubmission $submission */
    $submission = $this->dataManager->em->find(FASubmission::class, $unverifiedSubmission->submission_id);
    $this->assertSame($s->tests->submission->verified, $submission->verified, "Verified: $file");
    $this->assertSame($s->tests->submission->published, $submission->published, "Published: $file");
    $this->assertGreaterThanOrEqual($currentDate->format('Y-m-d H:i:s'), $submission->updated_at->format('Y-m-d H:i:s'));

    // make sure everything but the submission has changed.
    unset($unverifiedSubmission->user_data->submission);
    unset($submission->user_data->submission);
    $this->assertEquals($unverifiedSubmission->user_data, $submission->user_data, "User Data unchanged");
    $this->assertGreaterThan(0, $submission->person_id);
    if ($submission->submission_id > 0) {
      $this->assertEquals($s->admin_data->person_id, $submission->person_id);
    }

    //Statement: update email (from what?)
    $statement = $this->dataManager->em->getRepository(FAStatement::class)->findOneBy(['submission_id' => $submission->submission_id]);
    if ($s->admin_data->file) {
      $this->assertNotEmpty($statement->person_id);
      $this->assertNotEmpty($statement->filer_id);
      $this->assertEquals($s->person->name, $statement->name);
      $this->assertEquals($periodStart, $statement->period_start);
      $this->assertEquals($periodEnd, $statement->period_end);
      $this->assertGreaterThanOrEqual($currentDate->format('Y-m-d H:i:s'), $statement->updated_at->format('Y-m-d H:i:s'));
    }
    else {
      $this->assertNull($statement);
      if(!empty($s->admin_data->period_start)) {
        $this->assertEquals($s->admin_data->period_start, $submission->period_start->format('Y-m-d'));
        $this->assertEquals($s->admin_data->period_end, $submission->period_end->format('Y-m-d'));

        $this->assertEquals($s->admin_data->period_start, $submission->user_data->statement->period_start);
        $this->assertEquals($s->admin_data->period_end, $submission->user_data->statement->period_end);
      }
    }

    //Person:
    $person = $this->dataManager->em->find(Person::class, $submission->person_id);
    $this->assertEquals($s->person->name, $person->name);
    $this->assertEquals('test@a.invalid', $person->email);
    $this->assertNotEmpty($person->filer_id);

    //Official: Get all officials for person_id & make sure records are created/updated.
    $officialsRepo = $this->dataManager->em->getRepository(Official::class);
    if ($s->admin_data->offices) {
      foreach($s->offices as $office) {
        $officeStart = new DateTime($office->start_date);
        $o = $officialsRepo->findBy(['person_id' => $person->person_id, 'jurisdiction_id' => $office->jurisdiction_id,
          'office_code' => $office->office_code, 'start_date' => $officeStart]);
        $this->assertEquals(1, sizeOf($o));
        $o = $o[0];
        if (!empty($office->end_date) && $office->end_date <= date('Y-M-d', strtotime('+7 days'))) {
          $this->assertEquals($office->end_date, $o->end_date->format('Y-m-d'));
        }
        else {
          $this->assertNull($o->end_date);
        }
        $this->assertEquals($office->position_id, $o->position_id);
        $this->assertEquals($office->email, $o->email);
        $this->assertEquals($office->address, $o->address);
        $this->assertEquals($office->city, $o->city);
        $this->assertEquals($office->state, $o->state);
        $this->assertEquals($office->postcode, $o->postcode);
        $this->assertGreaterThanOrEqual($currentDate->format('Y-m-d H:i'), $o->updated_at->format('Y-m-d H:i'));
      }
    } else {
      $officials = $officialsRepo->findBy(['person_id' => $submission->person_id]);
      $this->assertEmpty($officials, "Officials empty: $file");
    }

    //Candidacy: Get all candidacies for person_id & make sure records are created/updated.
    $caRepo = $this->dataManager->em->getRepository(Candidacy::class);
    if ($s->admin_data->candidacies) {
      foreach($s->candidacies as $can) {
        $campaignStartDate = new DateTime($can->campaign_start_date);
        $c = $caRepo->findBy(['person_id' => $person->person_id, 'jurisdiction_id' => $can->jurisdiction_id,
          'office_code' => $can->office_code, 'election_code' => $can->election_code]);
        $this->assertEquals(1, sizeOf($c));
        $c = $c[0];
        $this->assertEquals($can->position_id, $c->position_id);
        $this->assertEquals($campaignStartDate, $c->campaign_start_date);
        $this->assertEquals($can->email, $c->declared_email);
        $this->assertEquals($can->address, $c->address);
        $this->assertEquals($can->city, $c->city);
        $this->assertEquals($can->state, $c->state);
        $this->assertEquals($can->postcode, $c->postcode);
      }
    }
    else {
      $candidacies = $caRepo->findBy(['person_id' => $submission->person_id]);
      $this->assertEmpty($candidacies, "Candidacies empty: $file");
    }

    //Make sure we are saving fa_statement_id on candidacies.
    $statementCandidacy = $submission->fa_statement_id ? $caRepo->findOneBy(['fa_statement_id' => $submission->fa_statement_id]) : NULL;
    if ($s->admin_data->file && $s->admin_data->candidacies) {
      $this->assertNotEmpty($statementCandidacy);
    }
    else {
      $this->assertNull($statementCandidacy);
    }

    //Make sure we have a person_authorization.
    $auth = $this->dataManager->em->getRepository(EntityAuthorization::class)->findOneBy(
      ['entity_id' => $person->person_id, 'uid' => $submission->uid, 'realm' => $submission->realm]);
    $this->assertNotEmpty($auth);
    $this->assertEquals("owner", $auth->role);

    //Removing auth to make sure it still gets added when there is no auth records
    $this->dataManager->db->executeStatement("delete from entity_authorization where entity_id = :entity_id and uid = :uid and realm = :realm",
    ["entity_id" => $s->admin_data->person_id, "uid" => $user->uid, "realm" => $user->realm]);

    $this->statementProcessor->verifySubmission($unverifiedSubmission->submission_id, $user->user_name, $s->admin_data, $mailer);
    // Clear after verification.
    $this->dataManager->em->clear();

    //Make sure we have a person_authorization.
    $auth = $this->dataManager->em->getRepository(EntityAuthorization::class)->findOneBy(
      ['entity_id' => $person->person_id, 'uid' => $submission->uid, 'realm' => $submission->realm]);
    $this->assertNotEmpty($auth);
    $this->assertEquals("owner", $auth->role);

    //Statement Log: Make sure we have a record for verification.
    /** @var FAStatementLog $log */
    $log = $this->dataManager->em->getRepository(FAStatementLog::class)->findOneBy(['person_id' => $submission->person_id]);
    $this->assertNotEmpty($log);
    if (!empty($s->admin_data->message)) {
      $this->assertStringContainsString($s->admin_data->message, $log->message);
    }
  }

  public function testDoNotChangePositionOnExistingCandidacy()
  {
    $dir = dirname(__DIR__) . "/data/verification";
    $s = Yaml::parseFile("$dir/existing-candidates.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    if ($s->admin_data->person_id) {
      $person = $this->createPerson($s->person);
      $s->admin_data->person_id = $person->person_id;
    }

    $this->createCandidacies($person->person_id, $s->candidacies);
    $candidacies = $this->dataManager->db->executeQuery("
      SELECT * FROM candidacy 
      where person_id = :person_id order by candidacy_id",
      [
        'person_id' => $person->person_id,
      ]
    )->fetchAllAssociative();

    $s->candidacies[0]->candidacy_id = $candidacies[0]['candidacy_id'];
    $s->candidacies[0]->position_id = 3862;
    $s->candidacies[1]->candidacy_id = $candidacies[1]['candidacy_id'];
    $s->candidacies[1]->position_id = 3863;

    $unverifiedSubmission = $this->createSubmission($s, TRUE, $s->submitted_at ?? NULL);

    if (!empty($s->tests->person_authorized)) {
      $this->submission->person_id = $this->person->person_id;
    }
    $this->dataManager->em->flush();

    $mailer = new MockMailer;
    $this->statementProcessor->verifySubmission($unverifiedSubmission->submission_id, 'testUser', $s->admin_data, $mailer);
    $this->dataManager->em->clear();

    $updatedCandidacies = $this->dataManager->db->executeQuery("SELECT * FROM candidacy where person_id = :person_id order by candidacy_id", ['person_id' => $person->person_id])->fetchAllAssociative();

    $this->assertEquals($candidacies[0]['position_id'], $updatedCandidacies[0]['position_id']);
    $this->assertEquals($s->candidacies[1]->position_id, $updatedCandidacies[1]['position_id']);

  }

  public function testCarryForward() {
    $dir = dirname(__DIR__) . "/data/submission";
    $old_submission = Yaml::parseFile("$dir/carry_forward.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $submission = $this->createSubmission($old_submission, false);
    $new_submission_setup = new stdClass();
    $new_submission_setup->statement = new stdClass();
    $new_submission_setup->statement->period_start = "2018-11-11";
    $new_submission_setup->statement->period_end = "2019-12-31";
    $new_submission_setup->person = new stdClass();
    $new_submission_setup->person->name = "Billy";
    $new_submission_setup->person->email = "billy@example.com";
    $new_submission_setup->statement->email = "billy@example.com";
    $new_submission_setup->statement->hasSpouse = false;
    $new_submission_setup->statement->hasDependent = false;
    $new_person = $this->createPerson($new_submission_setup->person);
    $new_submission_setup->statement->person_id = $new_person->person_id;
    $new_submission = $this->createSubmission($new_submission_setup, false);
    $new_submission_setup->submission = $new_submission;
    $u = new User();
    $u->realm = 'test';
    $u->uid = 'test please work!';
    $u->user_name = 'Rene Descartes';
    $combined_submission = $this->statementProcessor->carryForward($new_submission_setup, $submission->submission_id, $u);
    $original_submission = $submission->user_data->statement;
    $combined_statement = $combined_submission->statement;

    // Make sure import metadata is there
    $this->assertNotEmpty($combined_submission->statement->imported_from);
    $this->assertEquals($combined_submission->statement->imported_from->submission_id, $submission->submission_id);

    $this->assertEquals(1, $combined_submission->submission->save_count);
    $this->assertEquals($new_submission->user_data->statement->period_start, $combined_statement->period_start);
    $this->assertEquals($new_submission->user_data->statement->period_end, $combined_statement->period_end);
    $this->assertEquals($original_submission->judge_or_law_enforcement, $combined_statement->judge_or_law_enforcement);
    $this->assertEquals($original_submission->has_lobbies, $combined_statement->has_lobbies);
    $this->assertEquals($original_submission->has_creditors, $combined_statement->has_creditors);
    $this->assertEquals($original_submission->has_gifts, $combined_statement->has_gifts);
    $this->assertEquals($original_submission->real_estate_owned, $combined_statement->real_estate_owned);
    $this->assertEquals($original_submission->spouse, $combined_statement->spouse);
    $i = 0;
    foreach ($original_submission->income->categories as $category) {
      $this->assertEquals($category->value, $combined_statement->income->categories[$i]->value);
      $i++;
    }
    $this->assertEquals($original_submission->income->data[0]->name, $combined_statement->income->data[0]->name);
    $i = 0;
    foreach ($original_submission->accounts->categories as $category) {
     $this->assertEquals($category->value, $combined_statement->accounts->categories[$i]->value);
     $i++;
    }
    $this->assertEquals($original_submission->accounts->data[0]->name, $combined_statement->accounts->data[0]->name);
    $this->assertEquals($original_submission->accounts->data[0]->assets[0]->nature, $combined_statement->accounts->data[0]->assets[0]->nature);
    $i = 0;
    foreach ($original_submission->business_associations->categories as $category) {
      $this->assertEquals($category->value, $combined_statement->business_associations->categories[$i]->value);
      $i++;
    }
  }

  public function testValidations() {
    $dir = dirname(__DIR__) . "/data/submission";
    $validations = Yaml::parseFile("$dir/general.validations.yml", Yaml::PARSE_OBJECT_FOR_MAP);

    // Object has tests property which contains an array of test condition.
    foreach($validations->tests as $test) {
      // Start with the same initial data.
      $submission = Yaml::parseFile("$dir/general.yml", Yaml::PARSE_OBJECT_FOR_MAP);
      $expected_errors = [];

      // Each condition either an assertion of an error or and assertion of a particular initial state modified from
      // the base $submission
      foreach ($test as $condition => $value) {

        switch (true) {
          case ($condition == 'error'):
            if (is_array($value)) {
              foreach ($value as $error) {
                $expected_errors [] = $error;
              }
            } else {
              $expected_errors [] = $value;
            }
            break;
          default:
            $this->setSubmissionValue($submission, $condition, $value);
            break;
        }

      }

      $validator = new Validator();

      // Execute each general validator once.

      $validator->validateAddress( $submission->addresses, 'address', "Address");
      $validator->validateCategories($submission, 'income', ['employment', 'retirement'], 'income', "Income category");
      $validator->validateWho($submission, 'data', "Who");
      $validator->validateRequired($submission, ['required'], 'data', "Data");
      $validator->validateRange($submission, 'amount', 'data', "Data");
      $validator->validateDollarAmount($submission, 'dollar_amount', 'data', 'Data');
      foreach ($submission->locations as $location) {
        $validator->validateLocation($location, 'location', 'Location');
      }


      $errors = $validator->getErrors();
      $conditions = print_r($test, 1);
      if ($expected_errors && $expected_errors != ['']) {
        foreach ($expected_errors as $expected_error) {
          $this->assertArrayHasKey($expected_error, $errors, "Missing expected errors: $conditions");
        }
      } else {
        $error = implode("; ", $errors);
        $this->assertEmpty($errors, "Unexpected error(s) $error for validator : $conditions");
      }
    }
  }

  /**
   * @throws Exception
   */
  public function testAmendSubmission() {
    $dir = dirname(__DIR__) . "/data/submission";
    $old_submission = Yaml::parseFile("$dir/carry_forward.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $person = $this->createPerson($old_submission->person);
    $statement = $this->createFAStatement($old_submission->statement);
    $submission = $this->createSubmission($old_submission, true);
    $submission->person_id = $person->person_id;
    $submission->verified = true;
    $submission->fa_statement_id = $statement->statement_id;
    $statement->submission_id = $submission->submission_id;

    // Alter the dates on the statement so that we can test that the amendment gets the new dates
    $period_start = '2018-01-05';
    $period_end = '2019-01-31';
    $statement->period_start = new DateTime($period_start);
    $statement->period_end = new DateTime($period_end);

    $this->dataManager->em->flush();

    $old_submission->statement->person_id = $person->person_id;
    $u = new User();
    $u->realm = 'test';
    $u->uid = 'test please work!';
    $u->user_name = 'Rene Descartes';
    $combined_submission = $this->statementProcessor->amendSubmission($submission->submission_id, $u);
    $original_submission = $submission->user_data->statement;
    $combined_statement = $combined_submission->statement;


    // make sure metadata is there
    $this->assertEquals($combined_submission->submission->amend_id, $submission->submission_id);
    $this->assertEquals($combined_submission->submission->person_id, $person->person_id);
    $this->assertEquals($combined_submission->submission->fa_statement_id, $statement->statement_id);
    $this->assertEquals($period_start, $combined_submission->statement->period_start );
    $this->assertEquals($period_end, $combined_submission->statement->period_end);

    // make sure we have a different submission
    $this->assertNotEquals($submission->submission_id, $combined_submission->submission->submission_id);

    // Make sure submission data was copied.
    $this->assertEquals($submission->user_data->person->name, $combined_submission->person->name);
    $this->assertEquals(count($submission->user_data->offices), count($combined_submission->offices));
    $this->assertEquals(count($submission->user_data->candidacies), count($combined_submission->candidacies));
    $this->assertEquals(0, $combined_submission->submission->save_count);
    $this->assertEquals($original_submission->judge_or_law_enforcement, $combined_statement->judge_or_law_enforcement);
    $this->assertEquals($original_submission->has_lobbies, $combined_statement->has_lobbies);
    $this->assertEquals($original_submission->has_creditors, $combined_statement->has_creditors);
    $this->assertEquals($original_submission->has_gifts, $combined_statement->has_gifts);
    $this->assertEquals($original_submission->real_estate_owned, $combined_statement->real_estate_owned);
    $this->assertEquals($original_submission->spouse, $combined_statement->spouse);
    $i = 0;
    foreach ($original_submission->income->categories as $category) {
      $this->assertEquals($category->value, $combined_statement->income->categories[$i]->value);
      $i++;
    }
    $this->assertEquals($original_submission->income->data[0]->name, $combined_statement->income->data[0]->name);
    $i = 0;
    foreach ($original_submission->accounts->categories as $category) {
      $this->assertEquals($category->value, $combined_statement->accounts->categories[$i]->value);
      $i++;
    }
    $this->assertEquals($original_submission->accounts->data[0]->name, $combined_statement->accounts->data[0]->name);
    $this->assertEquals($original_submission->accounts->data[0]->assets[0]->nature, $combined_statement->accounts->data[0]->assets[0]->nature);
    $i = 0;
    foreach ($original_submission->business_associations->categories as $category) {
      $this->assertEquals($category->value, $combined_statement->business_associations->categories[$i]->value);
      $i++;
    }


    // Save the amendment so we can test the logic for loading existing amendments.
    $saved_submission = $this->statementProcessor->saveSubmission($combined_submission, $u);

    $existing_amendment = $this->statementProcessor->amendSubmission($submission->submission_id, $u);

    // Make sure we get back the same amendment on a call.
    $this->assertEquals($saved_submission->submission->submission_id, $existing_amendment->submission->submission_id);


  }

  /**
   * @throws Exception
   *
   */
  public function testAutoVerify() {
    $u = new User();
    $u->realm = 'test';
    $u->uid = 'test please work!';
    $u->user_name = 'Mortimer Snerdly';
    $dir = dirname(__DIR__) . "/data/verification";
    $old_submission = Yaml::parseFile("$dir/existing-person.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $submission = $this->createSubmission($old_submission, false);
    // Check for failure when no person_id exists
    $auto_verification = $this->statementProcessor->autoVerify($submission, $u);
    $this->assertFalse($auto_verification);
    // Make sure it works when person_id exists
    $person = $this->createPerson();
    $submission->person_id = $person->person_id;
    $authorization = new EntityAuthorization();
    $authorization->uid = $u->uid;
    $authorization->realm = $u->realm;
    $authorization->entity_id = $person->person_id;
    $authorization->role = "owner";
    $this->dataManager->em->persist($authorization);
    $this->dataManager->em->flush();
    $auto_verification = $this->statementProcessor->autoVerify($submission, $u);
    $this->assertTrue($auto_verification);
    $returned_submission = $this->dataManager->em->find(FASubmission::class, $submission->submission_id);
    $this->assertTrue($returned_submission->verified);

    // amendment the current submission
    $amend_submission = $this->statementProcessor->amendSubmission($returned_submission->submission_id, $u);
    $this->assertNull($amend_submission->submission->submission_id);

    // Save the amendment so we can test the logic for loading existing amendments.
    $amended_submission = $this->statementProcessor->saveSubmission($amend_submission, $u);
    $this->assertGreaterThan($amended_submission->submission->amend_id, $amended_submission->submission->submission_id);
    // statement is not a standard class - I need to find the submission and return the raw data into the autoverify
    $sub = $this->dataManager->em->find(FASubmission::class, $amended_submission->submission->submission_id);
    $auto_verification = $this->statementProcessor->autoVerify($sub, $u);
    $this->assertTrue($auto_verification);
    // find the Fa statement check that the submission_id is not null
    $st = $this->dataManager->em->find(FAStatement::class, $sub->fa_statement_id);
    $this->assertNotNull($st->submission_id);

    $statement = $submission->user_data->statement;
    $statement->reporting_modifications->residential_address = true;
    $auto_verification = $this->statementProcessor->autoVerify($submission, $u);
    $this->assertTrue($auto_verification);
  }

  /**
   * @throws Exception
   */
  public function testSetSubmissionsPublished(){
    $username = 'SUPER DUPER ADMIN';
    $dir = dirname(__DIR__) . "/data/verification";
    $old_submission = Yaml::parseFile("$dir/existing-person.yml", Yaml::PARSE_OBJECT_FOR_MAP);
    $submission = $this->createSubmission($old_submission, false);
    $submission2 = $this->createSubmission($old_submission, false);
    $this->statementProcessor->setSubmissionsPublished([$submission->submission_id, $submission2->submission_id], true, $username, "CHEESECAKE");
    $sub = $this->dataManager->em->find(FASubmission::class, $submission->submission_id);
    $sub2 = $this->dataManager->em->find(FASubmission::class, $submission2->submission_id);
    $this->assertTrue($sub->published);
    $this->assertTrue($sub2->published);
    $log = $this->dataManager->em->getRepository(FAStatementLog::class)->findOneBy(['submission_id' => $sub->submission_id, 'user_name' => $username]);
    $this->assertNotEmpty($log);
    $this->assertEquals($log->action, 'published');
    $this->assertEquals($log->message, 'CHEESECAKE');
    $this->statementProcessor->setSubmissionsPublished([$submission->submission_id, $submission2->submission_id], false, $username);
    $sub = $this->dataManager->em->find(FASubmission::class, $submission->submission_id);
    $sub2 = $this->dataManager->em->find(FASubmission::class, $submission2->submission_id);
    $this->assertFalse($sub->published);
    $this->assertFalse($sub2->published);
    $log = $this->dataManager->em->getRepository(FAStatementLog::class)->findOneBy(['submission_id' => $sub->submission_id, 'user_name' => $username, 'action' => 'unpublished']);
    $this->assertNotEmpty($log);

  }

  /**
   * @throws \Doctrine\DBAL\ConnectionException
   */
  protected function tearDown(): void {
    $this->dataManager->rollback();
    parent::tearDown();
  }

}
