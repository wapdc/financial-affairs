export const whoColumn = {
  key: 'receivedBy',
  label: 'Received By',
  align: 'left',
  sortable: false,
  width: '25%',
  format: (arr) => arr?.map((who) => who.label)?.join(', '),
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};
