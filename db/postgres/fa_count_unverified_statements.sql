CREATE OR REPLACE FUNCTION fa_count_unverified_statements() returns int language sql as $$
select count(*)
from fa_submission as result
where submitted is true
  and verified is false
  and submitted_at >= (now() - interval '90 days')
$$;
