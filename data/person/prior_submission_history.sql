select
       q.period_start,
       q.period_end,
       q.submitted_at,
       q.submission_id,
       q.published,
       CASE
           when q.r = 1 then  'original'
           else 'amendment'
          end orig_amend,
       CASE
          WHEN q.current_submission_id = q.submission_id THEN true
          else false end as is_current
from (
    SELECT fsta.period_start,
           fsta.period_end,
           fsub.submitted_at,
           fsub.submission_id,
           fsub.published,
           fsta.submission_id as current_submission_id,
           row_number() over (partition by fsta.statement_id order by fsub.submitted_at, fsub.submission_id ) as r
FROM fa_statement fsta JOIN fa_submission fsub ON fsta.statement_id = fsub.fa_statement_id
WHERE fsta.person_id = :person_id AND fsub.verified = TRUE) q
ORDER BY q.period_end desc, q.submitted_at desc, q.submission_id desc