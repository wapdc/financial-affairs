<?php


namespace WAPDC\FinancialAffairs\Model;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class Reports
 *
 * @package WAPDC\FinancialAffairs\Model
 * @Entity
 * @Table(name="WAPDC.dbo.reports")
 */
#[ORM\Entity]
#[ORM\Table(name: 'WAPDC.dbo.reports')]
class Reports {

  /**
   * @Id
   * @Column (type="integer")
   */
  #[ORM\Column(type: 'integer')]
  #[ORM\Id]
  public $repno;

  /**
   * @Column
   */
  #[ORM\Column]
  public $filer_id;

  /**
   * @Column
   */
  #[ORM\Column]
  public $election_year;

  /**
   * @Column
   */
  #[ORM\Column]
  public $filer_type;

  /**
   * @Column(type="string")
   */
  #[ORM\Column(type: 'string')]
  public $date_filed;

  /**
   * @Column
   */
  #[ORM\Column]
  public $form;

  /**
   * @Column
   */
  #[ORM\Column]
  public $howfiled;

}
