import { boot } from 'quasar/wrappers';
import keysAsClick from 'src/directives/keysAsClick.js';

export default boot(({ app }) => {
  app.directive('keys-as-click', keysAsClick);
});
