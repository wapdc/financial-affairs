import Vue from 'vue'
import Router from 'vue-router'
import UnverifiedSubmission from "./views/UnverifiedSubmission";
import UnverifiedStatements from "./components/UnverifiedStatements"
import Person from "./components/Person"
import MySubmissions from "./views/MySubmissions";
import SubmissionHistory from "./views/SubmissionHistory";
import Statement from "./views/Statement";
import PaperFile from "./dataentry/PaperFile"

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/my-submissions',
    },
    {
      path: '/admin/unverified-statements',
      name: 'admin.unverified-statements',
      component: UnverifiedStatements,
    },
    {
      path: '/person',
      name: 'person-search',
      component: Person,
    },
    {
      path: '/person/:person_id',
      name: 'person',
      component: Person,
    },
    {
      path: '/person/:person_id/legacy/submission/:submission_id',
      name: 'legacy-submission',
      component: UnverifiedSubmission,
    },
    {
      path: '/person/:person_id/submission/:submission_id',
      name: 'unverified-submission',
      component: UnverifiedSubmission,
    },
    {
      path: '/my-submissions',
      name: 'my-submissions',
      component: MySubmissions,
    },
    {
      path: '/submission-history',
      name: 'submission-history',
      component: SubmissionHistory,
    },
    {
      path: '/submission/:submission_id',
      name: 'submission-view',
      component: Statement,
    },
    {
      path: '/financial-affairs/admin/paper-file',
      name: 'paper-file',
      component: PaperFile,
    },
  ]
})
