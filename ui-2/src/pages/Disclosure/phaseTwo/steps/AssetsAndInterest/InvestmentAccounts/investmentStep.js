import { assetArrayValidator } from '../common/assetValidator';
import { CategoryHandler } from 'src/utils/CategoryHandler.class';
import rules from 'src/utils/rules';
import useWizard from 'src/wizard/useWizard';

const { lazyLoadWrapper, dataPath, getData } = useWizard();

const accountCategoryHandler = new CategoryHandler('accounts', 'investment');

export const investmentStep = {
  title: 'Investment accounts',
  order: 3,
  component: lazyLoadWrapper(() =>
    import('../InvestmentAccounts/InvestmentAccounts.vue'),
  ),
  fields: {
    hasInvestmentAccounts: {
      rules: [rules.required],
      blocking: false,
      data() {
        return dataPath(this, 'pendingTransform.hasInvestmentAccounts');
      },
      transform: (field) => ({
        writeOut: () => accountCategoryHandler.writeOutHas(field),
        readIn: () => accountCategoryHandler.readInHas(field),
      }),
    },
    investmentAccountArray: {
      blocking: false,
      data() {
        return dataPath(this, 'pendingTransform.investmentAccountArray');
      },
      transform: (field) => ({
        writeOut: () => accountCategoryHandler.writeOutData(field),
        readIn: () => accountCategoryHandler.readInData(field),
      }),
      requiredIf: () =>
        [true, 'true'].includes(
          getData().pendingTransform.hasInvestmentAccounts,
        ),
      template: {
        heldBy: {
          as: 'who',
          rules: [rules.requiredNonEmptyArray],
        },
        institutionName: {
          as: 'name',
          rules: [
            rules.requiredString,
            rules.maxLength(100),
            rules.alphbetical,
          ],
        },
        investmentArray: {
          as: 'assets',
          rules: [
            rules.requiredNonEmptyArray,
            (v) => assetArrayValidator(v, ['nature']),
          ],
        },
        streetAddress: {
          as: 'address',
          rules: [
            rules.requiredString,
            rules.maxLength(120),
          ],
        },
        city: {
          rules: [
            rules.requiredString,
            rules.alphbetical,
            rules.maxLength(120),
          ],
        },
        state: {
          rules: [rules.required, rules.state],
        },
        zip: {
          as: 'postcode',
          rules: [rules.required, rules.exactLength(5), rules.numbersOnly],
        },
      },
    },
  },
};
