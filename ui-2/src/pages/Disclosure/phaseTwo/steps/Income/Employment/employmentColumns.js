import { format } from 'quasar';
import { employmentIncomeStep } from './employmentStep';

const { capitalize } = format;

const keys = employmentIncomeStep.fields.employmentIncomeArray.template;

const receivedBy = {
  key: keys.receivedBy.as,
  label: 'Received By',
  align: 'left',
  sortable: false,
  width: '25%',
  format: (value) => capitalize(value?.join(', ')),
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

const employerName = {
  key: keys.employerName.as,
  label: 'Employer Name',
  align: 'left',
  sortable: false,
  width: '25%',
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

const occupation = {
  key: keys.occupation.as,
  label: 'Occupation',
  align: 'left',
  sortable: false,
  width: '25%',
  responsiveBehavior: {
    md: { width: '25%' },
    sm: { occlude: true },
  },
};

export default [receivedBy, employerName, occupation];
