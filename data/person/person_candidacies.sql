select ca.candidacy_id,
       ca.office_code,
       ca.election_code,
       ca.position_id,
       ca.address,
       ca.city,
       ca.state,
       ca.postcode,
       ca.declared_email as email,
       o.offtitle as office,
       j.jurisdiction_id,
       j.name as jurisdiction,
       e.title as election,
       ca.campaign_start_date
from candidacy ca
  JOIN foffice o ON o.offcode = ca.office_code
  JOIN jurisdiction j ON j.jurisdiction_id = ca.jurisdiction_id
  JOIN election e ON ca.election_code = e.election_code
  WHERE ca.person_id = :person_id
    AND ((e.election_date > now() - interval '1 year') OR (COALESCE(CAST(:current as VARCHAR), 'N') = 'N'))
order by e.election_date desc