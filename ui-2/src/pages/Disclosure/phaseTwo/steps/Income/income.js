import useWizard from 'src/wizard/useWizard';

const { lazyLoadWrapper } = useWizard();

import { employmentIncomeStep } from './Employment/employmentStep';
import { retirementIncomeStep } from './Retirement/retirementStep';
import { uncommonIncomeStep } from './Uncommon/uncommonStep';

const incomeDisclosureStep = {
  title: 'Income Disclosure',
  order: 0,
  component: lazyLoadWrapper(() => import('./IncomeDisclosure.vue')),
};

export const incomeSection = {
  title: 'Income',
  order: 2,
  steps: {
    incomeDisclosureStep,
    employmentIncomeStep,
    retirementIncomeStep,
    uncommonIncomeStep,
  },
};
