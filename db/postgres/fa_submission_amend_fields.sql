ALTER TABLE fa_submission ADD COLUMN IF NOT EXISTS amend_id int;
ALTER TABLE fa_submission ADD COLUMN IF NOT EXISTS period_start date;
ALTER TABLE fa_submission ADD COLUMN IF NOT EXISTS period_end date;
ALTER TABLE fa_submission ADD COLUMN IF NOT EXISTS name VARCHAR(255);
CREATE INDEX fa_submission_amend_id ON wapdc.public.fa_submission(amend_id);

