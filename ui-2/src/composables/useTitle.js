import {
  computed,
  inject,
  nextTick,
  onMounted,
  onUnmounted,
  provide,
  reactive,
} from 'vue';

const TitleAccessorSymbol = Symbol('TitleAccessorSymbol');

const BASE_TITLE = import.meta.env?.BASE_TITLE || 'Financial Affairs';

/**
 * Composable function to provide page title accessors to descendent components.
 * Must be instantiated in the nearest common ancestor of components that import the
 * useTitle composable.
 * @returns {void}
 */
export function useTitleProvider() {
  const state = reactive({
    pageTitle: '',
    pageSubtitle: '',
    pageTagline: '',
  });

  const computedTitle = computed(() => {
    if (
      typeof state.pageTitle !== 'string' ||
      !state.pageTitle?.trim()?.length
    ) {
      return BASE_TITLE;
    }
    const pageTitle = state.pageTitle.trim();
    return pageTitle ? `${BASE_TITLE} | ${pageTitle}` : BASE_TITLE;
  });

  function setPageTitle(title) {
    state.pageTitle = title;
    nextTick(() => (document.title = computedTitle.value));
  }

  function setPageSubtitle(subtitle) {
    state.pageSubtitle = subtitle;
  }

  function setPageTagline(pageTagline) {
    state.pageTagline = pageTagline;
  }

  onMounted(() => {
    setPageTitle();
  });

  const TitleAccessors = {
    title: computedTitle,
    subtitle: computed(() => state.pageSubtitle),
    tagline: computed(() => state.pageTagline),
    setPageTitle,
    setPageSubtitle,
    setPageTagline,
  };

  provide(TitleAccessorSymbol, TitleAccessors);

  return TitleAccessors;
}

/**
 * Composable function to return page title accessors. Must be used within a component where useTitleProvider
 * has been instantiated in an ancestor component.
 * @returns {{
 * title: ComputedRef<string>,
 * subtitle: ComputedRef<string>,
 * tagline: ComputedRef<string>,
 * setPageTitle: (title: string) => void,
 * setPageSubtitle: (subtitle: string) => void
 * setPageTagline: (tagline: string) => void
 * }} titleAccessor:
 *   - title: Computed current page title
 *   - subtitle: Computed current page subtitle
 *   - tagline: Computed current page tag
 *   - setPageTitle: A function to update the page title
 *   - setPageSubtitle: A function to update the page subtitle
 *   - setPageTagline: A function to update the page tagline
 * @throws {Error} Throws an error if useTitleProvider() has not been instantiated in an ancestor component
 */
export default function useTitle() {
  const titleAccessor = inject(TitleAccessorSymbol);

  onUnmounted(() => {
    titleAccessor.setPageSubtitle(null);
  });

  if (!titleAccessor) {
    throw new Error(
      'useTitle must be used within a descendant of a component instantiating useTitleProvider',
    );
  }

  return titleAccessor;
}
