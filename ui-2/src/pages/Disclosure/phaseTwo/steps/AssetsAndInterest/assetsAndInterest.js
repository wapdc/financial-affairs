import useWizard from 'src/wizard/useWizard';

const { lazyLoadWrapper } = useWizard();

import { bankAccountStep } from './BankAccounts/bankAccountStep';
import { insurancePolicyStep } from './InsurancePolicies/insurancePolicyStep';
import { investmentStep } from './InvestmentAccounts/investmentStep';
import { publicInvestmentStep } from './PublicInv/publicInvestmentStep';
import { otherInvestmentStep } from './Other/otherInvestmentStep';
import { uncommonAssetsStep } from './Uncommon/uncommonAssetsStep';

export const introStep = {
  title: 'Financial assets and interest income',
  order: 0,
  component: lazyLoadWrapper(() => import('./AssetsAndInterest.vue')),
};

export const assetsAndInterestSection = {
  title: 'Assets and Interest',
  order: 3,
  steps: {
    introStep,
    bankAccountStep,
    insurancePolicyStep,
    investmentStep,
    publicInvestmentStep,
    otherInvestmentStep,
    uncommonAssetsStep,
  },
};
