import { debtStep } from './debt.js';
import { format } from 'quasar';
import { renderIncomeRange } from 'src/utils/format.js';

const { capitalize } = format;

const keys = debtStep.fields.debtArray.template;

const heldBy = {
  key: 'who',
  label: 'Owed By',
  align: 'left',
  sortable: false,
  width: '25%',
  format: (value) => capitalize(value?.join(', ')),
  responsiveBehavior: {
    sm: { width: '30%' },
    xs: { width: '33%' },
  },
};

const creditorName = {
  key: keys.creditorName.as,
  label: 'Creditor',
  align: 'left',
  sortable: false,
  width: '30%',
};

const originalAmount = {
  key: keys.originalAmount.as,
  label: 'Original Amount',
  align: 'left',
  sortable: false,
  width: '20%',
  format: (value) => renderIncomeRange(value),
};

export default [heldBy, creditorName, originalAmount];
