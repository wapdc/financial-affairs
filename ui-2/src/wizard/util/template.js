import { computed } from 'vue';
import { _validateStep } from './step';

/**
 * @module wizard/util/template
 * @description Logic for working with field sets, template
 * subfields, and template-derived data arrays
 */

/**
 * Creates a new object from a template object and inserts
 * it into the designated array in the data object
 * @param {Object} fieldSet - The template field object
 */
export function newFromTemplate(fieldSet) {
  const targetArray = (fieldSet.data().value ??= []);

  const newIndex = targetArray.push({}) - 1;
  const newObject = targetArray[newIndex];

  // Copy the template object keys and initial values to the new object
  for (let key in fieldSet.template) {
    const as = fieldSet.template[key]?.as ?? key;
    newObject[as] = fieldSet.template[key]?.initialValue ?? null;

    fieldSet.template[key].meta ??= [];
    fieldSet.template[key].meta[newIndex] = {
      isDirty: false,
      isValid: false,
    };
  }

  return newIndex;
}

/** Set the validation state of a subfield */
export function validateDataEntrySubfield(subfield, dataIndex, value) {
  const isBlocking = subfield?.blocking ?? false;

  const hasRules = subfield.rules?.length > 0;
  const passesAllRules =
    !hasRules ||
    subfield.rules
      .filter((rule) => typeof rule === 'function')
      .every((rule) => rule(value, dataIndex) === true);

  subfield.meta ??= [];
  subfield.meta[dataIndex] ??= {};

  subfield.meta[dataIndex].isValid = passesAllRules;
  subfield.meta[dataIndex].isBlocking = isBlocking && !passesAllRules;
}

/** Utility functions for working with subfields (both the definition and the input data) */
function coalesceDataWithTemplate(state, fieldSet, dataIndex) {
  const dataKey = (key) => fieldSet.template[key]?.as || key;

  return {
    /** Get the value of a field in the data array, or the whole object if no key is provided */
    getValue(key) {
      if (key === undefined) {
        return fieldSet.data().value[dataIndex];
      }

      const as = dataKey(key);

      const value = fieldSet.data().value[dataIndex][as] ?? null;
      return value;
    },

    setValue(key, value) {
      if (key === undefined) {
        console.error('No key provided to setValue');
        return;
      }

      const as = dataKey(key);
      fieldSet.data().value[dataIndex][as] = value;

      fieldSet.meta.isDirty = true;

      const templateMetaAtIndex = fieldSet.template[key]?.meta?.[dataIndex];

      if (templateMetaAtIndex) {
        fieldSet.template[key].meta[dataIndex].isDirty = true;
      }

      _validateStep(fieldSet.meta.stepRef);
    },

    /** Get a computed property for a field in the data array */
    vModel(key) {
      return computed({
        get: () => this.getValue(key),
        set: (value) => this.setValue(key, value),
      });
    },

    /** Get a v-bind object for a field in the data array */
    vBind(key) {
      return {
        modelValue: this.getValue(key),
        rules: this.rules(key),
        'onUpdate:modelValue': (value) => this.setValue(key, value),
      };
    },

    /** Get the rules array for a template subfield */
    rules(key) {
      return fieldSet.template[key]?.rules || [];
    },

    /** Get the data-key string for a template subfield */
    dataKey,

    /** Reverse lookup for the template key from the data key */
    realKey(dataKey) {
      return (
        Object.entries(fieldSet.template).find(
          ([_, templateField]) => templateField.as === dataKey,
        )?.[0] || (fieldSet.template[dataKey] ? dataKey : null)
      );
    },
  };
}

/** Utility functions for working with template fields and their data arrays */
export function _getFieldSetUtils(state, fieldSet) {
  return {
    /** Get utils for acting on a data array element's properties */
    get(index) {
      return coalesceDataWithTemplate(state, fieldSet, index);
    },

    /** Get array of all data objects */
    getAllData() {
      return fieldSet.data() || [];
    },

    /** Instantiate new object from template, return data arr index */
    add() {
      const newIndex = newFromTemplate(fieldSet);
      fieldSet.meta.isDirty = true;
      return newIndex;
    },

    /** Remove object at index */
    remove(index) {
      removeEntry(fieldSet, index);
    },

    /** Remove all fieldset data objects */
    removeAll() {
      fieldSet.data().value = [];
      fieldSet.meta.entries = [];
      Object.values(fieldSet.template).forEach((field) => {
        field.meta = [];
      });
      validateFieldSet(fieldSet);
    },

    /** Get the meta object for the fieldset or template field */
    meta(index) {
      if (index === undefined) {
        return fieldSet.meta;
      }

      return computed(() => fieldSet.meta.entries[index]);
    },
  };
}

function removeEntry(fieldSet, dataIndex) {
  fieldSet.data().value.splice(dataIndex, 1);
  Object.values(fieldSet.template).forEach((field) => {
    field.meta.splice(dataIndex, 1);
  });
  fieldSet.meta.entries.splice(dataIndex, 1);
  validateFieldSet(fieldSet);
}

/**
 * Set the validation state of a fieldset
 */
export function validateFieldSet(fieldSet, dataIndex = null) {
  const fieldSetDataEntries = fieldSet.data().value;

  const dataEntriesCount = fieldSetDataEntries?.length || 0;

  if (dataEntriesCount < 1) {
    if (typeof fieldSet.requiredIf === 'function') {
      return (fieldSet.meta.isValid = !fieldSet.requiredIf());
    }
    return (fieldSet.meta.isValid = true);
  }

  if (dataIndex !== null) {
    validateDataEntry(fieldSet, fieldSetDataEntries, dataIndex);
    return;
  }

  /*
    Validate each data entry of the array defined by the field set
      1 fieldSet : N data entries
      definition : instance
  */
  dataIndex = 0;
  while (dataIndex < dataEntriesCount) {
    validateDataEntry(fieldSet, fieldSetDataEntries, dataIndex);
    dataIndex++;
  }
}

/**
 * Validate the data entry at the specified index
 */
function validateDataEntry(fieldSet, fieldSetDataEntries, dataIndex) {
  // Validate each property of the data set
  for (const [key, templateSubField] of Object.entries(fieldSet.template)) {
    const as = templateSubField?.as || key;
    const value = fieldSetDataEntries[dataIndex][as];

    validateDataEntrySubfield(templateSubField, dataIndex, value);
  }

  // Now, validate the data set as a whole
  const fieldSetIsValid = Object.values(fieldSet.template).every(
    (subField) => subField.meta[dataIndex].isValid,
  );

  const fieldSetIsBlocking = Object.values(fieldSet.template).some(
    (subField) => subField.meta[dataIndex].isBlocking,
  );

  fieldSet.meta ??= {};
  fieldSet.meta.entries ??= [];
  fieldSet.meta.entries[dataIndex] ??= {};

  fieldSet.meta.entries[dataIndex].isValid = fieldSetIsValid;
  fieldSet.meta.entries[dataIndex].isBlocking = fieldSetIsBlocking;

  fieldSet.meta.isValid = fieldSet.meta.entries.every((entry) => entry.isValid);
}
