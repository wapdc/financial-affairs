import { computed, nextTick, reactive, readonly } from 'vue';
import {
  _validateStep,
  addMetaToStepsAndFields,
  lazyLoadWrapper,
} from './util/step';
import { _getFieldSetUtils } from './util/template';
import {
  _setCurrentStep,
  _navigateOneTick,
  hideStep,
  getTableOfContents,
} from './util/navigation';
import { _dataPath, _save, _setData } from './util/data';

const state = reactive({
  form: {}, // Form structure definition object
  data: {}, // Data object to store user input

  // Status and positional data
  status: {
    currentSection: null,
    currentSectionIndex: null,
    currentSectionTitle: '',
    currentStep: null,
    currentStepIndex: null,
    currentStepTitle: '',

    nextAccessibleStep: null,
    prevAccessibleStep: null,
  },

  // Grouped event callbacks
  eventCallbacks: {
    onStepChange: null,
    onInitialized: null,
    onSave: null,
    onSettled: [],
  },

  // Unstructured related data object made available through useWizard import
  info: {},
});

/**
 * Composable function to manage a wizard form
 * @param {Object} initializingData Optional object containing form and info data
 * @param {Object} initializingData.form Form definition object with sections as properties
 * @param {Object} initializingData.info Optional data object to store additional information
 * @param {Function} initializingData.onStepChange Callback function to call on step change
 * @param {Function} initializingData.onInitialized Callback function to call on initialization
 * @param {Function} initializingData.onSave Callback function to call on save
 */
export default function useWizard(initializingData) {
  // If initializing data is provided, set the form and info data
  if (initializingData) {
    state.form = initializingData.form;
    addMetaToStepsAndFields(state.form);

    if (initializingData.info) {
      state.info = initializingData.info;
    }

    if (initializingData.onStepChange) {
      state.eventCallbacks.onStepChange = initializingData.onStepChange;
    }

    if (initializingData.onInitialized) {
      state.eventCallbacks.onInitialized = initializingData.onInitialized;
    }

    if (initializingData.onSave) {
      state.eventCallbacks.onSave = initializingData.onSave;
    }

    if (initializingData.onValidate) {
      state.eventCallbacks.onValidate = initializingData.onValidate;
    }

    if (state.eventCallbacks.onInitialized) {
      state.eventCallbacks.onInitialized();
    }

    nextTick(() => {
      setCurrentStep({ sectionIndex: 0, stepIndex: 0 });
    });
  }

  /** onSave callback defined by instantiating module */
  async function save() {
    await _save(state);

    runQueuedActions();
  }

  /** Set the ability to save the form */
  function setCanSave(canSave) {
    state.status.canSave = canSave;
  }

  /** Set data to payload compatible with field data definitions */
  function setData(payload) {
    _setData(state, payload);
  }

  /** Set the current step and section */
  function setCurrentStep({ stepIndex, sectionIndex }) {
    _setCurrentStep({ state, stepIndex, sectionIndex });
  }

  /** Navigates to next or previous available step */
  function navigateOneTick(direction) {
    _navigateOneTick(state, direction);
  }

  /** Get array of {title, sectionIndex, stepIndex, message} objects */
  function getInvalidSteps() {
    const invalidSteps = [];
    for (const section of Object.values(state.form)) {
      for (const step of Object.values(section.steps)) {
        validateStep(step);
        if (!step.meta.isValid) {
          invalidSteps.push({
            title: step.title,
            sectionIndex: section.order,
            stepIndex: step.order,
          });
        }
      }
    }

    return invalidSteps;
  }

  function validateStep(step, { innerOnly = false } = {}) {
    step ??= state.status.currentStep;
    _validateStep(step);

    if (innerOnly) return;
    state.eventCallbacks.onValidate?.();
  }

  /** Queue for delaying functions until wizard is settled (i.e. after transforms on save) */
  function queueAction(actionFn) {
    const actionType = typeof actionFn;
    if (actionType !== 'function')
      return console.error('Action must be a function');

    const actions = state.eventCallbacks.onSettled || [];
    state.eventCallbacks.onSettled = [...actions, actionFn];
  }

  /** Run all queued actions, then clear the queue */
  function runQueuedActions() {
    for (const action of state.eventCallbacks.onSettled) {
      action();
    }

    state.eventCallbacks.onSettled = [];
  }

  /** Get data at a specific location in an object, creating it if needed */
  function dataPath(field, accessString) {
    return _dataPath(state, field, accessString);
  }

  /** Happy path utility to set a field value and trigger validation */
  function setFieldInput(field, value) {
    field.data().value = value;
    field.meta.isDirty = true;

    validateStep(field.meta.stepRef);
    field.meta.stepRef.meta.isDirty = true;
  }

  /** Utility for typical field access needs */
  function getCommonFns(field) {
    return {
      set: (value) => setFieldInput(field, value),
      value() {
        return field.data().value;
      },

      meta: readonly(field.meta),
      self: field,

      // Vue 3 model binding
      binds: computed(() => ({
        modelValue: field.data().value,
        rules: field.rules,
      })),
      listeners() {
        return {
          'update:modelValue': (value) => {
            this.set(value);
          },
        };
      },
    };
  }

  /**
   * Get common field utilities by direct reference to definition object field
   * @param {Object} fieldReference Field object from the form definition
   */
  function fieldRaw(fieldReference) {
    return getCommonFns(fieldReference);
  }

  /**
   * Get common field utilities by field name in the current step
   * @param {String} fieldName Name of the field to retrieve
   */
  function field(fieldName) {
    const field = state.status?.currentStep?.fields?.[fieldName];
    if (!field) {
      console.error(`Field ${fieldName} not found in current step`);
      return null;
    }
    return getCommonFns(field);
  }

  /**
   * Get fieldSet utilities by reference to wizard field object
   * @param {Object} fieldReference Field object from the form definition
   */
  function getFieldSetUtils(fieldReference) {
    return _getFieldSetUtils(state, fieldReference);
  }

  /**
   * Get fieldSet utilities by reference to definition object field
   * @param {Object} fieldReference Field object from the form definition
   */
  function getFieldSetUtilsByPointer(fieldPointer) {
    const stepRef = fieldPointer?.meta?.stepRef?.value;
    const stepMeta = stepRef?.meta;
    const { stepIndex, sectionIndex } = stepMeta;

    const section = Object.values(state.form).find(
      (s) => s.order === sectionIndex,
    );
    const step = Object.values(section.steps).find(
      (s) => s.order === stepIndex,
    );
    const fieldSet = step.fields[fieldPointer.key];

    return getFieldSetUtils(fieldSet);
  }

  /**
   * Get fieldSet utilities by field name in the current step
   * @param {String} fieldName Name of the field to retrieve
   */
  function fieldSet(fieldName) {
    const field = state.status?.currentStep?.fields?.[fieldName];
    if (!field) {
      console.error(`Field ${fieldName} not found in current step`);
      return null;
    }
    return getFieldSetUtils(field);
  }

  return {
    // Access to "info" object (global or shared data)
    getInfo: () => state.info,

    // Direct state access (use with caution)
    getForm: () => state.form,
    getData: () => state.data,

    // Managed access (use these with confidence)
    fieldRaw,
    field,
    fieldSet,
    getFieldSetUtils,
    getFieldSetUtilsByPointer,

    // Step and navigation management
    getInvalidSteps,
    hideCurrentStep: () => hideStep(state.status.currentStep),
    hideStep,
    navigateOneTick,
    queueAction,
    setCurrentStep,
    status: state.status,
    tableOfContents: computed(() => getTableOfContents(state.form)),
    validateStep,

    // API
    setData,
    setCanSave,
    save,

    // Form definition utilities
    dataPath,
    lazyLoadWrapper,
  };
}
